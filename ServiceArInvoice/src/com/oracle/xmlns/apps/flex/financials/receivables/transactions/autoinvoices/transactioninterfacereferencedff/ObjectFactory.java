
package com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioninterfacereferencedff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioninterfacereferencedff package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InterfaceLineReferenceFLEXDOO_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "interfaceLineReferenceFLEXDOO");
    private final static QName _CPQCloud_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "cPQCloud");
    private final static QName _GbPcContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "gbPcContext");
    private final static QName _InterfaceLineReferenceFLEX_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "interfaceLineReferenceFLEX");
    private final static QName _GbPcContextTipoDeTransaccion_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "tipoDeTransaccion");
    private final static QName _GbPcContextFecha_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "fecha");
    private final static QName _GbPcContextAgencia_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "agencia");
    private final static QName _GbPcContextNoLinea_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "noLinea");
    private final static QName _GbPcContextNoTrx_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "noTrx");
    private final static QName _InterfaceLineReferenceFLEXDOOFulfillmentLineID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Fulfillment__Line__ID");
    private final static QName _InterfaceLineReferenceFLEXDOOSourceOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Source__Order__Number");
    private final static QName _InterfaceLineReferenceFLEXDOODeliveryName_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Delivery__Name");
    private final static QName _InterfaceLineReferenceFLEXDOOSourceOrderSystem_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Source__Order__System");
    private final static QName _InterfaceLineReferenceFLEXDOOSourceScheduleNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Source__Schedule__Number");
    private final static QName _InterfaceLineReferenceFLEXDOOCustomerItem_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Customer__Item");
    private final static QName _InterfaceLineReferenceFLEXDOOPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "period");
    private final static QName _InterfaceLineReferenceFLEXDOOProfitCenterBusinessUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Profit__Center__Business__Unit");
    private final static QName _InterfaceLineReferenceFLEXDOOBillOfLadingNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Bill__of__Lading__Number");
    private final static QName _InterfaceLineReferenceFLEXDOOPriceAdjustmentID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Price__Adjustment__ID");
    private final static QName _InterfaceLineReferenceFLEXDOOFulfillmentLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Fulfillment__Line__Number");
    private final static QName _InterfaceLineReferenceFLEXDOODOOOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_DOO__Order__Number");
    private final static QName _InterfaceLineReferenceFLEXDOOWayBillNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_WayBill__Number");
    private final static QName _InterfaceLineReferenceFLEXDOOFulfillLineSplitReference_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_Fulfill__Line__Split__Reference");
    private final static QName _CPQCloudOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "orderNumber");
    private final static QName _CPQCloudServiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "serviceNumber");
    private final static QName _CPQCloudOrderLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "orderLineNumber");
    private final static QName _InterfaceLineReferenceFLEXFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "__FLEX_Context");
    private final static QName _InterfaceLineReferenceFLEXFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "__FLEX_Context_DisplayValue");
    private final static QName _InterfaceLineReferenceFLEXInterfaceLineGuid_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "InterfaceLineGuid");
    private final static QName _InterfaceLineReferenceFLEXFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", "_FLEX_NumOfSegments");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioninterfacereferencedff
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GbPcContext }
     * 
     */
    public GbPcContext createGbPcContext() {
        return new GbPcContext();
    }

    /**
     * Create an instance of {@link InterfaceLineReferenceFLEX }
     * 
     */
    public InterfaceLineReferenceFLEX createInterfaceLineReferenceFLEX() {
        return new InterfaceLineReferenceFLEX();
    }

    /**
     * Create an instance of {@link CPQCloud }
     * 
     */
    public CPQCloud createCPQCloud() {
        return new CPQCloud();
    }

    /**
     * Create an instance of {@link InterfaceLineReferenceFLEXDOO }
     * 
     */
    public InterfaceLineReferenceFLEXDOO createInterfaceLineReferenceFLEXDOO() {
        return new InterfaceLineReferenceFLEXDOO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterfaceLineReferenceFLEXDOO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "interfaceLineReferenceFLEXDOO")
    public JAXBElement<InterfaceLineReferenceFLEXDOO> createInterfaceLineReferenceFLEXDOO(InterfaceLineReferenceFLEXDOO value) {
        return new JAXBElement<InterfaceLineReferenceFLEXDOO>(_InterfaceLineReferenceFLEXDOO_QNAME, InterfaceLineReferenceFLEXDOO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CPQCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "cPQCloud")
    public JAXBElement<CPQCloud> createCPQCloud(CPQCloud value) {
        return new JAXBElement<CPQCloud>(_CPQCloud_QNAME, CPQCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GbPcContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "gbPcContext")
    public JAXBElement<GbPcContext> createGbPcContext(GbPcContext value) {
        return new JAXBElement<GbPcContext>(_GbPcContext_QNAME, GbPcContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterfaceLineReferenceFLEX }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "interfaceLineReferenceFLEX")
    public JAXBElement<InterfaceLineReferenceFLEX> createInterfaceLineReferenceFLEX(InterfaceLineReferenceFLEX value) {
        return new JAXBElement<InterfaceLineReferenceFLEX>(_InterfaceLineReferenceFLEX_QNAME, InterfaceLineReferenceFLEX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "tipoDeTransaccion", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextTipoDeTransaccion(String value) {
        return new JAXBElement<String>(_GbPcContextTipoDeTransaccion_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "fecha", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextFecha(String value) {
        return new JAXBElement<String>(_GbPcContextFecha_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "agencia", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextAgencia(String value) {
        return new JAXBElement<String>(_GbPcContextAgencia_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "noLinea", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextNoLinea(String value) {
        return new JAXBElement<String>(_GbPcContextNoLinea_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "noTrx", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextNoTrx(String value) {
        return new JAXBElement<String>(_GbPcContextNoTrx_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Fulfillment__Line__ID", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOFulfillmentLineID(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOFulfillmentLineID_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Source__Order__Number", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOSourceOrderNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOSourceOrderNumber_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Delivery__Name", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOODeliveryName(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOODeliveryName_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Source__Order__System", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOSourceOrderSystem(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOSourceOrderSystem_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Source__Schedule__Number", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOSourceScheduleNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOSourceScheduleNumber_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Customer__Item", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOCustomerItem(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOCustomerItem_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "period", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOPeriod(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOPeriod_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Profit__Center__Business__Unit", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOProfitCenterBusinessUnit(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOProfitCenterBusinessUnit_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Bill__of__Lading__Number", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOBillOfLadingNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOBillOfLadingNumber_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Price__Adjustment__ID", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOPriceAdjustmentID(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOPriceAdjustmentID_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Fulfillment__Line__Number", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOFulfillmentLineNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOFulfillmentLineNumber_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_DOO__Order__Number", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOODOOOrderNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOODOOOrderNumber_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_WayBill__Number", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOWayBillNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOWayBillNumber_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_Fulfill__Line__Split__Reference", scope = InterfaceLineReferenceFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXDOOFulfillLineSplitReference(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOFulfillLineSplitReference_QNAME, String.class, InterfaceLineReferenceFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "orderNumber", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudOrderNumber(String value) {
        return new JAXBElement<String>(_CPQCloudOrderNumber_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "serviceNumber", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudServiceNumber(String value) {
        return new JAXBElement<String>(_CPQCloudServiceNumber_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "period", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudPeriod(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXDOOPeriod_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "orderLineNumber", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudOrderLineNumber(String value) {
        return new JAXBElement<String>(_CPQCloudOrderLineNumber_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "__FLEX_Context", scope = InterfaceLineReferenceFLEX.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXFLEXContext(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXFLEXContext_QNAME, String.class, InterfaceLineReferenceFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "__FLEX_Context_DisplayValue", scope = InterfaceLineReferenceFLEX.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXFLEXContextDisplayValue_QNAME, String.class, InterfaceLineReferenceFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "InterfaceLineGuid", scope = InterfaceLineReferenceFLEX.class)
    public JAXBElement<String> createInterfaceLineReferenceFLEXInterfaceLineGuid(String value) {
        return new JAXBElement<String>(_InterfaceLineReferenceFLEXInterfaceLineGuid_QNAME, String.class, InterfaceLineReferenceFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceReferenceDff/", name = "_FLEX_NumOfSegments", scope = InterfaceLineReferenceFLEX.class)
    public JAXBElement<Integer> createInterfaceLineReferenceFLEXFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_InterfaceLineReferenceFLEXFLEXNumOfSegments_QNAME, Integer.class, InterfaceLineReferenceFLEX.class, value);
    }

}
