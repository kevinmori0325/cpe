
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CiMorocco_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "ciMorocco");
    private final static QName _TransactionHeaderFLEX_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "transactionHeaderFLEX");
    private final static QName _Peru_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "peru");
    private final static QName _CiChile_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "ciChile");
    private final static QName _CiPeru_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "ciPeru");
    private final static QName _Morocco_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "morocco");
    private final static QName _Chile_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "chile");
    private final static QName _PeruLocPeArFeTipoNc_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArFeTipoNc");
    private final static QName _PeruLocPeArFeRazonNcnd_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArFeRazonNcnd");
    private final static QName _PeruLocPeArFeTipoNd_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArFeTipoNd");
    private final static QName _PeruLocPeArDocumentoRelacionado_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArDocumentoRelacionado");
    private final static QName _PeruLocPeArSerieCompRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArSerieCompRef");
    private final static QName _PeruLocPeArFechaDocRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArFechaDocRef");
    private final static QName _PeruLocPeArTipoDocSunatRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArTipoDocSunatRef");
    private final static QName _PeruLocPeArEstadoFe_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArEstadoFe");
    private final static QName _CiChileAgencia_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "agencia");
    private final static QName _TransactionHeaderFLEXFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "__FLEX_Context");
    private final static QName _TransactionHeaderFLEXFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionHeaderFLEXLugarDePago_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "lugarDePago");
    private final static QName _TransactionHeaderFLEXRuta_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "ruta");
    private final static QName _TransactionHeaderFLEXFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "_FLEX_NumOfSegments");
    private final static QName _MoroccoNumberOfCeve_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "numberOfCeve");
    private final static QName _MoroccoFolio_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "folio");
    private final static QName _MoroccoLocMaArFeRazonNcnd_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArFeRazonNcnd");
    private final static QName _MoroccoLocMaArFeTipoNc_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArFeTipoNc");
    private final static QName _MoroccoLocMaArFeTipoNd_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArFeTipoNd");
    private final static QName _MoroccoLocMaArSerieCompRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArSerieCompRef");
    private final static QName _MoroccoLocMaArFechaDocRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArFechaDocRef");
    private final static QName _MoroccoLocMaArTipoDocSunatRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArTipoDocSunatRef");
    private final static QName _MoroccoSalesNote_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "salesNote");
    private final static QName _MoroccoLocMaArEstadoFe_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArEstadoFe");
    private final static QName _MoroccoLocMaArDocumentoRelacionado_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locMaArDocumentoRelacionado");
    private final static QName _MoroccoShopOfBranch_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "shopOfBranch");
    private final static QName _ChileDetalleError_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "detalleError");
    private final static QName _ChileDetalleError2_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "detalleError2");
    private final static QName _ChileDetalleError3_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "detalleError3");
    private final static QName _ChileEstadoDocumento_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "estadoDocumento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Chile }
     * 
     */
    public Chile createChile() {
        return new Chile();
    }

    /**
     * Create an instance of {@link CiChile }
     * 
     */
    public CiChile createCiChile() {
        return new CiChile();
    }

    /**
     * Create an instance of {@link Peru }
     * 
     */
    public Peru createPeru() {
        return new Peru();
    }

    /**
     * Create an instance of {@link CiMorocco }
     * 
     */
    public CiMorocco createCiMorocco() {
        return new CiMorocco();
    }

    /**
     * Create an instance of {@link TransactionHeaderFLEX }
     * 
     */
    public TransactionHeaderFLEX createTransactionHeaderFLEX() {
        return new TransactionHeaderFLEX();
    }

    /**
     * Create an instance of {@link CiPeru }
     * 
     */
    public CiPeru createCiPeru() {
        return new CiPeru();
    }

    /**
     * Create an instance of {@link Morocco }
     * 
     */
    public Morocco createMorocco() {
        return new Morocco();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiMorocco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "ciMorocco")
    public JAXBElement<CiMorocco> createCiMorocco(CiMorocco value) {
        return new JAXBElement<CiMorocco>(_CiMorocco_QNAME, CiMorocco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderFLEX }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "transactionHeaderFLEX")
    public JAXBElement<TransactionHeaderFLEX> createTransactionHeaderFLEX(TransactionHeaderFLEX value) {
        return new JAXBElement<TransactionHeaderFLEX>(_TransactionHeaderFLEX_QNAME, TransactionHeaderFLEX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Peru }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "peru")
    public JAXBElement<Peru> createPeru(Peru value) {
        return new JAXBElement<Peru>(_Peru_QNAME, Peru.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiChile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "ciChile")
    public JAXBElement<CiChile> createCiChile(CiChile value) {
        return new JAXBElement<CiChile>(_CiChile_QNAME, CiChile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CiPeru }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "ciPeru")
    public JAXBElement<CiPeru> createCiPeru(CiPeru value) {
        return new JAXBElement<CiPeru>(_CiPeru_QNAME, CiPeru.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Morocco }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "morocco")
    public JAXBElement<Morocco> createMorocco(Morocco value) {
        return new JAXBElement<Morocco>(_Morocco_QNAME, Morocco.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Chile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "chile")
    public JAXBElement<Chile> createChile(Chile value) {
        return new JAXBElement<Chile>(_Chile_QNAME, Chile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArFeTipoNc", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArFeTipoNc(String value) {
        return new JAXBElement<String>(_PeruLocPeArFeTipoNc_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArFeRazonNcnd", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArFeRazonNcnd(String value) {
        return new JAXBElement<String>(_PeruLocPeArFeRazonNcnd_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArFeTipoNd", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArFeTipoNd(String value) {
        return new JAXBElement<String>(_PeruLocPeArFeTipoNd_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArDocumentoRelacionado", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArDocumentoRelacionado(String value) {
        return new JAXBElement<String>(_PeruLocPeArDocumentoRelacionado_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArSerieCompRef", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArSerieCompRef(String value) {
        return new JAXBElement<String>(_PeruLocPeArSerieCompRef_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArFechaDocRef", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArFechaDocRef(String value) {
        return new JAXBElement<String>(_PeruLocPeArFechaDocRef_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArTipoDocSunatRef", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArTipoDocSunatRef(String value) {
        return new JAXBElement<String>(_PeruLocPeArTipoDocSunatRef_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArEstadoFe", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArEstadoFe(String value) {
        return new JAXBElement<String>(_PeruLocPeArEstadoFe_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "agencia", scope = CiChile.class)
    public JAXBElement<String> createCiChileAgencia(String value) {
        return new JAXBElement<String>(_CiChileAgencia_QNAME, String.class, CiChile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "agencia", scope = CiMorocco.class)
    public JAXBElement<String> createCiMoroccoAgencia(String value) {
        return new JAXBElement<String>(_CiChileAgencia_QNAME, String.class, CiMorocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "__FLEX_Context", scope = TransactionHeaderFLEX.class)
    public JAXBElement<String> createTransactionHeaderFLEXFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionHeaderFLEXFLEXContext_QNAME, String.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "__FLEX_Context_DisplayValue", scope = TransactionHeaderFLEX.class)
    public JAXBElement<String> createTransactionHeaderFLEXFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionHeaderFLEXFLEXContextDisplayValue_QNAME, String.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "lugarDePago", scope = TransactionHeaderFLEX.class)
    public JAXBElement<String> createTransactionHeaderFLEXLugarDePago(String value) {
        return new JAXBElement<String>(_TransactionHeaderFLEXLugarDePago_QNAME, String.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "ruta", scope = TransactionHeaderFLEX.class)
    public JAXBElement<String> createTransactionHeaderFLEXRuta(String value) {
        return new JAXBElement<String>(_TransactionHeaderFLEXRuta_QNAME, String.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "_FLEX_NumOfSegments", scope = TransactionHeaderFLEX.class)
    public JAXBElement<Integer> createTransactionHeaderFLEXFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionHeaderFLEXFLEXNumOfSegments_QNAME, Integer.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "numberOfCeve", scope = Morocco.class)
    public JAXBElement<String> createMoroccoNumberOfCeve(String value) {
        return new JAXBElement<String>(_MoroccoNumberOfCeve_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "folio", scope = Morocco.class)
    public JAXBElement<String> createMoroccoFolio(String value) {
        return new JAXBElement<String>(_MoroccoFolio_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArFeRazonNcnd", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArFeRazonNcnd(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArFeRazonNcnd_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArFeTipoNc", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArFeTipoNc(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArFeTipoNc_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArFeTipoNd", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArFeTipoNd(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArFeTipoNd_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArSerieCompRef", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArSerieCompRef(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArSerieCompRef_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArFechaDocRef", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArFechaDocRef(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArFechaDocRef_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArTipoDocSunatRef", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArTipoDocSunatRef(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArTipoDocSunatRef_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "salesNote", scope = Morocco.class)
    public JAXBElement<String> createMoroccoSalesNote(String value) {
        return new JAXBElement<String>(_MoroccoSalesNote_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArEstadoFe", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArEstadoFe(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArEstadoFe_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locMaArDocumentoRelacionado", scope = Morocco.class)
    public JAXBElement<String> createMoroccoLocMaArDocumentoRelacionado(String value) {
        return new JAXBElement<String>(_MoroccoLocMaArDocumentoRelacionado_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "shopOfBranch", scope = Morocco.class)
    public JAXBElement<String> createMoroccoShopOfBranch(String value) {
        return new JAXBElement<String>(_MoroccoShopOfBranch_QNAME, String.class, Morocco.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "agencia", scope = CiPeru.class)
    public JAXBElement<String> createCiPeruAgencia(String value) {
        return new JAXBElement<String>(_CiChileAgencia_QNAME, String.class, CiPeru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "numberOfCeve", scope = Chile.class)
    public JAXBElement<String> createChileNumberOfCeve(String value) {
        return new JAXBElement<String>(_MoroccoNumberOfCeve_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "folio", scope = Chile.class)
    public JAXBElement<String> createChileFolio(String value) {
        return new JAXBElement<String>(_MoroccoFolio_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "detalleError", scope = Chile.class)
    public JAXBElement<String> createChileDetalleError(String value) {
        return new JAXBElement<String>(_ChileDetalleError_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "salesNote", scope = Chile.class)
    public JAXBElement<String> createChileSalesNote(String value) {
        return new JAXBElement<String>(_MoroccoSalesNote_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "detalleError2", scope = Chile.class)
    public JAXBElement<String> createChileDetalleError2(String value) {
        return new JAXBElement<String>(_ChileDetalleError2_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "detalleError3", scope = Chile.class)
    public JAXBElement<String> createChileDetalleError3(String value) {
        return new JAXBElement<String>(_ChileDetalleError3_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "shopOfBranch", scope = Chile.class)
    public JAXBElement<String> createChileShopOfBranch(String value) {
        return new JAXBElement<String>(_MoroccoShopOfBranch_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "estadoDocumento", scope = Chile.class)
    public JAXBElement<String> createChileEstadoDocumento(String value) {
        return new JAXBElement<String>(_ChileEstadoDocumento_QNAME, String.class, Chile.class, value);
    }

}
