
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Peru complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Peru">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/}TransactionHeaderFLEX">
 *       &lt;sequence>
 *         &lt;element name="locPeArEstadoFe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArTipoDocSunatRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArFechaDocRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArSerieCompRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArDocumentoRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArFeTipoNc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArFeTipoNd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArFeRazonNcnd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Peru", propOrder = {
    "locPeArEstadoFe",
    "locPeArTipoDocSunatRef",
    "locPeArFechaDocRef",
    "locPeArSerieCompRef",
    "locPeArDocumentoRelacionado",
    "locPeArFeTipoNc",
    "locPeArFeTipoNd",
    "locPeArFeRazonNcnd"
})
public class Peru
    extends TransactionHeaderFLEX
{

    @XmlElementRef(name = "locPeArEstadoFe", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArEstadoFe;
    @XmlElementRef(name = "locPeArTipoDocSunatRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArTipoDocSunatRef;
    @XmlElementRef(name = "locPeArFechaDocRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArFechaDocRef;
    @XmlElementRef(name = "locPeArSerieCompRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArSerieCompRef;
    @XmlElementRef(name = "locPeArDocumentoRelacionado", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArDocumentoRelacionado;
    @XmlElementRef(name = "locPeArFeTipoNc", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArFeTipoNc;
    @XmlElementRef(name = "locPeArFeTipoNd", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArFeTipoNd;
    @XmlElementRef(name = "locPeArFeRazonNcnd", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArFeRazonNcnd;

    /**
     * Gets the value of the locPeArEstadoFe property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArEstadoFe() {
        return locPeArEstadoFe;
    }

    /**
     * Sets the value of the locPeArEstadoFe property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArEstadoFe(JAXBElement<String> value) {
        this.locPeArEstadoFe = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArTipoDocSunatRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArTipoDocSunatRef() {
        return locPeArTipoDocSunatRef;
    }

    /**
     * Sets the value of the locPeArTipoDocSunatRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArTipoDocSunatRef(JAXBElement<String> value) {
        this.locPeArTipoDocSunatRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArFechaDocRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArFechaDocRef() {
        return locPeArFechaDocRef;
    }

    /**
     * Sets the value of the locPeArFechaDocRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArFechaDocRef(JAXBElement<String> value) {
        this.locPeArFechaDocRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArSerieCompRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArSerieCompRef() {
        return locPeArSerieCompRef;
    }

    /**
     * Sets the value of the locPeArSerieCompRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArSerieCompRef(JAXBElement<String> value) {
        this.locPeArSerieCompRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArDocumentoRelacionado property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArDocumentoRelacionado() {
        return locPeArDocumentoRelacionado;
    }

    /**
     * Sets the value of the locPeArDocumentoRelacionado property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArDocumentoRelacionado(JAXBElement<String> value) {
        this.locPeArDocumentoRelacionado = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArFeTipoNc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArFeTipoNc() {
        return locPeArFeTipoNc;
    }

    /**
     * Sets the value of the locPeArFeTipoNc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArFeTipoNc(JAXBElement<String> value) {
        this.locPeArFeTipoNc = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArFeTipoNd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArFeTipoNd() {
        return locPeArFeTipoNd;
    }

    /**
     * Sets the value of the locPeArFeTipoNd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArFeTipoNd(JAXBElement<String> value) {
        this.locPeArFeTipoNd = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArFeRazonNcnd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArFeRazonNcnd() {
        return locPeArFeRazonNcnd;
    }

    /**
     * Sets the value of the locPeArFeRazonNcnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArFeRazonNcnd(JAXBElement<String> value) {
        this.locPeArFeRazonNcnd = ((JAXBElement<String> ) value);
    }

}
