
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionlinegdf;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionlinegdf package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionLineGdf_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "transactionLineGdf");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "transactionLineGdfJE_5FIT_5FESL_5FOF_5FSERVICES");
    private final static QName _TransactionLineGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "FLEX_PARAM_GLOBAL_COUNTRY_CODE");
    private final static QName _TransactionLineGdfFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "__FLEX_Context");
    private final static QName _TransactionLineGdfFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionLineGdfFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "_FLEX_NumOfSegments");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "reportingExclusionIndicator");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "_Service__Code");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "_Service__Code_Display");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "_Service__Mode");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "_Service__Mode_Display");
    private final static QName _TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", "reportingExclusionIndicator_Display");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionlinegdf
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES }
     * 
     */
    public TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICES() {
        return new TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES();
    }

    /**
     * Create an instance of {@link TransactionLineGdf }
     * 
     */
    public TransactionLineGdf createTransactionLineGdf() {
        return new TransactionLineGdf();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLineGdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "transactionLineGdf")
    public JAXBElement<TransactionLineGdf> createTransactionLineGdf(TransactionLineGdf value) {
        return new JAXBElement<TransactionLineGdf>(_TransactionLineGdf_QNAME, TransactionLineGdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "transactionLineGdfJE_5FIT_5FESL_5FOF_5FSERVICES")
    public JAXBElement<TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICES(TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES value) {
        return new JAXBElement<TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES_QNAME, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "FLEX_PARAM_GLOBAL_COUNTRY_CODE", scope = TransactionLineGdf.class)
    public JAXBElement<String> createTransactionLineGdfFLEXPARAMGLOBALCOUNTRYCODE(String value) {
        return new JAXBElement<String>(_TransactionLineGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME, String.class, TransactionLineGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "__FLEX_Context", scope = TransactionLineGdf.class)
    public JAXBElement<String> createTransactionLineGdfFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionLineGdfFLEXContext_QNAME, String.class, TransactionLineGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "__FLEX_Context_DisplayValue", scope = TransactionLineGdf.class)
    public JAXBElement<String> createTransactionLineGdfFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionLineGdfFLEXContextDisplayValue_QNAME, String.class, TransactionLineGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "_FLEX_NumOfSegments", scope = TransactionLineGdf.class)
    public JAXBElement<Integer> createTransactionLineGdfFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionLineGdfFLEXNumOfSegments_QNAME, Integer.class, TransactionLineGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "reportingExclusionIndicator", scope = TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator(String value) {
        return new JAXBElement<String>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME, String.class, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "_Service__Code", scope = TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceCode(String value) {
        return new JAXBElement<String>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME, String.class, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "_Service__Code_Display", scope = TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay(String value) {
        return new JAXBElement<String>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME, String.class, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "_Service__Mode", scope = TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceMode(String value) {
        return new JAXBElement<String>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME, String.class, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "_Service__Mode_Display", scope = TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay(String value) {
        return new JAXBElement<String>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME, String.class, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineGdf/", name = "reportingExclusionIndicator_Display", scope = TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay(String value) {
        return new JAXBElement<String>(_TransactionLineGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME, String.class, TransactionLineGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

}
