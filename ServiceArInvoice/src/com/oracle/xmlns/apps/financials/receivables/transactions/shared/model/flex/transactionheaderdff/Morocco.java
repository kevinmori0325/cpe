
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Morocco complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Morocco">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/}TransactionHeaderFLEX">
 *       &lt;sequence>
 *         &lt;element name="locMaArEstadoFe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArTipoDocSunatRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArFechaDocRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArSerieCompRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArDocumentoRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArFeTipoNc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArFeTipoNd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locMaArFeRazonNcnd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numberOfCeve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shopOfBranch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="salesNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Morocco", propOrder = {
    "locMaArEstadoFe",
    "locMaArTipoDocSunatRef",
    "locMaArFechaDocRef",
    "locMaArSerieCompRef",
    "locMaArDocumentoRelacionado",
    "locMaArFeTipoNc",
    "locMaArFeTipoNd",
    "locMaArFeRazonNcnd",
    "folio",
    "numberOfCeve",
    "shopOfBranch",
    "salesNote"
})
public class Morocco
    extends TransactionHeaderFLEX
{

    @XmlElementRef(name = "locMaArEstadoFe", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArEstadoFe;
    @XmlElementRef(name = "locMaArTipoDocSunatRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArTipoDocSunatRef;
    @XmlElementRef(name = "locMaArFechaDocRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArFechaDocRef;
    @XmlElementRef(name = "locMaArSerieCompRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArSerieCompRef;
    @XmlElementRef(name = "locMaArDocumentoRelacionado", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArDocumentoRelacionado;
    @XmlElementRef(name = "locMaArFeTipoNc", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArFeTipoNc;
    @XmlElementRef(name = "locMaArFeTipoNd", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArFeTipoNd;
    @XmlElementRef(name = "locMaArFeRazonNcnd", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locMaArFeRazonNcnd;
    @XmlElementRef(name = "folio", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> folio;
    @XmlElementRef(name = "numberOfCeve", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> numberOfCeve;
    @XmlElementRef(name = "shopOfBranch", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> shopOfBranch;
    @XmlElementRef(name = "salesNote", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> salesNote;

    /**
     * Gets the value of the locMaArEstadoFe property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArEstadoFe() {
        return locMaArEstadoFe;
    }

    /**
     * Sets the value of the locMaArEstadoFe property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArEstadoFe(JAXBElement<String> value) {
        this.locMaArEstadoFe = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArTipoDocSunatRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArTipoDocSunatRef() {
        return locMaArTipoDocSunatRef;
    }

    /**
     * Sets the value of the locMaArTipoDocSunatRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArTipoDocSunatRef(JAXBElement<String> value) {
        this.locMaArTipoDocSunatRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArFechaDocRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArFechaDocRef() {
        return locMaArFechaDocRef;
    }

    /**
     * Sets the value of the locMaArFechaDocRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArFechaDocRef(JAXBElement<String> value) {
        this.locMaArFechaDocRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArSerieCompRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArSerieCompRef() {
        return locMaArSerieCompRef;
    }

    /**
     * Sets the value of the locMaArSerieCompRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArSerieCompRef(JAXBElement<String> value) {
        this.locMaArSerieCompRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArDocumentoRelacionado property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArDocumentoRelacionado() {
        return locMaArDocumentoRelacionado;
    }

    /**
     * Sets the value of the locMaArDocumentoRelacionado property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArDocumentoRelacionado(JAXBElement<String> value) {
        this.locMaArDocumentoRelacionado = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArFeTipoNc property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArFeTipoNc() {
        return locMaArFeTipoNc;
    }

    /**
     * Sets the value of the locMaArFeTipoNc property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArFeTipoNc(JAXBElement<String> value) {
        this.locMaArFeTipoNc = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArFeTipoNd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArFeTipoNd() {
        return locMaArFeTipoNd;
    }

    /**
     * Sets the value of the locMaArFeTipoNd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArFeTipoNd(JAXBElement<String> value) {
        this.locMaArFeTipoNd = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locMaArFeRazonNcnd property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocMaArFeRazonNcnd() {
        return locMaArFeRazonNcnd;
    }

    /**
     * Sets the value of the locMaArFeRazonNcnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocMaArFeRazonNcnd(JAXBElement<String> value) {
        this.locMaArFeRazonNcnd = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the folio property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFolio() {
        return folio;
    }

    /**
     * Sets the value of the folio property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFolio(JAXBElement<String> value) {
        this.folio = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the numberOfCeve property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumberOfCeve() {
        return numberOfCeve;
    }

    /**
     * Sets the value of the numberOfCeve property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumberOfCeve(JAXBElement<String> value) {
        this.numberOfCeve = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the shopOfBranch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShopOfBranch() {
        return shopOfBranch;
    }

    /**
     * Sets the value of the shopOfBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShopOfBranch(JAXBElement<String> value) {
        this.shopOfBranch = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the salesNote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSalesNote() {
        return salesNote;
    }

    /**
     * Sets the value of the salesNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSalesNote(JAXBElement<String> value) {
        this.salesNote = ((JAXBElement<String> ) value);
    }

}
