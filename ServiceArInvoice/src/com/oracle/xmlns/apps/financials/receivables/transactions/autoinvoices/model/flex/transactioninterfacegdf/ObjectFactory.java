
package com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactioninterfacegdf;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactioninterfacegdf package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionInterfaceGdf_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdf");
    private final static QName _TransactionInterfaceGdfJAxKRReceivablesInformation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJAxKRReceivablesInformation");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJL_5FBR_5FARXTWMAI_5FAdditional");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FES_5FMODELO415_5F347");
    private final static QName _TransactionInterfaceGdfJE5FDE5FZ45FREPORTING_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FDE_5FZ4_5FREPORTING");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FIT_5FESL_5FOF_5FSERVICES");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO349_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FES_5FMODELO349");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO347_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FES_5FMODELO347");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO340_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FES_5FMODELO340");
    private final static QName _JExESOnlineVatReporting_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "jExESOnlineVatReporting");
    private final static QName _JLxMXReceivablesInformation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "jLxMXReceivablesInformation");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO347PR_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FES_5FMODELO347PR");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PR_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "transactionInterfaceGdfJE_5FES_5FMODELO415_5F347PR");
    private final static QName _JLxMXReceivablesInformationCFDCBBSerialNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "CFDCBBSerialNumber");
    private final static QName _JLxMXReceivablesInformationCFDCBBInvoiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "CFDCBBInvoiceNumber");
    private final static QName _JLxMXReceivablesInformationCFDIUniqueIdentifier_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "CFDIUniqueIdentifier");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Year__of__Amount__Received__in__Cas");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Transaction__Date");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO340TransactionDeadline_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Transaction__Deadline");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TaxAuthorityStatus_Display");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "MessageCode");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocationDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Property__Location_Display");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TransactionStatus_Display");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TransactionStatus");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TaxAuthorityStatus");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Property__Location");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "MessageDescription");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "DateLastUpdated");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Transmission__of__Property_Display");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Transmission__of__Property");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposedDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "vatNotExposed_Display");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "reportingExclusionIndicator_Display");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESErrorCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "ErrorCode");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESEDeclarationId_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "EDeclarationId");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Service__Mode");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Service__Code");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Reporting__Payment__Method");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "reportingExclusionIndicator");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposed_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "vatNotExposed");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountryDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Reporting__Payment__Country_Display");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethodDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Reporting__Payment__Method_Display");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Service__Mode_Display");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Service__Code_Display");
    private final static QName _TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountry_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Reporting__Payment__Country");
    private final static QName _TransactionInterfaceGdfJAxKRReceivablesInformationSentByElectronicMediaDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_SentByElectronicMedia_Display");
    private final static QName _TransactionInterfaceGdfJAxKRReceivablesInformationSentByElectronicMedia_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_SentByElectronicMedia");
    private final static QName _TransactionInterfaceGdfFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_FLEX_NumOfSegments");
    private final static QName _TransactionInterfaceGdfInterfaceLineGuid_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "InterfaceLineGuid");
    private final static QName _TransactionInterfaceGdfFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionInterfaceGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "FLEX_PARAM_GLOBAL_COUNTRY_CODE");
    private final static QName _TransactionInterfaceGdfFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "__FLEX_Context");
    private final static QName _TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGRecordType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Record__Type");
    private final static QName _TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGRecordTypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Record__Type_Display");
    private final static QName _TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGReasonDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Reason_Display");
    private final static QName _TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGReason_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Reason");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO349CorrectionYear_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Correction__Year");
    private final static QName _TransactionInterfaceGdfJE5FES5FMODELO349CorrectionPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Correction__Period");
    private final static QName _JExESOnlineVatReportingThirdPartyInvoiceDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "ThirdPartyInvoice_Display");
    private final static QName _JExESOnlineVatReportingTransmissionOfPropertySubjectT_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TransmissionOfPropertySubjectT");
    private final static QName _JExESOnlineVatReportingOriginalInvoiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "OriginalInvoiceNumber");
    private final static QName _JExESOnlineVatReportingTransmissionOfPropertySubjectTDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TransmissionOfPropertySubjectT_Display");
    private final static QName _JExESOnlineVatReportingDateTransactionPerformed_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "DateTransactionPerformed");
    private final static QName _JExESOnlineVatReportingDocumentTypeOverrideDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "DocumentTypeOverride_Display");
    private final static QName _JExESOnlineVatReportingPropertyLocation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "PropertyLocation");
    private final static QName _JExESOnlineVatReportingRegisterType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "RegisterType");
    private final static QName _JExESOnlineVatReportingSpecialRegime_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "SpecialRegime");
    private final static QName _JExESOnlineVatReportingCorrectionPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "CorrectionPeriod");
    private final static QName _JExESOnlineVatReportingLastDocumentNumberOfSummaryInv_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "LastDocumentNumberOfSummaryInv");
    private final static QName _JExESOnlineVatReportingTransactionDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TransactionDate");
    private final static QName _JExESOnlineVatReportingIntraEUDeclaredKeyDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "IntraEUDeclaredKey_Display");
    private final static QName _JExESOnlineVatReportingDocumentTypeOverride_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "DocumentTypeOverride");
    private final static QName _JExESOnlineVatReportingIntraEUSubtypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "IntraEUSubtype_Display");
    private final static QName _JExESOnlineVatReportingTransactionDeadline_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "TransactionDeadline");
    private final static QName _JExESOnlineVatReportingRegisterTypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "RegisterType_Display");
    private final static QName _JExESOnlineVatReportingPropertyLocationDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "PropertyLocation_Display");
    private final static QName _JExESOnlineVatReportingIntraEUSubtype_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "IntraEUSubtype");
    private final static QName _JExESOnlineVatReportingCorrectionYear_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "CorrectionYear");
    private final static QName _JExESOnlineVatReportingIntraEUDeclaredKey_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "IntraEUDeclaredKey");
    private final static QName _JExESOnlineVatReportingThirdPartyInvoice_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "ThirdPartyInvoice");
    private final static QName _JExESOnlineVatReportingYearOfAmountReceivedInCash_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "YearOfAmountReceivedInCash");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestGraceDays_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Grace__Days");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyRate2FAmount_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Penalty__Rate___2F__Amount");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Type");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestFormula_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Formula");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestRate2FAmount_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Rate___2F__Amount");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Period");
    private final static QName _TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", "_Interest__Penalty__Type");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactioninterfacegdf
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FES5FMODELO340 }
     * 
     */
    public TransactionInterfaceGdfJE5FES5FMODELO340 createTransactionInterfaceGdfJE5FES5FMODELO340() {
        return new TransactionInterfaceGdfJE5FES5FMODELO340();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJAxKRReceivablesInformation }
     * 
     */
    public TransactionInterfaceGdfJAxKRReceivablesInformation createTransactionInterfaceGdfJAxKRReceivablesInformation() {
        return new TransactionInterfaceGdfJAxKRReceivablesInformation();
    }

    /**
     * Create an instance of {@link JExESOnlineVatReporting }
     * 
     */
    public JExESOnlineVatReporting createJExESOnlineVatReporting() {
        return new JExESOnlineVatReporting();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES }
     * 
     */
    public TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES() {
        return new TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FES5FMODELO4155F347PR }
     * 
     */
    public TransactionInterfaceGdfJE5FES5FMODELO4155F347PR createTransactionInterfaceGdfJE5FES5FMODELO4155F347PR() {
        return new TransactionInterfaceGdfJE5FES5FMODELO4155F347PR();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdf }
     * 
     */
    public TransactionInterfaceGdf createTransactionInterfaceGdf() {
        return new TransactionInterfaceGdf();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FES5FMODELO347PR }
     * 
     */
    public TransactionInterfaceGdfJE5FES5FMODELO347PR createTransactionInterfaceGdfJE5FES5FMODELO347PR() {
        return new TransactionInterfaceGdfJE5FES5FMODELO347PR();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FES5FMODELO349 }
     * 
     */
    public TransactionInterfaceGdfJE5FES5FMODELO349 createTransactionInterfaceGdfJE5FES5FMODELO349() {
        return new TransactionInterfaceGdfJE5FES5FMODELO349();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FES5FMODELO347 }
     * 
     */
    public TransactionInterfaceGdfJE5FES5FMODELO347 createTransactionInterfaceGdfJE5FES5FMODELO347() {
        return new TransactionInterfaceGdfJE5FES5FMODELO347();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional }
     * 
     */
    public TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional() {
        return new TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FES5FMODELO4155F347 }
     * 
     */
    public TransactionInterfaceGdfJE5FES5FMODELO4155F347 createTransactionInterfaceGdfJE5FES5FMODELO4155F347() {
        return new TransactionInterfaceGdfJE5FES5FMODELO4155F347();
    }

    /**
     * Create an instance of {@link JLxMXReceivablesInformation }
     * 
     */
    public JLxMXReceivablesInformation createJLxMXReceivablesInformation() {
        return new JLxMXReceivablesInformation();
    }

    /**
     * Create an instance of {@link TransactionInterfaceGdfJE5FDE5FZ45FREPORTING }
     * 
     */
    public TransactionInterfaceGdfJE5FDE5FZ45FREPORTING createTransactionInterfaceGdfJE5FDE5FZ45FREPORTING() {
        return new TransactionInterfaceGdfJE5FDE5FZ45FREPORTING();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdf")
    public JAXBElement<TransactionInterfaceGdf> createTransactionInterfaceGdf(TransactionInterfaceGdf value) {
        return new JAXBElement<TransactionInterfaceGdf>(_TransactionInterfaceGdf_QNAME, TransactionInterfaceGdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJAxKRReceivablesInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJAxKRReceivablesInformation")
    public JAXBElement<TransactionInterfaceGdfJAxKRReceivablesInformation> createTransactionInterfaceGdfJAxKRReceivablesInformation(TransactionInterfaceGdfJAxKRReceivablesInformation value) {
        return new JAXBElement<TransactionInterfaceGdfJAxKRReceivablesInformation>(_TransactionInterfaceGdfJAxKRReceivablesInformation_QNAME, TransactionInterfaceGdfJAxKRReceivablesInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJL_5FBR_5FARXTWMAI_5FAdditional")
    public JAXBElement<TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional(TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional value) {
        return new JAXBElement<TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional_QNAME, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FES5FMODELO4155F347 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FES_5FMODELO415_5F347")
    public JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO4155F347> createTransactionInterfaceGdfJE5FES5FMODELO4155F347(TransactionInterfaceGdfJE5FES5FMODELO4155F347 value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO4155F347>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347_QNAME, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FDE5FZ45FREPORTING }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FDE_5FZ4_5FREPORTING")
    public JAXBElement<TransactionInterfaceGdfJE5FDE5FZ45FREPORTING> createTransactionInterfaceGdfJE5FDE5FZ45FREPORTING(TransactionInterfaceGdfJE5FDE5FZ45FREPORTING value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FDE5FZ45FREPORTING>(_TransactionInterfaceGdfJE5FDE5FZ45FREPORTING_QNAME, TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FIT_5FESL_5FOF_5FSERVICES")
    public JAXBElement<TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES(TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES_QNAME, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FES5FMODELO349 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FES_5FMODELO349")
    public JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO349> createTransactionInterfaceGdfJE5FES5FMODELO349(TransactionInterfaceGdfJE5FES5FMODELO349 value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO349>(_TransactionInterfaceGdfJE5FES5FMODELO349_QNAME, TransactionInterfaceGdfJE5FES5FMODELO349 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FES5FMODELO347 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FES_5FMODELO347")
    public JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO347> createTransactionInterfaceGdfJE5FES5FMODELO347(TransactionInterfaceGdfJE5FES5FMODELO347 value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO347>(_TransactionInterfaceGdfJE5FES5FMODELO347_QNAME, TransactionInterfaceGdfJE5FES5FMODELO347 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FES5FMODELO340 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FES_5FMODELO340")
    public JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO340> createTransactionInterfaceGdfJE5FES5FMODELO340(TransactionInterfaceGdfJE5FES5FMODELO340 value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO340>(_TransactionInterfaceGdfJE5FES5FMODELO340_QNAME, TransactionInterfaceGdfJE5FES5FMODELO340 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JExESOnlineVatReporting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "jExESOnlineVatReporting")
    public JAXBElement<JExESOnlineVatReporting> createJExESOnlineVatReporting(JExESOnlineVatReporting value) {
        return new JAXBElement<JExESOnlineVatReporting>(_JExESOnlineVatReporting_QNAME, JExESOnlineVatReporting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JLxMXReceivablesInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "jLxMXReceivablesInformation")
    public JAXBElement<JLxMXReceivablesInformation> createJLxMXReceivablesInformation(JLxMXReceivablesInformation value) {
        return new JAXBElement<JLxMXReceivablesInformation>(_JLxMXReceivablesInformation_QNAME, JLxMXReceivablesInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FES5FMODELO347PR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FES_5FMODELO347PR")
    public JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO347PR> createTransactionInterfaceGdfJE5FES5FMODELO347PR(TransactionInterfaceGdfJE5FES5FMODELO347PR value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO347PR>(_TransactionInterfaceGdfJE5FES5FMODELO347PR_QNAME, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceGdfJE5FES5FMODELO4155F347PR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "transactionInterfaceGdfJE_5FES_5FMODELO415_5F347PR")
    public JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO4155F347PR> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PR(TransactionInterfaceGdfJE5FES5FMODELO4155F347PR value) {
        return new JAXBElement<TransactionInterfaceGdfJE5FES5FMODELO4155F347PR>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PR_QNAME, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "CFDCBBSerialNumber", scope = JLxMXReceivablesInformation.class)
    public JAXBElement<String> createJLxMXReceivablesInformationCFDCBBSerialNumber(String value) {
        return new JAXBElement<String>(_JLxMXReceivablesInformationCFDCBBSerialNumber_QNAME, String.class, JLxMXReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "CFDCBBInvoiceNumber", scope = JLxMXReceivablesInformation.class)
    public JAXBElement<String> createJLxMXReceivablesInformationCFDCBBInvoiceNumber(String value) {
        return new JAXBElement<String>(_JLxMXReceivablesInformationCFDCBBInvoiceNumber_QNAME, String.class, JLxMXReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "CFDIUniqueIdentifier", scope = JLxMXReceivablesInformation.class)
    public JAXBElement<String> createJLxMXReceivablesInformationCFDIUniqueIdentifier(String value) {
        return new JAXBElement<String>(_JLxMXReceivablesInformationCFDIUniqueIdentifier_QNAME, String.class, JLxMXReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionInterfaceGdfJE5FES5FMODELO340 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO340 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Date", scope = TransactionInterfaceGdfJE5FES5FMODELO340 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO340TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO340 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Deadline", scope = TransactionInterfaceGdfJE5FES5FMODELO340 .class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJE5FES5FMODELO340TransactionDeadline(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDeadline_QNAME, BigDecimal.class, TransactionInterfaceGdfJE5FES5FMODELO340 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageCode", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Property__Location_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocationDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocationDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Property__Location", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocation(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocation_QNAME, BigDecimal.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageDescription", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateLastUpdated", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Date", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRYearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "vatNotExposed_Display", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposedDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposedDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "reportingExclusionIndicator_Display", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "ErrorCode", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESErrorCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESErrorCode_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "EDeclarationId", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESEDeclarationId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESEDeclarationId_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Service__Mode", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceMode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Service__Code", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Reporting__Payment__Method", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethod(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethod_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "reportingExclusionIndicator", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "vatNotExposed", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposed(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposed_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Reporting__Payment__Country_Display", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountryDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountryDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Reporting__Payment__Method_Display", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethodDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethodDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Service__Mode_Display", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Service__Code_Display", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Reporting__Payment__Country", scope = TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountry(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountry_QNAME, String.class, TransactionInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_SentByElectronicMedia_Display", scope = TransactionInterfaceGdfJAxKRReceivablesInformation.class)
    public JAXBElement<String> createTransactionInterfaceGdfJAxKRReceivablesInformationSentByElectronicMediaDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJAxKRReceivablesInformationSentByElectronicMediaDisplay_QNAME, String.class, TransactionInterfaceGdfJAxKRReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_SentByElectronicMedia", scope = TransactionInterfaceGdfJAxKRReceivablesInformation.class)
    public JAXBElement<String> createTransactionInterfaceGdfJAxKRReceivablesInformationSentByElectronicMedia(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJAxKRReceivablesInformationSentByElectronicMedia_QNAME, String.class, TransactionInterfaceGdfJAxKRReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRTaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageCode", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRMessageCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Property__Location_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRPropertyLocationDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocationDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRTransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRTransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRTaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Property__Location", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJE5FES5FMODELO347PRPropertyLocation(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRPropertyLocation_QNAME, BigDecimal.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageDescription", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRMessageDescription(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateLastUpdated", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO347PRDateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Date", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO347PRTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRYearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRTransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property", scope = TransactionInterfaceGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347PRTransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_FLEX_NumOfSegments", scope = TransactionInterfaceGdf.class)
    public JAXBElement<Integer> createTransactionInterfaceGdfFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionInterfaceGdfFLEXNumOfSegments_QNAME, Integer.class, TransactionInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "InterfaceLineGuid", scope = TransactionInterfaceGdf.class)
    public JAXBElement<String> createTransactionInterfaceGdfInterfaceLineGuid(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfInterfaceLineGuid_QNAME, String.class, TransactionInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "__FLEX_Context_DisplayValue", scope = TransactionInterfaceGdf.class)
    public JAXBElement<String> createTransactionInterfaceGdfFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfFLEXContextDisplayValue_QNAME, String.class, TransactionInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "FLEX_PARAM_GLOBAL_COUNTRY_CODE", scope = TransactionInterfaceGdf.class)
    public JAXBElement<String> createTransactionInterfaceGdfFLEXPARAMGLOBALCOUNTRYCODE(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME, String.class, TransactionInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "__FLEX_Context", scope = TransactionInterfaceGdf.class)
    public JAXBElement<String> createTransactionInterfaceGdfFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfFLEXContext_QNAME, String.class, TransactionInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateLastUpdated", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO4155F347DateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageDescription", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347MessageDescription(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageCode", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347MessageCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347YearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Date", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO4155F347TaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateLastUpdated", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO347DateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageDescription", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347MessageDescription(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageCode", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347MessageCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347TaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347YearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Date", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO347TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347TransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347TransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347TransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transmission__of__Property", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347TransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO347TaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Record__Type", scope = TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FDE5FZ45FREPORTINGRecordType(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGRecordType_QNAME, String.class, TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Record__Type_Display", scope = TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FDE5FZ45FREPORTINGRecordTypeDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGRecordTypeDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Reason_Display", scope = TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FDE5FZ45FREPORTINGReasonDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGReasonDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Reason", scope = TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FDE5FZ45FREPORTINGReason(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FDE5FZ45FREPORTINGReason_QNAME, String.class, TransactionInterfaceGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Correction__Year", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349CorrectionYear(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO349CorrectionYear_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateLastUpdated", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO349DateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageDescription", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349MessageDescription(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageCode", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349MessageCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349TaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Correction__Period", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349CorrectionPeriod(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO349CorrectionPeriod_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Date", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionInterfaceGdfJE5FES5FMODELO349TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Transaction__Deadline", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJE5FES5FMODELO349TransactionDeadline(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJE5FES5FMODELO340TransactionDeadline_QNAME, BigDecimal.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus_Display", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349TransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349TransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus", scope = TransactionInterfaceGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionInterfaceGdfJE5FES5FMODELO349TaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME, String.class, TransactionInterfaceGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "ThirdPartyInvoice_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingThirdPartyInvoiceDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingThirdPartyInvoiceDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransmissionOfPropertySubjectT", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransmissionOfPropertySubjectT(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingTransmissionOfPropertySubjectT_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageCode", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingMessageCode(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageCode_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "OriginalInvoiceNumber", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingOriginalInvoiceNumber(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingOriginalInvoiceNumber_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransmissionOfPropertySubjectT_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransmissionOfPropertySubjectTDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingTransmissionOfPropertySubjectTDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatus_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateTransactionPerformed", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingDateTransactionPerformed(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JExESOnlineVatReportingDateTransactionPerformed_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DocumentTypeOverride_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingDocumentTypeOverrideDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingDocumentTypeOverrideDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "PropertyLocation", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingPropertyLocation(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingPropertyLocation_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "MessageDescription", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingMessageDescription(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRMessageDescription_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "RegisterType", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingRegisterType(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingRegisterType_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "SpecialRegime", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingSpecialRegime(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingSpecialRegime_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "CorrectionPeriod", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingCorrectionPeriod(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingCorrectionPeriod_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "LastDocumentNumberOfSummaryInv", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingLastDocumentNumberOfSummaryInv(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingLastDocumentNumberOfSummaryInv_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionDate", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JExESOnlineVatReportingTransactionDate_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "IntraEUDeclaredKey_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUDeclaredKeyDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUDeclaredKeyDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DocumentTypeOverride", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingDocumentTypeOverride(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingDocumentTypeOverride_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "IntraEUSubtype_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUSubtypeDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUSubtypeDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionDeadline", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingTransactionDeadline(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingTransactionDeadline_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TransactionStatus_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "TaxAuthorityStatus", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "RegisterType_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingRegisterTypeDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingRegisterTypeDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "PropertyLocation_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingPropertyLocationDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingPropertyLocationDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "DateLastUpdated", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingDateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionInterfaceGdfJE5FES5FMODELO4155F347PRDateLastUpdated_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "IntraEUSubtype", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUSubtype(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUSubtype_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "CorrectionYear", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingCorrectionYear(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingCorrectionYear_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "IntraEUDeclaredKey", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUDeclaredKey(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUDeclaredKey_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "ThirdPartyInvoice", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingThirdPartyInvoice(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingThirdPartyInvoice_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "YearOfAmountReceivedInCash", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingYearOfAmountReceivedInCash(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingYearOfAmountReceivedInCash_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Grace__Days", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestGraceDays(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestGraceDays_QNAME, BigDecimal.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Penalty__Rate___2F__Amount", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyRate2FAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyRate2FAmount_QNAME, BigDecimal.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Type", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<String> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestType(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestType_QNAME, String.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Formula", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<String> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestFormula(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestFormula_QNAME, String.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Rate___2F__Amount", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestRate2FAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestRate2FAmount_QNAME, BigDecimal.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Period", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPeriod(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPeriod_QNAME, BigDecimal.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", name = "_Interest__Penalty__Type", scope = TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<String> createTransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyType(String value) {
        return new JAXBElement<String>(_TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyType_QNAME, String.class, TransactionInterfaceGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

}
