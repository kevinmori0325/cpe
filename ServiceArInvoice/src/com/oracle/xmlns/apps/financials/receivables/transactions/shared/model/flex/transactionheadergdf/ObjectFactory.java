
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheadergdf;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheadergdf package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347PR_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FES_5FMODELO415_5F347PR");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJL_5FBR_5FARXTWMAI_5FAdditional");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO347PR_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FES_5FMODELO347PR");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO347_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FES_5FMODELO347");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO349_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FES_5FMODELO349");
    private final static QName _TransactionHeaderGdfJAxKRReceivablesInformation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJAxKRReceivablesInformation");
    private final static QName _TransactionHeaderGdf_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdf");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO340_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FES_5FMODELO340");
    private final static QName _Jaxtwtransactions_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "jaxtwtransactions");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FIT_5FESL_5FOF_5FSERVICES");
    private final static QName _TransactionHeaderGdfJE5FDE5FZ45FREPORTING_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FDE_5FZ4_5FREPORTING");
    private final static QName _Jlxcotransactions_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "jlxcotransactions");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "transactionHeaderGdfJE_5FES_5FMODELO415_5F347");
    private final static QName _JLxMXReceivablesInformation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "jLxMXReceivablesInformation");
    private final static QName _Jlxcltransactions_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "jlxcltransactions");
    private final static QName _Jlxartransactions_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "jlxartransactions");
    private final static QName _JExESOnlineVatReporting_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "jExESOnlineVatReporting");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethodDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Reporting__Payment__Method_Display");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountryDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Reporting__Payment__Country_Display");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Service__Mode_Display");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "ReportingExclusionIndicator_Display");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountry_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Reporting__Payment__Country");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "ReportingExclusionIndicator");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Service__Code_Display");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESEDeclarationId_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "EDeclarationId");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESErrorCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "ErrorCode");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposedDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "vatNotExposed_Display");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Service__Code");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Reporting__Payment__Method");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposed_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "vatNotExposed");
    private final static QName _TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Service__Mode");
    private final static QName _JLxMXReceivablesInformationCFDCBBSerialNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "CFDCBBSerialNumber");
    private final static QName _JLxMXReceivablesInformationCFDCBBInvoiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "CFDCBBInvoiceNumber");
    private final static QName _JLxMXReceivablesInformationCFDIUniqueIdentifier_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "CFDIUniqueIdentifier");
    private final static QName _JlxcotransactionsOriginalTransactionTypeId_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "originalTransactionTypeId");
    private final static QName _JlxcotransactionsCopyStatus_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "copyStatus");
    private final static QName _JlxcotransactionsCopyStatusDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "copyStatus_Display");
    private final static QName _JlxartransactionsCaiDueDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "caiDueDate");
    private final static QName _JlxartransactionsCaiNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "caiNumber");
    private final static QName _JlxartransactionsNumericBarCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "numericBarCode");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO340TransactionDeadline_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Transaction__Deadline");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Year__of__Amount__Received__in__Cas");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Transaction__Date");
    private final static QName _JaxtwtransactionsWineAndCigarette_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "wineAndCigarette");
    private final static QName _JaxtwtransactionsExportTypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportType_Display");
    private final static QName _JaxtwtransactionsExportNameDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportName_Display");
    private final static QName _JaxtwtransactionsExportName_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportName");
    private final static QName _JaxtwtransactionsLegacyUniformInvoice_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "legacyUniformInvoice");
    private final static QName _JaxtwtransactionsExportMethod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportMethod");
    private final static QName _JaxtwtransactionsOriginalTransactionNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "originalTransactionNumber");
    private final static QName _JaxtwtransactionsExportMethodDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportMethod_Display");
    private final static QName _JaxtwtransactionsDeductibleType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "deductibleType");
    private final static QName _JaxtwtransactionsExportType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportType");
    private final static QName _JaxtwtransactionsWineAndCigaretteDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "wineAndCigarette_Display");
    private final static QName _JaxtwtransactionsExportCertificateNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportCertificateNumber");
    private final static QName _JaxtwtransactionsDeductibleTypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "deductibleType_Display");
    private final static QName _JaxtwtransactionsExportDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "exportDate");
    private final static QName _TransactionHeaderGdfFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionHeaderGdfFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_FLEX_NumOfSegments");
    private final static QName _TransactionHeaderGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "FLEX_PARAM_GLOBAL_COUNTRY_CODE");
    private final static QName _TransactionHeaderGdfFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "__FLEX_Context");
    private final static QName _TransactionHeaderGdfJAxKRReceivablesInformationSentByElectronicMediaDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_SentByElectronicMedia_Display");
    private final static QName _TransactionHeaderGdfJAxKRReceivablesInformationSentByElectronicMedia_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_SentByElectronicMedia");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "DateLastUpdated");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "MessageDescription");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "MessageCode");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TaxAuthorityStatus_Display");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Transmission__of__Property_Display");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TaxAuthorityStatus");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfProperty_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Transmission__of__Property");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TransactionStatus");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TransactionStatus_Display");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO349CorrectionPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Correction__Period");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO349CorrectionYear_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Correction__Year");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Property__Location");
    private final static QName _TransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocationDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Property__Location_Display");
    private final static QName _JExESOnlineVatReportingFLEXPARAMInvoiceDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "FLEX_PARAM_InvoiceDate");
    private final static QName _JExESOnlineVatReportingTransactiontransactionDeadline_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TransactiontransactionDeadline");
    private final static QName _JExESOnlineVatReportingFLEXPARAMBillToSiteUseId_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "FLEX_PARAM_BillToSiteUseId");
    private final static QName _JExESOnlineVatReportingPropertyLocationDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "PropertyLocation_Display");
    private final static QName _JExESOnlineVatReportingRegisterTypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "RegisterType_Display");
    private final static QName _JExESOnlineVatReportingYearOfAmountReceivedInCash_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "YearOfAmountReceivedInCash");
    private final static QName _JExESOnlineVatReportingThirdPartyInvoice_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "ThirdPartyInvoice");
    private final static QName _JExESOnlineVatReportingIntraEUDeclaredKey_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "IntraEUDeclaredKey");
    private final static QName _JExESOnlineVatReportingIntraEUSubtype_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "IntraEUSubtype");
    private final static QName _JExESOnlineVatReportingCorrectionYear_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "CorrectionYear");
    private final static QName _JExESOnlineVatReportingDocumentTypeOverride_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "DocumentTypeOverride");
    private final static QName _JExESOnlineVatReportingIntraEUDeclaredKeyDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "IntraEUDeclaredKey_Display");
    private final static QName _JExESOnlineVatReportingSpecialRegimeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "SpecialRegime_Display");
    private final static QName _JExESOnlineVatReportingIntraEUSubtypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "IntraEUSubtype_Display");
    private final static QName _JExESOnlineVatReportingFLEXPARAMCustomerTrxId_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "FLEX_PARAM_CustomerTrxId");
    private final static QName _JExESOnlineVatReportingDocumentTypeOverrideDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "DocumentTypeOverride_Display");
    private final static QName _JExESOnlineVatReportingPropertyLocation_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "PropertyLocation");
    private final static QName _JExESOnlineVatReportingRegisterType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "RegisterType");
    private final static QName _JExESOnlineVatReportingTransactionDate_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TransactionDate");
    private final static QName _JExESOnlineVatReportingLastDocumentNumberOfSummaryInv_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "LastDocumentNumberOfSummaryInv");
    private final static QName _JExESOnlineVatReportingSpecialRegime_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "SpecialRegime");
    private final static QName _JExESOnlineVatReportingCorrectionPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "CorrectionPeriod");
    private final static QName _JExESOnlineVatReportingTransmissionOfPropertySubjectTDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TransmissionOfPropertySubjectT_Display");
    private final static QName _JExESOnlineVatReportingOriginalInvoiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "OriginalInvoiceNumber");
    private final static QName _JExESOnlineVatReportingTransmissionOfPropertySubjectT_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "TransmissionOfPropertySubjectT");
    private final static QName _JExESOnlineVatReportingThirdPartyInvoiceDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "ThirdPartyInvoice_Display");
    private final static QName _JExESOnlineVatReportingDateTransactionPerformed_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "DateTransactionPerformed");
    private final static QName _JExESOnlineVatReportingOriginalInvoiceNumberDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "OriginalInvoiceNumber_Display");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Type");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestGraceDays_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Grace__Days");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyRate2FAmount_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Penalty__Rate___2F__Amount");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Period");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Penalty__Type");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestFormula_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Formula");
    private final static QName _TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestRate2FAmount_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Interest__Rate___2F__Amount");
    private final static QName _TransactionHeaderGdfJE5FDE5FZ45FREPORTINGRecordTypeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Record__Type_Display");
    private final static QName _TransactionHeaderGdfJE5FDE5FZ45FREPORTINGRecordType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Record__Type");
    private final static QName _TransactionHeaderGdfJE5FDE5FZ45FREPORTINGReason_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Reason");
    private final static QName _TransactionHeaderGdfJE5FDE5FZ45FREPORTINGReasonDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", "_Reason_Display");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheadergdf
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FES5FMODELO340 }
     * 
     */
    public TransactionHeaderGdfJE5FES5FMODELO340 createTransactionHeaderGdfJE5FES5FMODELO340() {
        return new TransactionHeaderGdfJE5FES5FMODELO340();
    }

    /**
     * Create an instance of {@link Jaxtwtransactions }
     * 
     */
    public Jaxtwtransactions createJaxtwtransactions() {
        return new Jaxtwtransactions();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FDE5FZ45FREPORTING }
     * 
     */
    public TransactionHeaderGdfJE5FDE5FZ45FREPORTING createTransactionHeaderGdfJE5FDE5FZ45FREPORTING() {
        return new TransactionHeaderGdfJE5FDE5FZ45FREPORTING();
    }

    /**
     * Create an instance of {@link JExESOnlineVatReporting }
     * 
     */
    public JExESOnlineVatReporting createJExESOnlineVatReporting() {
        return new JExESOnlineVatReporting();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FES5FMODELO4155F347PR }
     * 
     */
    public TransactionHeaderGdfJE5FES5FMODELO4155F347PR createTransactionHeaderGdfJE5FES5FMODELO4155F347PR() {
        return new TransactionHeaderGdfJE5FES5FMODELO4155F347PR();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdf }
     * 
     */
    public TransactionHeaderGdf createTransactionHeaderGdf() {
        return new TransactionHeaderGdf();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES }
     * 
     */
    public TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES() {
        return new TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES();
    }

    /**
     * Create an instance of {@link Jlxartransactions }
     * 
     */
    public Jlxartransactions createJlxartransactions() {
        return new Jlxartransactions();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJAxKRReceivablesInformation }
     * 
     */
    public TransactionHeaderGdfJAxKRReceivablesInformation createTransactionHeaderGdfJAxKRReceivablesInformation() {
        return new TransactionHeaderGdfJAxKRReceivablesInformation();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FES5FMODELO4155F347 }
     * 
     */
    public TransactionHeaderGdfJE5FES5FMODELO4155F347 createTransactionHeaderGdfJE5FES5FMODELO4155F347() {
        return new TransactionHeaderGdfJE5FES5FMODELO4155F347();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FES5FMODELO347PR }
     * 
     */
    public TransactionHeaderGdfJE5FES5FMODELO347PR createTransactionHeaderGdfJE5FES5FMODELO347PR() {
        return new TransactionHeaderGdfJE5FES5FMODELO347PR();
    }

    /**
     * Create an instance of {@link Jlxcltransactions }
     * 
     */
    public Jlxcltransactions createJlxcltransactions() {
        return new Jlxcltransactions();
    }

    /**
     * Create an instance of {@link JLxMXReceivablesInformation }
     * 
     */
    public JLxMXReceivablesInformation createJLxMXReceivablesInformation() {
        return new JLxMXReceivablesInformation();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FES5FMODELO347 }
     * 
     */
    public TransactionHeaderGdfJE5FES5FMODELO347 createTransactionHeaderGdfJE5FES5FMODELO347() {
        return new TransactionHeaderGdfJE5FES5FMODELO347();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional }
     * 
     */
    public TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional() {
        return new TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional();
    }

    /**
     * Create an instance of {@link TransactionHeaderGdfJE5FES5FMODELO349 }
     * 
     */
    public TransactionHeaderGdfJE5FES5FMODELO349 createTransactionHeaderGdfJE5FES5FMODELO349() {
        return new TransactionHeaderGdfJE5FES5FMODELO349();
    }

    /**
     * Create an instance of {@link Jlxcotransactions }
     * 
     */
    public Jlxcotransactions createJlxcotransactions() {
        return new Jlxcotransactions();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FES5FMODELO4155F347PR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FES_5FMODELO415_5F347PR")
    public JAXBElement<TransactionHeaderGdfJE5FES5FMODELO4155F347PR> createTransactionHeaderGdfJE5FES5FMODELO4155F347PR(TransactionHeaderGdfJE5FES5FMODELO4155F347PR value) {
        return new JAXBElement<TransactionHeaderGdfJE5FES5FMODELO4155F347PR>(_TransactionHeaderGdfJE5FES5FMODELO4155F347PR_QNAME, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJL_5FBR_5FARXTWMAI_5FAdditional")
    public JAXBElement<TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional(TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional value) {
        return new JAXBElement<TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional_QNAME, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FES5FMODELO347PR }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FES_5FMODELO347PR")
    public JAXBElement<TransactionHeaderGdfJE5FES5FMODELO347PR> createTransactionHeaderGdfJE5FES5FMODELO347PR(TransactionHeaderGdfJE5FES5FMODELO347PR value) {
        return new JAXBElement<TransactionHeaderGdfJE5FES5FMODELO347PR>(_TransactionHeaderGdfJE5FES5FMODELO347PR_QNAME, TransactionHeaderGdfJE5FES5FMODELO347PR.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FES5FMODELO347 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FES_5FMODELO347")
    public JAXBElement<TransactionHeaderGdfJE5FES5FMODELO347> createTransactionHeaderGdfJE5FES5FMODELO347(TransactionHeaderGdfJE5FES5FMODELO347 value) {
        return new JAXBElement<TransactionHeaderGdfJE5FES5FMODELO347>(_TransactionHeaderGdfJE5FES5FMODELO347_QNAME, TransactionHeaderGdfJE5FES5FMODELO347 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FES5FMODELO349 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FES_5FMODELO349")
    public JAXBElement<TransactionHeaderGdfJE5FES5FMODELO349> createTransactionHeaderGdfJE5FES5FMODELO349(TransactionHeaderGdfJE5FES5FMODELO349 value) {
        return new JAXBElement<TransactionHeaderGdfJE5FES5FMODELO349>(_TransactionHeaderGdfJE5FES5FMODELO349_QNAME, TransactionHeaderGdfJE5FES5FMODELO349 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJAxKRReceivablesInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJAxKRReceivablesInformation")
    public JAXBElement<TransactionHeaderGdfJAxKRReceivablesInformation> createTransactionHeaderGdfJAxKRReceivablesInformation(TransactionHeaderGdfJAxKRReceivablesInformation value) {
        return new JAXBElement<TransactionHeaderGdfJAxKRReceivablesInformation>(_TransactionHeaderGdfJAxKRReceivablesInformation_QNAME, TransactionHeaderGdfJAxKRReceivablesInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdf")
    public JAXBElement<TransactionHeaderGdf> createTransactionHeaderGdf(TransactionHeaderGdf value) {
        return new JAXBElement<TransactionHeaderGdf>(_TransactionHeaderGdf_QNAME, TransactionHeaderGdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FES5FMODELO340 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FES_5FMODELO340")
    public JAXBElement<TransactionHeaderGdfJE5FES5FMODELO340> createTransactionHeaderGdfJE5FES5FMODELO340(TransactionHeaderGdfJE5FES5FMODELO340 value) {
        return new JAXBElement<TransactionHeaderGdfJE5FES5FMODELO340>(_TransactionHeaderGdfJE5FES5FMODELO340_QNAME, TransactionHeaderGdfJE5FES5FMODELO340 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Jaxtwtransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "jaxtwtransactions")
    public JAXBElement<Jaxtwtransactions> createJaxtwtransactions(Jaxtwtransactions value) {
        return new JAXBElement<Jaxtwtransactions>(_Jaxtwtransactions_QNAME, Jaxtwtransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FIT_5FESL_5FOF_5FSERVICES")
    public JAXBElement<TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES(TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES value) {
        return new JAXBElement<TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES_QNAME, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FDE5FZ45FREPORTING }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FDE_5FZ4_5FREPORTING")
    public JAXBElement<TransactionHeaderGdfJE5FDE5FZ45FREPORTING> createTransactionHeaderGdfJE5FDE5FZ45FREPORTING(TransactionHeaderGdfJE5FDE5FZ45FREPORTING value) {
        return new JAXBElement<TransactionHeaderGdfJE5FDE5FZ45FREPORTING>(_TransactionHeaderGdfJE5FDE5FZ45FREPORTING_QNAME, TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Jlxcotransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "jlxcotransactions")
    public JAXBElement<Jlxcotransactions> createJlxcotransactions(Jlxcotransactions value) {
        return new JAXBElement<Jlxcotransactions>(_Jlxcotransactions_QNAME, Jlxcotransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderGdfJE5FES5FMODELO4155F347 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "transactionHeaderGdfJE_5FES_5FMODELO415_5F347")
    public JAXBElement<TransactionHeaderGdfJE5FES5FMODELO4155F347> createTransactionHeaderGdfJE5FES5FMODELO4155F347(TransactionHeaderGdfJE5FES5FMODELO4155F347 value) {
        return new JAXBElement<TransactionHeaderGdfJE5FES5FMODELO4155F347>(_TransactionHeaderGdfJE5FES5FMODELO4155F347_QNAME, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JLxMXReceivablesInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "jLxMXReceivablesInformation")
    public JAXBElement<JLxMXReceivablesInformation> createJLxMXReceivablesInformation(JLxMXReceivablesInformation value) {
        return new JAXBElement<JLxMXReceivablesInformation>(_JLxMXReceivablesInformation_QNAME, JLxMXReceivablesInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Jlxcltransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "jlxcltransactions")
    public JAXBElement<Jlxcltransactions> createJlxcltransactions(Jlxcltransactions value) {
        return new JAXBElement<Jlxcltransactions>(_Jlxcltransactions_QNAME, Jlxcltransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Jlxartransactions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "jlxartransactions")
    public JAXBElement<Jlxartransactions> createJlxartransactions(Jlxartransactions value) {
        return new JAXBElement<Jlxartransactions>(_Jlxartransactions_QNAME, Jlxartransactions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JExESOnlineVatReporting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "jExESOnlineVatReporting")
    public JAXBElement<JExESOnlineVatReporting> createJExESOnlineVatReporting(JExESOnlineVatReporting value) {
        return new JAXBElement<JExESOnlineVatReporting>(_JExESOnlineVatReporting_QNAME, JExESOnlineVatReporting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Reporting__Payment__Method_Display", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethodDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethodDisplay_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Reporting__Payment__Country_Display", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountryDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountryDisplay_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Service__Mode_Display", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "ReportingExclusionIndicator_Display", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Reporting__Payment__Country", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountry(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentCountry_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "ReportingExclusionIndicator", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Service__Code_Display", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "EDeclarationId", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESEDeclarationId(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESEDeclarationId_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "ErrorCode", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESErrorCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESErrorCode_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "vatNotExposed_Display", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposedDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposedDisplay_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Service__Code", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Reporting__Payment__Method", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethod(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESReportingPaymentMethod_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "vatNotExposed", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposed(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESVatNotExposed_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Service__Mode", scope = TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceMode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME, String.class, TransactionHeaderGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "CFDCBBSerialNumber", scope = JLxMXReceivablesInformation.class)
    public JAXBElement<String> createJLxMXReceivablesInformationCFDCBBSerialNumber(String value) {
        return new JAXBElement<String>(_JLxMXReceivablesInformationCFDCBBSerialNumber_QNAME, String.class, JLxMXReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "CFDCBBInvoiceNumber", scope = JLxMXReceivablesInformation.class)
    public JAXBElement<String> createJLxMXReceivablesInformationCFDCBBInvoiceNumber(String value) {
        return new JAXBElement<String>(_JLxMXReceivablesInformationCFDCBBInvoiceNumber_QNAME, String.class, JLxMXReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "CFDIUniqueIdentifier", scope = JLxMXReceivablesInformation.class)
    public JAXBElement<String> createJLxMXReceivablesInformationCFDIUniqueIdentifier(String value) {
        return new JAXBElement<String>(_JLxMXReceivablesInformationCFDIUniqueIdentifier_QNAME, String.class, JLxMXReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "originalTransactionTypeId", scope = Jlxcotransactions.class)
    public JAXBElement<String> createJlxcotransactionsOriginalTransactionTypeId(String value) {
        return new JAXBElement<String>(_JlxcotransactionsOriginalTransactionTypeId_QNAME, String.class, Jlxcotransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "copyStatus", scope = Jlxcotransactions.class)
    public JAXBElement<String> createJlxcotransactionsCopyStatus(String value) {
        return new JAXBElement<String>(_JlxcotransactionsCopyStatus_QNAME, String.class, Jlxcotransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "copyStatus_Display", scope = Jlxcotransactions.class)
    public JAXBElement<String> createJlxcotransactionsCopyStatusDisplay(String value) {
        return new JAXBElement<String>(_JlxcotransactionsCopyStatusDisplay_QNAME, String.class, Jlxcotransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "caiDueDate", scope = Jlxartransactions.class)
    public JAXBElement<XMLGregorianCalendar> createJlxartransactionsCaiDueDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JlxartransactionsCaiDueDate_QNAME, XMLGregorianCalendar.class, Jlxartransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "caiNumber", scope = Jlxartransactions.class)
    public JAXBElement<BigDecimal> createJlxartransactionsCaiNumber(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JlxartransactionsCaiNumber_QNAME, BigDecimal.class, Jlxartransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "numericBarCode", scope = Jlxartransactions.class)
    public JAXBElement<String> createJlxartransactionsNumericBarCode(String value) {
        return new JAXBElement<String>(_JlxartransactionsNumericBarCode_QNAME, String.class, Jlxartransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "originalTransactionTypeId", scope = Jlxartransactions.class)
    public JAXBElement<String> createJlxartransactionsOriginalTransactionTypeId(String value) {
        return new JAXBElement<String>(_JlxcotransactionsOriginalTransactionTypeId_QNAME, String.class, Jlxartransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "copyStatus", scope = Jlxartransactions.class)
    public JAXBElement<String> createJlxartransactionsCopyStatus(String value) {
        return new JAXBElement<String>(_JlxcotransactionsCopyStatus_QNAME, String.class, Jlxartransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "copyStatus_Display", scope = Jlxartransactions.class)
    public JAXBElement<String> createJlxartransactionsCopyStatusDisplay(String value) {
        return new JAXBElement<String>(_JlxcotransactionsCopyStatusDisplay_QNAME, String.class, Jlxartransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Deadline", scope = TransactionHeaderGdfJE5FES5FMODELO340 .class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJE5FES5FMODELO340TransactionDeadline(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDeadline_QNAME, BigDecimal.class, TransactionHeaderGdfJE5FES5FMODELO340 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionHeaderGdfJE5FES5FMODELO340 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO340 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Date", scope = TransactionHeaderGdfJE5FES5FMODELO340 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO340TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO340 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "originalTransactionTypeId", scope = Jlxcltransactions.class)
    public JAXBElement<String> createJlxcltransactionsOriginalTransactionTypeId(String value) {
        return new JAXBElement<String>(_JlxcotransactionsOriginalTransactionTypeId_QNAME, String.class, Jlxcltransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "copyStatus", scope = Jlxcltransactions.class)
    public JAXBElement<String> createJlxcltransactionsCopyStatus(String value) {
        return new JAXBElement<String>(_JlxcotransactionsCopyStatus_QNAME, String.class, Jlxcltransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "copyStatus_Display", scope = Jlxcltransactions.class)
    public JAXBElement<String> createJlxcltransactionsCopyStatusDisplay(String value) {
        return new JAXBElement<String>(_JlxcotransactionsCopyStatusDisplay_QNAME, String.class, Jlxcltransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "wineAndCigarette", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsWineAndCigarette(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsWineAndCigarette_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportType_Display", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportTypeDisplay(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportTypeDisplay_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportName_Display", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportNameDisplay(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportNameDisplay_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportName", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportName(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportName_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "legacyUniformInvoice", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsLegacyUniformInvoice(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsLegacyUniformInvoice_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportMethod", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportMethod(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportMethod_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "originalTransactionNumber", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsOriginalTransactionNumber(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsOriginalTransactionNumber_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportMethod_Display", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportMethodDisplay(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportMethodDisplay_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "deductibleType", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsDeductibleType(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsDeductibleType_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportType", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportType(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportType_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "wineAndCigarette_Display", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsWineAndCigaretteDisplay(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsWineAndCigaretteDisplay_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportCertificateNumber", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsExportCertificateNumber(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsExportCertificateNumber_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "deductibleType_Display", scope = Jaxtwtransactions.class)
    public JAXBElement<String> createJaxtwtransactionsDeductibleTypeDisplay(String value) {
        return new JAXBElement<String>(_JaxtwtransactionsDeductibleTypeDisplay_QNAME, String.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "exportDate", scope = Jaxtwtransactions.class)
    public JAXBElement<XMLGregorianCalendar> createJaxtwtransactionsExportDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JaxtwtransactionsExportDate_QNAME, XMLGregorianCalendar.class, Jaxtwtransactions.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "__FLEX_Context_DisplayValue", scope = TransactionHeaderGdf.class)
    public JAXBElement<String> createTransactionHeaderGdfFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfFLEXContextDisplayValue_QNAME, String.class, TransactionHeaderGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_FLEX_NumOfSegments", scope = TransactionHeaderGdf.class)
    public JAXBElement<Integer> createTransactionHeaderGdfFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionHeaderGdfFLEXNumOfSegments_QNAME, Integer.class, TransactionHeaderGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "FLEX_PARAM_GLOBAL_COUNTRY_CODE", scope = TransactionHeaderGdf.class)
    public JAXBElement<String> createTransactionHeaderGdfFLEXPARAMGLOBALCOUNTRYCODE(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME, String.class, TransactionHeaderGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "__FLEX_Context", scope = TransactionHeaderGdf.class)
    public JAXBElement<String> createTransactionHeaderGdfFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfFLEXContext_QNAME, String.class, TransactionHeaderGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_SentByElectronicMedia_Display", scope = TransactionHeaderGdfJAxKRReceivablesInformation.class)
    public JAXBElement<String> createTransactionHeaderGdfJAxKRReceivablesInformationSentByElectronicMediaDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJAxKRReceivablesInformationSentByElectronicMediaDisplay_QNAME, String.class, TransactionHeaderGdfJAxKRReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_SentByElectronicMedia", scope = TransactionHeaderGdfJAxKRReceivablesInformation.class)
    public JAXBElement<String> createTransactionHeaderGdfJAxKRReceivablesInformationSentByElectronicMedia(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJAxKRReceivablesInformationSentByElectronicMedia_QNAME, String.class, TransactionHeaderGdfJAxKRReceivablesInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateLastUpdated", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageDescription", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageCode", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfProperty_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347YearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Date", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO4155F347TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Correction__Period", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349CorrectionPeriod(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO349CorrectionPeriod_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Correction__Year", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349CorrectionYear(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO349CorrectionYear_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateLastUpdated", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO349DateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageDescription", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349MessageDescription(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageCode", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349MessageCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349TaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349TaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Deadline", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJE5FES5FMODELO349TransactionDeadline(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDeadline_QNAME, BigDecimal.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Date", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO349TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349TransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO349 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO349TransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO349 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Property__Location", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocation(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocation_QNAME, BigDecimal.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageDescription", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRMessageDescription(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateLastUpdated", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO347PRDateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRTransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRTransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfProperty_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Date", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO347PRTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRYearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Property__Location_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocationDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocationDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRTaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageCode", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRMessageCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRTaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRTransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347PRTransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "FLEX_PARAM_InvoiceDate", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingFLEXPARAMInvoiceDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JExESOnlineVatReportingFLEXPARAMInvoiceDate_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactiontransactionDeadline", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingTransactiontransactionDeadline(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingTransactiontransactionDeadline_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "FLEX_PARAM_BillToSiteUseId", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingFLEXPARAMBillToSiteUseId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingFLEXPARAMBillToSiteUseId_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "PropertyLocation_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingPropertyLocationDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingPropertyLocationDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "RegisterType_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingRegisterTypeDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingRegisterTypeDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateLastUpdated", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingDateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "YearOfAmountReceivedInCash", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingYearOfAmountReceivedInCash(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingYearOfAmountReceivedInCash_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "ThirdPartyInvoice", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingThirdPartyInvoice(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingThirdPartyInvoice_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "IntraEUDeclaredKey", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUDeclaredKey(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUDeclaredKey_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "IntraEUSubtype", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUSubtype(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUSubtype_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "CorrectionYear", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingCorrectionYear(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingCorrectionYear_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DocumentTypeOverride", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingDocumentTypeOverride(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingDocumentTypeOverride_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "IntraEUDeclaredKey_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUDeclaredKeyDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUDeclaredKeyDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "SpecialRegime_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingSpecialRegimeDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingSpecialRegimeDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "IntraEUSubtype_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingIntraEUSubtypeDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingIntraEUSubtypeDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "FLEX_PARAM_CustomerTrxId", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingFLEXPARAMCustomerTrxId(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingFLEXPARAMCustomerTrxId_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DocumentTypeOverride_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingDocumentTypeOverrideDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingDocumentTypeOverrideDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "PropertyLocation", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingPropertyLocation(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingPropertyLocation_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageDescription", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingMessageDescription(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "RegisterType", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingRegisterType(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingRegisterType_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionDate", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JExESOnlineVatReportingTransactionDate_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "LastDocumentNumberOfSummaryInv", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingLastDocumentNumberOfSummaryInv(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingLastDocumentNumberOfSummaryInv_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "SpecialRegime", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingSpecialRegime(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingSpecialRegime_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "CorrectionPeriod", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingCorrectionPeriod(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingCorrectionPeriod_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransmissionOfPropertySubjectT_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransmissionOfPropertySubjectTDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingTransmissionOfPropertySubjectTDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "OriginalInvoiceNumber", scope = JExESOnlineVatReporting.class)
    public JAXBElement<BigDecimal> createJExESOnlineVatReportingOriginalInvoiceNumber(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_JExESOnlineVatReportingOriginalInvoiceNumber_QNAME, BigDecimal.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransmissionOfPropertySubjectT", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransmissionOfPropertySubjectT(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingTransmissionOfPropertySubjectT_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "ThirdPartyInvoice_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingThirdPartyInvoiceDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingThirdPartyInvoiceDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageCode", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingMessageCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateTransactionPerformed", scope = JExESOnlineVatReporting.class)
    public JAXBElement<XMLGregorianCalendar> createJExESOnlineVatReportingDateTransactionPerformed(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_JExESOnlineVatReportingDateTransactionPerformed_QNAME, XMLGregorianCalendar.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingTransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "OriginalInvoiceNumber_Display", scope = JExESOnlineVatReporting.class)
    public JAXBElement<String> createJExESOnlineVatReportingOriginalInvoiceNumberDisplay(String value) {
        return new JAXBElement<String>(_JExESOnlineVatReportingOriginalInvoiceNumberDisplay_QNAME, String.class, JExESOnlineVatReporting.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Property__Location", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRPropertyLocation(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocation_QNAME, BigDecimal.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageDescription", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRMessageDescription(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateLastUpdated", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRDateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfProperty_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Date", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRYearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Property__Location_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRPropertyLocationDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO347PRPropertyLocationDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageCode", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRMessageCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO4155F347PRTransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO4155F347PR.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Type", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<String> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestType(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestType_QNAME, String.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Grace__Days", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestGraceDays(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestGraceDays_QNAME, BigDecimal.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Penalty__Rate___2F__Amount", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyRate2FAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyRate2FAmount_QNAME, BigDecimal.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Period", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPeriod(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPeriod_QNAME, BigDecimal.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Penalty__Type", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<String> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyType(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestPenaltyType_QNAME, String.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Formula", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<String> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestFormula(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestFormula_QNAME, String.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Interest__Rate___2F__Amount", scope = TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class)
    public JAXBElement<BigDecimal> createTransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestRate2FAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditionalInterestRate2FAmount_QNAME, BigDecimal.class, TransactionHeaderGdfJL5FBR5FARXTWMAI5FAdditional.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "DateLastUpdated", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO347DateLastUpdated(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO4155F347DateLastUpdated_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageDescription", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347MessageDescription(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageDescription_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "MessageCode", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347MessageCode(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347MessageCode_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347TaxAuthorityStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347TransmissionOfPropertyDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfPropertyDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TaxAuthorityStatus", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347TaxAuthorityStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TaxAuthorityStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transmission__of__Property", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347TransmissionOfProperty(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransmissionOfProperty_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Year__of__Amount__Received__in__Cas", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347YearOfAmountReceivedInCas(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO340YearOfAmountReceivedInCas_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Transaction__Date", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<XMLGregorianCalendar> createTransactionHeaderGdfJE5FES5FMODELO347TransactionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TransactionHeaderGdfJE5FES5FMODELO340TransactionDate_QNAME, XMLGregorianCalendar.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347TransactionStatus(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatus_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "TransactionStatus_Display", scope = TransactionHeaderGdfJE5FES5FMODELO347 .class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FES5FMODELO347TransactionStatusDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FES5FMODELO4155F347TransactionStatusDisplay_QNAME, String.class, TransactionHeaderGdfJE5FES5FMODELO347 .class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Record__Type_Display", scope = TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FDE5FZ45FREPORTINGRecordTypeDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FDE5FZ45FREPORTINGRecordTypeDisplay_QNAME, String.class, TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Record__Type", scope = TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FDE5FZ45FREPORTINGRecordType(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FDE5FZ45FREPORTINGRecordType_QNAME, String.class, TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Reason", scope = TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FDE5FZ45FREPORTINGReason(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FDE5FZ45FREPORTINGReason_QNAME, String.class, TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", name = "_Reason_Display", scope = TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class)
    public JAXBElement<String> createTransactionHeaderGdfJE5FDE5FZ45FREPORTINGReasonDisplay(String value) {
        return new JAXBElement<String>(_TransactionHeaderGdfJE5FDE5FZ45FREPORTINGReasonDisplay_QNAME, String.class, TransactionHeaderGdfJE5FDE5FZ45FREPORTING.class, value);
    }

}
