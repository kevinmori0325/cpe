
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionlinedff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PeruPercepcion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PeruPercepcion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/}TransactionLineFLEX">
 *       &lt;sequence>
 *         &lt;element name="locPeArPercTrxId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArPercSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArPercCorr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArPercTrxNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArPercCashId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PeruPercepcion", propOrder = {
    "locPeArPercTrxId",
    "locPeArPercSerie",
    "locPeArPercCorr",
    "locPeArPercTrxNum",
    "locPeArPercCashId"
})
public class PeruPercepcion
    extends TransactionLineFLEX
{

    @XmlElementRef(name = "locPeArPercTrxId", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArPercTrxId;
    @XmlElementRef(name = "locPeArPercSerie", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArPercSerie;
    @XmlElementRef(name = "locPeArPercCorr", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArPercCorr;
    @XmlElementRef(name = "locPeArPercTrxNum", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArPercTrxNum;
    @XmlElementRef(name = "locPeArPercCashId", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArPercCashId;

    /**
     * Gets the value of the locPeArPercTrxId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArPercTrxId() {
        return locPeArPercTrxId;
    }

    /**
     * Sets the value of the locPeArPercTrxId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArPercTrxId(JAXBElement<String> value) {
        this.locPeArPercTrxId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArPercSerie property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArPercSerie() {
        return locPeArPercSerie;
    }

    /**
     * Sets the value of the locPeArPercSerie property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArPercSerie(JAXBElement<String> value) {
        this.locPeArPercSerie = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArPercCorr property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArPercCorr() {
        return locPeArPercCorr;
    }

    /**
     * Sets the value of the locPeArPercCorr property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArPercCorr(JAXBElement<String> value) {
        this.locPeArPercCorr = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArPercTrxNum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArPercTrxNum() {
        return locPeArPercTrxNum;
    }

    /**
     * Sets the value of the locPeArPercTrxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArPercTrxNum(JAXBElement<String> value) {
        this.locPeArPercTrxNum = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArPercCashId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArPercCashId() {
        return locPeArPercCashId;
    }

    /**
     * Sets the value of the locPeArPercCashId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArPercCashId(JAXBElement<String> value) {
        this.locPeArPercCashId = ((JAXBElement<String> ) value);
    }

}
