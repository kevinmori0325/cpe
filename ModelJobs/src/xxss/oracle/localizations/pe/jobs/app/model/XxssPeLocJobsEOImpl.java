package xxss.oracle.localizations.pe.jobs.app.model;

import oracle.jbo.AttributeList;
import oracle.jbo.Key;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.SequenceImpl;
import oracle.jbo.server.TransactionEvent;

import xxss.oracle.localizations.pe.app.utils.JSFUtils;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Jun 21 10:58:05 COT 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class XxssPeLocJobsEOImpl extends EntityImpl {
    private static EntityDefImpl mDefinitionObject;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        JobId {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getJobId();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setJobId((Number)value);
            }
        }
        ,
        JobName {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getJobName();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setJobName((String)value);
            }
        }
        ,
        JobDescription {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getJobDescription();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setJobDescription((String)value);
            }
        }
        ,
        CronExpression {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getCronExpression();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setCronExpression((String)value);
            }
        }
        ,
        JobClassName {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getJobClassName();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setJobClassName((String)value);
            }
        }
        ,
        CreatedBy {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setCreatedBy((String)value);
            }
        }
        ,
        CreationDate {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getCreationDate();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setCreationDate((Date)value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setLastUpdatedBy((String)value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(XxssPeLocJobsEOImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(XxssPeLocJobsEOImpl obj, Object value) {
                obj.setLastUpdateDate((Date)value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(XxssPeLocJobsEOImpl object);

        public abstract void put(XxssPeLocJobsEOImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int JOBID = AttributesEnum.JobId.index();
    public static final int JOBNAME = AttributesEnum.JobName.index();
    public static final int JOBDESCRIPTION = AttributesEnum.JobDescription.index();
    public static final int CRONEXPRESSION = AttributesEnum.CronExpression.index();
    public static final int JOBCLASSNAME = AttributesEnum.JobClassName.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();

    /**
     * This is the default constructor (do not remove).
     */
    public XxssPeLocJobsEOImpl() {
    }


    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        if (mDefinitionObject == null) {
            mDefinitionObject = EntityDefImpl.findDefObject("xxss.oracle.localizations.pe.jobs.app.model.XxssPeLocJobsEO");
        }
        return mDefinitionObject;
    }

    /**
     * Gets the attribute value for JobId, using the alias name JobId.
     * @return the JobId
     */
    public Number getJobId() {
        return (Number)getAttributeInternal(JOBID);
    }

    /**
     * Sets <code>value</code> as the attribute value for JobId.
     * @param value value to set the JobId
     */
    public void setJobId(Number value) {
        setAttributeInternal(JOBID, value);
    }

    /**
     * Gets the attribute value for JobName, using the alias name JobName.
     * @return the JobName
     */
    public String getJobName() {
        return (String)getAttributeInternal(JOBNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for JobName.
     * @param value value to set the JobName
     */
    public void setJobName(String value) {
        setAttributeInternal(JOBNAME, value);
    }

    /**
     * Gets the attribute value for JobDescription, using the alias name JobDescription.
     * @return the JobDescription
     */
    public String getJobDescription() {
        return (String)getAttributeInternal(JOBDESCRIPTION);
    }

    /**
     * Sets <code>value</code> as the attribute value for JobDescription.
     * @param value value to set the JobDescription
     */
    public void setJobDescription(String value) {
        setAttributeInternal(JOBDESCRIPTION, value);
    }

    /**
     * Gets the attribute value for CronExpression, using the alias name CronExpression.
     * @return the CronExpression
     */
    public String getCronExpression() {
        return (String)getAttributeInternal(CRONEXPRESSION);
    }

    /**
     * Sets <code>value</code> as the attribute value for CronExpression.
     * @param value value to set the CronExpression
     */
    public void setCronExpression(String value) {
        setAttributeInternal(CRONEXPRESSION, value);
    }

    /**
     * Gets the attribute value for JobClassName, using the alias name JobClassName.
     * @return the JobClassName
     */
    public String getJobClassName() {
        return (String)getAttributeInternal(JOBCLASSNAME);
    }

    /**
     * Sets <code>value</code> as the attribute value for JobClassName.
     * @param value value to set the JobClassName
     */
    public void setJobClassName(String value) {
        setAttributeInternal(JOBCLASSNAME, value);
    }

    /**
     * Gets the attribute value for CreatedBy, using the alias name CreatedBy.
     * @return the CreatedBy
     */
    public String getCreatedBy() {
        return (String)getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as the attribute value for CreatedBy.
     * @param value value to set the CreatedBy
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CreationDate, using the alias name CreationDate.
     * @return the CreationDate
     */
    public Date getCreationDate() {
        return (Date)getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for CreationDate.
     * @param value value to set the CreationDate
     */
    public void setCreationDate(Date value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for LastUpdatedBy, using the alias name LastUpdatedBy.
     * @return the LastUpdatedBy
     */
    public String getLastUpdatedBy() {
        return (String)getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as the attribute value for LastUpdatedBy.
     * @param value value to set the LastUpdatedBy
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LastUpdateDate, using the alias name LastUpdateDate.
     * @return the LastUpdateDate
     */
    public Date getLastUpdateDate() {
        return (Date)getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as the attribute value for LastUpdateDate.
     * @param value value to set the LastUpdateDate
     */
    public void setLastUpdateDate(Date value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index,
                                           AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value,
                                         AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }


    /**
     * @param jobId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number jobId) {
        return new Key(new Object[]{jobId});
    }

    public Number getDBSequenceValue(String pSequenceName) {
        Number sequenceValue = new Number(0);
        if (pSequenceName != null && !pSequenceName.equals("")) {
            SequenceImpl dbSequence =
                new SequenceImpl(pSequenceName, this.getDBTransaction());

            sequenceValue = dbSequence.getSequenceNumber();
        }

        return sequenceValue;
    }


    /**
     * Add attribute defaulting logic in this method.
     * @param attributeList list of attribute names/values to initialize the row
     */
    protected void create(AttributeList attributeList) {
        super.create(attributeList);

        Number id = this.getDBSequenceValue("xxss_pe_loc_jobs_s");
        this.setJobId(id);

        Date fecha =
            new Date(new java.sql.Timestamp(System.currentTimeMillis()));
        this.setCreationDate(fecha);
        this.setLastUpdateDate(fecha);
        String userName =
            JSFUtils.resolveExpressionAsString("#{pageFlowScope.userName}");
        this.setCreatedBy(userName);
        this.setLastUpdatedBy(userName);
    }

    /**
     * Add entity remove logic in this method.
     */
    public void remove() {
        super.remove();
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        Date fecha = new Date(new java.sql.Timestamp(System.currentTimeMillis()));
        String userName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.userName}");

        if (operation == EntityImpl.DML_INSERT) {
            this.setCreationDate(fecha);
            this.setLastUpdateDate(fecha);
        } else if (operation == EntityImpl.DML_UPDATE) {
            this.setLastUpdateDate(fecha);
            
            this.setLastUpdatedBy(userName);
        }

        super.doDML(operation, e);
    }
}
