package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class CreateGuiaRemisionBean {
    private RichPopup createPopup;

    public CreateGuiaRemisionBean() {
    }
    
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    
    public void createSerieAction(ActionEvent actionEvent) {
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }
    
    public void okCreatePopup(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        popup.hide();
    }
    
    public void cancelCreatePopup(ActionEvent actionEvent) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        popup.hide();
    }

    public void setCreatePopup(RichPopup createPopup) {
        this.createPopup = createPopup;
    }

    public RichPopup getCreatePopup() {
        return createPopup;
    }
}
