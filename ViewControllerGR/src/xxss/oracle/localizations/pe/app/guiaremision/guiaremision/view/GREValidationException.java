package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

public class GREValidationException extends Exception {
    public GREValidationException(Throwable throwable) {
        super(throwable);
    }

    public GREValidationException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public GREValidationException(String string) {
        super(string);
    }

    public GREValidationException() {
        super();
    }
}
