package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

public class SignatureBean {
    String url;
    String area;
    String pass;
    String jcsUrl;
    
    public SignatureBean() {
        super();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass() {
        return pass;
    }

    public void setJcsUrl(String jcsUrl) {
        this.jcsUrl = jcsUrl;
    }

    public String getJcsUrl() {
        return jcsUrl;
    }
}
