package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import java.math.BigDecimal;

import java.sql.SQLException;
import java.sql.Types;

import java.util.Date;
import java.util.Iterator;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;

import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;

import org.apache.myfaces.trinidad.model.RowKeySetImpl;

import xxss.oracle.localizations.pe.app.guiaremision.model.XxncPeLocGuiaRemisionAMImpl;
import xxss.oracle.localizations.pe.app.utils.ADFUtils;

public class ManageLineaGuiaRemisionBean {
    private static Logger log = Logger.getLogger(ManageLineaGuiaRemisionBean.class);
    private RichTable tableTransactionsToGR;
    private RichTable tableGuiaRemiLinesTmp;
    private RichTable tableGuiaRemiLinesTmpVentas;
    private RichTable tableGuiaRemiLinesTmpCompras;
    private RichTable tableGuiaRemiLinesTmpTransfer;
    private RichInputText direccionDestinoBind;
    private RichInputText pedidoVentaBind;
    private RichInputText despachoBind;
    private RichInputComboboxListOfValues tipoTransaccionBind;
    private RichInputText idTransaccionBind;
    private RichInputText idCabTransaccion;
    private RichInputText ordenCompraBind;
    private RichInputText nroRecepcionBind;
    private RichInputText guiaNroBind;
    private RichSelectOneChoice guiaEstadoBind;
    private RichTable tableMaterialTrx;
    private RichInputText transferOrderBind;
    private RichInputText asnBind;

    public ManageLineaGuiaRemisionBean() {
    }
    
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setTableTransactionsToGR(RichTable tableTransactionsToGR) {
        this.tableTransactionsToGR = tableTransactionsToGR;
    }

    public RichTable getTableTransactionsToGR() {
        return tableTransactionsToGR;
    }
    
    public void setTableGuiaRemiLinesTmp(RichTable tableGuiaRemiLinesTmp) {
        this.tableGuiaRemiLinesTmp = tableGuiaRemiLinesTmp;
    }

    public RichTable getTableGuiaRemiLinesTmp() {
        return tableGuiaRemiLinesTmp;
    }
    
    public void setTableGuiaRemiLinesTmpVentas(RichTable tableGuiaRemiLinesTmpVentas) {
        this.tableGuiaRemiLinesTmpVentas = tableGuiaRemiLinesTmpVentas;
    }

    public RichTable getTableGuiaRemiLinesTmpVentas() {
        return tableGuiaRemiLinesTmpVentas;
    }

    public void setTableGuiaRemiLinesTmpCompras(RichTable tableGuiaRemiLinesTmpCompras) {
        this.tableGuiaRemiLinesTmpCompras = tableGuiaRemiLinesTmpCompras;
    }

    public RichTable getTableGuiaRemiLinesTmpCompras() {
        return tableGuiaRemiLinesTmpCompras;
    }

    public void setTableGuiaRemiLinesTmpTransfer(RichTable tableGuiaRemiLinesTmpTransfer) {
        this.tableGuiaRemiLinesTmpTransfer = tableGuiaRemiLinesTmpTransfer;
    }

    public RichTable getTableGuiaRemiLinesTmpTransfer() {
        return tableGuiaRemiLinesTmpTransfer;
    }

    public void insertTemporaryLine(ActionEvent actionEvent) {
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();        
        XxncPeLocGuiaRemisionAMImpl guiaAM = (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject vo = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
        Row row = vo.getCurrentRow();
        
        //Iterator it = tableTransactionsToGR.getSelectedRowKeys().iterator();
        Iterator it = tableMaterialTrx.getSelectedRowKeys().iterator();
        //log.info("we have " + tableTransactionsToGR.getSelectedRowKeys().size() + " rows selected");
        log.info("we have " + tableMaterialTrx.getSelectedRowKeys().size() + " rows selected");
        while (it.hasNext()) {
            Key key = (Key)((List)it.next()).get(0);
            //System.out.println(key.getKeyValues()[0]);
            callProcessInsertTmpTrx((BigDecimal)key.getKeyValues()[0], (BigDecimal)row.getAttribute("IdGuiaRemision"));
            callProcessInsertExtraDataToCab((BigDecimal)key.getKeyValues()[0], (BigDecimal)row.getAttribute("IdGuiaRemision"));
        }
        
        this.continueDetailsAction(actionEvent);
        
        refreshTableTrxToGR();
        refreshTableTempLinesGR();
        refreshTableTempLinesVentas();
        refreshTableTempLinesCompras();
        refreshTableTempLinesTransfer();
    }
    
    public void callProcessInsertExtraDataToCab(BigDecimal pTrxId, BigDecimal pIdGR){
        ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl("XxncPeLocGuiaRemisionAMDataControl");
        DBTransaction trans = am.getDBTransaction();
        try {
            String st = 
             "begin XXNC_PE_LOC_GUIA_REMISION_PKG.p_insert_extra_data_cab(pn_trx_id => ?, pn_id_guia_remision => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.createCallableStatement(st, -1);

            acs.setNUMBER(1, new Number(pTrxId));
            acs.setNUMBER(2, new Number(pIdGR));
            
            acs.executeUpdate();
            
        }catch (SQLException exsql){
            log.error("Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_insert_extra_data_cab: " + exsql.getMessage(), exsql);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_insert_extra_data_cab: " + exsql.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch (Exception e){
            log.error("Error en InsertLineaGuiaRemisionBean: " + e.getMessage(), e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, null, "Error en InsertLineaGuiaRemisionBean: " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void callProcessInsertTmpTrx(BigDecimal pTrxId, BigDecimal pIdGR){
        ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl("XxncPeLocGuiaRemisionAMDataControl");
        DBTransaction trans = am.getDBTransaction();
        try {
            String st = 
             "begin XXNC_PE_LOC_GUIA_REMISION_PKG.p_insert_temp_trx(pn_trx_id => ?, pn_id_guia_remision => ?, pv_status => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.createCallableStatement(st, -1);
            
            acs.setNUMBER(1, new Number(pTrxId));
            acs.setNUMBER(2, new Number(pIdGR));
            acs.registerOutParameter(3, Types.VARCHAR, 0, 15);
            
            acs.executeUpdate();
            
            String status = acs.getString(3);
            
            if (status.equals("ERROR")) {
              log.error("Error al insertar lineas de distinto pedido de venta o transfer order");
              FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Solo se tienen que seleccionar trx. del mismo Pedido de Venta y Despacho (Carga)");
              FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            
        }catch (SQLException exsql){
            log.error("Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_insert_temp_trx: " + exsql.getMessage(), exsql);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_insert_temp_trx: " + exsql.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch (Exception e){
            log.error("Error en InsertLineaGuiaRemisionBean: " + e.getMessage(), e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, null, "Error en InsertLineaGuiaRemisionBean: " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void deleteTemporaryLine(ActionEvent actionEvent) {
        Iterator it = tableGuiaRemiLinesTmp.getSelectedRowKeys().iterator();
        while (it.hasNext()) {
            Key key = (Key)((List)it.next()).get(0);
            //System.out.println(key.getKeyValues()[0]);
            callProcessDeleteTmpLines((BigDecimal)key.getKeyValues()[0]);
        }
        
        refreshTableTrxToGR();
        refreshTableTempLinesGR();
        refreshTableTempLinesVentas();
        refreshTableTempLinesCompras();
        refreshTableTempLinesTransfer();
    }
    
    public void callProcessDeleteTmpLines(BigDecimal pGRLineTmpId){
        ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl("XxncPeLocGuiaRemisionAMDataControl");
        DBTransaction trans = am.getDBTransaction();
        try {
            String st = 
             "begin XXNC_PE_LOC_GUIA_REMISION_PKG.p_delete_temp_lines(p_gr_line_tmp_id => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.createCallableStatement(st, -1);
            
            acs.setNUMBER(1, new Number(pGRLineTmpId));
            
            acs.executeUpdate();
            
        }catch (SQLException exsql){
            log.error("Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_delete_temp_lines: " + exsql.getMessage(), exsql);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_delete_temp_lines: " + exsql.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch (Exception e){
            log.error("Error en InsertLineaGuiaRemisionBean" + e.getMessage(), e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, null, "Error en InsertLineaGuiaRemisionBean: " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void refreshTableTrxToGR(){
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        //DCIteratorBinding tableTrxToGR = bindings.findIteratorBinding("XxncPeLocGRTransaccionesParaGrVO2Iterator");
        DCIteratorBinding tableTrxToGR = bindings.findIteratorBinding("XxncPeLocGRMaterialTrxQueryVO1Iterator");
        ViewObject voTableTrxToGR = tableTrxToGR.getViewObject();
        voTableTrxToGR.executeQuery();
        //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableTransactionsToGR());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableMaterialTrx());
    }
    
    public void refreshTableTempLinesGR(){
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        DCIteratorBinding tableGrLinesTmp =
            bindings.findIteratorBinding("XxncPeLocGRGuiaRemLineasTmpVO2Iterator");
        ViewObject voTableGrLinesTmp = tableGrLinesTmp.getViewObject();
        voTableGrLinesTmp.executeQuery();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableGuiaRemiLinesTmp());
    }
    
    public void refreshTableTempLinesVentas(){
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        DCIteratorBinding tableGrLinesTmpVentas =
            bindings.findIteratorBinding("XxncPeLocGRLinesTmpVentaVO2Iterator");
        ViewObject voTableGrLinesTmpVentas = tableGrLinesTmpVentas.getViewObject();
        voTableGrLinesTmpVentas.executeQuery();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableGuiaRemiLinesTmpVentas());
    }
    
    public void refreshTableTempLinesCompras(){
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        DCIteratorBinding tableGrLinesTmpCompras =
            bindings.findIteratorBinding("XxncPeLocGRLinesTmpCompraVO2Iterator");
        ViewObject voTableGrLinesTmpCompras = tableGrLinesTmpCompras.getViewObject();
        voTableGrLinesTmpCompras.executeQuery();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableGuiaRemiLinesTmpCompras());
    }
    public void refreshTableTempLinesTransfer(){
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        DCIteratorBinding tableGrLinesTmpTransfer =
            bindings.findIteratorBinding("XxncPeLocGRLinesTmpTransferenciaVO2Iterator");
        ViewObject voTableGrLinesTmpTransfer = tableGrLinesTmpTransfer.getViewObject();
        voTableGrLinesTmpTransfer.executeQuery();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableGuiaRemiLinesTmpTransfer());
    }


    public void returnToBusqGRPage(ActionEvent actionEvent) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
    }

    public void selectAllButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        RowKeySet rks = new RowKeySetImpl();
        //CollectionModel model = (CollectionModel)this.getTableTransactionsToGR().getValue();
        CollectionModel model = (CollectionModel)this.getTableMaterialTrx().getValue();
        
        int rowcount = model.getRowCount();
         
        for (int i = 0; i < rowcount; i++) {
            model.setRowIndex(i);
            Object key = model.getRowKey();
            rks.add(key);
        }
         
        //this.getTableTransactionsToGR().setSelectedRowKeys(rks);
        this.getTableMaterialTrx().setSelectedRowKeys(rks);
        //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableTransactionsToGR());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableMaterialTrx());
    }

    public void selectNoneButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        RowKeySet rks = new RowKeySetImpl();
        //this.getTableTransactionsToGR().setSelectedRowKeys(rks);
        this.getTableMaterialTrx().setSelectedRowKeys(rks);
        //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableTransactionsToGR());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableMaterialTrx());
    }

    public void searchTrxButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject voCab = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
        Row rowCab = voCab.getCurrentRow();
        BigDecimal idAlmacen = (BigDecimal)rowCab.getAttribute("IdAlmacen");
        
        String tipoGR = (String)rowCab.getAttribute("TipoGr");
        
        BigDecimal idReceptor = (BigDecimal)rowCab.getAttribute("IdReceptor");
        
        ViewObject vo = guiaAM.getXxncPeLocGRMaterialTrxQueryVO1();
        
        vo.setNamedWhereClauseParam("pDireccionDestino", direccionDestinoBind.getValue());
        vo.setNamedWhereClauseParam("pPedidoVenta", pedidoVentaBind.getValue());
        vo.setNamedWhereClauseParam("pTransferOrder", transferOrderBind.getValue());
        vo.setNamedWhereClauseParam("pDespacho", despachoBind.getValue());
        vo.setNamedWhereClauseParam("pOrdenCompra", ordenCompraBind.getValue());
        vo.setNamedWhereClauseParam("pNroRecepcion", nroRecepcionBind.getValue());
        vo.setNamedWhereClauseParam("pTipoTransaccion", tipoTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pIdCabTransaccion", idCabTransaccion.getValue());
        vo.setNamedWhereClauseParam("pIdTransaccion", idTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pGuiaNro", guiaNroBind.getValue());
        vo.setNamedWhereClauseParam("pGuiaEstado", guiaEstadoBind.getValue());
        vo.setNamedWhereClauseParam("pIdAlmacen", idAlmacen);
        vo.setNamedWhereClauseParam("pTipoGR", tipoGR);
        vo.setNamedWhereClauseParam("pReceptor", idReceptor);
        vo.setNamedWhereClauseParam("pAsn", asnBind.getValue());
        
        vo.executeQuery();
    }

    public void resetTrxButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject voCriteria = guiaAM.getXxncPeLocGRMaterialTrxQueryCriteriaVO1();
        ViewObject vo = guiaAM.getXxncPeLocGRMaterialTrxQueryVO1();
        
        vo.setNamedWhereClauseParam("pDireccionDestino", direccionDestinoBind.getValue());
        vo.setNamedWhereClauseParam("pPedidoVenta", pedidoVentaBind.getValue());
        vo.setNamedWhereClauseParam("pDespacho", despachoBind.getValue());
        vo.setNamedWhereClauseParam("pOrdenCompra", ordenCompraBind.getValue());
        vo.setNamedWhereClauseParam("pNroRecepcion", nroRecepcionBind.getValue());
        vo.setNamedWhereClauseParam("pTipoTransaccion", tipoTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pIdCabTransaccion", idCabTransaccion.getValue());
        vo.setNamedWhereClauseParam("pIdTransaccion", idTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pGuiaNro", guiaNroBind.getValue());
        vo.setNamedWhereClauseParam("pGuiaEstado", guiaEstadoBind.getValue());
        vo.setNamedWhereClauseParam("pIdAlmacen", new BigDecimal(-99));
        vo.executeQuery();
        
        voCriteria.executeQuery();
    }

    public void setDireccionDestinoBind(RichInputText direccionDestinoBind) {
        this.direccionDestinoBind = direccionDestinoBind;
    }

    public RichInputText getDireccionDestinoBind() {
        return direccionDestinoBind;
    }

    public void setPedidoVentaBind(RichInputText pedidoVentaBind) {
        this.pedidoVentaBind = pedidoVentaBind;
    }

    public RichInputText getPedidoVentaBind() {
        return pedidoVentaBind;
    }

    public void setDespachoBind(RichInputText despachoBind) {
        this.despachoBind = despachoBind;
    }

    public RichInputText getDespachoBind() {
        return despachoBind;
    }

    public void setTipoTransaccionBind(RichInputComboboxListOfValues tipoTransaccionBind) {
        this.tipoTransaccionBind = tipoTransaccionBind;
    }

    public RichInputComboboxListOfValues getTipoTransaccionBind() {
        return tipoTransaccionBind;
    }

    public void setIdTransaccionBind(RichInputText idTransaccionBind) {
        this.idTransaccionBind = idTransaccionBind;
    }

    public RichInputText getIdTransaccionBind() {
        return idTransaccionBind;
    }

    public void setIdCabTransaccion(RichInputText idCabTransaccion) {
        this.idCabTransaccion = idCabTransaccion;
    }

    public RichInputText getIdCabTransaccion() {
        return idCabTransaccion;
    }

    public void setOrdenCompraBind(RichInputText ordenCompraBind) {
        this.ordenCompraBind = ordenCompraBind;
    }

    public RichInputText getOrdenCompraBind() {
        return ordenCompraBind;
    }

    public void setNroRecepcionBind(RichInputText nroRecepcionBind) {
        this.nroRecepcionBind = nroRecepcionBind;
    }

    public RichInputText getNroRecepcionBind() {
        return nroRecepcionBind;
    }

    public void setGuiaNroBind(RichInputText guiaNroBind) {
        this.guiaNroBind = guiaNroBind;
    }

    public RichInputText getGuiaNroBind() {
        return guiaNroBind;
    }

    public void setGuiaEstadoBind(RichSelectOneChoice guiaEstadoBind) {
        this.guiaEstadoBind = guiaEstadoBind;
    }

    public RichSelectOneChoice getGuiaEstadoBind() {
        return guiaEstadoBind;
    }

    public void setTableMaterialTrx(RichTable tableMaterialTrx) {
        this.tableMaterialTrx = tableMaterialTrx;
    }

    public RichTable getTableMaterialTrx() {
        return tableMaterialTrx;
    }

    public void selectNoneGRLinesAction(ActionEvent actionEvent) {
        // Add event code here...
        RowKeySet rks = new RowKeySetImpl();
        //this.getTableTransactionsToGR().setSelectedRowKeys(rks);
        this.getTableGuiaRemiLinesTmp().setSelectedRowKeys(rks);
        //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableTransactionsToGR());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableGuiaRemiLinesTmp());
    }

    public void selectAllGRLinesAction(ActionEvent actionEvent) {
        // Add event code here...
        RowKeySet rks = new RowKeySetImpl();
        //CollectionModel model = (CollectionModel)this.getTableTransactionsToGR().getValue();
        CollectionModel model = (CollectionModel)this.getTableGuiaRemiLinesTmp().getValue();
        
        int rowcount = model.getRowCount();
         
        for (int i = 0; i < rowcount; i++) {
            model.setRowIndex(i);
            Object key = model.getRowKey();
            rks.add(key);
        }
         
        //this.getTableTransactionsToGR().setSelectedRowKeys(rks);
        this.getTableGuiaRemiLinesTmp().setSelectedRowKeys(rks);
        //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableTransactionsToGR());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTableGuiaRemiLinesTmp());
    }

    public void continueDetailsAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject voCab = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
        Row rowCab = voCab.getCurrentRow();
        
        ViewObject voCab2 = guiaAM.getXxncPeLocGRDefaultValuesVO1();
        voCab2.executeQuery();
        Row rowCab2 = voCab2.first();
        
        rowCab.refresh(Row.REFRESH_UNDO_CHANGES | Row.REFRESH_WITH_DB_FORGET_CHANGES);
        
        BigDecimal idCabGuia = (BigDecimal)rowCab.getAttribute("IdGuiaRemision");
        
        //Default Value of Fecha Inicio Traslado
        Date defaultValue = new Date();
        defaultValue = (Date)rowCab.getAttribute("FechaEmision");
        rowCab.setAttribute("FechaInicioTraslado", defaultValue);
        
        //Default Value of Tipo Documento Destino
        if (rowCab2 != null) {
            rowCab.setAttribute("TipoDocDestino", (String)rowCab2.getAttribute("IdAttribute4"));
        }
        
        //Default Value of Motivo Traslado
        if (rowCab.getAttribute("MotivoTraslado") == null) {

            if (rowCab2 != null) {
                    
                if ("VTA".equals(rowCab.getAttribute("TipoGr"))) {
                    rowCab.setAttribute("MotivoTraslado", "14");
                }else{
                    rowCab.setAttribute("MotivoTraslado", "04");
                }
            }
        }
        
        //Default Value of Transportista
        if (rowCab.getAttribute("TransportistaName") == null) {

            if (rowCab2 != null) {
                rowCab.setAttribute("TransportistaName", (String)rowCab2.getAttribute("Attribute3"));
            }
        }
        
        DBTransaction trans = guiaAM.getDBTransaction();
        
        //Default Value of Cantidad Envases        
        try {
            String st = 
             "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_total_envases(?,?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.createCallableStatement(st, -1);
            
            acs.setNUMBER(1, new Number(idCabGuia));
            acs.registerOutParameter(2, Types.NUMERIC);
            
            acs.executeUpdate();
            
            BigDecimal totalEnvases = acs.getBigDecimal(2);
            
            rowCab.setAttribute("NroPaquetes", totalEnvases);
            
        }catch (SQLException exsql){
            log.error("Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_total_envases: " + exsql.getMessage(), exsql);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_total_envases: " + exsql.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch (Exception e){
            log.error("Error en continueDetailsAction: " + e.getMessage(), e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, null, "Error en continueDetailsAction: " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        try {
            String st = 
             "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_total_weight(?,?,?,?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.createCallableStatement(st, -1);
            
            acs.setNUMBER(1, new Number(idCabGuia));
            acs.registerOutParameter(2, Types.NUMERIC);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 15);
            acs.registerOutParameter(4, Types.VARCHAR, 0, 15);
            
            acs.executeUpdate();
            
            BigDecimal totalQuantity = acs.getBigDecimal(2);
            //String totalUom = acs.getString(3);
            String totalIsoUom = acs.getString(4);
            
            rowCab.setAttribute("PesoBrutoTotalBienes", totalQuantity);
            rowCab.setAttribute("Uom", totalIsoUom);
            
        }catch (SQLException exsql){
            log.error("Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_total_weight: " + exsql.getMessage(), exsql);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_total_weight: " + exsql.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch (Exception e){
            log.error("Error en continueDetailsAction: " + e.getMessage(), e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, null, "Error en continueDetailsAction: " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void setTransferOrderBind(RichInputText transferOrderBind) {
        this.transferOrderBind = transferOrderBind;
    }

    public RichInputText getTransferOrderBind() {
        return transferOrderBind;
    }

    public void setAsnBind(RichInputText asnBind) {
        this.asnBind = asnBind;
    }

    public RichInputText getAsnBind() {
        return asnBind;
    }
}
