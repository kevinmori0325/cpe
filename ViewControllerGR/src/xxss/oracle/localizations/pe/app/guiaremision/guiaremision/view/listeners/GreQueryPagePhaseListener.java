package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view.listeners;

import java.sql.SQLException;
import java.sql.Types;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.controller.v2.lifecycle.PagePhaseEvent;
import oracle.adf.controller.v2.lifecycle.PagePhaseListener;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.server.ApplicationModuleImpl;

import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

																				  
																			   
import xxss.oracle.localizations.pe.app.utils.ADFUtils;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.services.fusion.userdetailsservice.proxy.UserDetailsService;
import xxss.oracle.localizations.services.fusion.userdetailsservice.types.UserDetails;
import xxss.oracle.localizations.services.fusion.userdetailsservice.types.UserDetailsResult;
import xxss.oracle.localizations.services.fusion.userdetailsservice.types.UserPersonDetails;
import xxss.oracle.localizations.services.util.UserDetailsWSUtil;

public class GreQueryPagePhaseListener implements PagePhaseListener {
    public GreQueryPagePhaseListener() {
        super();
    }

    public void afterPhase(PagePhaseEvent pagePhaseEvent) {
    }

    public void beforePhase(PagePhaseEvent pagePhaseEvent) {
        if (pagePhaseEvent.getPhaseId() == Lifecycle.PREPARE_MODEL_ID) {
            if (!AdfFacesContext.getCurrentInstance().isPostback()) {
                System.out.println(JSFUtils.resolveExpressionAsString("#{param.jwt}"));
                String jwt =
                    JSFUtils.resolveExpressionAsString("#{param.jwt}");


                ApplicationModuleImpl am =
                    (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl("XxncPeLocGuiaRemisionAMDataControl");

                try {
                    this.setGlobalParameters(am.getDBTransaction());
                    String instanceName =
                        JSFUtils.resolveExpressionAsString("#{pageFlowScope.instanceName}");
                    String dataCenter =
                        JSFUtils.resolveExpressionAsString("#{pageFlowScope.dataCenter}");
                    /*this.getUserDetails(jwt, am.getDBTransaction(),
                                        instanceName, dataCenter);*/
                } catch (Exception ex) {
                    FacesContext fctx = FacesContext.getCurrentInstance();
                    FacesMessage message = new FacesMessage(ex.getMessage());
                    fctx.addMessage(null, message);
                    ex.printStackTrace();
                    JSFUtils.setExpressionValue("#{pageFlowScope.isJwtValid}",
                                                "N");
                    return;
                }

            }
        }
    }
    
    private void getUserDetails(String pJwt, DBTransaction trans,
                                String instanceName, String dataCenter) {
        
        UserDetailsService svc = null;
        
        try {
            
            svc = UserDetailsWSUtil.createExternalReportWSSService(pJwt, instanceName, dataCenter);

            if (svc != null) {
                System.out.println("UserDetailsService created");
            } else {
                
                if (pJwt == null){
                                    System.out.println("UserDetailsService created");
                                }else{
                
                FacesContext fctx = FacesContext.getCurrentInstance();
                FacesMessage message =
                    new FacesMessage("UserDetailsService is not created.");
                fctx.addMessage(null, message);
                System.out.println("UserDetailsService is not created.");
                JSFUtils.setExpressionValue("#{pageFlowScope.isJwtValid}",
                                            "N");
                return;
            }
            }
            //UserDetailsResult result = svc.findSelfUserDetails();
            UserDetailsResult result;
            
            if (pJwt != null) {
                            result = svc.findSelfUserDetails();
                        }else {
                            result = null;
                        }
            
            if (result != null) {
                UserDetails userDet = result.getValue().get(0);
                String userName = userDet.getUsername().getValue();
                UserPersonDetails personDetails =
                    userDet.getUserPersonDetails().get(0);
                String personEmail =
                    personDetails.getEmailAddress().getValue();
                JSFUtils.setExpressionValue("#{pageFlowScope.userName}",
                                            userName);
                JSFUtils.setExpressionValue("#{pageFlowScope.personEmail}",
                                            personEmail);
                

                this.setUserDetails(userName, personEmail,
                                    trans);
            } /*else {
                JSFUtils.setExpressionValue("#{pageFlowScope.isJwtValid}",
                                            "N");
            }*/
                       
            JSFUtils.setExpressionValue("#{pageFlowScope.isJwtValid}",
                                        "Y");

        } catch (Exception ex) {
            FacesContext fctx = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(ex.getMessage());
            fctx.addMessage(null, message);
            ex.printStackTrace();
            JSFUtils.setExpressionValue("#{pageFlowScope.isJwtValid}",
                                        "N");
            return;
        }
    }

    public void setUserDetails(String pUserName, String pPersonEmail,
                               DBTransaction trans) throws SQLException {
        String st =
            "begin XXSS_PE_LOC_GLOBAL_PKG.set_fusion_user_info(pv_user_name => ?, pv_user_email => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.createCallableStatement(st, -1);

        acs.setString(1, pUserName);
        acs.setString(2, pPersonEmail);

        acs.executeUpdate();
    }

    private void setGlobalParameters(DBTransaction trans) throws SQLException {
        String st =
            "begin XXSS_PE_LOC_GLOBAL_PKG.pr_get_global_parameters(?,?,?,?,?,?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.createCallableStatement(st, -1);

        acs.registerOutParameter(1, Types.VARCHAR, 0, 100);
        acs.registerOutParameter(2, Types.VARCHAR, 0, 100);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 100);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 100);
        acs.registerOutParameter(5, Types.VARCHAR, 0, 100);
        acs.registerOutParameter(6, Types.VARCHAR, 0, 100);

        acs.executeUpdate();

        String instanceName = acs.getString(1);
        String dataCenter = acs.getString(2);
        String emailServerName = acs.getString(3);
        String emailFrom = acs.getString(4);
        String biUserName = acs.getString(5);
        String biUserPass = acs.getString(6);

        JSFUtils.setExpressionValue("#{pageFlowScope.instanceName}",
                                    instanceName);
        JSFUtils.setExpressionValue("#{pageFlowScope.dataCenter}", dataCenter);
        JSFUtils.setExpressionValue("#{pageFlowScope.emailServerName}",
                                    emailServerName);
        JSFUtils.setExpressionValue("#{pageFlowScope.emailFrom}", emailFrom);
        JSFUtils.setExpressionValue("#{pageFlowScope.biUserName}", biUserName);
        JSFUtils.setExpressionValue("#{pageFlowScope.biUserPass}", biUserPass);
    }

}