package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import java.math.BigDecimal;

public class GREBean {
    BigDecimal idGRE;
    String voidFlag;
    
    public GREBean(BigDecimal pIdGre, String pVoidFlag) {
        super();
        idGRE = pIdGre;
        voidFlag = pVoidFlag;
        
    }

    public void setIdGRE(BigDecimal idGRE) {
        this.idGRE = idGRE;
    }

    public BigDecimal getIdGRE() {
        return idGRE;
    }

    public void setVoidFlag(String voidFlag) {
        this.voidFlag = voidFlag;
    }

    public String getVoidFlag() {
        return voidFlag;
    }
}
