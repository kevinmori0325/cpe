package xxss.oracle.localizations.pe.app.guiaremision.transportistas.view;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailHeader;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.QueryEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.event.SelectionEvent;

import xxss.oracle.localizations.pe.app.utils.ADFUtil;
import xxss.oracle.localizations.pe.app.utils.ADFUtils;

public class GRTransportistasBean {
    private RichTable transpTable;
    private RichTable vehiculoTable;
    private RichTable conductorTable;
    private RichInputText idTransportista;
    private RichPopup createPopup;
    private RichShowDetailHeader detailHeaderVehiculo;
    private RichShowDetailHeader detailHeaderConductor;
    private RichPopup createConfirmationPopup;
    private RichPopup createTransportistaPopup;
    private RichPopup createConductorPopup;

    public GRTransportistasBean() {
    }
    
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setTranspTable(RichTable transpTable) {
        this.transpTable = transpTable;
    }

    public RichTable getTranspTable() {
        return transpTable;
    }

    public void setVehiculoTable(RichTable vehiculoTable) {
        this.vehiculoTable = vehiculoTable;
    }

    public RichTable getVehiculoTable() {
        return vehiculoTable;
    }

    public void setConductorTable(RichTable conductorTable) {
        this.conductorTable = conductorTable;
    }

    public RichTable getConductorTable() {
        return conductorTable;
    }
 
    public void transportistaSelectionListener(SelectionEvent selectionEvent) {
        /*ADFUtil.invokeEL("#{bindings.EmpDeptVO.collectionModel.makeCurrent}",
        new Class[] { SelectionEvent.class },
        new Object[] { selectionEvent });*/
        
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        DCIteratorBinding vehiculoIter =
            bindings.findIteratorBinding("XxncPeLocGRVehiculosVO1Iterator");
        DCIteratorBinding conductorIter =
            bindings.findIteratorBinding("XxncPeLocGRConductoresVO1Iterator");

        ViewObject voTableVehiculoData = vehiculoIter.getViewObject();
        ViewObject voTableConductorData = conductorIter.getViewObject();
        voTableVehiculoData.executeQuery();
        voTableConductorData.executeQuery();

        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTranspTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getVehiculoTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getConductorTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDetailHeaderVehiculo());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDetailHeaderConductor());
    }

    private void invokeMethodExpression(String expr, QueryEvent queryEvent) {
     FacesContext fctx = FacesContext.getCurrentInstance();
     ELContext elContext = fctx.getELContext();
     ExpressionFactory eFactory =
         fctx.getApplication().getExpressionFactory();
     MethodExpression mexpr =
         eFactory.createMethodExpression(elContext, expr, Object.class,
                                         new Class[] { QueryEvent.class });
     mexpr.invoke(elContext, new Object[] { queryEvent });
    }

    public void processQuery(QueryEvent queryEvent) {
        invokeMethodExpression("#{bindings.XxncPeLocGRTransportistasVOCriteriaQuery.processQuery}", queryEvent);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getVehiculoTable());        
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getConductorTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDetailHeaderVehiculo());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDetailHeaderConductor());
    }

    public void createTransportistaAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }
    
    public void createTranspAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert1");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreateTransportistaPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }
    
    public void createConductorAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert2");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreateConductorPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }
    
    public void okCreatePopup(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getVehiculoTable());
        /*AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSeriesTable());*/
        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Archivo Guardado Correctamente.", "");
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void cancelCreatePopup(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getCreatePopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getVehiculoTable());
    }
    
    public void openDeleteValidationPopup(ActionEvent actionEvent) {
        // Add event code here...
        /*BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }*/

        /*RichPopup popup = this.getCreateConfirmationPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);*/
        
        Object result;
        BindingContainer bindings = getBindings();
        OperationBinding operationBindingDelete =
            bindings.getOperationBinding("Delete");
        result = operationBindingDelete.execute();
        if (!operationBindingDelete.getErrors().isEmpty()) {
            return;
        }

        OperationBinding operationBindingCommit =
            bindings.getOperationBinding("Commit");
        result = operationBindingCommit.execute();
        if (!operationBindingCommit.getErrors().isEmpty()) {
            return;
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getVehiculoTable());
    }
    
    public void deleteTransportistaAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Delete");
        Object result = operationBinding.execute();
        
        OperationBinding operationBinding2 =
            bindings.getOperationBinding("Commit");
        Object result2 = operationBinding.execute();
        
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
        
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Veh�culo eliminado satisfactoriamente.");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        RichPopup popup = this.getCreateConfirmationPopup();
        popup.hide();
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getVehiculoTable());
    }
    
    public void cancelDeleteTransportistaAction(ActionEvent actionEvent) {
        RichPopup popup = this.getCreateConfirmationPopup();
        popup.hide();
    }

    public void setCreatePopup(RichPopup createPopup) {
        this.createPopup = createPopup;
    }

    public RichPopup getCreatePopup() {
        return createPopup;
    }

    public void setDetailHeaderVehiculo(RichShowDetailHeader detailHeaderVehiculo) {
        this.detailHeaderVehiculo = detailHeaderVehiculo;
    }

    public RichShowDetailHeader getDetailHeaderVehiculo() {
        return detailHeaderVehiculo;
    }

    public void setDetailHeaderConductor(RichShowDetailHeader detailHeaderConductor) {
        this.detailHeaderConductor = detailHeaderConductor;
    }

    public RichShowDetailHeader getDetailHeaderConductor() {
        return detailHeaderConductor;
    }

    public void setCreateConfirmationPopup(RichPopup createConfirmationPopup) {
        this.createConfirmationPopup = createConfirmationPopup;
    }

    public RichPopup getCreateConfirmationPopup() {
        return createConfirmationPopup;
    }

    public void setCreateTransportistaPopup(RichPopup createTransportistaPopup) {
        this.createTransportistaPopup = createTransportistaPopup;
    }

    public RichPopup getCreateTransportistaPopup() {
        return createTransportistaPopup;
    }

    public void setCreateConductorPopup(RichPopup createConductorPopup) {
        this.createConductorPopup = createConductorPopup;
    }

    public RichPopup getCreateConductorPopup() {
        return createConductorPopup;
    }

    public void okCreateTransportPopup(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreateTransportistaPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTranspTable());
    }

    public void cancelCreateTranspPopup(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getCreateTransportistaPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTranspTable());
    }

    public void okCreateConductorPopup(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreateConductorPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getConductorTable());
    }

    public void cancelCreateConductorPopup(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getCreateConductorPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getConductorTable());
    }

    public void deleteConductorButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Delete2");
        Object result = operationBinding.execute();
        
        OperationBinding operationBinding2 =
            bindings.getOperationBinding("Commit");
        Object result2 = operationBinding.execute();
        
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
        
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Conductor eliminado satisfactoriamente.");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        //RichPopup popup = this.getCreateConfirmationPopup();
        //popup.hide();
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getConductorTable());
    }

    public void deleteTransportistaButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Delete1");
        Object result = operationBinding.execute();
        
        OperationBinding operationBinding2 =
            bindings.getOperationBinding("Commit");
        Object result2 = operationBinding.execute();
        
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
        
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, null, "Transportista eliminado satisfactoriamente.");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        //RichPopup popup = this.getCreateConfirmationPopup();
        //popup.hide();
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTranspTable());
    }
}
