package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import org.apache.myfaces.trinidad.context.RequestContext;

import org.apache.myfaces.trinidad.model.RowKeySet;

import xxss.oracle.localizations.pe.app.guiaremision.etd.GREJcsHandler;
import xxss.oracle.localizations.pe.app.guiaremision.model.XxncPeLocGuiaRemisionAMImpl;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;

import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;

public class DetalleGuiaRemision {
    private RichInputText placaText;
    private RichInputText marcaText;
    private RichInputText descripUnidadText;
    private RichInputText numeroDocConductorText;
    private RichSelectOneChoice tipoDocConductorText;
    private RichInputText apellidoNombreConductorText;
    private RichInputText descripMotivoTraslado;
    private RichSelectOneChoice modalidadTrasladoCombo;
    private RichInputText licConducirConductorText;
    
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public DetalleGuiaRemision() {
    }

    public void setPlacaText(RichInputText placaText) {
        this.placaText = placaText;
    }

    public RichInputText getPlacaText() {
        return placaText;
    }

    public void setMarcaText(RichInputText marcaText) {
        this.marcaText = marcaText;
    }

    public RichInputText getMarcaText() {
        return marcaText;
    }

    public void setDescripUnidadText(RichInputText descripUnidadText) {
        this.descripUnidadText = descripUnidadText;
    }

    public RichInputText getDescripUnidadText() {
        return descripUnidadText;
    }

    public void vehiculoLovChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getMarcaText());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getPlacaText());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDescripUnidadText());
    }

    public void setNumeroDocConductorText(RichInputText numeroDocConductorText) {
        this.numeroDocConductorText = numeroDocConductorText;
    }

    public RichInputText getNumeroDocConductorText() {
        return numeroDocConductorText;
    }

    public void setTipoDocConductorText(RichSelectOneChoice tipoDocConductorText) {
        this.tipoDocConductorText = tipoDocConductorText;
    }

    public RichSelectOneChoice getTipoDocConductorText() {
        return tipoDocConductorText;
    }

    public void setApellidoNombreConductorText(RichInputText apellidoNombreConductorText) {
        this.apellidoNombreConductorText = apellidoNombreConductorText;
    }

    public RichInputText getApellidoNombreConductorText() {
        return apellidoNombreConductorText;
    }

    public void conductorLovValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getNumeroDocConductorText());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTipoDocConductorText());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getApellidoNombreConductorText());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getLicConducirConductorText());
    }

    public void motivoTrasladoLovValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getDescripMotivoTraslado());
    }

    public void setDescripMotivoTraslado(RichInputText descripMotivoTraslado) {
        this.descripMotivoTraslado = descripMotivoTraslado;
    }

    public RichInputText getDescripMotivoTraslado() {
        return descripMotivoTraslado;
    }

    public void setModalidadTrasladoCombo(RichSelectOneChoice modalidadTrasladoCombo) {
        this.modalidadTrasladoCombo = modalidadTrasladoCombo;
    }

    public RichSelectOneChoice getModalidadTrasladoCombo() {
        return modalidadTrasladoCombo;
    }

    public void transportistaValueChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getModalidadTrasladoCombo());
    }

    public void setLicConducirConductorText(RichInputText licConducirConductorText) {
        this.licConducirConductorText = licConducirConductorText;
    }

    public RichInputText getLicConducirConductorText() {
        return licConducirConductorText;
    }
    
    public void saveGRAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
        
        this.sendNewGREAction(actionEvent);
        
        XxncPeLocGuiaRemisionAMImpl guiaAM =
                    (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject vo = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
        Row row = vo.getCurrentRow();
        
        row.setAttribute("EstadoGr", "EXEC");
        
        String statusGr = (String)row.getAttribute("EstadoGr");
        
        if ("NS".equals(statusGr) || "EXEC".equals(statusGr)){
            RequestContext.getCurrentInstance().getPageFlowScope().put("currentGrStatus", statusGr);
            RequestContext.getCurrentInstance().getPageFlowScope().put("grReadOnlyFlag", "Y");
        }
        
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "La Gu�a de Remisi�n fue enviada. Regresar a la pantalla de b�squeda de Gu�as para refrescar el estado.", "");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        //Regrescar p�gina para que se bloqueen los campos en caso entre en la condici�n anterior, sino refrescar� y no har� nada.
        FacesContext fctx = FacesContext.getCurrentInstance();
        String page = fctx.getViewRoot().getViewId();
        ViewHandler ViewH = fctx.getApplication().getViewHandler();
        UIViewRoot UIV = ViewH.createView(fctx, page);
        UIV.setViewId(page);
        fctx.setViewRoot(UIV);
    }
    
    public void sendNewGREAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
        Row row = vo.getCurrentRow();
        
        BigDecimal idGRE = (BigDecimal)row.getAttribute("IdGuiaRemision");
        
        /*DCIteratorBinding grIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator grRSIter = grIter.getRowSetIterator();

        RowKeySet selectedGR = this.getGrTable().getSelectedRowKeys();
        Iterator selectedGRIter = selectedGR.iterator();*/

        List grList = new ArrayList();

        /*while (selectedGRIter.hasNext()) {
            Key key = (Key)((List)selectedGRIter.next()).get(0);
            Row currentRow = grRSIter.getRow(key);
            BigDecimal idGRE = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");*/
        
        GRQueryBean grQueryBean = new GRQueryBean();
        
        try {
            grQueryBean.validateSendGRE(idGRE, "N", guiaAM.getDBTransaction());
            grList.add(new GREBean(idGRE, "N"));
        } catch (GREValidationException gex) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, gex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
            return;
        } catch (SQLException sqlex) {
            FacesMessage msg =
                 new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
            return;
        }

        if (grList.size() > 0) {
        
           try {
               for (Object obj : grList) {
                   GREBean gre = (GREBean)obj;

                   //GREHandler greRunnable = new GREHandler();
                   GREJcsHandler greRunnable = new GREJcsHandler();
                        
                    //GREJcsHandler greRunnable = (Class<GREJcsHandler>)GRQueryBean.class.getClassLoader().loadClass("xxss.oracle.localizations.pe.app.guiaremision.etd.GREJcsHandler");

                   String instanceName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.instanceName}");
                   String dataCenter = JSFUtils.resolveExpressionAsString("#{pageFlowScope.dataCenter}");
                   String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

                   SignatureBean sign = this.getSignatureParams(guiaAM.getDBTransaction());

                   greRunnable.setJwt(jwt);
                   greRunnable.setInstanceName(instanceName);
                   greRunnable.setDataCenter(dataCenter);
                   greRunnable.setIdGre(gre.getIdGRE());
                   greRunnable.setVoidFlag(gre.getVoidFlag());
                   greRunnable.setSignatureURL(sign.getUrl());
                   greRunnable.setSignatureArea(sign.getArea());
                   greRunnable.setSignaturePass(sign.getPass());
                   greRunnable.setJcsSignatureURL(sign.getJcsUrl());
                   greRunnable.setInvTrxValueSet("LOC_PE_INV_TRX_ADD_INFO");

                   Thread t = new Thread(greRunnable);
                   t.start();
               }
                    
               FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "La Gu�a de Remisi�n fue enviada. Regresar a la pantalla de b�squeda de Gu�as para refrescar el estado.", "");
               FacesContext.getCurrentInstance().addMessage(null, msg);
               
           } catch (SQLException sqlex) {
               FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
               FacesContext.getCurrentInstance().addMessage(null, msg);
               //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
               return;
           }

        } else {
            FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar por lo menos un registro.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    private SignatureBean getSignatureParams(DBTransaction trans) throws SQLException {
        SignatureBean sign = new SignatureBean();

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_signature_params(?, ?, ?, ?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.createCallableStatement(st, -1);
        acs.registerOutParameter(1, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(2, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 150);

        acs.executeUpdate();

        String url = acs.getString(1);
        String area = acs.getString(2);
        String pass = acs.getString(3);
        String jcsUrl = acs.getString(4);

        sign.setUrl(url);
        sign.setArea(area);
        sign.setPass(pass);
        sign.setJcsUrl(jcsUrl);

        return (sign);
    }

}
