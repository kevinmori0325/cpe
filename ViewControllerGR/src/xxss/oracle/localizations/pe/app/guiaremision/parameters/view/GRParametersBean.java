package xxss.oracle.localizations.pe.app.guiaremision.parameters.view;

import javax.el.ELContext;

import javax.el.ExpressionFactory;

import javax.el.MethodExpression;

import javax.faces.context.FacesContext;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.QueryEvent;

import oracle.jbo.Row;

import oracle.jbo.ViewObject;

import org.apache.myfaces.trinidad.event.SelectionEvent;

import xxss.oracle.localizations.pe.app.utils.ADFUtil;
import xxss.oracle.localizations.pe.app.utils.ADFUtils;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;

public class GRParametersBean {
    private RichTable trxTypeTable;
    private RichTable trxTypeTable1;

    public GRParametersBean() {
    }

    public void LegalEntitySelectionListener(SelectionEvent selectionEvent) {
        ADFUtil.invokeEL("#{bindings.EmpDeptVO.collectionModel.makeCurrent}",
        new Class[] { SelectionEvent.class },
        new Object[] { selectionEvent });
        
        DCBindingContainer bindings = (DCBindingContainer)ADFUtils.getBindingContainer();
        DCIteratorBinding trxtypeIter =
            bindings.findIteratorBinding("XxncPeLocGRTipoTrxVO1Iterator");

        ViewObject voTableData = trxtypeIter.getViewObject();
        voTableData.executeQuery();

        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTrxTypeTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTrxTypeTable1());
    }

    public void setTrxTypeTable(RichTable trxTypeTable) {
        this.trxTypeTable = trxTypeTable;
    }

    public RichTable getTrxTypeTable() {
        return trxTypeTable;
    }

    public void setTrxTypeTable1(RichTable trxTypeTable1) {
        this.trxTypeTable1 = trxTypeTable1;
    }

    public RichTable getTrxTypeTable1() {
        return trxTypeTable1;
    }

    private void invokeMethodExpression(String expr, QueryEvent queryEvent) {
     FacesContext fctx = FacesContext.getCurrentInstance();
     ELContext elContext = fctx.getELContext();
     ExpressionFactory eFactory =
         fctx.getApplication().getExpressionFactory();
     MethodExpression mexpr =
         eFactory.createMethodExpression(elContext, expr, Object.class,
                                         new Class[] { QueryEvent.class });
     mexpr.invoke(elContext, new Object[] { queryEvent });
    }

    public void processQuery(QueryEvent queryEvent) {
        invokeMethodExpression("#{bindings.XxncPeLocGRParametersVOCriteriaQuery.processQuery}", queryEvent);
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTrxTypeTable());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getTrxTypeTable1());
    }
}
