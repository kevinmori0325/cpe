package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import xxss.oracle.localizations.services.signature.helper.response.ProcessResult;

public class ETDBean {
    String request;
    String response;
    ProcessResult processResult;
    
    public ETDBean() {
        super();
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setProcessResult(ProcessResult processResult) {
        this.processResult = processResult;
    }

    public ProcessResult getProcessResult() {
        return processResult;
    }
}
