package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Arrays;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;

import oracle.jbo.domain.ClobDomain;
import oracle.jbo.domain.generic.GenericClob;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

public class GRQueryViewBean {
    private static Logger log = Logger.getLogger(GRQueryViewBean.class);
    
    public GRQueryViewBean() {
    }

    public void sendRequestDownloadAction(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("SendRequestClob");
        if (attr == null) {
            log.error("Can't find SendRequestClob binding...");
            return;
        }
        
        GenericClob clob = (GenericClob)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en sendRequestDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void sendResponseDownloadAction(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("SendResponseClob");
        if (attr == null) {
            log.error("Can't find SendResponseClob binding...");
            return;
        }
        
        GenericClob clob = (GenericClob)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en sendResponseDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void statusRequestDownloadAction(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("StatusRequestClob");
        if (attr == null) {
            log.error("Can't find StatusRequestClob binding...");
            return;
        }
        
        GenericClob clob = (GenericClob)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en statusRequestDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void statusResponseDownloadAction(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("StatusResponseClob");
        if (attr == null) {
            log.error("Can't find StatusResponseClob binding...");
            return;
        }
        
        GenericClob clob = (GenericClob)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en statusResponseDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
}
