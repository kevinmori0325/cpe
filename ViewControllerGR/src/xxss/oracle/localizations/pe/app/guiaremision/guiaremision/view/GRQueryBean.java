package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import java.io.IOException;
import java.io.OutputStream;

import java.math.BigDecimal;

import java.sql.SQLException;

import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;

import oracle.jbo.domain.ClobDomain;
import oracle.jbo.domain.Number;

import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.model.RowKeySet;

import org.joda.time.format.DateTimeFormatter;

import org.quartz.Job;

import xxss.oracle.localizations.pe.app.guiaremision.model.XxncPeLocGuiaRemisionAMImpl;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.pe.app.guiaremision.etd.GREJcsHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import oracle.adfinternal.view.faces.bi.util.JsfUtils;

import oracle.jbo.server.ApplicationModuleImpl;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;

import xxss.oracle.localizations.pe.app.utils.ADFUtils;

public class GRQueryBean {
    private static Logger log = Logger.getLogger(GRQueryBean.class);
    private RichInputText idGuiaRemisionBind;
    private RichSelectOneChoice estadoBind;
    private RichInputText serieBind;
    private RichInputText nroGuiaBind;
    private RichSelectOneChoice tipoGuiaBind;
    private RichInputText idAlmacenBind;
    private RichInputText idTipoTransaccionBind;
    private RichInputText idCabTransaccionBind;
    private RichInputText idClienteBind;
    private RichInputText pedidoVentaBind;
    private RichInputText idProveedor;
    private RichInputText ordenCompraBind;
    private RichInputText nroRecepcionBind;
    private RichInputComboboxListOfValues almacenLOV;
    private RichInputComboboxListOfValues tipoTransaccionLOV;
    private RichInputComboboxListOfValues clienteLOV;
    private RichInputComboboxListOfValues proveedorLOV;
    private RichInputText idTransaccionBind;
    private RichPopup createGRPopup;
    private RichDialog createGRDialog;
    private RichTable grTable;

    public GRQueryBean() {
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void searchListener(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject vo = guiaAM.getXxncPeLocGRQueryVO1();
        
        //int idx = 0;
        //StringBuilder sb = new StringBuilder();
        //sb.append("(:pEstado is null or ESTADO_GR = :pEstado)");
        //sb.append(" and (:pIdGuiaRemision is null or ID_GUIA_REMISION = :pIdGuiaRemision)");

        //vo.setWhereClause(sb.toString());
        vo.setNamedWhereClauseParam("pEstado", estadoBind.getValue());
        vo.setNamedWhereClauseParam("pIdGuiaRemision", idGuiaRemisionBind.getValue());
        vo.setNamedWhereClauseParam("pSerie", serieBind.getValue());
        vo.setNamedWhereClauseParam("pNumeroGr", nroGuiaBind.getValue());
        vo.setNamedWhereClauseParam("pTipoGR", tipoGuiaBind.getValue());
        vo.setNamedWhereClauseParam("pIdAlmacen", idAlmacenBind.getValue());
        vo.setNamedWhereClauseParam("pIdTransaccion", idTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pTipoTransaccion", idTipoTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pIdCliente", idClienteBind.getValue());
        vo.setNamedWhereClauseParam("pIdProveedor", idProveedor.getValue());
        vo.setNamedWhereClauseParam("pPedidoVenta", pedidoVentaBind.getValue());
        vo.setNamedWhereClauseParam("pOrdenCompra", ordenCompraBind.getValue());
        vo.setNamedWhereClauseParam("pNroRecepcion", nroRecepcionBind.getValue());
        vo.setNamedWhereClauseParam("pIdCabTransaccion", idCabTransaccionBind.getValue());
        vo.executeQuery();
    }
    
    public void searchListener_test(ActionEvent actionEvent) {               
        // Add event code here...
        /*RowKeySet selectedServs = this.getGrTable().getSelectedRowKeys();
        Iterator selectedServIter = selectedServs.iterator();
        List servIdList = new ArrayList();*/

        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding servIter = bindings.findIteratorBinding("XxncPeLocGRGuiaRemisionCabVO1Iterator");
        /*RowSetIterator servRSIter = servIter.getRowSetIterator();*/

        /*if (selectedServIter.hasNext()) {
            Key key = (Key)((List)selectedServIter.next()).get(0);
            Row currentRow = servRSIter.getRow(key);
            BigDecimal idGR = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");
            
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "BANDERA1", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);

            XxncPeLocGuiaRemisionAMImpl guiaAM =
                (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
            ViewObject vo = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
            Row row = vo.getRow(new Key(new Object[] { idGR }));
            vo.setCurrentRow(row);
            String statusGr = (String)row.getAttribute("EstadoGr");
            
            if ("VO".equals(statusGr)){
               RequestContext.getCurrentInstance().getPageFlowScope().put("grDisableField", "Y");
            }
        }*/
        
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject vo2 = guiaAM.getXxncPeLocGRQueryVO1();
                    
        vo2.setNamedWhereClauseParam("pEstado", estadoBind.getValue());
        vo2.setNamedWhereClauseParam("pIdGuiaRemision", idGuiaRemisionBind.getValue());
        vo2.setNamedWhereClauseParam("pSerie", serieBind.getValue());
        vo2.setNamedWhereClauseParam("pNumeroGr", nroGuiaBind.getValue());
        vo2.setNamedWhereClauseParam("pTipoGR", tipoGuiaBind.getValue());
        vo2.setNamedWhereClauseParam("pIdAlmacen", idAlmacenBind.getValue());
        vo2.setNamedWhereClauseParam("pIdTransaccion", idTransaccionBind.getValue());
        vo2.setNamedWhereClauseParam("pTipoTransaccion", idTipoTransaccionBind.getValue());
        vo2.setNamedWhereClauseParam("pIdCliente", idClienteBind.getValue());
        vo2.setNamedWhereClauseParam("pIdProveedor", idProveedor.getValue());
        vo2.setNamedWhereClauseParam("pPedidoVenta", pedidoVentaBind.getValue());
        vo2.setNamedWhereClauseParam("pOrdenCompra", ordenCompraBind.getValue());
        vo2.setNamedWhereClauseParam("pNroRecepcion", nroRecepcionBind.getValue());
        vo2.setNamedWhereClauseParam("pIdCabTransaccion", idCabTransaccionBind.getValue());        
        vo2.executeQuery();
        
        RowKeySet rks = new RowKeySetImpl();
        
        CollectionModel model = (CollectionModel)this.getGrTable().getValue();
        
        int rowcount = model.getRowCount();
         
        for (int i = 0; i < rowcount; i++) {
            model.setRowIndex(i);
            Object key = model.getRowKey();
            rks.add(key);
        }
        
        this.getGrTable().setSelectedRowKeys(rks);
        
        
        DCIteratorBinding grIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator grRSIter = grIter.getRowSetIterator();

        RowKeySet selectedGR = this.getGrTable().getSelectedRowKeys();
        Iterator selectedGRIter = selectedGR.iterator();
        
        List grList = new ArrayList();

        while (selectedGRIter.hasNext()) {
            Key key = (Key)((List)selectedGRIter.next()).get(0);
            Row currentRow = grRSIter.getRow(key);
            BigDecimal idGRE = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");
            
            if ("VO".equals(currentRow.getAttribute("EstadoGr"))){
                RequestContext.getCurrentInstance().getPageFlowScope().put("grDisableField", "Y");
            }
        }
        
        /*RowKeySet selectedServs = this.getGrTable().getSelectedRowKeys();
        Iterator selectedServIter = selectedServs.iterator();
        List servIdList = new ArrayList();
        RowSetIterator servRSIter = servIter.getRowSetIterator();
        
        if (selectedServIter.hasNext()) {
            Key key = (Key)((List)selectedServIter.next()).get(0);
            Row currentRow = servRSIter.getRow(key);
            BigDecimal idGR = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");
            
            XxncPeLocGuiaRemisionAMImpl guiaAM2 =
                (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
            
            ViewObject vo = guiaAM2.getXxncPeLocGRGuiaRemisionCabVO1();
            Row row = vo.getRow(new Key(new Object[] { idGR }));
            vo.setCurrentRow(row);
            String statusGr = (String)row.getAttribute("EstadoGr");
                
            if ("VO".equals(statusGr)){
                RequestContext.getCurrentInstance().getPageFlowScope().put("grDisableField", "Y");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "BANDERA1", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }*/

        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
        
    }
    
    public void resetListener(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        ViewObject vo = guiaAM.getXxncPeLocGRQueryVO1();
        ViewObject voCriteria = guiaAM.getXxncPeLocGRQueryCriteriaVO1();
        //ViewObject voCriteria = guiaAM.getXxncPeLocGRQueryVO1();

        //voQuery.setWhereClause("");
        vo.setNamedWhereClauseParam("pEstado", "XXX");
        vo.setNamedWhereClauseParam("pIdGuiaRemision", idGuiaRemisionBind.getValue());
        vo.setNamedWhereClauseParam("pSerie", serieBind.getValue());
        vo.setNamedWhereClauseParam("pNumeroGr", nroGuiaBind.getValue());
        vo.setNamedWhereClauseParam("pTipoGR", tipoGuiaBind.getValue());
        vo.setNamedWhereClauseParam("pIdAlmacen", idAlmacenBind.getValue());
        vo.setNamedWhereClauseParam("pIdTransaccion", idTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pTipoTransaccion", idTipoTransaccionBind.getValue());
        vo.setNamedWhereClauseParam("pIdCliente", idClienteBind.getValue());
        vo.setNamedWhereClauseParam("pIdProveedor", idProveedor.getValue());
        vo.setNamedWhereClauseParam("pPedidoVenta", pedidoVentaBind.getValue());
        vo.setNamedWhereClauseParam("pOrdenCompra", ordenCompraBind.getValue());
        vo.setNamedWhereClauseParam("pNroRecepcion", nroRecepcionBind.getValue());
        vo.setNamedWhereClauseParam("pIdCabTransaccion", idCabTransaccionBind.getValue());
        //voQuery.executeQuery();
        voCriteria.executeQuery();
        estadoBind.resetValue();
        idGuiaRemisionBind.resetValue();
        serieBind.resetValue();
        nroGuiaBind.resetValue();
        tipoGuiaBind.resetValue();
        idAlmacenBind.resetValue();
        idTransaccionBind.resetValue();
        idTipoTransaccionBind.resetValue();
        idClienteBind.resetValue();
        idProveedor.resetValue();
        pedidoVentaBind.resetValue();
        ordenCompraBind.resetValue();
        nroRecepcionBind.resetValue();
        idCabTransaccionBind.resetValue();
        clienteLOV.resetValue();
        proveedorLOV.resetValue();
        tipoTransaccionLOV.resetValue();
    }

    public void setIdGuiaRemisionBind(RichInputText idGuiaRemisionBind) {
        this.idGuiaRemisionBind = idGuiaRemisionBind;
    }

    public RichInputText getIdGuiaRemisionBind() {
        return idGuiaRemisionBind;
    }

    public void setEstadoBind(RichSelectOneChoice estadoBind) {
        this.estadoBind = estadoBind;
    }

    public RichSelectOneChoice getEstadoBind() {
        return estadoBind;
    }

    public void setSerieBind(RichInputText serieBind) {
        this.serieBind = serieBind;
    }

    public RichInputText getSerieBind() {
        return serieBind;
    }

    public void setNroGuiaBind(RichInputText nroGuiaBind) {
        this.nroGuiaBind = nroGuiaBind;
    }

    public RichInputText getNroGuiaBind() {
        return nroGuiaBind;
    }

    public void setTipoGuiaBind(RichSelectOneChoice tipoGuiaBind) {
        this.tipoGuiaBind = tipoGuiaBind;
    }

    public RichSelectOneChoice getTipoGuiaBind() {
        return tipoGuiaBind;
    }

    public void setIdAlmacenBind(RichInputText idAlmacenBind) {
        this.idAlmacenBind = idAlmacenBind;
    }

    public RichInputText getIdAlmacenBind() {
        return idAlmacenBind;
    }

    public void setIdTipoTransaccionBind(RichInputText idTipoTransaccionBind) {
        this.idTipoTransaccionBind = idTipoTransaccionBind;
    }

    public RichInputText getIdTipoTransaccionBind() {
        return idTipoTransaccionBind;
    }

    public void setIdCabTransaccionBind(RichInputText idCabTransaccionBind) {
        this.idCabTransaccionBind = idCabTransaccionBind;
    }

    public RichInputText getIdCabTransaccionBind() {
        return idCabTransaccionBind;
    }

    public void setIdClienteBind(RichInputText idClienteBind) {
        this.idClienteBind = idClienteBind;
    }

    public RichInputText getIdClienteBind() {
        return idClienteBind;
    }

    public void setPedidoVentaBind(RichInputText pedidoVentaBind) {
        this.pedidoVentaBind = pedidoVentaBind;
    }

    public RichInputText getPedidoVentaBind() {
        return pedidoVentaBind;
    }

    public void setIdProveedor(RichInputText idProveedor) {
        this.idProveedor = idProveedor;
    }

    public RichInputText getIdProveedor() {
        return idProveedor;
    }

    public void setOrdenCompraBind(RichInputText ordenCompraBind) {
        this.ordenCompraBind = ordenCompraBind;
    }

    public RichInputText getOrdenCompraBind() {
        return ordenCompraBind;
    }

    public void setNroRecepcionBind(RichInputText nroRecepcionBind) {
        this.nroRecepcionBind = nroRecepcionBind;
    }

    public RichInputText getNroRecepcionBind() {
        return nroRecepcionBind;
    }

    public void setAlmacenLOV(RichInputComboboxListOfValues almacenLOV) {
        this.almacenLOV = almacenLOV;
    }

    public RichInputComboboxListOfValues getAlmacenLOV() {
        return almacenLOV;
    }

    public void setTipoTransaccionLOV(RichInputComboboxListOfValues tipoTransaccionLOV) {
        this.tipoTransaccionLOV = tipoTransaccionLOV;
    }

    public RichInputComboboxListOfValues getTipoTransaccionLOV() {
        return tipoTransaccionLOV;
    }

    public void setClienteLOV(RichInputComboboxListOfValues clienteLOV) {
        this.clienteLOV = clienteLOV;
    }

    public RichInputComboboxListOfValues getClienteLOV() {
        return clienteLOV;
    }

    public void setProveedorLOV(RichInputComboboxListOfValues proveedorLOV) {
        this.proveedorLOV = proveedorLOV;
    }

    public RichInputComboboxListOfValues getProveedorLOV() {
        return proveedorLOV;
    }

    public void setIdTransaccionBind(RichInputText idTransaccionBind) {
        this.idTransaccionBind = idTransaccionBind;
    }

    public RichInputText getIdTransaccionBind() {
        return idTransaccionBind;
    }

    public void setCreateGRPopup(RichPopup createGRPopup) {
        this.createGRPopup = createGRPopup;
    }

    public RichPopup getCreateGRPopup() {
        return createGRPopup;
    }

    public void setCreateGRDialog(RichDialog createGRDialog) {
        this.createGRDialog = createGRDialog;
    }

    public RichDialog getCreateGRDialog() {
        return createGRDialog;
    }

    public void saveGRAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
        
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
        Row row = vo.getCurrentRow();
        String statusGr = (String)row.getAttribute("EstadoGr");
        String readOnlyFlag = "";
        
        ViewObject vo2 = guiaAM.getXxncPeLocGRMaterialTrxQueryVO1();
        vo2.setNamedWhereClauseParam("pDireccionDestino", null);
        vo2.setNamedWhereClauseParam("pPedidoVenta", null);
        vo2.setNamedWhereClauseParam("pDespacho", null);
        vo2.setNamedWhereClauseParam("pOrdenCompra", null);
        vo2.setNamedWhereClauseParam("pNroRecepcion", null);
        vo2.setNamedWhereClauseParam("pTipoTransaccion", null);
        vo2.setNamedWhereClauseParam("pIdCabTransaccion", null);
        vo2.setNamedWhereClauseParam("pIdTransaccion", null);
        vo2.setNamedWhereClauseParam("pGuiaNro", null);
        vo2.setNamedWhereClauseParam("pGuiaEstado", null);
        vo2.setNamedWhereClauseParam("pIdAlmacen", 0);
        vo2.executeQuery();
        
        if ("ERR".equals(statusGr) || "NES".equals(statusGr) || "NET".equals(statusGr) || "PEND".equals(statusGr) || "".equals(statusGr)) {
            readOnlyFlag = "N";
        } else {
            readOnlyFlag = "Y";
        }
        
        RequestContext.getCurrentInstance().getPageFlowScope().put("currentGrStatus", statusGr);
        RequestContext.getCurrentInstance().getPageFlowScope().put("grReadOnlyFlag", readOnlyFlag);
    }

    public void cancelGRAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreateGRPopup();
        popup.hide();
    }

    public void createGRAction(ActionEvent actionEvent) {
        // Add event code here...
        //BindingContainer bindings = this.getBindings(); //TEST
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreateGRPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }

    public void idGRLinkAction(ActionEvent actionEvent) {
        // Add event code here...
        RowKeySet selectedServs = this.getGrTable().getSelectedRowKeys();
        Iterator selectedServIter = selectedServs.iterator();
        List servIdList = new ArrayList();

        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding servIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator servRSIter = servIter.getRowSetIterator();

        /*while (selectedServIter.hasNext()) {
            Key key = (Key)((List)selectedServIter.next()).get(0);
            Row currentRow = servRSIter.getRow(key);
            servIdList.add(currentRow.getAttribute("ServiceId"));
        }*/

        if (selectedServIter.hasNext()) {
            Key key = (Key)((List)selectedServIter.next()).get(0);
            Row currentRow = servRSIter.getRow(key);
            BigDecimal idGR = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");

            XxncPeLocGuiaRemisionAMImpl guiaAM =
                (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
            ViewObject vo = guiaAM.getXxncPeLocGRGuiaRemisionCabVO1();
            Row row = vo.getRow(new Key(new Object[] { idGR }));
            vo.setCurrentRow(row);
            String statusGr = (String)row.getAttribute("EstadoGr");
            
            String readOnlyFlag = "";
            
            if ("ERR".equals(statusGr) || "NES".equals(statusGr) || "NET".equals(statusGr) || "PEND".equals(statusGr) || "".equals(statusGr)) {
                readOnlyFlag = "N";
            } else {
                readOnlyFlag = "Y";
            }
            
            RequestContext.getCurrentInstance().getPageFlowScope().put("currentGrStatus", statusGr);
            RequestContext.getCurrentInstance().getPageFlowScope().put("grReadOnlyFlag", readOnlyFlag);
            
            ViewObject vo2 = guiaAM.getXxncPeLocGRMaterialTrxQueryVO1();
            vo2.setNamedWhereClauseParam("pDireccionDestino", null);
            vo2.setNamedWhereClauseParam("pPedidoVenta", null);
            vo2.setNamedWhereClauseParam("pDespacho", null);
            vo2.setNamedWhereClauseParam("pOrdenCompra", null);
            vo2.setNamedWhereClauseParam("pNroRecepcion", null);
            vo2.setNamedWhereClauseParam("pTipoTransaccion", null);
            vo2.setNamedWhereClauseParam("pIdCabTransaccion", null);
            vo2.setNamedWhereClauseParam("pIdTransaccion", null);
            vo2.setNamedWhereClauseParam("pGuiaNro", null);
            vo2.setNamedWhereClauseParam("pGuiaEstado", null);
            vo2.setNamedWhereClauseParam("pIdAlmacen", 0);
            vo2.executeQuery();
        }
    }

    public void setGrTable(RichTable grTable) {
        this.grTable = grTable;
    }

    public RichTable getGrTable() {
        return grTable;
    }

    public void validateDeleteGRE(BigDecimal pIdGre, DBTransaction trans) throws SQLException, GREValidationException {
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_validate_delete_gre(?, ?, ?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.createCallableStatement(st, -1);

        acs.setBigDecimal(1, pIdGre);
        acs.registerOutParameter(2, Types.VARCHAR, 0, 1);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 2000);

        acs.executeUpdate();

        String status = acs.getString(2);

        if (!"S".equals(status)) {
            String message = acs.getString(3);
            throw new GREValidationException(message);
        }
    }

    public void validateSendGRE(BigDecimal pIdGre, String pVoidFlag, DBTransaction trans) throws SQLException,
                                                                                                 GREValidationException {
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_validate_gre(?, ?, ?, ?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.createCallableStatement(st, -1);

        acs.setBigDecimal(1, pIdGre);
        acs.setString(2, pVoidFlag);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 2000);

        acs.executeUpdate();

        String status = acs.getString(3);

        if (!"S".equals(status)) {
            String message = acs.getString(4);
            throw new GREValidationException(message);
        }
    }

    private SignatureBean getSignatureParams(DBTransaction trans) throws SQLException {
        SignatureBean sign = new SignatureBean();

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_get_signature_params(?, ?, ?, ?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.createCallableStatement(st, -1);
        acs.registerOutParameter(1, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(2, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 150);

        acs.executeUpdate();

        String url = acs.getString(1);
        String area = acs.getString(2);
        String pass = acs.getString(3);
        String jcsUrl = acs.getString(4);

        sign.setUrl(url);
        sign.setArea(area);
        sign.setPass(pass);
        sign.setJcsUrl(jcsUrl);

        return (sign);
    }

    public void deleteGREAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();

        DCIteratorBinding grIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator grRSIter = grIter.getRowSetIterator();

        RowKeySet selectedGR = this.getGrTable().getSelectedRowKeys();
        Iterator selectedGRIter = selectedGR.iterator();
        
        if (selectedGR.size() > 0) {
            while (selectedGRIter.hasNext()) {
                Key key = (Key)((List)selectedGRIter.next()).get(0);
                Row currentRow = grRSIter.getRow(key);
                BigDecimal idGRE = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");

                try {
                    this.validateDeleteGRE(idGRE, guiaAM.getDBTransaction());
                } catch (GREValidationException gex) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, gex.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                    return;
                } catch (SQLException sqlex) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                    return;
                }
            }
            
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Guias de Remision Eliminadas satisfactoriamente.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            this.searchListener(actionEvent);
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
        }

    }

    public void sendNewGREAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();

        DCIteratorBinding grIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator grRSIter = grIter.getRowSetIterator();

        RowKeySet selectedGR = this.getGrTable().getSelectedRowKeys();
        Iterator selectedGRIter = selectedGR.iterator();

        List grList = new ArrayList();

        while (selectedGRIter.hasNext()) {
            Key key = (Key)((List)selectedGRIter.next()).get(0);
            Row currentRow = grRSIter.getRow(key);
            BigDecimal idGRE = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");

            try {
                this.validateSendGRE(idGRE, "N", guiaAM.getDBTransaction());
                grList.add(new GREBean(idGRE, "N"));
            } catch (GREValidationException gex) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, gex.getMessage(), "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                return;
            } catch (SQLException sqlex) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                return;
            }

            if (grList.size() > 0) {

                try {
                    for (Object obj : grList) {
                        GREBean gre = (GREBean)obj;

                        //GREHandler greRunnable = new GREHandler();
                        GREJcsHandler greRunnable = new GREJcsHandler();
                        
                        //GREJcsHandler greRunnable = (Class<GREJcsHandler>)GRQueryBean.class.getClassLoader().loadClass("xxss.oracle.localizations.pe.app.guiaremision.etd.GREJcsHandler");

                        String instanceName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.instanceName}");
                        String dataCenter = JSFUtils.resolveExpressionAsString("#{pageFlowScope.dataCenter}");
                        String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

                        SignatureBean sign = this.getSignatureParams(guiaAM.getDBTransaction());

                        greRunnable.setJwt(jwt);
                        greRunnable.setInstanceName(instanceName);
                        greRunnable.setDataCenter(dataCenter);
                        greRunnable.setIdGre(gre.getIdGRE());
                        greRunnable.setVoidFlag(gre.getVoidFlag());
                        greRunnable.setSignatureURL(sign.getUrl());
                        greRunnable.setSignatureArea(sign.getArea());
                        greRunnable.setSignaturePass(sign.getPass());
                        greRunnable.setJcsSignatureURL(sign.getJcsUrl());
                        greRunnable.setInvTrxValueSet("LOC_PE_INV_TRX_ADD_INFO");

                        Thread t = new Thread(greRunnable);
                        t.start();
                    }
                    
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se enviaron las transacciones seleccionadas. Dar click en Buscar para refrescar el estado.", "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                } catch (SQLException sqlex) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                    return;
                } /*catch (ClassNotFoundException classEx) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + classEx.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                    return;
                }*/


            } else {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar por lo menos un registro.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
    }

    public void sendVoidGREAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();

        DCIteratorBinding grIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator grRSIter = grIter.getRowSetIterator();

        RowKeySet selectedGR = this.getGrTable().getSelectedRowKeys();
        Iterator selectedGRIter = selectedGR.iterator();
        
        List grList = new ArrayList();

        while (selectedGRIter.hasNext()) {
            Key key = (Key)((List)selectedGRIter.next()).get(0);
            Row currentRow = grRSIter.getRow(key);
            BigDecimal idGRE = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");

            try {
                this.validateSendGRE(idGRE, "Y", guiaAM.getDBTransaction());
                grList.add(new GREBean(idGRE, "Y"));
            } catch (GREValidationException gex) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, gex.getMessage(), "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                return;
            } catch (SQLException sqlex) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                return;
            }
            
            if (grList.size() > 0) {

                try {
                    for (Object obj : grList) {
                        GREBean gre = (GREBean)obj;

                        GREHandler greRunnable = new GREHandler();

                        String instanceName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.instanceName}");
                        String dataCenter = JSFUtils.resolveExpressionAsString("#{pageFlowScope.dataCenter}");
                        String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

                        SignatureBean sign = this.getSignatureParams(guiaAM.getDBTransaction());

                        greRunnable.setJwt(jwt);
                        greRunnable.setInstanceName(instanceName);
                        greRunnable.setDataCenter(dataCenter);
                        greRunnable.setIdGre(gre.getIdGRE());
                        greRunnable.setVoidFlag(gre.getVoidFlag());
                        greRunnable.setSignatureURL(sign.getUrl());
                        greRunnable.setSignatureArea(sign.getArea());
                        greRunnable.setSignaturePass(sign.getPass());
                        greRunnable.setJcsSignatureURL(sign.getJcsUrl());
                        greRunnable.setInvTrxValueSet("LOC_PE_INV_TRX_ADD_INFO");

                        Thread t = new Thread(greRunnable);
                        t.start();
                    }
                    
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se enviaron las transacciones seleccionadas. Dar click en Buscar para refrescar el estado.", "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                } catch (SQLException sqlex) {
                    FacesMessage msg =
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                    return;
                }


            } else {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar por lo menos un registro.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
    }
    
    public void voidGREAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();

        DCIteratorBinding grIter = bindings.findIteratorBinding("XxncPeLocGRQueryVO1Iterator");
        RowSetIterator grRSIter = grIter.getRowSetIterator();

        RowKeySet selectedGR = this.getGrTable().getSelectedRowKeys();
        Iterator selectedGRIter = selectedGR.iterator();
        
        List grList = new ArrayList();

        while (selectedGRIter.hasNext()) {
            Key key = (Key)((List)selectedGRIter.next()).get(0);
            Row currentRow = grRSIter.getRow(key);
            BigDecimal idGRE = (BigDecimal)currentRow.getAttribute("IdGuiaRemision");

            try {
                this.validateSendGRE(idGRE, "Y", guiaAM.getDBTransaction());
                grList.add(new GREBean(idGRE, "Y"));
            } catch (GREValidationException gex) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, gex.getMessage(), "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                return;
            } catch (SQLException sqlex) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error SQL: " + sqlex.getMessage(), "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getGrTable());
                return;
            }
         
            
            String processStatus = "VO";
            
            if (grList.size() > 0) {
                this.deleteVoidLines(idGRE);
                this.updateStatusGRE(idGRE, processStatus, "Documento Anulado", null, guiaAM.getDBTransaction());
                //RequestContext.getCurrentInstance().getPageFlowScope().put("grDisableField", "Y");
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Se anularon las transacciones seleccionadas. Dar click en Buscar para refrescar el estado.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            } else {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar por lo menos un registro.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        }
    }

    public void sendRequestDownloadListener(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("SendRequestClob");
        if (attr == null) {
            log.error("Can't find SendRequestClob binding...");
            return;
        }
        
        ClobDomain clob = (ClobDomain)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en sendRequestDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void sendResponseDownloadListener(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("SendResponseClob");
        if (attr == null) {
            log.error("Can't find SendResponseClob binding...");
            return;
        }
        
        ClobDomain clob = (ClobDomain)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en sendResponseDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void statusRequestDownloadListener(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("StatusRequestClob");
        if (attr == null) {
            log.error("Can't find StatusRequestClob binding...");
            return;
        }
        
        ClobDomain clob = (ClobDomain)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en statusRequestDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void statusResponseDownloadListener(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("StatusResponseClob");
        if (attr == null) {
            log.error("Can't find StatusResponseClob binding...");
            return;
        }
        
        ClobDomain clob = (ClobDomain)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en statusResponseDownloadListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void updateStatusGRE(BigDecimal pIdGRE, String status, String message, String url, DBTransaction trans) {
        try {
            String st =
                "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_update_gr_status(?, ?, ?, ?); end;";
            OracleCallableStatement acs = (OracleCallableStatement)trans.createCallableStatement(st, -1);

            acs.setBigDecimal(1, pIdGRE);
            acs.setString(2, status);
            acs.setString(3, message);
            acs.setString(4, url); 

            acs.executeUpdate();
        } catch (SQLException sqlex) {
            log.error("Error en updateStatusGRE(): " + sqlex.getMessage(), sqlex);
            log.error(Arrays.toString(sqlex.getStackTrace()));
        }
    }
    
    public void deleteVoidLines(BigDecimal pIdGRE){
        ApplicationModuleImpl am = (ApplicationModuleImpl)ADFUtils.getApplicationModuleForDataControl("XxncPeLocGuiaRemisionAMDataControl");
        DBTransaction trans = am.getDBTransaction();
        try {
            String st = 
             "begin XXNC_PE_LOC_GUIA_REMISION_PKG.p_delete_void_lines(pn_id_guia => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.createCallableStatement(st, -1);
            
            acs.setNUMBER(1, new Number(pIdGRE));
            
            acs.executeUpdate();
            
        }catch (SQLException exsql){
            log.error("Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_delete_temp_lines: " + exsql.getMessage(), exsql);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Error al ejecutar el procedimiento pl/sql XXNC_PE_LOC_GUIA_REMISION_PKG.p_delete_temp_lines: " + exsql.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }catch (Exception e){
            log.error("Error en InsertLineaGuiaRemisionBean" + e.getMessage(), e);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, null, "Error en InsertLineaGuiaRemisionBean: " + e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

}
