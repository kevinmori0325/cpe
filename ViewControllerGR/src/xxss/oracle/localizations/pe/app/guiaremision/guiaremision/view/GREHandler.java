package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.view;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import java.math.BigDecimal;

import java.net.MalformedURLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import java.util.List;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;

import oracle.jbo.domain.Number;

import org.apache.commons.io.IOUtils;

import org.apache.log4j.Logger;

import org.xml.sax.InputSource;

//import xxss.oracle.localizations.pe.app.jobs.beans.CheckSignatureInvJob;
//import xxss.oracle.localizations.pe.app.jobs.beans.factelectronica.FEEntidadLegal;
import xxss.oracle.localizations.pe.app.utils.SpanishNumberToWords;
import xxss.oracle.localizations.pe.app.utils.WebServiceUtils;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.proxy.FndManageImportExportFilesService;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.proxy.ServiceException;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types.DocumentDetails;
import xxss.oracle.localizations.services.signature.helper.request.Anulacion;
import xxss.oracle.localizations.services.signature.helper.request.Campo;
import xxss.oracle.localizations.services.signature.helper.request.CdgItem;
import xxss.oracle.localizations.services.signature.helper.request.DTE;
import xxss.oracle.localizations.services.signature.helper.request.Detalle;
import xxss.oracle.localizations.services.signature.helper.request.DetalleDocAnulado;
import xxss.oracle.localizations.services.signature.helper.request.DocPersonalizado;
import xxss.oracle.localizations.services.signature.helper.request.DocRecep;
import xxss.oracle.localizations.services.signature.helper.request.DocTransp;
import xxss.oracle.localizations.services.signature.helper.request.Documento;
import xxss.oracle.localizations.services.signature.helper.request.DomFiscal;
import xxss.oracle.localizations.services.signature.helper.request.DomFiscalRcp;
import xxss.oracle.localizations.services.signature.helper.request.Emisor;
import xxss.oracle.localizations.services.signature.helper.request.Encabezado;
import xxss.oracle.localizations.services.signature.helper.request.IdDoc;
import xxss.oracle.localizations.services.signature.helper.request.Impuestos;
import xxss.oracle.localizations.services.signature.helper.request.ImpuestosDet;
import xxss.oracle.localizations.services.signature.helper.request.Local;
import xxss.oracle.localizations.services.signature.helper.request.MedioTransporte;
import xxss.oracle.localizations.services.signature.helper.request.NombreEmisor;
import xxss.oracle.localizations.services.signature.helper.request.NombreRecep;
import xxss.oracle.localizations.services.signature.helper.request.Personalizados;
import xxss.oracle.localizations.services.signature.helper.request.Receptor;
import xxss.oracle.localizations.services.signature.helper.request.Referencia;
import xxss.oracle.localizations.services.signature.helper.request.TotSubMonto;
import xxss.oracle.localizations.services.signature.helper.request.Totales;
import xxss.oracle.localizations.services.signature.helper.request.Transporte;
import xxss.oracle.localizations.services.signature.helper.request.Traslado;
import xxss.oracle.localizations.services.signature.helper.response.ProcessResult;
import xxss.oracle.localizations.services.signature.proxy.CoreSoap;
import xxss.oracle.localizations.services.util.FndImportExportFileServiceUtil;
import xxss.oracle.localizations.services.util.SignatureWSUtil;

public class GREHandler implements Runnable {
    private static Logger log = Logger.getLogger(GREHandler.class);
    private BigDecimal idGre;
    private String voidFlag;
    private String instanceName;
    private String dataCenter;
    private String jwt;
    private String signatureURL;
    private String signatureArea;
    private String signaturePass;
    private String invTrxValueSet;
    private String jcsSignatureURL;

    public GREHandler() {
        super();
    }
    
    public void updateStatusGRE(BigDecimal pIdGRE, String status, String message, String url,
                                        OracleConnection trans) {
        try {
            String st =
                "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_update_gr_status(?, ?, ?, ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setBigDecimal(1, pIdGRE);
            acs.setString(2, status);
            acs.setString(3, message);
            acs.setString(4, url); 

            acs.executeUpdate();
        } catch (SQLException sqlex) {
            log.error("Error en updateStatusGRE(): " + sqlex.getMessage(), sqlex);
            log.error(Arrays.toString(sqlex.getStackTrace()));
        }
    }

    private void callInsertCtrl(String pRegistrationCode, String pDocumentType, String pSerialNum, String pTrxNum,
                                String pEntityName, String pEntityId, String pAdditionalInfo, String pVoidFlag, Date pDocDate,
                                OracleConnection trans) throws SQLException, Exception {

        String st = "begin XXSS_PE_LOC_ELECT_INV_PKG.pr_insert_ctl_row(?,?,?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, pEntityName);
        acs.setString(2, pEntityId);
        acs.setString(3, pRegistrationCode);
        acs.setString(4, pDocumentType);
        acs.setString(5, pSerialNum);
        acs.setString(6, pTrxNum);
        acs.setString(7, pAdditionalInfo);
        acs.setString(8, pVoidFlag);
        acs.setDate(9, new java.sql.Date(pDocDate.getTime()));

        acs.executeUpdate();
        acs.close();
    }

    private void callUpdateCtrl(String pEntityName, String pEntityId, String pProcessStatus, String pProviderStatus,
                                String pProviderMessage, String pVoidFlag, OracleConnection trans) throws SQLException,
                                                                                                          Exception {

        String st = "begin XXSS_PE_LOC_ELECT_INV_PKG.pr_update_ctl_row_sup(?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, pEntityName);
        acs.setString(2, pEntityId);
        acs.setString(3, pProcessStatus);
        acs.setString(4, pProviderStatus);
        acs.setString(5, pProviderMessage);
        acs.setString(6, pVoidFlag);

        acs.executeUpdate();
        acs.close();
    }

    private void callUpdateCtrlDff(String pEntityName, String pEntityId, String pUpdateDffStatus, String pVoidFlag,
                                   OracleConnection trans) throws SQLException, Exception {

        String st = "begin XXSS_PE_LOC_ELECT_INV_PKG.pr_update_ctl_row_dff(?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, pEntityName);
        acs.setString(2, pEntityId);
        acs.setString(3, pUpdateDffStatus);
        acs.setString(4, pVoidFlag);

        acs.executeUpdate();
        acs.close();
    }

    private ProcessResult signatureConvertDocument(CoreSoap svc, String area, String pass, String docType,
                                                   DTE docContent) throws MalformedURLException, JAXBException,
                                                                          IOException {
        ProcessResult procResult = new ProcessResult();

        StringWriter swDTE = new StringWriter();

        JAXBContext jaxbContextDTE = JAXBContext.newInstance(DTE.class);
        Marshaller jaxbMarshallerDTE = jaxbContextDTE.createMarshaller();

        // output pretty printed
        jaxbMarshallerDTE.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshallerDTE.marshal(docContent, swDTE);

        System.out.println("*************TRAMA REQUEST*******************");
        System.out.println(area);
        System.out.println(pass);
        System.out.println(docType);
        System.out.println(swDTE.toString());
        System.out.println("*********************************************");
        System.out.println("Enviando factura electronica...");
        String procResultStr = svc.convertDocument(area, pass, docType, swDTE.toString());
        System.out.println("*************TRAMA RESPONSE*******************");
        System.out.println(procResultStr);
        System.out.println("*********************************************");

        JAXBContext jaxbContextProcResult = JAXBContext.newInstance(ProcessResult.class);
        Unmarshaller jaxbUnmarshallerProcResult = jaxbContextProcResult.createUnmarshaller();

        InputStream in = IOUtils.toInputStream(procResultStr, "UTF-8");

        procResult = (ProcessResult)jaxbUnmarshallerProcResult.unmarshal(new InputSource(in));

        return (procResult);
    }

    private ProcessResult signatureConvertDocumentAnul(CoreSoap svc, String area, String pass, String docType,
                                                       Anulacion anulContent) throws MalformedURLException,
                                                                                     JAXBException, IOException {
        ProcessResult procResult = new ProcessResult();

        StringWriter swAnulacion = new StringWriter();

        JAXBContext jaxbContextDTE = JAXBContext.newInstance(Anulacion.class);
        Marshaller jaxbMarshallerDTE = jaxbContextDTE.createMarshaller();

        // output pretty printed
        jaxbMarshallerDTE.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshallerDTE.marshal(anulContent, swAnulacion);

        System.out.println("*************TRAMA REQUEST*******************");
        System.out.println(area);
        System.out.println(pass);
        System.out.println(docType);
        System.out.println(swAnulacion.toString());
        System.out.println("*********************************************");
        System.out.println("Enviando Comunicacion de Baja...");
        String procResultStr = svc.convertDocument(area, pass, docType, swAnulacion.toString());
        System.out.println("*************TRAMA RESPONSE*******************");
        System.out.println(procResultStr);
        System.out.println("*********************************************");

        JAXBContext jaxbContextProcResult = JAXBContext.newInstance(ProcessResult.class);
        Unmarshaller jaxbUnmarshallerProcResult = jaxbContextProcResult.createUnmarshaller();

        InputStream in = IOUtils.toInputStream(procResultStr, "UTF-8");

        procResult = (ProcessResult)jaxbUnmarshallerProcResult.unmarshal(new InputSource(in));

        return (procResult);
    }

    private byte[] createVSFile(List pIdList, String pValueSetName) {
        String separator = "|";
        String newLine = System.getProperty("line.separator"); // System.lineSeparator() for java > 1.6
        String defEnabledFlag = "Y";
        String headerRow = "ValueSetCode|Value|Description|EnabledFlag|FlexValueAttribute15|FlexValueAttribute14|FlexValueAttribute13" + newLine;
        String stringRow;
        byte[] fileBytes;

        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(headerRow);


        for (Object obj : pIdList) {
            String flexValue = "", attribute15 = "", attribute14 = "", attribute13 = "";
            String[] split = String.valueOf(obj).split(";");
            
            flexValue = String.valueOf(obj).split(";")[0];
            attribute15 = String.valueOf(obj).split(";")[1];
            attribute14 = String.valueOf(obj).split(";")[2];
            attribute13 = String.valueOf(obj).split(";")[3];

            /*if (split.length == 2) {
                flexValue = String.valueOf(obj).split(";")[0];
                attribute15 = String.valueOf(obj).split(";")[1];
            } else if (split.length == 1) {
                flexValue = String.valueOf(obj).split(";")[0];
            }*/

            stringRow =
                    pValueSetName + separator + flexValue + separator + "" + separator + defEnabledFlag + separator +
                    attribute15 + separator + attribute14 + separator + attribute13;
            strBuffer.append(stringRow + newLine);
        }
        
        log.info("strBuffer: " + strBuffer);

        fileBytes = strBuffer.toString().getBytes();

        return (fileBytes);
    }

    private String loadFileForVS(FndManageImportExportFilesService svc, List pIdList,
                                 String pValueSetName) throws ServiceException {
        byte[] fileBytes;
        String result = "";

        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmssZ");
        String fileName = "filevsimportcom" + format.format(new Date()) + ".txt";

        fileBytes = createVSFile(pIdList, pValueSetName);

        if (fileBytes.length == 0) {
            log.info("Nothing to load on value set " + pValueSetName);
            result = "0";
            return (result);
        }

        DocumentDetails document = FndImportExportFileServiceUtil.buildVSDocumentDetails(fileName, fileBytes);

        String fileId = svc.uploadFiletoUCM(document);

        if (fileId != null) {
            String vsResponse = svc.valueSetValuesDataLoader(new Long(fileId));
            log.info("ValueSetLoader Result: " + vsResponse);
            result = "1";
        } else {
            result = "0";
        }

        return result;
    }

    private void sendInfoToSignature(CoreSoap svcSign, FndManageImportExportFilesService pSvcFileImport,
                                     String signArea, String signPass, BigDecimal pIdGuiaRemision, String pValueSetName,
                                     OracleConnection trans) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        String lquery = "select ID_GUIA_REMISION,\n" +
            "       ESTADO_GR,\n" +
            "       SERIE_GR,\n" +
            "       NUMERO_GR,\n" +
            "       FECHA_EMISION,\n" +
            "       TIPO_GR,\n" +
            "       LEGAL_ENTITY_NAME,\n" +
            "       LE_REGISTRATION_NUM,\n" +
            "       LE_ADDRESS1,\n" +
            "       LE_CITY,\n" +
            "       LE_STATE,\n" +
            "       LE_COUNTRY,\n" +
            "       LE_POSTAL_CODE,\n" +
            "       RECEP_TIPO_DOC,\n" +
            "       RECEP_NUM_DOC,\n" +
            "       RECEP_NOMBRE,\n" +
            "       RECEP_DIRECCION,\n" +
            "       RECEP_CIUDAD,\n" +
            "       RECEP_PAIS,\n" +
            "       TRANSP_TIPO_DOC,\n" +
            "       TRANSP_NUM_DOC,\n" +
            "       TRANSP_NOMBRE,\n" +
            "       TRANSP_PLACA,\n" +
            "       TRANSP_METODO_TRANSP,\n" +
            "       PESO_UOM,\n" +
            "       PESO_CANTIDAD,\n" +
            "       LOCAL_TIPO_DOC,\n" +
            "       LOCAL_NRO_DOC,\n" +
            "       LOCAL_NOMBRE,\n" +
            "       MOTIVO_TRASLADO,\n" +
            "       DESC_MOTIVO_TRASLADO,\n" +
            "       INDICADOR_TRASBORDO_PROGRAMADO,\n" +
            "       NRO_CONTENEDOR,\n" +
            "       LUGAR_EMBARQUE,\n" +
            "       UBIGEO_ORIGEN,\n" +
            "       DIRECCION_ORIGEN,\n" +
            "       UBIGEO_DESTINO,\n" +
            "       DIRECCION_DESTINO,\n" +
            "       FECHA_INICIO_TRASLADO,\n" +
            "       NRO_PAQUETES\n" +
            "  from xxnc_pe_loc_gr_sign_cab_v\n" +
            "  where ID_GUIA_REMISION = ?" +
            "  and estado_gr = 'EXEC'"
            ;

        ResultSet resultado;

        OraclePreparedStatement lStmt =
            (OraclePreparedStatement)trans.prepareStatement(lquery, OracleResultSet.TYPE_FORWARD_ONLY,
                                                            OracleResultSet.CONCUR_READ_ONLY);
        lStmt.setBigDecimal(1, pIdGuiaRemision);
        resultado = lStmt.executeQuery();

        DTE dte = null;
        List listIds = new ArrayList();

        String registrationNumber = "";
        String serie = "";
        String numero = "";
        String tipoDoc = "09";
        String idGuiaRemision = "";
        Date documentDate = new Date();

        if (resultado.next()) {
            registrationNumber = resultado.getString("LE_REGISTRATION_NUM");
            serie = resultado.getString("SERIE_GR");
            numero = resultado.getString("NUMERO_GR");
            idGuiaRemision = resultado.getString("ID_GUIA_REMISION");
            documentDate = resultado.getDate("FECHA_EMISION");

            Encabezado encab = new Encabezado();

            IdDoc idDoc = new IdDoc();
            idDoc.setTipo("09");
            idDoc.setSerie(resultado.getString("SERIE_GR"));
            idDoc.setNumero(resultado.getString("NUMERO_GR"));
            idDoc.setNumeroInterno(resultado.getString("ID_GUIA_REMISION"));
            idDoc.setFechaEmis(sdf.format(documentDate));
            encab.setIdDoc(idDoc);

            Emisor emisor = new Emisor();
            emisor.setIdEmisor(resultado.getString("LE_REGISTRATION_NUM"));
            emisor.setNmbEmisor(resultado.getString("LEGAL_ENTITY_NAME"));
            emisor.setDomFiscal(new DomFiscal());
            emisor.getDomFiscal().setCalle(resultado.getString("LE_ADDRESS1"));
            emisor.getDomFiscal().setCiudad(resultado.getString("LE_CITY"));
            emisor.getDomFiscal().setCodigoPostal(resultado.getString("LE_POSTAL_CODE"));
            emisor.getDomFiscal().setPais(resultado.getString("LE_COUNTRY"));
            encab.setEmisor(emisor);

            Receptor receptor = new Receptor();
            receptor.setDocRecep(new DocRecep());
            receptor.getDocRecep().setTipoDocRecep(resultado.getString("RECEP_TIPO_DOC"));
            receptor.getDocRecep().setNroDocRecep(resultado.getString("RECEP_NUM_DOC"));
            receptor.setNmbRecep(resultado.getString("RECEP_NOMBRE"));
            receptor.setDomFiscalRcp(new DomFiscalRcp());
            receptor.getDomFiscalRcp().setCalle(resultado.getString("RECEP_DIRECCION"));
            receptor.getDomFiscalRcp().setCiudad(resultado.getString("RECEP_CIUDAD"));
            receptor.getDomFiscalRcp().setPais(resultado.getString("RECEP_PAIS"));
            encab.setReceptor(receptor);

            Transporte transporte = new Transporte();
            transporte.setDocTransp(new DocTransp());
            transporte.getDocTransp().setTipoDocTransp(resultado.getString("TRANSP_TIPO_DOC"));
            transporte.getDocTransp().setNroDocTransp(resultado.getString("TRANSP_NUM_DOC"));
            transporte.setNmbTransp(resultado.getString("TRANSP_NOMBRE"));
            transporte.setMedioTransporte(new MedioTransporte());
            transporte.getMedioTransporte().setMetodoTransp(resultado.getString("TRANSP_METODO_TRANSP"));
            transporte.getMedioTransporte().setNroRefTransp(resultado.getString("TRANSP_PLACA"));
            encab.setTransporte(transporte);

            Totales totales = new Totales();
            totales.setIndLista(resultado.getString("PESO_UOM"));
            totales.setTipolista(resultado.getString("PESO_UOM"));
            totales.setTipoQtyItem(resultado.getString("PESO_CANTIDAD"));
            encab.setTotales(totales);

            Local local = new Local();
            local.setTipoLoc(resultado.getString("LOCAL_TIPO_DOC"));
            local.setCodigoLoc(resultado.getString("LOCAL_NRO_DOC"));
            local.setNombreLoc(resultado.getString("LOCAL_NOMBRE"));
            encab.setLocal(local);

            Traslado traslado = new Traslado();
            traslado.setTipo("01"); // por defecto
            traslado.setMotivo(resultado.getString("MOTIVO_TRASLADO"));
            traslado.setDescripcion(resultado.getString("DESC_MOTIVO_TRASLADO"));
            traslado.setIndicador(resultado.getString("INDICADOR_TRASBORDO_PROGRAMADO"));
            traslado.setNumero(resultado.getString("NRO_CONTENEDOR"));
            traslado.setNroExp(resultado.getString("LUGAR_EMBARQUE"));
            traslado.setIdOrigen(resultado.getString("UBIGEO_ORIGEN"));
            traslado.setOrigen(resultado.getString("DIRECCION_ORIGEN"));
            traslado.setIdDestino(resultado.getString("UBIGEO_DESTINO"));
            traslado.setDestino(resultado.getString("DIRECCION_DESTINO"));
            traslado.setPeriodoDesde(sdf.format(resultado.getDate("FECHA_INICIO_TRASLADO")));
            traslado.setQtyTraslado(resultado.getString("NRO_PAQUETES"));
            encab.setTraslado(traslado);

            String lqueryDet =
                "select mmt.articulo, mmt.descripcion, mmt.iso_uom_code, mmt.cantidad, mmt.id_trx \n" +
                "  from xxnc_pe_loc_gr_guia_rem_l_tmp l\n" +
                " inner join xxnc_pe_loc_gr_trx_to_gr mmt\n" +
                "    on (l.id_transaccion = mmt.id_trx)\n" +
                " where l.id_guia_remision = ? \n";

            ResultSet resultadoDet = null;

            OraclePreparedStatement lStmtDet =
                (OraclePreparedStatement)trans.prepareStatement(lqueryDet, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                OracleResultSet.CONCUR_READ_ONLY);
            lStmtDet.setBigDecimal(1, pIdGuiaRemision);
            resultadoDet = lStmtDet.executeQuery();

            int idx = 0;
            List<Detalle> detalleArray = new ArrayList<Detalle>();

            while (resultadoDet.next()) {
                idx++;
                Detalle detalle = new Detalle();
                detalle.setNroLinDet(String.valueOf(idx));
                detalle.setCdgItem(new CdgItem[] { new CdgItem() });
                detalle.getCdgItem()[0].setTpoCodigo("INT");
                detalle.getCdgItem()[0].setVlrCodigo(resultadoDet.getString("ARTICULO"));
                detalle.setDscItem(resultadoDet.getString("DESCRIPCION"));
                detalle.setQtyItem(resultadoDet.getString("CANTIDAD"));
                detalle.setUnmdItem(resultadoDet.getString("ISO_UOM_CODE"));

                detalleArray.add(detalle);
                listIds.add(resultadoDet.getString("ID_TRX"));
                
            }
            
            lStmtDet.close();
            
            Documento doc = new Documento();
            doc.setId("ID0000001");
            doc.setEncabezado(encab);
            doc.setDetalle(detalleArray.toArray(new Detalle[detalleArray.size()]));

            dte = new DTE();
            dte.setDocumento(doc);
            
        }

        lStmt.close();

        if (dte != null) {
            try {
                log.info("Insercion a tablas de control...");
                String additionalInfoStr = "";

                this.callInsertCtrl(registrationNumber, tipoDoc, serie, numero, "Guia Remision", idGuiaRemision,
                                    additionalInfoStr, "N", documentDate, trans);

                ProcessResult procResult = this.signatureConvertDocument(svcSign, signArea, signPass, tipoDoc, dte);

                log.info("procResult.getHasError(): " + procResult.getHasError());
                log.info("procResult.getTextData(): " + procResult.getTextData());

                String processStatus = "";

                if ("false".equals(procResult.getHasError())) {
                    processStatus = "NS";
                } else {
                    processStatus = "NES";
                }

                this.callUpdateCtrl("Guia Remision", idGuiaRemision, processStatus, procResult.getError()[0].getCode(),
                                    procResult.getError()[0].getDescription(), "N", trans);

                for (int k = 0; k < listIds.size(); k++) {
                    String tempStr = (String)listIds.get(k) + ";" + processStatus + ";" + serie + ";" + numero;
                    listIds.set(k, tempStr);
                }

                String result = this.loadFileForVS(pSvcFileImport, listIds, pValueSetName);

                log.info("loadFileForVS.result(): " + result);

                this.callUpdateCtrlDff("Guia Remision", idGuiaRemision, result, "N", trans);
                
                this.updateStatusGRE(this.idGre, processStatus, procResult.getError()[0].getDescription(), "", trans);

            } catch (JAXBException e) {
                //e.printStackTrace();
                throw new Exception(e);
            } catch (MalformedURLException mEx) {
                //mEx.printStackTrace();
                throw new Exception(mEx);
            } catch (IOException ioEx) {
                //ioEx.printStackTrace();
                throw new Exception(ioEx);
            }
        }


    }

    private void sendInfoToSignatureAnul(CoreSoap svcSign, FndManageImportExportFilesService pSvcFileImport,
                                         String signArea, String signPass, BigDecimal pIdGuiaRemision, String pValueSetName,
                                         OracleConnection trans) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        String lquery = "select ID_GUIA_REMISION,\n" +
            "       ESTADO_GR,\n" +
            "       SERIE_GR,\n" +
            "       NUMERO_GR,\n" +
            "       FECHA_EMISION,\n" +
            "       TIPO_GR,\n" +
            "       LEGAL_ENTITY_NAME,\n" +
            "       LE_REGISTRATION_NUM,\n" +
            "       LE_ADDRESS1,\n" +
            "       LE_CITY,\n" +
            "       LE_STATE,\n" +
            "       LE_COUNTRY,\n" +
            "       LE_POSTAL_CODE,\n" +
            "       RAZON_ANULACION,\n" +
            "       FECHA_ANULACION\n" +
            "  from xxnc_pe_loc_gr_sign_cab_anul_v\n" +
            " where ID_GUIA_REMISION = ? " +
            "  and estado_gr = 'EXEC'"
            ;

        ResultSet resultado;

        OraclePreparedStatement lStmt =
            (OraclePreparedStatement)trans.prepareStatement(lquery, OracleResultSet.TYPE_FORWARD_ONLY,
                                                            OracleResultSet.CONCUR_READ_ONLY);
        lStmt.setBigDecimal(1, pIdGuiaRemision);
        resultado = lStmt.executeQuery();

        Anulacion anul = null;

        String registrationNumber = "";
        String serie = "";
        String numero = "";
        String tipoDoc = "09";
        String idGuiaRemision = "";
        Date documentDate = new Date();

        if (resultado.next()) {
            registrationNumber = resultado.getString("LE_REGISTRATION_NUM");
            serie = resultado.getString("SERIE_GR");
            numero = resultado.getString("NUMERO_GR");
            idGuiaRemision = resultado.getString("ID_GUIA_REMISION");
            documentDate = resultado.getDate("FECHA_ANULACION");

            Encabezado encab = new Encabezado();

            encab.setTipo("10");

            Emisor emisor = new Emisor();
            emisor.setIdEmisor(resultado.getString("LE_REGISTRATION_NUM"));
            emisor.setIdEmisorAdd("6");
            emisor.setNmbEmisor(resultado.getString("LEGAL_ENTITY_NAME"));

            encab.setEmisor(emisor);

            encab.setNumeroInterno(resultado.getString("ID_GUIA_REMISION"));
            encab.setFechaRef(resultado.getString("FECHA_EMISION"));
            encab.setFechaEmis(resultado.getString("FECHA_ANULACION"));

            DetalleDocAnulado docAnul = new DetalleDocAnulado();
            docAnul.setNroLinDet("1");
            docAnul.setTipoDoc(resultado.getString(tipoDoc));
            docAnul.setSerie(resultado.getString("SERIE_GR"));
            docAnul.setNumero(resultado.getString("NUMERO_GR"));
            docAnul.setDscRzonAnulacion(resultado.getString("RAZON_ANULACION"));

            anul = new Anulacion();
            anul.setEncabezado(encab);
            anul.setDetalleDocAnulado(docAnul);
        }

        lStmt.close();

        if (anul != null) {
            try {
                String additionalInfoStr = "";

                this.callInsertCtrl(registrationNumber, tipoDoc,
                                    serie, numero, "Receivables Invoice",
                                    idGuiaRemision, additionalInfoStr, "Y", documentDate, trans);

                ProcessResult procResult =
                    this.signatureConvertDocumentAnul(svcSign, signArea, signPass, "10", anul);

                System.out.println("procResult.getHasError(): " + procResult.getHasError());
                System.out.println("procResult.getTextData(): " + procResult.getTextData());

                String processStatus = "";

                if ("false".equals(procResult.getHasError())) {
                    processStatus = "VS";
                } else {
                    processStatus = "VES";
                }

                this.callUpdateCtrl("Receivables Invoice", idGuiaRemision, processStatus,
                                    procResult.getError()[0].getCode(),
                                    procResult.getError()[0].getDescription(), "Y", trans);
                
                //listIds
                String lqueryDet =
                    "select l.id_transaccion ID_TRANSACCION\n" + 
                    "   from xxnc_pe_loc_gr_guia_rem_lin l\n" + 
                    "  where l.id_guia_remision = ? ";

                ResultSet resultadoDet = null;
                List listIds = new ArrayList();

                OraclePreparedStatement lStmtDet =
                    (OraclePreparedStatement)trans.prepareStatement(lqueryDet, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                    OracleResultSet.CONCUR_READ_ONLY);
                lStmtDet.setBigDecimal(1, pIdGuiaRemision);
                resultado = lStmtDet.executeQuery();
                
                while (resultadoDet.next()) {
                    String idStr = resultadoDet.getString("ID_TRANSACCION") + ";" + processStatus + ";" + serie + ";" + 
                                   numero;
                    listIds.add(idStr);
                }
                
                lStmtDet.close();

                String result = this.loadFileForVS(pSvcFileImport, listIds, pValueSetName);

                log.info("loadFileForVS.result(): " + result);

                this.callUpdateCtrlDff("Guia Remision", idGuiaRemision, result, "Y", trans);
                
                this.updateStatusGRE(this.idGre, processStatus, procResult.getError()[0].getDescription(), "", trans);
                
            } catch (JAXBException e) {
                //e.printStackTrace();
                throw new Exception(e);
            } catch (MalformedURLException mEx) {
                //mEx.printStackTrace();
                throw new Exception(mEx);
            } catch (IOException ioEx) {
                //ioEx.printStackTrace();
                throw new Exception(ioEx);
            }
        }
    }

    public void run() {
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        CoreSoap svcSignature = null;
        FndManageImportExportFilesService svcManageFile = null;
        
        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
            this.updateStatusGRE(this.idGre, "EXEC", "", "", conn);
            
            if (!WebServiceUtils.checkConnection(this.signatureURL)) {
                throw new Exception("No se puede conectar a la URL: " + this.signatureURL);
            }
                        
            svcSignature = SignatureWSUtil.createCoreSoap(this.signatureURL);

            if (svcSignature != null) {
                log.info("SignatureWS created");
            } else {
                throw new Exception("SignatureWS not created");
            }
            
            log.info("Instance Name: " + instanceName);
            log.info("dataCenter: " + dataCenter);
            log.info("jwt: " + jwt);
            
            svcManageFile = FndImportExportFileServiceUtil.createImportServiceJwt(jwt, instanceName, dataCenter);
                
            if (svcManageFile != null) {
                log.info("FndImportExportFileService created");
            } else {
                throw new Exception("FndImportExportFileService is not created.");
            }
            
            if ("N".equals(this.voidFlag)) {
                this.sendInfoToSignature(svcSignature, svcManageFile, this.signatureArea, this.signaturePass, this.idGre, this.invTrxValueSet, conn);
            } else {
                this.sendInfoToSignatureAnul(svcSignature, svcManageFile, this.signatureArea, this.signaturePass, this.idGre, this.invTrxValueSet, conn);
            }
            
        } catch (Exception ex) {
            log.error("Error en run(): " + ex.getMessage(), ex);
            log.error(Arrays.toString(ex.getStackTrace()));
            this.updateStatusGRE(this.idGre, "ERR", ex.getMessage() + " - " + Arrays.toString(ex.getStackTrace()), "", conn);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on run(): " + sqlex.getMessage(), sqlex);
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
            }
        }
    }

    public void setIdGre(BigDecimal idGre) {
        this.idGre = idGre;
    }

    public BigDecimal getIdGre() {
        return idGre;
    }

    public void setVoidFlag(String voidFlag) {
        this.voidFlag = voidFlag;
    }

    public String getVoidFlag() {
        return voidFlag;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setDataCenter(String dataCenter) {
        this.dataCenter = dataCenter;
    }

    public String getDataCenter() {
        return dataCenter;
    }

    public void setSignatureURL(String signatureURL) {
        this.signatureURL = signatureURL;
    }

    public String getSignatureURL() {
        return signatureURL;
    }

    public void setSignatureArea(String signatureArea) {
        this.signatureArea = signatureArea;
    }

    public String getSignatureArea() {
        return signatureArea;
    }

    public void setSignaturePass(String signaturePass) {
        this.signaturePass = signaturePass;
    }

    public String getSignaturePass() {
        return signaturePass;
    }

    public void setInvTrxValueSet(String invTrxValueSet) {
        this.invTrxValueSet = invTrxValueSet;
    }

    public String getInvTrxValueSet() {
        return invTrxValueSet;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJcsSignatureURL(String jcsSignatureURL) {
        this.jcsSignatureURL = jcsSignatureURL;
    }

    public String getJcsSignatureURL() {
        return jcsSignatureURL;
    }
}
