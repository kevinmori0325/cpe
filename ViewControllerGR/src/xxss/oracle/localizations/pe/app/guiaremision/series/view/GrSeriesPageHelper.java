package xxss.oracle.localizations.pe.app.guiaremision.series.view;

import java.sql.Date;

import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputComboboxListOfValues;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.validator.ValidatorException;

import xxss.oracle.localizations.pe.app.guiaremision.model.XxncPeLocGuiaRemisionAMImpl;

public class GrSeriesPageHelper  {
    private RichPopup createPopup;
    private RichTable seriesTable;
    private RichPopup asocSeriePopup;
    private RichInputComboboxListOfValues businessNameLov;
    private RichInputComboboxListOfValues almacenNameLov;
    private RichInputDate fechaFinBind;
    private RichInputDate fechaInicioBind;

    public GrSeriesPageHelper() {
    }
    
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setCreatePopup(RichPopup createPopup) {
        this.createPopup = createPopup;
    }

    public RichPopup getCreatePopup() {
        return createPopup;
    }
    
    public void createSerieAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }
    
    public void okCreatePopup(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        
        /*XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRSeriesVO1();
        Row row = vo.getCurrentRow();
        
        Date fechaInicio = (Date)row.getAttribute("FechaInicio");
        Date fechaFin = (Date)row.getAttribute("FechaFin");
        
        if (fechaInicio != null && fechaFin != null) {
            if (fechaInicio.compareTo(fechaFin) > 0) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fecha Fin debe ser posterior a Fecha Inicio.", "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }
        }*/
        
        //this.getFechaInicioBind().validate(FacesContext.getCurrentInstance());
        //this.getFechaFinBind().validate(FacesContext.getCurrentInstance());

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getCreatePopup();
        popup.hide();
        /*AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSeriesTable());*/
        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Archivo Guardado Correctamente.", "");
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void cancelCreatePopup(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getCreatePopup();
        popup.hide();
        /*AdfFacesContext.getCurrentInstance().addPartialTarget(this.getSeriesTable());*/
    }

    public void setSeriesTable(RichTable seriesTable) {
        this.seriesTable = seriesTable;
    }

    public RichTable getSeriesTable() {
        return seriesTable;
    }

    public void setAsocSeriePopup(RichPopup asocSeriePopup) {
        this.asocSeriePopup = asocSeriePopup;
    }

    public RichPopup getAsocSeriePopup() {
        return asocSeriePopup;
    }

    public void saveAsocSerieAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getAsocSeriePopup();
        popup.hide();
    }

    public void cancelAsocSerieAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        /*this.getAttachInputFile().resetValue();*/

        RichPopup popup = this.getAsocSeriePopup();
        popup.hide();
    }

    public void legalEntityChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        //this.getBusinessNameLov().setValue("");
        //this.getAlmacenNameLov().setValue("");
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRSeriesVO1();
        Row row = vo.getCurrentRow();
        
        row.setAttribute("BusinessUnitName", null);
        row.setAttribute("OrgId", null);
        row.setAttribute("AlmacenName", null);
        row.setAttribute("IdAlmacen", null);
        
        this.getBusinessNameLov().resetValue();
        this.getAlmacenNameLov().resetValue();
        
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getBusinessNameLov());
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAlmacenNameLov());
    }

    public void businessNameChangeListener(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRSeriesVO1();
        Row row = vo.getCurrentRow();
        
        row.setAttribute("AlmacenName", null);
        row.setAttribute("IdAlmacen", null);
        this.getAlmacenNameLov().resetValue();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAlmacenNameLov());
    }

    public void setBusinessNameLov(RichInputComboboxListOfValues businessNameLov) {
        this.businessNameLov = businessNameLov;
    }

    public RichInputComboboxListOfValues getBusinessNameLov() {
        return businessNameLov;
    }

    public void setAlmacenNameLov(RichInputComboboxListOfValues almacenNameLov) {
        this.almacenNameLov = almacenNameLov;
    }

    public RichInputComboboxListOfValues getAlmacenNameLov() {
        return almacenNameLov;
    }

    public void fechaInicioValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        Date fechaInicio = (Date)object;
        Date fechaFin = (Date)this.getFechaFinBind().getValue();
        
        /*DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRSeriesVO1();
        Row row = vo.getCurrentRow();
        
        Date fechaInicio = (Date)row.getAttribute("FechaInicio");
        Date fechaFin = (Date)row.getAttribute("FechaFin");*/
        
        if (fechaInicio != null  && fechaFin != null) {
            if (fechaInicio.compareTo(fechaFin) > 0) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fecha Fin debe ser posterior a Fecha Inicio.", "")); 
            }
        }
    }

    public void fechaFinValidator(FacesContext facesContext, UIComponent uIComponent, Object object) {
        // Add event code here...
        Date fechaInicio = (Date)this.getFechaInicioBind().getValue();
        Date fechaFin = (Date)object;
        
        /*DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        
        XxncPeLocGuiaRemisionAMImpl guiaAM =
            (XxncPeLocGuiaRemisionAMImpl)bindings.findDataControl("XxncPeLocGuiaRemisionAMDataControl").getApplicationModule();
        
        ViewObject vo = guiaAM.getXxncPeLocGRSeriesVO1();
        Row row = vo.getCurrentRow();
        
        Date fechaInicio = (Date)row.getAttribute("FechaInicio");
        Date fechaFin = (Date)row.getAttribute("FechaFin");*/
        
        if (fechaInicio != null  && fechaFin != null) {
            if (fechaInicio.compareTo(fechaFin) > 0) {
                throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fecha Fin debe ser posterior a Fecha Inicio.", "")); 
            }
        }
    }

    public void setFechaFinBind(RichInputDate fechaFinBind) {
        this.fechaFinBind = fechaFinBind;
    }

    public RichInputDate getFechaFinBind() {
        return fechaFinBind;
    }

    public void setFechaInicioBind(RichInputDate fechaInicioBind) {
        this.fechaInicioBind = fechaInicioBind;
    }

    public RichInputDate getFechaInicioBind() {
        return fechaInicioBind;
    }
}
