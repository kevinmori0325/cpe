package xxss.oracle.localizations.pe.app.jobs.integration;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;

public class IntegrationJob implements Job {
    private static Logger log = Logger.getLogger(IntegrationJob.class);
    
    public IntegrationJob() {
        super();
    }

    public void execute(JobExecutionContext jobExecutionContext) {
        log.info("Entro ejecucion new IntegrationJob");
    }
}
