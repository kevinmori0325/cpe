package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.model;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Jul 06 16:52:57 GMT-05:00 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class XxncPeLocGRTipoDocPerVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public XxncPeLocGRTipoDocPerVOImpl() {
    }
}
