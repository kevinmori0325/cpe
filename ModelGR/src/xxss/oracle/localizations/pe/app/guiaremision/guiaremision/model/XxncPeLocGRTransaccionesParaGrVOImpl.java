package xxss.oracle.localizations.pe.app.guiaremision.guiaremision.model;

import java.math.BigDecimal;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jun 27 20:08:27 GMT-05:00 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class XxncPeLocGRTransaccionesParaGrVOImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public XxncPeLocGRTransaccionesParaGrVOImpl() {
    }


    /**
     * Returns the variable value for pPedidoVenta.
     * @return variable value for pPedidoVenta
     */
    public String getpPedidoVenta() {
        return (String)ensureVariableManager().getVariableValue("pPedidoVenta");
    }

    /**
     * Sets <code>value</code> for variable pPedidoVenta.
     * @param value value to bind as pPedidoVenta
     */
    public void setpPedidoVenta(String value) {
        ensureVariableManager().setVariableValue("pPedidoVenta", value);
    }


    /**
     * Returns the variable value for pOrdenCompra.
     * @return variable value for pOrdenCompra
     */
    public String getpOrdenCompra() {
        return (String)ensureVariableManager().getVariableValue("pOrdenCompra");
    }

    /**
     * Sets <code>value</code> for variable pOrdenCompra.
     * @param value value to bind as pOrdenCompra
     */
    public void setpOrdenCompra(String value) {
        ensureVariableManager().setVariableValue("pOrdenCompra", value);
    }

    /**
     * Returns the variable value for pNumRecepcion.
     * @return variable value for pNumRecepcion
     */
    public String getpNumRecepcion() {
        return (String)ensureVariableManager().getVariableValue("pNumRecepcion");
    }

    /**
     * Sets <code>value</code> for variable pNumRecepcion.
     * @param value value to bind as pNumRecepcion
     */
    public void setpNumRecepcion(String value) {
        ensureVariableManager().setVariableValue("pNumRecepcion", value);
    }

    /**
     * Returns the variable value for pTipoTrx.
     * @return variable value for pTipoTrx
     */
    public String getpTipoTrx() {
        return (String)ensureVariableManager().getVariableValue("pTipoTrx");
    }

    /**
     * Sets <code>value</code> for variable pTipoTrx.
     * @param value value to bind as pTipoTrx
     */
    public void setpTipoTrx(String value) {
        ensureVariableManager().setVariableValue("pTipoTrx", value);
    }

    /**
     * Returns the variable value for pIdCabTrx.
     * @return variable value for pIdCabTrx
     */
    public BigDecimal getpIdCabTrx() {
        return (BigDecimal)ensureVariableManager().getVariableValue("pIdCabTrx");
    }

    /**
     * Sets <code>value</code> for variable pIdCabTrx.
     * @param value value to bind as pIdCabTrx
     */
    public void setpIdCabTrx(BigDecimal value) {
        ensureVariableManager().setVariableValue("pIdCabTrx", value);
    }

    /**
     * Returns the variable value for pIdTrx.
     * @return variable value for pIdTrx
     */
    public BigDecimal getpIdTrx() {
        return (BigDecimal)ensureVariableManager().getVariableValue("pIdTrx");
    }

    /**
     * Sets <code>value</code> for variable pIdTrx.
     * @param value value to bind as pIdTrx
     */
    public void setpIdTrx(BigDecimal value) {
        ensureVariableManager().setVariableValue("pIdTrx", value);
    }

    /**
     * Returns the variable value for pSerieGr.
     * @return variable value for pSerieGr
     */
    public String getpSerieGr() {
        return (String)ensureVariableManager().getVariableValue("pSerieGr");
    }

    /**
     * Sets <code>value</code> for variable pSerieGr.
     * @param value value to bind as pSerieGr
     */
    public void setpSerieGr(String value) {
        ensureVariableManager().setVariableValue("pSerieGr", value);
    }

    /**
     * Returns the variable value for pNumeroGr.
     * @return variable value for pNumeroGr
     */
    public String getpNumeroGr() {
        return (String)ensureVariableManager().getVariableValue("pNumeroGr");
    }

    /**
     * Sets <code>value</code> for variable pNumeroGr.
     * @param value value to bind as pNumeroGr
     */
    public void setpNumeroGr(String value) {
        ensureVariableManager().setVariableValue("pNumeroGr", value);
    }

    /**
     * Returns the variable value for pEstadoGr.
     * @return variable value for pEstadoGr
     */
    public String getpEstadoGr() {
        return (String)ensureVariableManager().getVariableValue("pEstadoGr");
    }

    /**
     * Sets <code>value</code> for variable pEstadoGr.
     * @param value value to bind as pEstadoGr
     */
    public void setpEstadoGr(String value) {
        ensureVariableManager().setVariableValue("pEstadoGr", value);
    }
}
