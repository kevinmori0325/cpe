package xxss.oracle.localizations.pe.app.guiaremision.parameters.model;

import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jun 12 12:12:35 GMT-05:00 2017
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class XxncPeLocGRTipoTrxVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        IdTipoTrx {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getIdTipoTrx();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setIdTipoTrx((Number)value);
            }
        }
        ,
        TipoTrx {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getTipoTrx();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setTipoTrx((String)value);
            }
        }
        ,
        Accion {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getAccion();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setAccion((String)value);
            }
        }
        ,
        GuiaRemision {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getGuiaRemision();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setGuiaRemision((String)value);
            }
        }
        ,
        TipoGuia {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getTipoGuia();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setTipoGuia((String)value);
            }
        }
        ,
        LegalEntityId {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getLegalEntityId();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setLegalEntityId((Number)value);
            }
        }
        ,
        CreatedBy {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getCreatedBy();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setCreatedBy((String)value);
            }
        }
        ,
        CreationDate {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getCreationDate();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setCreationDate((Date)value);
            }
        }
        ,
        LastUpdatedBy {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getLastUpdatedBy();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setLastUpdatedBy((String)value);
            }
        }
        ,
        LastUpdateDate {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getLastUpdateDate();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setLastUpdateDate((Date)value);
            }
        }
        ,
        Status {
            public Object get(XxncPeLocGRTipoTrxVORowImpl obj) {
                return obj.getStatus();
            }

            public void put(XxncPeLocGRTipoTrxVORowImpl obj, Object value) {
                obj.setStatus((String)value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public abstract Object get(XxncPeLocGRTipoTrxVORowImpl object);

        public abstract void put(XxncPeLocGRTipoTrxVORowImpl object,
                                 Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    public static final int IDTIPOTRX = AttributesEnum.IdTipoTrx.index();
    public static final int TIPOTRX = AttributesEnum.TipoTrx.index();
    public static final int ACCION = AttributesEnum.Accion.index();
    public static final int GUIAREMISION = AttributesEnum.GuiaRemision.index();
    public static final int TIPOGUIA = AttributesEnum.TipoGuia.index();
    public static final int LEGALENTITYID = AttributesEnum.LegalEntityId.index();
    public static final int CREATEDBY = AttributesEnum.CreatedBy.index();
    public static final int CREATIONDATE = AttributesEnum.CreationDate.index();
    public static final int LASTUPDATEDBY = AttributesEnum.LastUpdatedBy.index();
    public static final int LASTUPDATEDATE = AttributesEnum.LastUpdateDate.index();
    public static final int STATUS = AttributesEnum.Status.index();

    /**
     * This is the default constructor (do not remove).
     */
    public XxncPeLocGRTipoTrxVORowImpl() {
    }

    /**
     * Gets XxncPeLocGRTipoTrxEO entity object.
     * @return the XxncPeLocGRTipoTrxEO
     */
    public XxncPeLocGRTipoTrxEOImpl getXxncPeLocGRTipoTrxEO() {
        return (XxncPeLocGRTipoTrxEOImpl)getEntity(0);
    }

    /**
     * Gets the attribute value for ID_TIPO_TRX using the alias name IdTipoTrx.
     * @return the ID_TIPO_TRX
     */
    public Number getIdTipoTrx() {
        return (Number) getAttributeInternal(IDTIPOTRX);
    }

    /**
     * Sets <code>value</code> as attribute value for ID_TIPO_TRX using the alias name IdTipoTrx.
     * @param value value to set the ID_TIPO_TRX
     */
    public void setIdTipoTrx(Number value) {
        setAttributeInternal(IDTIPOTRX, value);
    }

    /**
     * Gets the attribute value for TIPO_TRX using the alias name TipoTrx.
     * @return the TIPO_TRX
     */
    public String getTipoTrx() {
        return (String) getAttributeInternal(TIPOTRX);
    }

    /**
     * Sets <code>value</code> as attribute value for TIPO_TRX using the alias name TipoTrx.
     * @param value value to set the TIPO_TRX
     */
    public void setTipoTrx(String value) {
        setAttributeInternal(TIPOTRX, value);
    }

    /**
     * Gets the attribute value for ACCION using the alias name Accion.
     * @return the ACCION
     */
    public String getAccion() {
        return (String) getAttributeInternal(ACCION);
    }

    /**
     * Sets <code>value</code> as attribute value for ACCION using the alias name Accion.
     * @param value value to set the ACCION
     */
    public void setAccion(String value) {
        setAttributeInternal(ACCION, value);
    }

    /**
     * Gets the attribute value for GUIA_REMISION using the alias name GuiaRemision.
     * @return the GUIA_REMISION
     */
    public String getGuiaRemision() {
        return (String) getAttributeInternal(GUIAREMISION);
    }

    /**
     * Sets <code>value</code> as attribute value for GUIA_REMISION using the alias name GuiaRemision.
     * @param value value to set the GUIA_REMISION
     */
    public void setGuiaRemision(String value) {
        setAttributeInternal(GUIAREMISION, value);
    }

    /**
     * Gets the attribute value for TIPO_GUIA using the alias name TipoGuia.
     * @return the TIPO_GUIA
     */
    public String getTipoGuia() {
        return (String) getAttributeInternal(TIPOGUIA);
    }

    /**
     * Sets <code>value</code> as attribute value for TIPO_GUIA using the alias name TipoGuia.
     * @param value value to set the TIPO_GUIA
     */
    public void setTipoGuia(String value) {
        setAttributeInternal(TIPOGUIA, value);
    }

    /**
     * Gets the attribute value for LEGAL_ENTITY_ID using the alias name LegalEntityId.
     * @return the LEGAL_ENTITY_ID
     */
    public Number getLegalEntityId() {
        return (Number) getAttributeInternal(LEGALENTITYID);
    }

    /**
     * Sets <code>value</code> as attribute value for LEGAL_ENTITY_ID using the alias name LegalEntityId.
     * @param value value to set the LEGAL_ENTITY_ID
     */
    public void setLegalEntityId(Number value) {
        setAttributeInternal(LEGALENTITYID, value);
    }

    /**
     * Gets the attribute value for CREATED_BY using the alias name CreatedBy.
     * @return the CREATED_BY
     */
    public String getCreatedBy() {
        return (String) getAttributeInternal(CREATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATED_BY using the alias name CreatedBy.
     * @param value value to set the CREATED_BY
     */
    public void setCreatedBy(String value) {
        setAttributeInternal(CREATEDBY, value);
    }

    /**
     * Gets the attribute value for CREATION_DATE using the alias name CreationDate.
     * @return the CREATION_DATE
     */
    public Date getCreationDate() {
        return (Date) getAttributeInternal(CREATIONDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for CREATION_DATE using the alias name CreationDate.
     * @param value value to set the CREATION_DATE
     */
    public void setCreationDate(Date value) {
        setAttributeInternal(CREATIONDATE, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @return the LAST_UPDATED_BY
     */
    public String getLastUpdatedBy() {
        return (String) getAttributeInternal(LASTUPDATEDBY);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATED_BY using the alias name LastUpdatedBy.
     * @param value value to set the LAST_UPDATED_BY
     */
    public void setLastUpdatedBy(String value) {
        setAttributeInternal(LASTUPDATEDBY, value);
    }

    /**
     * Gets the attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @return the LAST_UPDATE_DATE
     */
    public Date getLastUpdateDate() {
        return (Date) getAttributeInternal(LASTUPDATEDATE);
    }

    /**
     * Sets <code>value</code> as attribute value for LAST_UPDATE_DATE using the alias name LastUpdateDate.
     * @param value value to set the LAST_UPDATE_DATE
     */
    public void setLastUpdateDate(Date value) {
        setAttributeInternal(LASTUPDATEDATE, value);
    }

    /**
     * Gets the attribute value for STATUS using the alias name Status.
     * @return the STATUS
     */
    public String getStatus() {
        return (String) getAttributeInternal(STATUS);
    }

    /**
     * Sets <code>value</code> as attribute value for STATUS using the alias name Status.
     * @param value value to set the STATUS
     */
    public void setStatus(String value) {
        setAttributeInternal(STATUS, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index,
                                           AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value,
                                         AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}
