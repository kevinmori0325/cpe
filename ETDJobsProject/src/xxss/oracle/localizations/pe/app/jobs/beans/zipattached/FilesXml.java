package xxss.oracle.localizations.pe.app.jobs.beans.zipattached;

import java.io.InputStream;

public class FilesXml {
    String fileName;
    String fileExt;
    InputStream fileXml;

    public FilesXml(){
        super();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFileXml(InputStream fileXml) {
        this.fileXml = fileXml;
    }

    public InputStream getFileXml() {
        return fileXml;
    }
}
