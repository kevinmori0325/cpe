package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import javax.xml.bind.annotation.XmlElement;

public class CPEInvLegalEntity {
    String registeredName;
    long registrationId;
    String registrationNumber;
    long partyId;
    String juridictionCode;
    String country;
    String registrationCodeLe;
    String city;
    String state;
    String address1;
    String postalCode;
    long legalEntityId;
    CPEInvHeader[] headers;
    
    public CPEInvLegalEntity() {
        super();
    }

    @XmlElement(name = "REGISTERED_NAME")
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    public String getRegisteredName() {
        return registeredName;
    }

    @XmlElement(name = "REGISTRATION_ID")
    public void setRegistrationId(long registrationId) {
        this.registrationId = registrationId;
    }

    public long getRegistrationId() {
        return registrationId;
    }

    @XmlElement(name = "REGISTRATION_NUMBER")
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @XmlElement(name = "PARTY_ID")
    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public long getPartyId() {
        return partyId;
    }

    @XmlElement(name = "JURISDICTION_CODE")
    public void setJuridictionCode(String juridictionCode) {
        this.juridictionCode = juridictionCode;
    }

    public String getJuridictionCode() {
        return juridictionCode;
    }

    @XmlElement(name = "COUNTRY")
    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @XmlElement(name = "REGISTRATION_CODE_LE")
    public void setRegistrationCodeLe(String registrationCodeLe) {
        this.registrationCodeLe = registrationCodeLe;
    }

    public String getRegistrationCodeLe() {
        return registrationCodeLe;
    }

    @XmlElement(name = "CITY")
    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    @XmlElement(name = "STATE")
    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    @XmlElement(name = "ADDRESS1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }

    @XmlElement(name = "POSTAL_CODE")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    @XmlElement(name = "LEGAL_ENTITY_ID")
    public void setLegalEntityId(long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public long getLegalEntityId() {
        return legalEntityId;
    }

    @XmlElement(name = "G_CAB")
    public void setHeaders(CPEInvHeader[] headers) {
        this.headers = headers;
    }

    public CPEInvHeader[] getHeaders() {
        return headers;
    }
}
