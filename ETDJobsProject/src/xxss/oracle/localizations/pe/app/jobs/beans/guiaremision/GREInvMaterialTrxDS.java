package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class GREInvMaterialTrxDS {
    GREInvTransactions[] materialTransactions;
        
    public GREInvMaterialTrxDS() {
        super();
    }

    @XmlElement(name = "G_TRX")
    public void setMaterialTransactions(GREInvTransactions[] materialTransactions) {
        this.materialTransactions = materialTransactions;
    }

    public GREInvTransactions[] getMaterialTransactions() {
        return materialTransactions;
    }
}
