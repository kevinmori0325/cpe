package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.File;

import java.sql.SQLException;

import oracle.jdbc.OracleConnection;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DeleteZipFiles extends XxssJob {
    public DeleteZipFiles() {
        super();
    }

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.implementClassName = CleanRequests.class.getName();
        super.execute(jobExecutionContext);
    }

    public void doAction(JobDataMap dataMap, OracleConnection conn) throws SQLException {
        // Method: Delete zip files that have been processed
        this.deleteZipFilesProcessed(conn);
    }

    private void deleteZipFilesProcessed(OracleConnection conn) {
        // For to iterate the fileNames
        String pathFileName = "";
        deleteZipFiles(pathFileName);
    }
    
    private void deleteZipFiles(String pathFileName){
        System.out.println("Deleting..." + pathFileName);
        File myObj = new File(pathFileName);
        if (myObj.delete()) { 
          System.out.println("Deleted the file: " + myObj.getName());
        } else {
          System.out.println("Failed to delete the file.");
        }
    }
}
