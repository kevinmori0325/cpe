package xxss.oracle.localizations.pe.app.jobs.beans.common;

public class FlexValueBean {
    String flexValue;
    String entityName;
    String voidFlag;
    String attribute15;
    String attribute14;
    String attribute13;
    String attribute12;
    String attribute11;
    String attribute10;
    
    public FlexValueBean() {
        super();
        attribute15 = "";
        attribute14 = "";
        attribute13 = "";
        attribute12 = "";
        attribute11 = "";
        attribute10 = "";
    }

    public void setFlexValue(String flexValue) {
        this.flexValue = flexValue;
    }

    public String getFlexValue() {
        return flexValue;
    }

    public void setAttribute15(String attribute15) {
        this.attribute15 = attribute15;
    }

    public String getAttribute15() {
        return attribute15;
    }

    public void setAttribute14(String attribute14) {
        this.attribute14 = attribute14;
    }

    public String getAttribute14() {
        return attribute14;
    }

    public void setAttribute13(String attribute13) {
        this.attribute13 = attribute13;
    }

    public String getAttribute13() {
        return attribute13;
    }

    public void setAttribute12(String attribute12) {
        this.attribute12 = attribute12;
    }

    public String getAttribute12() {
        return attribute12;
    }

    public void setAttribute11(String attribute11) {
        this.attribute11 = attribute11;
    }

    public String getAttribute11() {
        return attribute11;
    }

    public void setAttribute10(String attribute10) {
        this.attribute10 = attribute10;
    }

    public String getAttribute10() {
        return attribute10;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setVoidFlag(String voidFlag) {
        this.voidFlag = voidFlag;
    }

    public String getVoidFlag() {
        return voidFlag;
    }
}
