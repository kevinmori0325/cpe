package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GRELegalEntity {
    long legalEntityId;
    String registeredName;
    String registrationNumber;
    String country;
    String city;
    String state;
    String address1;
    String postalCode;
    GREBusinessUnit[] businessUnits;
    
    
    public GRELegalEntity() {
        super();
    }

    @XmlElement(name = "LEGAL_ENTITY_ID")
    public void setLegalEntityId(long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public long getLegalEntityId() {
        return legalEntityId;
    }

    @XmlElement(name = "REGISTERED_NAME")
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    public String getRegisteredName() {
        return registeredName;
    }

    @XmlElement(name = "REGISTRATION_NUMBER")
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @XmlElement(name = "COUNTRY")
    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @XmlElement(name = "G_BU")
    public void setBusinessUnits(GREBusinessUnit[] businessUnits) {
        this.businessUnits = businessUnits;
    }

    public GREBusinessUnit[] getBusinessUnits() {
        return businessUnits;
    }

    @XmlElement(name = "CITY")
    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    @XmlElement(name = "STATE")
    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    @XmlElement(name = "ADDRESS1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }

    @XmlElement(name = "POSTAL_CODE")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }
}
