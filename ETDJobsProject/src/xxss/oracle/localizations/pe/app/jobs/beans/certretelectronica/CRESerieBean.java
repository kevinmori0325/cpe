package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

public class CRESerieBean {
    String docSerie;
    String docNum;
    
    public CRESerieBean() {
        super();
    }

    public void setDocSerie(String docSerie) {
        this.docSerie = docSerie;
    }

    public String getDocSerie() {
        return docSerie;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getDocNum() {
        return docNum;
    }
}
