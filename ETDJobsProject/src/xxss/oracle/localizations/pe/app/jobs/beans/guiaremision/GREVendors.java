package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GREVendors {
    String repRegistrationNum;
    String tipoDocIdentProv;
    String nombreProv;
    String direccionProv;
    String ciudadProv;
    String estadoProv;
    String paisProv;
    String codigoPostalProv;
    long vendorId;
    long partyId;
    String segment1;
    
    public GREVendors() {
        super();
        ciudadProv = "";
        estadoProv = "";
        paisProv = "";
        codigoPostalProv = "";
        direccionProv = "";
        repRegistrationNum = "";
        nombreProv = "";
        tipoDocIdentProv = "";
    }

    @XmlElement(name = "REP_REGISTRATION_NUMBER")
    public void setRepRegistrationNum(String repRegistrationNum) {
        this.repRegistrationNum = repRegistrationNum;
    }

    public String getRepRegistrationNum() {
        return repRegistrationNum;
    }

    @XmlElement(name = "TIPO_DOC_IDENT_PROV")
    public void setTipoDocIdentProv(String tipoDocIdentProv) {
        this.tipoDocIdentProv = tipoDocIdentProv;
    }

    public String getTipoDocIdentProv() {
        return tipoDocIdentProv;
    }

    @XmlElement(name = "NOMBRE_PROV")
    public void setNombreProv(String nombreProv) {
        this.nombreProv = nombreProv;
    }

    public String getNombreProv() {
        return nombreProv;
    }

    @XmlElement(name = "DIRECCION_PROV")
    public void setDireccionProv(String direccionProv) {
        this.direccionProv = direccionProv;
    }

    public String getDireccionProv() {
        return direccionProv;
    }

    @XmlElement(name = "CIUDAD_PROV")
    public void setCiudadProv(String ciudadProv) {
        this.ciudadProv = ciudadProv;
    }

    public String getCiudadProv() {
        return ciudadProv;
    }

    @XmlElement(name = "ESTADO_PROV")
    public void setEstadoProv(String estadoProv) {
        this.estadoProv = estadoProv;
    }

    public String getEstadoProv() {
        return estadoProv;
    }

    @XmlElement(name = "PAIS_PROV")
    public void setPaisProv(String paisProv) {
        this.paisProv = paisProv;
    }

    public String getPaisProv() {
        return paisProv;
    }

    @XmlElement(name = "CODIGO_POSTAL_PROV")
    public void setCodigoPostalProv(String codigoPostalProv) {
        this.codigoPostalProv = codigoPostalProv;
    }

    public String getCodigoPostalProv() {
        return codigoPostalProv;
    }

    @XmlElement(name = "VENDOR_ID")
    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }

    public long getVendorId() {
        return vendorId;
    }

    @XmlElement(name = "PARTY_ID")
    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public long getPartyId() {
        return partyId;
    }

    @XmlElement(name = "SEGMENT1")
    public void setSegment1(String segment1) {
        this.segment1 = segment1;
    }

    public String getSegment1() {
        return segment1;
    }
}
