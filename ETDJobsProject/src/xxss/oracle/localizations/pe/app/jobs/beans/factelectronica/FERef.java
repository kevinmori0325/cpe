package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;

public class FERef {
    String fechaEmision;
    String tipoDoc;
    String serieDoc;
    String numDoc;
    String codTipoNcNd;
    String razonDescripcion;
    
    public FERef() {
        super();
    }

    @XmlElement(name = "FECHA_EMISION")
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    @XmlElement(name = "DOC_TIPO")
    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    @XmlElement(name = "DOC_SERIE")
    public void setSerieDoc(String serieDoc) {
        this.serieDoc = serieDoc;
    }

    public String getSerieDoc() {
        return serieDoc;
    }

    @XmlElement(name = "DOC_NUM")
    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    @XmlElement(name = "COD_TIPO_NC_ND")
    public void setCodTipoNcNd(String codTipoNcNd) {
        this.codTipoNcNd = codTipoNcNd;
    }

    public String getCodTipoNcNd() {
        return codTipoNcNd;
    }

    @XmlElement(name = "RAZON_DESCRIP")
    public void setRazonDescripcion(String razonDescripcion) {
        this.razonDescripcion = razonDescripcion;
    }

    public String getRazonDescripcion() {
        return razonDescripcion;
    }
}
