package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import java.net.MalformedURLException;

import java.sql.SQLException;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.xml.sax.InputSource;

import xxss.oracle.localizations.pe.app.jobs.beans.factelectronica.PartyClassifications;
import xxss.oracle.localizations.pe.app.jobs.beans.factelectronica.PartyClassificationsDS;
import xxss.oracle.localizations.services.fusion.externalreportservice.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil;

@DisallowConcurrentExecution
public class SyncPartyClassificationsNewJob extends XxssJob {
    public SyncPartyClassificationsNewJob() {
        super();
    }
    
    //CREATE BY KMORI 27/02/2019
    
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.implementClassName = SyncPartyClassificationsNewJob.class.getName();
        super.execute(jobExecutionContext);
    }

    
    public void doAction(JobDataMap dataMap, OracleConnection conn)  throws Exception {
        
        String userName = dataMap.getString("USER_NAME"); 
        String userPass = dataMap.getString("USER_PASS"); 
        String instanceName = dataMap.getString("INSTANCE_NAME"); 
        String dataCenter = dataMap.getString("DATA_CENTER"); 

        String partyClassifXMLPath = dataMap.getString("PARTY_CLASSIF_DATA_PATH");
        
        byte[] reportOutputBytes;
        InputStream reportIs;
        JAXBContext jaxbContextDataDS;
        Unmarshaller jaxbUnmarshaller;
        
        ExternalReportWSSService svcReport = ExternalReportWSUtil.createExternalReportWSSServiceUserToken(instanceName, dataCenter, userName,
                                                                                 userPass);
        
        if (svcReport != null) {
            logWithTimestamp("ExternalReportWS created");
        } else {
            throw new Exception("ExternalReportWS is not created.");
        }
        
        //Extraccion de Party  Classifications
        reportOutputBytes = null;
        reportOutputBytes = this.runReport(svcReport, partyClassifXMLPath);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(partyClassifXMLPath + " report cannot be executed.");
        }
        
        logWithTimestamp(partyClassifXMLPath + " report was executed. " + reportOutputBytes.length);

        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(PartyClassificationsDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        PartyClassificationsDS materialTrxDS = (PartyClassificationsDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));

        this.processPartyClassifData( materialTrxDS, conn);

        conn.commit();
        
    }
    
    private void processPartyClassifData(PartyClassificationsDS dataDS, OracleConnection conn) throws SQLException
                                                                                             {
        PartyClassifications[] txns = dataDS.getPartyClassifications();
               
        if (txns != null) {
            for (int i=0; i<txns.length; i++) {
                this.callSyncMaterialTransactions(txns[i], conn);              
            }
            

        }        
    }
    
    private void callSyncMaterialTransactions(PartyClassifications transaction, OracleConnection trans) throws SQLException {
        String st = "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_sync_classifications(?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, transaction.getPartyClassifications());
        acs.setString(2, transaction.getTypeCode());
        acs.setString(3, transaction.getTypeName());
       
       

        acs.executeUpdate();
        acs.close();
    }
    
    private byte[] runReport(ExternalReportWSSService svc, String reportPath) throws MalformedURLException, Exception {

        xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportRequest req =
            new xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfParamNameValue arrayParam =
            new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        req.setAttributeLocale("Spanish (Peru)");
        req.setParameterNameValues(arrayParam);
        req.setReportAbsolutePath(reportPath);
        req.setSizeOfDataChunkDownload(-1);
        req.setAttributeFormat("xml");

        xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportResponse resp =
            svc.runReport(req, "");

        return (resp.getReportBytes());

    }
}
