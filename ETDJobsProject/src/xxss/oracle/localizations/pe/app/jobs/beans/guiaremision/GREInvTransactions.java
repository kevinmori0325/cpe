package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class GREInvTransactions {
    long transactionId;
    long organizationId;
    long transactionTypeId;
    double transactionQuantity;
    double primaryQuantity;
    Date transactionDate;
    long inventoryItemId;
    String greType;
    String itemNumber;
    String itemDescription;
    double unitWeight;
    String transactionUom;
    String weightUom;
    String salesOrderNumber;
    String transactionSetId;
    long customerPartyId;
    String receiptNumber;
    String poNumber;
    long vendorId;
    String isoUomCode;
    String isoWeightUomCoude;
    String primaryUomCode;
    String shipmentNumber;
    String shipmentToAddress;
    long transferOrganizationId;
    String transferOrganizationCode;
    String transferOrganizationName;
    String transferOrder;
    String asn;
    String clienteOrgDest;
    double envases;
    double piezas;
    double precioUnitario;
    double precioTotal;
    String uomSalesOrder;
    Date requestDate;
    
    public GREInvTransactions() {
        super();
        weightUom = "";
        salesOrderNumber = "";
        transactionSetId = "";
        receiptNumber = "";
        poNumber = "";
    }

    @XmlElement(name = "TRANSACTION_ID")
    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public long getTransactionId() {
        return transactionId;
    }

    @XmlElement(name = "ORGANIZATION_ID")
    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    @XmlElement(name = "TRANSACTION_TYPE_ID")
    public void setTransactionTypeId(long transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public long getTransactionTypeId() {
        return transactionTypeId;
    }

    @XmlElement(name = "TRANSACTION_QUANTITY")
    public void setTransactionQuantity(double transactionQuantity) {
        this.transactionQuantity = transactionQuantity;
    }

    public double getTransactionQuantity() {
        return transactionQuantity;
    }

    @XmlElement(name = "PRIMARY_QUANTITY")
    public void setPrimaryQuantity(double primaryQuantity) {
        this.primaryQuantity = primaryQuantity;
    }

    public double getPrimaryQuantity() {
        return primaryQuantity;
    }

    @XmlElement(name = "TRANSACTION_DATE")
    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    @XmlElement(name = "INVENTORY_ITEM_ID")
    public void setInventoryItemId(long inventoryItemId) {
        this.inventoryItemId = inventoryItemId;
    }

    public long getInventoryItemId() {
        return inventoryItemId;
    }

    @XmlElement(name = "GRE_TYPE")
    public void setGreType(String greType) {
        this.greType = greType;
    }

    public String getGreType() {
        return greType;
    }

    @XmlElement(name = "ITEM_NUMBER")
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    @XmlElement(name = "ITEM_DESCRIPTION")
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    @XmlElement(name = "UNIT_WEIGHT")
    public void setUnitWeight(double unitWeight) {
        this.unitWeight = unitWeight;
    }

    public double getUnitWeight() {
        return unitWeight;
    }

    @XmlElement(name = "TRX_UNIT_OF_MEASURE")
    public void setTransactionUom(String transactionUom) {
        this.transactionUom = transactionUom;
    }

    public String getTransactionUom() {
        return transactionUom;
    }

    @XmlElement(name = "WEIGHT_UNIT_OF_MEASURE")
    public void setWeightUom(String weightUom) {
        this.weightUom = weightUom;
    }

    public String getWeightUom() {
        return weightUom;
    }

    @XmlElement(name = "SALES_ORDER_NUMBER")
    public void setSalesOrderNumber(String salesOrderNumber) {
        this.salesOrderNumber = salesOrderNumber;
    }

    public String getSalesOrderNumber() {
        return salesOrderNumber;
    }

    @XmlElement(name = "TRANSACTION_SET_ID")
    public void setTransactionSetId(String transactionSetId) {
        this.transactionSetId = transactionSetId;
    }

    public String getTransactionSetId() {
        return transactionSetId;
    }

    @XmlElement(name = "CUSTOMER_PARTY_ID")
    public void setCustomerPartyId(long customerPartyId) {
        this.customerPartyId = customerPartyId;
    }

    public long getCustomerPartyId() {
        return customerPartyId;
    }

    @XmlElement(name = "RECEIPT_NUM")
    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    @XmlElement(name = "PO_NUMBER")
    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    @XmlElement(name = "VENDOR_ID")
    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }

    public long getVendorId() {
        return vendorId;
    }

    @XmlElement(name = "ISO_UOM_CODE")
    public void setIsoUomCode(String isoUomCode) {
        this.isoUomCode = isoUomCode;
    }

    public String getIsoUomCode() {
        return isoUomCode;
    }

    @XmlElement(name = "ISO_WEIGHT_UOM_CODE")
    public void setIsoWeightUomCoude(String isoWeightUomCoude) {
        this.isoWeightUomCoude = isoWeightUomCoude;
    }

    public String getIsoWeightUomCoude() {
        return isoWeightUomCoude;
    }

    @XmlElement(name = "PRIMARY_UNIT_OF_MEASURE")
    public void setPrimaryUomCode(String primaryUomCode) {
        this.primaryUomCode = primaryUomCode;
    }

    public String getPrimaryUomCode() {
        return primaryUomCode;
    }

    @XmlElement(name = "SHIPMENT_NUMBER")
    public void setShipmentNumber(String shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getShipmentNumber() {
        return shipmentNumber;
    }

    @XmlElement(name = "SHIP_TO_ADDRESS")
    public void setShipmentToAddress(String shipmentToAddress) {
        this.shipmentToAddress = shipmentToAddress;
    }

    public String getShipmentToAddress() {
        return shipmentToAddress;
    }
    
    @XmlElement(name = "TRANSFER_ORGANIZATION_ID")
    public void setTransferOrganizationId(long transferOrganizationId) {
        this.transferOrganizationId = transferOrganizationId;
    }

    public long getTransferOrganizationId() {
        return transferOrganizationId;
    }

    @XmlElement(name = "TRANSFER_ORGANIZATION_CODE")
    public void setTransferOrganizationCode(String transferOrganizationCode) {
        this.transferOrganizationCode = transferOrganizationCode;
    }

    public String getTransferOrganizationCode() {
        return transferOrganizationCode;
    }

    @XmlElement(name = "TRANSFER_ORGANIZATION_NAME")
    public void setTransferOrganizationName(String transferOrganizationName) {
        this.transferOrganizationName = transferOrganizationName;
    }

    public String getTransferOrganizationName() {
        return transferOrganizationName;
    }

    @XmlElement(name = "TRANSFER_ORDER")
    public void setTransferOrder(String transferOrder) {
        this.transferOrder = transferOrder;
    }

    public String getTransferOrder() {
        return transferOrder;
    }

    @XmlElement(name = "ASN")
    public void setAsn(String asn) {
        this.asn = asn;
    }

    public String getAsn() {
        return asn;
    }

    @XmlElement(name = "CLIENTE_ORG_DEST")
    public void setClienteOrgDest(String clienteOrgDest) {
        this.clienteOrgDest = clienteOrgDest;
    }

    public String getClienteOrgDest() {
        return clienteOrgDest;
    }

    @XmlElement(name = "ENVASES")
    public void setEnvases(double envases) {
        this.envases = envases;
    }

    public double getEnvases() {
        return envases;
    }

    @XmlElement(name = "PIEZAS")
    public void setPiezas(double piezas) {
        this.piezas = piezas;
    }

    public double getPiezas() {
        return piezas;
    }
    
    @XmlElement(name = "PRECIO_UNITARIO")
    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    @XmlElement(name = "PRECIO_TOTAL")
    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    @XmlElement(name = "UOM_SALES_ORDER")
    public void setUomSalesOrder(String uomSalesOrder) {
        this.uomSalesOrder = uomSalesOrder;
    }

    public String getUomSalesOrder() {
        return uomSalesOrder;
    }

    @XmlElement(name = "REQUEST_DATE")
    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

}
