package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.PasswordAuthentication;
import java.net.URL;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;

import oracle.jdbc.OracleResultSet;

import org.apache.commons.io.IOUtils;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.xml.sax.InputSource;

import xxss.oracle.localizations.pe.app.jobs.beans.cpe.CPESyncDS;
import xxss.oracle.localizations.pe.app.jobs.beans.cpe.RespuestaErrorUploadAp;
import xxss.oracle.localizations.pe.app.jobs.beans.cpe.RespuestaImportAP;
import xxss.oracle.localizations.pe.app.jobs.beans.cpe.RespuestaUploadAP;
import xxss.oracle.localizations.pe.app.jobs.beans.factelectronica.FEDataDS;
import xxss.oracle.localizations.services.fusion.erpintegrationservice.proxy.ErpIntegrationService;
import xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.util.ErpIntegrationServiceUtil;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil3;


@DisallowConcurrentExecution
public class CargaComprobanteElectronico extends XxssJob {
    
    private SimpleDateFormat targetDateFormat;
    public CargaComprobanteElectronico() {
        super();
        this.targetDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }
    
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.implementClassName = CargaComprobanteElectronico.class.getName();
        super.execute(jobExecutionContext);
    }


    @Override
    public void doAction(JobDataMap dataMap, OracleConnection conn) throws Exception {
        // TODO Implement this method
        
        String userName = dataMap.getString("USER_NAME");
        String userPass = dataMap.getString("USER_PASS");
        String instanceName = dataMap.getString("INSTANCE_NAME");
        String dataCenter = dataMap.getString("DATA_CENTER");
        String reportPath = dataMap.getString("REPORT_PATH");
        String soapActionUpLoad = dataMap.getString("SOAP_ACTION_WS_UP");
        String soapActionImport = dataMap.getString("SOAP_ACTION_WS_IMP");
        String urlWsInterface = dataMap.getString("URL_WS_INTERFACE");
        String urlUpdate = dataMap.getString("URL_WS_UPDATE");
        String isPurge = dataMap.getString("IS_PURGE");
        
        
        ExternalReportWSSService svcReport = ExternalReportWSUtil3.createExternalReportWSSServiceUserToken3(instanceName, dataCenter, userName,
                                                                                 userPass);
        
        if (svcReport != null) {
            logWithTimestamp("ExternalReportWS created");
        } else {
            throw new Exception("ExternalReportWS is not created.");
        }
        
        ErpIntegrationService svcErpIntegration = ErpIntegrationServiceUtil.createIntegrationServiceUser(userName, userPass, instanceName, dataCenter);

        if (svcErpIntegration != null) {
            this.logWithTimestamp("ErpIntegrationService created");
        } else {
            this.logWithTimestamp("ErpIntegrationSer+vice is not created.");
        }

        
        this.logWithTimestamp(soapActionUpLoad);
        this.logWithTimestamp(soapActionImport);
        this.logWithTimestamp(urlWsInterface);
        this.logWithTimestamp(urlUpdate);
        
       /* String xmlImport = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:submitInvoiceImport>\n" + 
        "         <typ:ledgerId>"+"123"+"</typ:ledgerId>\n" + 
        "         <typ:orgId>"+"1234"+"</typ:orgId>\n" + 
        "         <typ:importSet>"+""+"</typ:importSet>\n" + 
        "         <typ:groupName>"+""+"</typ:groupName>\n" + 
        "         <typ:accountingDate>"+""+"</typ:accountingDate>\n" + 
        "         <typ:hold>"+""+"</typ:hold>\n" + 
        "         <typ:holdReason>"+""+"</typ:holdReason>\n" + 
        "         <typ:summarizeReport>"+""+"</typ:summarizeReport>\n" + 
        "      </typ:submitInvoiceImport>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>";
        this.logWithTimestamp(xmlImport);
        
        String soap = this.executeApInterfaceWs(xmlImport,
                                    urlWsInterface,
                                    userName,
                                    userPass,
                                    soapActionImport);*/
        
        this.sendInterfaceAp(userName, userPass, instanceName, dataCenter, urlWsInterface, soapActionUpLoad, soapActionImport, urlUpdate, reportPath, isPurge, svcReport,svcErpIntegration,  conn);

        /*byte[] reportOutputBytes = runReport(svcReport, reportPath);
        if ((reportOutputBytes == null) || (reportOutputBytes.length == 0)) {
            throw new Exception("FE AR Pending Invoice XML report cannot be executed.");
        }


        InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);

        JAXBContext jaxbContextDataDS = JAXBContext.newInstance(new Class[] { FEDataDS.class });
        Unmarshaller jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        FEDataDS dataDS = (FEDataDS) jaxbUnmarshaller.unmarshal(new InputSource(reportIs));*/
        

    }
    
    private void cargaComprobanteElec(){
        
        
        
        
    }
    
    public  void updateInvoices(String status, OracleConnection trans) {
        try {
            String st = "begin NC_CPE_CARGA_FACTURAS_PKG.PR_UPDATE_INVOICES(pv_param1 => ?); end;";
            OracleCallableStatement acs = (OracleCallableStatement) trans.prepareCall(st);

            acs.setString(1, status);
        

            acs.executeUpdate();
            acs.close();

    

        } catch (SQLException exsql) {
            this.logWithTimestamp("Error SQLException: " + exsql.getMessage());
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage()+" - "+Arrays.toString(exsql.getStackTrace()));
        } catch (Exception e) {
            this.logWithTimestamp("Error Exception: " + e.getMessage());
            System.out.println("Error en readReportOutput() - " + e.getMessage()+" - "+Arrays.toString(e.getStackTrace()));
        }

    }

    
    public String sendInterfaceAp(String userName, String pass, String instanceName, String dataCenter, String url,
                                  String soapActionUpLoad, String soapActionImport, String urlUpdate, String reportPath,
                                  String isPurge, ExternalReportWSSService svcReport, ErpIntegrationService svcErpIntegration,
                                  OracleConnection trans) 
     {

     String soap = "";
     String requestId ="";
     String message = "";
     boolean dataExist = false;
     SimpleDateFormat paramDateFormat = new SimpleDateFormat("yyyy-MM-dd");
     
     try {
        
        
        String XmlLines = "";
        long orgId = 0;
        long ledgerId = 0;
        long   cpeInvoiceId= 0;
        String poNumberCab = "";
        
        this.logWithTimestamp("Inicio send Interface");
        
       
        //Proceso que actualiza a en proceso los que estan en FULL y ERROR_INTERFACE
        this.updateInvoices("IN_PROCESS",trans);
        
        String lquery = "select CPE_INVOICE_ID,\n" +
                                        "       SOURCE,\n" +
                                        "       ORG_ID,\n" +
                                        "       LEDGER_ID,\n" +
                                        "       BU_NAME,\n"+
                                        "       VENDOR_ID,\n" +
                                        "       VENDOR_NAME,\n"+
                                        "       VENDOR_SITE_CODE,\n" +
                                        "       ATTRIBUTE14,\n" +
                                        "       ATTRIBUTE15,\n" +
                                        "       INVOICE_TYPE,\n"+
                                        "       ATTRIBUTE_CATEGORY,\n" +
                                        "       DOCUMENT_SUBTYPE,\n" +
                                        "       EXCHANGE_DATE,\n" +
                                        "       EXCHANGE_RATE_TYPE,\n" +
                                        "       GOODS_RECEIVED_DATE,\n" +
                                        "       INVOICE_AMOUNT,\n" +
                                        "       INVOICE_DATE,\n" +
                                        "       INVOICE_CURRENCY_CODE,\n" +
                                        "       INVOICE_NUMBER,\n" +
                                        "       PO_NUMBER,\n" +
                                        "       INVOICE_RECEIVED_DATE,\n" +
                                        "       INVOICE_TYPE_LOOKUP_CODE,\n" +
                                        "       LEGAL_ENTITY_NAME,\n" +
                                        "       LEGAL_ENTITY_ID,\n" +
                                        "       PDF_CLOB,\n" +
                                        "       STATUS\n" +
                                        "  from NC_CPE_AP_INVOICE_HEADERS_ALL \n" +
                                        " where 1 = 1\n" +
                                        " and STATUS IN ('IN_PROCESS')\n" +
                                        " order by INVOICE_NUMBER";
        //IN ('FULL','ERROR_INT')
        
        ResultSet resultado;

                OraclePreparedStatement lStmt;
        
            lStmt =
                (OraclePreparedStatement) trans.prepareStatement(lquery, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                 OracleResultSet.CONCUR_READ_ONLY);
        
        //lStmt.setString(1, pIdProcess);
                resultado = lStmt.executeQuery();
                
                
        while (resultado.next()) {
            
            this.logWithTimestamp("While Cab");

                    //String message    = "";
                    String status     = "";
                    //int    lineNumber = 0; 
                    dataExist         = true;
                    
                    cpeInvoiceId             = resultado.getLong  ("CPE_INVOICE_ID");
                    String source                   = resultado.getString("SOURCE");
                    orgId                    = resultado.getLong("ORG_ID");
                    ledgerId                 = resultado.getLong("LEDGER_ID");
                    String buName                   = resultado.getNString("BU_NAME");
                    double vendorId                 = resultado.getDouble("VENDOR_ID");
                    String vendorName               = resultado.getString("VENDOR_NAME");
                    String vendorSiteCode           = resultado.getString("VENDOR_SITE_CODE");
                    String attribute14              = resultado.getString("ATTRIBUTE14");
                    String attribute15              = resultado.getString("ATTRIBUTE15");
                    String attributeCategory        = resultado.getString("ATTRIBUTE_CATEGORY");
                    String documentSubtype          = resultado.getString("DOCUMENT_SUBTYPE");
                    Date   exchangeDate             = resultado.getDate  ("EXCHANGE_DATE");
                    String exchangeRateType         = resultado.getString("EXCHANGE_RATE_TYPE");
                    Date   goodsReceivedDate        = resultado.getDate  ("GOODS_RECEIVED_DATE");
                    double invoiceAmount            = resultado.getDouble("INVOICE_AMOUNT");
                    Date   invoiceDate              = resultado.getDate  ("INVOICE_DATE");
                    String invoiceCurrencyCode      = resultado.getString("INVOICE_CURRENCY_CODE");
                    String invoiceNumber            = resultado.getString("INVOICE_NUMBER");
                    Date   invoiceReceivedDate      = resultado.getDate  ("INVOICE_RECEIVED_DATE");
                    String invoiceTypeLookupCode    = resultado.getString("INVOICE_TYPE_LOOKUP_CODE");
                    String legalEntityName          = resultado.getString("LEGAL_ENTITY_NAME");
                    long legalEntityId            = resultado.getLong("LEGAL_ENTITY_ID");
                    String pdfClob                  = resultado.getString("PDF_CLOB");
                    status                          = resultado.getString("STATUS");
                    String invoiceType              = resultado.getString("INVOICE_TYPE");
                    poNumberCab                     = resultado.getString("PO_NUMBER");
            
        //this.callUpdateStatus("IN_PROCESS", ""  ,"", 0, "", "", "", 0, urlUpdate+cpeInvoiceId);   
            
        this.executePurgeInterfaceTable(String.valueOf(cpeInvoiceId), reportPath, isPurge, svcErpIntegration, svcReport);    
        
        //this.purgeTableInterface(String.valueOf(cpeInvoiceId), svcErpIntegration);    
            
        String lqueryDet =  "select CPE_INVOICE_ID,\n" +
                                            "       CPE_INVOICE_LINE_ID,\n" +
                                            "       AMOUNT,\n" +
                                            "       LINE_TYPE_LOOKUP_CODE,\n" +
                                            "       LINE_NUMBER,\n" +
                                            "       PO_NUMBER,\n" +
                                            "       PO_LINE_NUMBER,\n" +
                                            "       INVOICE_QUANTITY,\n" +
                                            "       RECEIPT_LINE_NUMBER,\n" +
                                            "       RECEIPT_NUMBER,\n" +
                                            "       TAX_CLASSIFICATION_CODE,\n" +
                                            "       CURRENCY_CODE,\n" +
                                            "       PRODUCT_TYPE,\n" +
                                            "       USER_DEFINED_FISC_CLASS,\n" +
                                            "       UNIT_CODE\n"+
                                            "  from NC_CPE_AP_INVOICE_LINES_ALL \n" +
                                            " where CPE_INVOICE_ID = ? " ;

        ResultSet resultadoDet = null;

                OraclePreparedStatement lStmtDet =
                    (OraclePreparedStatement)trans.prepareStatement(lqueryDet, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                    OracleResultSet.CONCUR_READ_ONLY);
                lStmtDet.setLong  (1, cpeInvoiceId);
                resultadoDet = lStmtDet.executeQuery();
            
        StringBuffer  strBuffer = new StringBuffer(); ;   
        while (resultadoDet.next()) {

                    //String message    = "";
                    //int    lineNumber = 0; 
                    //dataExist         = true;
            
                    
            
                    this.logWithTimestamp("While Det");
                    
                    //long   cpeInvoiceId             = resultadoDet.getLong  ("CPE_INVOICE_ID");
                    //long cpeInvoiceLineId         = resultadoDet.getLong("CPE_INVOICE_LINE_ID");
                    double amount                   = resultadoDet.getDouble("AMOUNT");
                    String lineTypeLookupCode       = resultadoDet.getString("LINE_TYPE_LOOKUP_CODE");
                    int lineNumber               = resultadoDet.getInt("LINE_NUMBER");
                    String poNumber                 = resultadoDet.getString("PO_NUMBER");
                    int poLineNumber             = resultadoDet.getInt("PO_LINE_NUMBER");
                    int invoiceQuantity             = resultadoDet.getInt("INVOICE_QUANTITY");
                    int receiptLineNumber        = resultadoDet.getInt("RECEIPT_LINE_NUMBER");
                    String receiptNumber            = resultadoDet.getString("RECEIPT_NUMBER");
                    String taxClassificationCode    = resultadoDet.getString("TAX_CLASSIFICATION_CODE");
                    String currencyCode             = resultadoDet.getString("CURRENCY_CODE");
                    String productType              = resultadoDet.getString("PRODUCT_TYPE");
                    String userDefinedFiscClass     = resultadoDet.getString("USER_DEFINED_FISC_CLASS");
                    String unitCode                 = resultadoDet.getString("UNIT_CODE");
            
                
            
                 XmlLines = "<inv:InvoiceInterfaceLine>\n" + 
                            "               <inv:Amount currencyCode=\""+currencyCode+"\">"+amount+"</inv:Amount>\n" + 
                            "               <inv:LineTypeLookupCode>"+lineTypeLookupCode+"</inv:LineTypeLookupCode>\n" + 
                            "               <inv:LineNumber>"+lineNumber+"</inv:LineNumber>\n"+
                            "               <inv:PONumber>"+poNumber+"</inv:PONumber>\n" + 
                            "               <inv:POLineNumber>"+poLineNumber+"</inv:POLineNumber>\n" + 
                            "               <inv:ProductType>"+productType+"</inv:ProductType>              \n" + 
                            "               <inv:InvoicedQuantity unitCode=\""+""+"\">"+invoiceQuantity+"</inv:InvoicedQuantity>\n" + 
                            "               <inv:ReceiptLineNumber>"+receiptLineNumber+"</inv:ReceiptLineNumber>\n" + 
                            "               <inv:ReceiptNumber>"+receiptNumber+"</inv:ReceiptNumber>              \n" + 
                            "               <inv:TaxClassificationCode>"+taxClassificationCode+"</inv:TaxClassificationCode>              \n" + 
                            "               <inv:UserDefinedFiscClass>"+userDefinedFiscClass+"</inv:UserDefinedFiscClass>      \n" + 
                            "            </inv:InvoiceInterfaceLine>\n";
            
            
                
               // XmlLines = XmlLines + XmlLines;
               strBuffer.append(XmlLines);
            
            }
            
            lStmtDet.close();
            
            String xmlPoNum = "";
            String xmlExchangeRate = "";
            
            if(!"".equals(poNumberCab) && poNumberCab != null){
                
                this.logWithTimestamp(poNumberCab);
                
             xmlPoNum =  "<inv:PONumber>"+poNumberCab+"</inv:PONumber>\n";
                
            }
            
            if(!"PEN".equals(invoiceCurrencyCode)){
            xmlExchangeRate = "<inv:ExchangeDate>"+this.targetDateFormat.format(exchangeDate)+"</inv:ExchangeDate>\n" + 
            "<inv:ExchangeRateType>"+exchangeRateType+"</inv:ExchangeRateType>\n";
            
            }
            
            String xmlInput =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\" xmlns:inv=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/\">\n" + 
                "   <soapenv:Header/>\n" + 
                "   <soapenv:Body>\n" + 
                "      <typ:createInvoiceInterfaceWithAttachments>\n" + 
                "         <typ:invoiceInterfaceHeader>\n" + 
                xmlPoNum+
                "            <inv:Source>"+source+"</inv:Source>\n" + 
                "            <inv:OperatingUnit>"+buName+"</inv:OperatingUnit>\n" + 
                "            <inv:VendorName>"+vendorName+"</inv:VendorName>\n" + 
                "            <inv:VendorSiteCode>"+vendorSiteCode+"</inv:VendorSiteCode>   \n" +         
                "            <inv:Attribute14>"+attribute14+"</inv:Attribute14>\n" + 
                "            <inv:Attribute15>"+attribute15+"</inv:Attribute15>          \n" + 
                "            <inv:AttributeCategory>"+attributeCategory+"</inv:AttributeCategory>           \n" + 
                "            <inv:DocumentSubType>"+documentSubtype+"</inv:DocumentSubType>\n" + 
               xmlExchangeRate+
                "            <inv:InvoiceAmount currencyCode=\""+invoiceCurrencyCode+"\">"+invoiceAmount+"</inv:InvoiceAmount>\n" + 
                "            <inv:InvoiceCurrencyCode>"+invoiceCurrencyCode+"</inv:InvoiceCurrencyCode>\n" + 
                "            <inv:InvoiceDate>"+invoiceDate+"</inv:InvoiceDate>\n" + 
                "            <inv:InvoiceNumber>"+invoiceNumber+"</inv:InvoiceNumber>\n" + 
                "            <inv:InvoiceReceivedDate>"+this.targetDateFormat.format(invoiceReceivedDate)+"</inv:InvoiceReceivedDate>\n" + 
                "            <inv:InvoiceTypeLookupCode>"+invoiceType+"</inv:InvoiceTypeLookupCode>\n" + 
                "            <inv:LegalEntityName>"+legalEntityName+"</inv:LegalEntityName>\n" + 
                "            <inv:LoadRequestId>"+cpeInvoiceId+"</inv:LoadRequestId>\n" +
                strBuffer.toString()+
                "         </typ:invoiceInterfaceHeader>\n" + 
                "      </typ:createInvoiceInterfaceWithAttachments>\n" + 
                "   </soapenv:Body>\n" + 
                "</soapenv:Envelope>\n";
            
            /*
            "<inv:attachments>\n" + 
            "               <inv:AttachmentType>"+"FILE"+"</inv:AttachmentType>\n" + 
            "               <inv:Category>"+"AP_SUPPORTING_DOC"+"</inv:Category>\n" + 
            "               <inv:Title>"+invoiceNumber+".pdf"+"</inv:Title>\n" + 
            "               <inv:Content>"+pdfClob+"</inv:Content>\n" + 
            "</inv:attachments>"+  
             
              */

            this.logWithTimestamp(xmlInput);
            

            soap = this.executeApInterfaceWs(xmlInput, url, userName, pass, soapActionUpLoad,urlUpdate+cpeInvoiceId);
            
            this.logWithTimestamp(soap); 
            
            if(!"".equals(soap)){

            String[] split =   soap.split("<env:Body>");
            String xml1 = split[0];
            String xml0 = soap.replace(xml1, "");
            String[] split2 =   xml0.split("</env:Body>");
            String xml2 = split2[0];
            String pay = xml2.replace("<env:Body>", "");
            
            String payload =  pay.replace(" xmlns:ns0=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\"","");
            String payload2 = payload.replace(" xmlns:ns2=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\" xmlns:ns1=\"http://xmlns.oracle.com/adf/svc/types/\" xmlns:tns=\"http://xmlns.oracle.com/adf/svc/errors/\" xmlns:ns0=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns0:InterfaceResponseResult\"","");
            String payload3 = payload2.replace("ns0:","");
            String payload4 = payload3.replace("ns2:","");
            String payload5 = payload4.replace(" xsi:nil=\"true\"","");
            
            this.logWithTimestamp(payload5);
            
            RespuestaUploadAP respuestaUploadAp = new RespuestaUploadAP();

            JAXBContext jaxbContextProcResult = JAXBContext.newInstance(RespuestaUploadAP.class);
            Unmarshaller jaxbUnmarshallerProcResult = jaxbContextProcResult.createUnmarshaller();

            InputStream in = IOUtils.toInputStream(payload5, "UTF-8");
            respuestaUploadAp = (RespuestaUploadAP) jaxbUnmarshallerProcResult.unmarshal(new InputSource(in));
            
            if(!(respuestaUploadAp.getResult() != null)){
               
                this.logWithTimestamp("Entro if");
                
             return "";
            }
            
            }else{
                    this.logWithTimestamp("Entro else");
                }
         

                   
        }
         
        lStmt.close(); 
         
        if (!dataExist) {
           logWithTimestamp("No hay filas para procesar.");
           return "E";   
        }
         
         
        
        String xmlImport = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:submitInvoiceImport>\n" + 
        "         <typ:ledgerId>"+ledgerId+"</typ:ledgerId>\n" + 
        "         <typ:orgId>"+orgId+"</typ:orgId>\n" + 
        "         <typ:importSet>"+""+"</typ:importSet>\n" + 
        "         <typ:groupName>"+""+"</typ:groupName>\n" + 
        "         <typ:accountingDate>"+paramDateFormat.format(new Date())+"</typ:accountingDate>\n" + 
        "         <typ:hold>"+""+"</typ:hold>\n" + 
        "         <typ:holdReason>"+""+"</typ:holdReason>\n" + 
        "         <typ:summarizeReport>"+""+"</typ:summarizeReport>\n" + 
        "      </typ:submitInvoiceImport>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>";
         
        this.logWithTimestamp(xmlImport);
        
        soap = this.executeApInterfaceWs(xmlImport,
                                    url,
                                    userName,
                                    pass,
                                    soapActionImport,
                                    urlUpdate+cpeInvoiceId     
                                    );
         
        String[] split =   soap.split("<env:Body>");
        String xml1 = split[0];
        String xml0 = soap.replace(xml1, "");
        String[] split2 =   xml0.split("</env:Body>");
        String xml2 = split2[0];
            
        String xml = xml2.replace("<env:Body>", "");
        String payload = xml.replace(" xmlns:ns0=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\"","");
        String payload2 = payload.replace(" xmlns=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\"","");

        String payload3 = payload2.replace("ns0:","");
        this.logWithTimestamp(payload3);
            

            
        RespuestaImportAP respuestaImportAp = new RespuestaImportAP();

        JAXBContext jaxbContextProcResult = JAXBContext.newInstance(RespuestaImportAP.class);
        Unmarshaller jaxbUnmarshallerProcResult = jaxbContextProcResult.createUnmarshaller();


        InputStream in = IOUtils.toInputStream(payload3, "UTF-8");
        respuestaImportAp = (RespuestaImportAP) jaxbUnmarshallerProcResult.unmarshal(new InputSource(in));
         

               
        String[] splitRe = respuestaImportAp.getResult().split("=");
         
         if(splitRe[0].equals("requestId")){
             
             requestId = splitRe[1]; 
             
             this.logWithTimestamp("Request Id: "+requestId);
             
             this.updateInvoices("EXECUTE_IMPORT",trans);
             //this.updateStatusXInvoice(urlUpdate, requestId, trans);
            // this.callUpdateStatus("EXECUTE_IMPORT", ""  ,"", Long.parseLong(requestId), "", "", "", 0, urlUpdate+cpeInvoiceId);    
             //this.callUpdateStatus("FULL", ""  ,requestId, 0, payload3, "", "", 0, urlUpdate);
                
         }else{
             
             
            this.logWithTimestamp("Request Error: "+respuestaImportAp.getResult()); 
             
            this.callUpdateStatus("ERROR_LOAD", "ERROR"  ,respuestaImportAp.getResult(), 0, payload3, "", "", 0, urlUpdate);
             
         } 
        
         

        this.logWithTimestamp("Fin");
        
        
        
        } catch (SQLException e) {    
         
            message = "Error SQLException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
            this.logWithTimestamp(message);
         
       }  catch (MalformedURLException e) {
         
            message = "Error MalformedURLException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
            this.logWithTimestamp(message);
         
        } catch (IOException e) {
         
            message = "Error IOException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
            this.logWithTimestamp(message);
         
        } catch (JAXBException e) {
                       
            message = "Error JAXBException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
            this.logWithTimestamp(message);
        } catch (NullPointerException e) {
                           
                message = "Error NullPointerException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
                this.logWithTimestamp(message);
        } catch (Exception e) {
                               
               message = "Error Exception: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
               this.logWithTimestamp(message);     
        }finally{
         
         
         
        
        }
        

        
        return soap;
    }
    
    public void updateStatusXInvoice(String urlUpdate,String requestId, OracleConnection trans) throws SQLException,
                                                                                                  MalformedURLException,
                                                                                                  IOException {
        
        String lquery = "select CPE_INVOICE_ID,\n" +
                                        "       SOURCE,\n" +
                                        "       ORG_ID,\n" +
                                        "       LEDGER_ID,\n" +
                                        "       BU_NAME,\n"+
                                        "       VENDOR_ID,\n" +
                                        "       VENDOR_NAME,\n"+
                                        "       VENDOR_SITE_CODE,\n" +
                                        "       ATTRIBUTE14,\n" +
                                        "       ATTRIBUTE15,\n" +
                                        "       INVOICE_TYPE,\n"+
                                        "       ATTRIBUTE_CATEGORY,\n" +
                                        "       DOCUMENT_SUBTYPE,\n" +
                                        "       EXCHANGE_DATE,\n" +
                                        "       EXCHANGE_RATE_TYPE,\n" +
                                        "       GOODS_RECEIVED_DATE,\n" +
                                        "       INVOICE_AMOUNT,\n" +
                                        "       INVOICE_DATE,\n" +
                                        "       INVOICE_CURRENCY_CODE,\n" +
                                        "       INVOICE_NUMBER,\n" +
                                        "       PO_NUMBER,\n" +
                                        "       INVOICE_RECEIVED_DATE,\n" +
                                        "       INVOICE_TYPE_LOOKUP_CODE,\n" +
                                        "       LEGAL_ENTITY_NAME,\n" +
                                        "       LEGAL_ENTITY_ID,\n" +
                                        "       PDF_CLOB,\n" +
                                        "       STATUS\n" +
                                        "  from NC_CPE_AP_INVOICE_HEADERS_ALL \n" +
                                        " where 1 = 1\n" +
                                        " and STATUS IN ('IN_PROCESS')";   
        
        ResultSet resultado;

                OraclePreparedStatement lStmt;
        
            lStmt =
                (OraclePreparedStatement) trans.prepareStatement(lquery, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                 OracleResultSet.CONCUR_READ_ONLY);
        
        //lStmt.setString(1, pIdProcess);
                resultado = lStmt.executeQuery();
                
                
        while (resultado.next()) {
        
            long cpeInvoiceId             = resultado.getLong  ("CPE_INVOICE_ID");
            this.logWithTimestamp("Update x Invoice - Cpe Invoice Id: "+cpeInvoiceId); 
            
            this.callUpdateStatus("EXECUTE_IMPORT", ""  ,"", Long.parseLong(requestId), "", "", "", 0, urlUpdate+cpeInvoiceId);
            
        }
        
    }
    
    public String executeApInterfaceWs(String requestPayload, String url, String userName, String pass, String soapAction, String urlUpdate) {
        
       
       String soap = ""; 
       URL oURL = null;
       HttpURLConnection httpConn = null ;

     try {  
       oURL = new URL(url);
         
       this.logWithTimestamp("entro executeApInterfaceWs");

       httpConn = (HttpURLConnection) oURL.openConnection();


       byte[] request = new byte[requestPayload.length()];
       request = requestPayload.getBytes();

       String userCredentials = userName + ":" + pass;
       String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

       /* Authenticator.setDefault(new Authenticator() {
           @Override
           protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(userName, pass.toCharArray());
           }
       });*/

       httpConn.setRequestProperty("Authorization", basicAuth);
       httpConn.setRequestProperty("Content-Length", String.valueOf(request.length));
       httpConn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
       httpConn.setRequestProperty("SOAPAction", soapAction);
       httpConn.setRequestMethod("POST");
       httpConn.setDoOutput(true);
       httpConn.setDoInput(true);

       this.logWithTimestamp("entro OutputStream");
       OutputStream out = httpConn.getOutputStream();
       //Write the content of the request to the outputstream of the HTTP Connection.
       out.write(request);
       out.flush();
       out.close();
         
       this.logWithTimestamp("salio responseCode");  
       //Ready with sending the request.
       //Read the response.
       //httpConn.connect();

       int responseCode = httpConn.getResponseCode();
         
       this.logWithTimestamp("entro responseCode");  

       StringBuilder builder = new StringBuilder();
       InputStreamReader in = new InputStreamReader(httpConn.getInputStream(), "UTF-8");
       BufferedReader br = new BufferedReader(in);
       this.logWithTimestamp("entro InputStreamReader");  
       String output;
       if (responseCode == HttpURLConnection.HTTP_OK) {
           //Write the SOAP message response to a String.
           this.logWithTimestamp("Entro Lectura XML");
           while ((output = br.readLine()) != null) {
               builder.append(output).append("\n");
           }
       }
         
        this.logWithTimestamp("salio InputStreamReader");  
       soap = builder.toString();
       this.logWithTimestamp(soap);
       br.close();
       httpConn.disconnect();   
         
        } catch (IOException e) {
             this.logWithTimestamp("entro exception");
             try {
                 this.logWithTimestamp("ini getErrorHttpUrlConnection");
                 this.getErrorHttpUrlConnection(httpConn,urlUpdate);
                 this.logWithTimestamp("fin getErrorHttpUrlConnection");
             } catch (IOException f) {
                 
                 this.logWithTimestamp(f.getMessage());
                 
             } catch (JAXBException f) {
                this.logWithTimestamp(f.getMessage());
            }
      }catch (Exception e){
          this.logWithTimestamp("Exception"+e.getMessage()+Arrays.toString(e.getStackTrace()));
     
      } finally {
      
         this.logWithTimestamp("Finally");
     }
        
     return soap;   
    }
    
    private void getErrorHttpUrlConnection(HttpURLConnection httpConn, String urlUpdate) throws IOException, JAXBException {

            if(httpConn.getErrorStream() != null){
                StringBuilder builder = new StringBuilder();
                InputStreamReader in = new InputStreamReader(httpConn.getErrorStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                while ((output = br.readLine()) != null) {
                    builder.append(output).append("\n");
                }

               // pwc.getStatus(ERROR);
               // pwc.getMessage("HTTP Error al realizar peticion " + builder.toString()+ ", con codigo: " + httpConn.getResponseCode());
               this.logWithTimestamp("HTTP Error al realizar peticion 1" + builder.toString()+ ", con codigo: " + httpConn.getResponseCode());
               
                String[] split =   builder.toString().split("<env:Body>");
                String xml1 = split[0];

                String xml0 = builder.toString().replace(xml1, "");

                String[] split2 =   xml0.split("</env:Body>");
                String xml2 = split2[0];
                
                String xml = xml2.replace("<env:Body>", "");
                System.out.println(xml);
                
                String xm = xml.replace("env:", "");
                
                this.logWithTimestamp(xm);
               
                RespuestaErrorUploadAp respuestaErrorUpload = new RespuestaErrorUploadAp();

                JAXBContext jaxbContextProcResult = JAXBContext.newInstance(RespuestaErrorUploadAp.class);
                Unmarshaller jaxbUnmarshallerProcResult = jaxbContextProcResult.createUnmarshaller();


                InputStream ins = IOUtils.toInputStream(xm, "UTF-8");
                respuestaErrorUpload = (RespuestaErrorUploadAp) jaxbUnmarshallerProcResult.unmarshal(new InputSource(ins));
                
                this.logWithTimestamp(respuestaErrorUpload.getFaultString()+ " " + respuestaErrorUpload.getFaultCode());
                
                this.callUpdateStatus("ERROR_LOAD", respuestaErrorUpload.getFaultCode(), respuestaErrorUpload.getFaultString(), 0, builder.toString(), "", "", 0, urlUpdate);
                
               
               
                br.close();
                httpConn.disconnect();
                
                return;
            }
            
            //pwc.getStatus(ERROR);
            //pwc.getMessage("HTTP Error al realizar peticion "+ httpConn.getResponseMessage() +",con codigo "+httpConn.getResponseCode());
            this.logWithTimestamp("HTTP Error al realizar peticion 2"+ httpConn.getResponseMessage() +",con codigo "+httpConn.getResponseCode());
        }
    
    public String callUpdateStatus(String status, 
                                    String errorCode, 
                                    String errorDescription,
                                    long   loadRequestId, 
                                    String errorPayload,
                                    String requestPayload,
                                    String sunatRequestPayload,
                                    long   invoiceId,
                                    String urlUpdate) throws MalformedURLException, IOException {

            String responseString = "";
            String outputString = "";


           URL oURL = new URL(urlUpdate);
           
            HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
       
            logWithTimestamp("URL REST UPDATE : "+urlUpdate);
           
            String jsonInput ="{\n" + 
            "\"status_in\" : \""+status+"\",\n" + 
            "\"errorCode\" : \""+errorCode+"\",\n" + 
            "\"errorDescription\" : \""+errorDescription+"\",\n" + 
            "\"loadRequestId\" : "+loadRequestId+",\n" + 
            "\"errorPayload\" : \""+errorPayload+"\",\n" + 
            "\"requestPayload\" : \""+requestPayload+"\",\n" + 
            "\"sunatRequestPayload\" : \""+sunatRequestPayload+"\",\n" +
            "\"invoiceApId\" : "+invoiceId+"\n" +                   
            "\n" + 
            "}";
           
            logWithTimestamp("JSON : "+jsonInput);

            byte[] buffer = new byte[jsonInput.length()];
            buffer = jsonInput.getBytes();

            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
            httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
            httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
           
            /*String userCredentials = usuario+":"+password;
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));      
            httpConn.setRequestProperty("Authorization", basicAuth);*/
           

            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
           

            OutputStream out = httpConn.getOutputStream();
            //Write the content of the request to the outputstream of the HTTP Connection.
            out.write(buffer);
            out.flush();
            out.close();
            //Ready with sending the request.

            //Read the response.
            //httpConn.connect();
            logWithTimestamp("Codigo respuesta servicio: "+httpConn.getResponseCode());
            if (httpConn.getResponseCode() != 200) {            
              return String.valueOf(httpConn.getResponseCode());//"Failed : HTTP Error code : " + httpConn.getResponseCode();
             }
           
           
           
            InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            //Write the SOAP message response to a String.
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }
            
            this.logWithTimestamp(outputString);
           
            in.close();
            httpConn.disconnect();  
           
            return outputString;
        }
    
    private byte[] runReport(xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService svc,
                             String reportPath, String loadRequestId, String isPurge) throws MalformedURLException, Exception {

        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ReportRequest req =
            new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfParamNameValue arrayParam =
            new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        
        param =  new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("P_LOAD_REQUEST_ID");
        arrayValues.getItem().add(loadRequestId);
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        param =  new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("P_PURGE");
        arrayValues.getItem().add(isPurge);
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);


        req.setParameterNameValues(arrayParam);
        req.setReportAbsolutePath(reportPath);
        req.setSizeOfDataChunkDownload(-1);
        req.setAttributeFormat("xml");

        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ReportResponse resp =
            svc.runReport(req, "");

        return (resp.getReportBytes());

    }
    
    public void executePurgeInterfaceTable(String loadRequestId,String reportPath, String isPurge,ErpIntegrationService svcErpIntegration,ExternalReportWSSService svcReport ) throws MalformedURLException, Exception {
        
        byte[] reportOutputBytes;
        InputStream reportIs;
        JAXBContext jaxbContextDataDS;
        Unmarshaller jaxbUnmarshaller;
        
        reportOutputBytes = this.runReport(svcReport, reportPath, loadRequestId,isPurge);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(reportPath + " report cannot be executed.");
        }
        
        logWithTimestamp(reportPath + " report was executed. " + reportOutputBytes.length);
        
        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(CPESyncDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        CPESyncDS dataCPEInvoicesDS =
            (CPESyncDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));
        
        if (dataCPEInvoicesDS.getErrorInterface() != null && dataCPEInvoicesDS.getErrorInterface().length > 0){
           
            logWithTimestamp("Entro Execute Purge Table");
            this.purgeTableInterface(loadRequestId, svcErpIntegration);
            
            
            
        }
            
    }
    
    public String purgeTableInterface(String loadRequestId,ErpIntegrationService svcErpIntegration){
         
       
         try{
                                 
             List<String> paramListAutoInvoice = null;
             long purgeJobId = 0;
             
             paramListAutoInvoice = new ArrayList<String>();
             paramListAutoInvoice.add("1"); // Import Payables Invoices
             paramListAutoInvoice.add(loadRequestId); // load request id
             paramListAutoInvoice.add("#NULL"); //3
             paramListAutoInvoice.add("#NULL");//4
             paramListAutoInvoice.add("#NULL");//5
             paramListAutoInvoice.add("ORA_FBDI");//6
             paramListAutoInvoice.add("USER");//7
             paramListAutoInvoice.add("#NULL");//8
             paramListAutoInvoice.add("#NULL");//9
         
             purgeJobId = svcErpIntegration.submitESSJobRequest("/oracle/apps/ess/financials/commonModules/shared/common/interfaceLoader",
                     // Import AutoInvoice
                     "InterfaceLoaderPurge", paramListAutoInvoice);
             
             boolean valid2 = true;
             String statusStr2 = "";
             
            // logWithTimestamp("Purge Job Id: "+purgeJobId);
             
           /*  while (valid2) {
                 statusStr2 = svcErpIntegration.getESSJobStatus(purgeJobId);
               
               logWithTimestamp("Status Purge: "+statusStr2);

                 if ("SUCCEEDED".equals(statusStr2) || "ERROR".equals(statusStr2) || "CANCELLED".equals(statusStr2)) {
                     valid2 = false;
                 } else {
                     
                         Thread.sleep(5000);                    
                 }
               
               
             }

             if (!"SUCCEEDED".equals(statusStr2)) {
               
                 logWithTimestamp("Error en InterfaceLoaderPurge AR.Revisar log en ERP Cloud-Schedule Processes: "+purgeJobId);
               return "E";
             }*/
        
             
             logWithTimestamp("InterfaceLoaderPurge: " + "Se ejecuto proceso Purge Interface Tables: "+purgeJobId);
                        
             
             //return

         }catch (xxss.oracle.localizations.services.fusion.erpintegrationservice.proxy.ServiceException e) {
             String message = "Error erpintegrationservice: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
             //lStmt.close();
             logWithTimestamp(message);
           return "E";
         } /* catch (InterruptedException e) {
              String message = "Error DatatypeConfigurationException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());

              logWithTimestamp(message);
            return "E";
          }*/
         
      return "OK Purge Interface";
     }
}
