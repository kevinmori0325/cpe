package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class PartyClassificationsDS {
    PartyClassifications[] partyClassifications;
    public PartyClassificationsDS() {
        super();
    }
    
    @XmlElement(name = "G_FISCAL")
    public void setPartyClassifications(PartyClassifications[] partyClassifications) {
        this.partyClassifications = partyClassifications;
    }

    public PartyClassifications[] getPartyClassifications() {
        return partyClassifications;
    }
}
