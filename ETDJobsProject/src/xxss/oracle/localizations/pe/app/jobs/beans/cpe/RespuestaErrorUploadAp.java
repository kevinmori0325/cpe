package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Fault")
public class RespuestaErrorUploadAp {
    String faultCode;
    String faultString;
    
    public RespuestaErrorUploadAp() {
        super();
    }
    
    @XmlElement(name = "faultcode")
    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultCode() {
        return faultCode;
    }

    @XmlElement(name = "faultstring")
    public void setFaultString(String faultString) {
        this.faultString = faultString;
    }

    public String getFaultString() {
        return faultString;
    }
}
