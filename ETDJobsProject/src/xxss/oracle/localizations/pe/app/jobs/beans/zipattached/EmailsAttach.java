package xxss.oracle.localizations.pe.app.jobs.beans.zipattached;

import java.io.InputStream;

import java.util.ArrayList;

public class EmailsAttach {
    String emailNum;
    ArrayList<FilesCompressed> fileCompressed;
    ArrayList<FilesXml> filesXml;
    ArrayList<FilesPdf> filesPdf;
    ArrayList<FilesOther> filesOther;
    public EmailsAttach() {
        super();
    }

    public void setEmailNum(String emailNum) {
        this.emailNum = emailNum;
    }

    public String getEmailNum() {
        return emailNum;
    }

    public void setFileCompressed(ArrayList<FilesCompressed> fileCompressed) {
        this.fileCompressed = fileCompressed;
    }

    public ArrayList<FilesCompressed> getFileCompressed() {
        return fileCompressed;
    }

    public void setFilesXml(ArrayList<FilesXml> filesXml) {
        this.filesXml = filesXml;
    }

    public ArrayList<FilesXml> getFilesXml() {
        return filesXml;
    }

    public void setFilesPdf(ArrayList<FilesPdf> filesPdf) {
        this.filesPdf = filesPdf;
    }

    public ArrayList<FilesPdf> getFilesPdf() {
        return filesPdf;
    }

    public void setFilesOther(ArrayList<FilesOther> filesOther) {
        this.filesOther = filesOther;
    }

    public ArrayList<FilesOther> getFilesOther() {
        return filesOther;
    }
}
