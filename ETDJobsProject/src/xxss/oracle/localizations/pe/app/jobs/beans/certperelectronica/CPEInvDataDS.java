package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class CPEInvDataDS {
    CPEInvLegalEntity[] legalEntity;
    
    public CPEInvDataDS() {
        super();
    }

    @XmlElement(name = "G_LE")    
    public void setLegalEntity(CPEInvLegalEntity[] legalEntity) {
        this.legalEntity = legalEntity;
    }

    public CPEInvLegalEntity[] getLegalEntity() {
        return legalEntity;
    }
}
