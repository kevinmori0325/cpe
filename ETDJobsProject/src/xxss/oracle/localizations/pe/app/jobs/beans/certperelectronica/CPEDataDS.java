package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class CPEDataDS {
    CPELegalEntity[] legalEntity;
    
    public CPEDataDS() {
        super();
    }

    @XmlElement(name = "G_LEGAL_ENTITY")    
    public void setLegalEntity(CPELegalEntity[] legalEntity) {
        this.legalEntity = legalEntity;
    }

    public CPELegalEntity[] getLegalEntity() {
        return legalEntity;
    }
}
