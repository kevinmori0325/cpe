package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;

public class PartyClassifications {
    String partyClassifications;
    String typeCode;
    String typeName;
    
    public PartyClassifications() {
        super();
    }

    @XmlElement(name = "OWNER_ID_CHAR")
    public void setPartyClassifications(String partyClassifications) {
        this.partyClassifications = partyClassifications;
    }

    public String getPartyClassifications() {
        return partyClassifications;
    }

    @XmlElement(name = "CLASSIFICATION_TYPE_CODE")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeCode() {
        return typeCode;
    }

    @XmlElement(name = "CLASSIFICATION_TYPE_NAME")
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }
}
