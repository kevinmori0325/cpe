package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;

public class FETax {
      String taxClassificationCode;
      String nombreTributo;
      String identTributo;
      String codTributo;
      
    public FETax() {
        super();
    }

    @XmlElement(name="TAX_CLASSIFICATION_CODE")
    public void setTaxClassificationCode(String taxClassificationCode) {
        this.taxClassificationCode = taxClassificationCode;
    }

    public String getTaxClassificationCode() {
        return taxClassificationCode;
    }

    @XmlElement(name="NOMBRE_TRIBUTO")
    public void setNombreTributo(String nombreTributo) {
        this.nombreTributo = nombreTributo;
    }

    public String getNombreTributo() {
        return nombreTributo;
    }

    @XmlElement(name="IDENTIFICACION_TRIBUTO")
    public void setIdentTributo(String identTributo) {
        this.identTributo = identTributo;
    }

    public String getIdentTributo() {
        return identTributo;
    }

    @XmlElement(name="CODIGO_TRIBUTO")
    public void setCodTributo(String codTributo) {
        this.codTributo = codTributo;
    }

    public String getCodTributo() {
        return codTributo;
    }
}
