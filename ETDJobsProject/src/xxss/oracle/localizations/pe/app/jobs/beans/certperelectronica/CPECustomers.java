package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CPECustomers {
    long cashReceiptId;
    long billToCustomerId;
    String accountNumber;
    String tipoDocIdentCli;
    String nroDocIdentCli;
    String nombreCli;
    String direccionCli;
    String ciudadCli;
    String municipioCli;
    String paisCli;
    String codigoPostalCli;
    String emailCli;
    String codigoTipoMonto;
    double tasaConcepto;
    CPELines[] lines;
    
    public CPECustomers() {
        super();
    }

    @XmlElement(name = "CASH_RECEIPT_ID")
    public void setCashReceiptId(long cashReceiptId) {
        this.cashReceiptId = cashReceiptId;
    }

    public long getCashReceiptId() {
        return cashReceiptId;
    }

    @XmlElement(name = "ACCOUNT_NUMBER")
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
    
    @XmlElement(name = "TIPO_DOC_IDEN_CLI")
    public void setTipoDocIdentCli(String tipoDocIdentCli) {
        this.tipoDocIdentCli = tipoDocIdentCli;
    }

    public String getTipoDocIdentCli() {
        return tipoDocIdentCli;
    }

    @XmlElement(name = "NRO_DOC_IDENT_CLI")
    public void setNroDocIdentCli(String nroDocIdentCli) {
        this.nroDocIdentCli = nroDocIdentCli;
    }

    public String getNroDocIdentCli() {
        return nroDocIdentCli;
    }

    @XmlElement(name = "NOMBRE1_CLI")
    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    @XmlElement(name = "DIRECCION_CLI")
    public void setDireccionCli(String direccionCli) {
        this.direccionCli = direccionCli;
    }

    public String getDireccionCli() {
        return direccionCli;
    }

    @XmlElement(name = "CIUDAD_CLI")
    public void setCiudadCli(String ciudadCli) {
        this.ciudadCli = ciudadCli;
    }

    public String getCiudadCli() {
        return ciudadCli;
    }

    @XmlElement(name = "MUNICIPIO_CLI")
    public void setMunicipioCli(String municipioCli) {
        this.municipioCli = municipioCli;
    }

    public String getMunicipioCli() {
        return municipioCli;
    }

    @XmlElement(name = "PAIS_CLI")
    public void setPaisCli(String paisCli) {
        this.paisCli = paisCli;
    }

    public String getPaisCli() {
        return paisCli;
    }

    @XmlElement(name = "CODIGO_POSTAL_CLI")
    public void setCodigoPostalCli(String codigoPostalCli) {
        this.codigoPostalCli = codigoPostalCli;
    }

    public String getCodigoPostalCli() {
        return codigoPostalCli;
    }

    @XmlElement(name = "EMAIL_CLI")
    public void setEmailCli(String emailCli) {
        this.emailCli = emailCli;
    }

    public String getEmailCli() {
        return emailCli;
    }

    @XmlElement(name = "CODIGO_TIPO_MONTO")
    public void setCodigoTipoMonto(String codigoTipoMonto) {
        this.codigoTipoMonto = codigoTipoMonto;
    }

    public String getCodigoTipoMonto() {
        return codigoTipoMonto;
    }

    @XmlElement(name = "TASA_CONCEPTO")
    public void setTasaConcepto(double tasaConcepto) {
        this.tasaConcepto = tasaConcepto;
    }

    public double getTasaConcepto() {
        return tasaConcepto;
    }

    @XmlElement(name = "G_LINES")
    public void setLines(CPELines[] lines) {
        this.lines = lines;
    }

    public CPELines[] getLines() {
        return lines;
    }

    @XmlElement(name = "BILL_TO_CUSTOMER_ID")
    public void setBillToCustomerId(long billToCustomerId) {
        this.billToCustomerId = billToCustomerId;
    }

    public long getBillToCustomerId() {
        return billToCustomerId;
    }
}
