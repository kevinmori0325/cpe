package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GRECarriers {
    String carrierName;
    long carrierId;
    String razonSocial;
    String nroDoc;
    String tipoDocIdent;
    GRECarrierContacts[] contacts;
    
    public GRECarriers() {
        super();
        razonSocial = "";
        nroDoc = "";
        tipoDocIdent = "";
    }

    @XmlElement(name = "CARRIER_NAME")
    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCarrierName() {
        return carrierName;
    }

    @XmlElement(name = "CARRIER_ID")
    public void setCarrierId(long carrierId) {
        this.carrierId = carrierId;
    }

    public long getCarrierId() {
        return carrierId;
    }

    @XmlElement(name = "RAZON_SOCIAL")
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    @XmlElement(name = "NRO_DOC")
    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public String getNroDoc() {
        return nroDoc;
    }
    
    @XmlElement(name = "TIPO_DOC_IDE")
    public void setTipoDocIdent(String tipoDocIdent) {
        this.tipoDocIdent = tipoDocIdent;
    }

    public String getTipoDocIdent() {
        return tipoDocIdent;
    }

    @XmlElement(name = "G_CARRIER_CONTACTS")
    public void setContacts(GRECarrierContacts[] contacts) {
        this.contacts = contacts;
    }

    public GRECarrierContacts[] getContacts() {
        return contacts;
    }
}
