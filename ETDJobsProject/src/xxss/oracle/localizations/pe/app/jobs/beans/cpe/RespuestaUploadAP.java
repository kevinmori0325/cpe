package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "createInvoiceInterfaceWithAttachmentsResponse")
public class RespuestaUploadAP {
    Result result;
    public RespuestaUploadAP() {
        super();
    }

    @XmlElement(name = "result")
    public void setResult(Result result) {
        this.result = result;
    }

    public Result getResult() {
        return result;
    }
}
