package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GRECustomers {
    String tipoDocIdentCli;
    String nroDocIdentCli;
    String nombreCli;
    String countryCli;
    String address1Cli;
    String cityCli;
    String stateCli;
    String postalCodeCli;
    long custAccountID;
    long partyId;
    
    public GRECustomers() {
        super();
        nroDocIdentCli = "";
        tipoDocIdentCli = "";
        countryCli = "";
        address1Cli = "";
        cityCli = "";
        stateCli = "";
        postalCodeCli = "";
        nombreCli = "";
    }

    @XmlElement(name = "TIPO_DOC_IDENT_CLI")
    public void setTipoDocIdentCli(String tipoDocIdentCli) {
        this.tipoDocIdentCli = tipoDocIdentCli;
    }

    public String getTipoDocIdentCli() {
        return tipoDocIdentCli;
    }

    @XmlElement(name = "NOMBRE_CLI")
    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    @XmlElement(name = "COUNTRY_CLI")
    public void setCountryCli(String countryCli) {
        this.countryCli = countryCli;
    }

    public String getCountryCli() {
        return countryCli;
    }

    @XmlElement(name = "ADDRESS1_CLI")
    public void setAddress1Cli(String address1Cli) {
        this.address1Cli = address1Cli;
    }

    public String getAddress1Cli() {
        return address1Cli;
    }

    @XmlElement(name = "CITY_CLI")
    public void setCityCli(String cityCli) {
        this.cityCli = cityCli;
    }

    public String getCityCli() {
        return cityCli;
    }

    @XmlElement(name = "STATE_CLI")
    public void setStateCli(String stateCli) {
        this.stateCli = stateCli;
    }

    public String getStateCli() {
        return stateCli;
    }

    @XmlElement(name = "POSTAL_CODE_CLI")
    public void setPostalCodeCli(String postalCodeCli) {
        this.postalCodeCli = postalCodeCli;
    }

    public String getPostalCodeCli() {
        return postalCodeCli;
    }

    @XmlElement(name = "CUST_ACCOUNT_ID")
    public void setCustAccountID(long custAccountID) {
        this.custAccountID = custAccountID;
    }

    public long getCustAccountID() {
        return custAccountID;
    }

    @XmlElement(name = "PARTY_ID")
    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public long getPartyId() {
        return partyId;
    }

    @XmlElement(name = "NRO_DOC_IDENT_CLI")
    public void setNroDocIdentCli(String nroDocIdentCli) {
        this.nroDocIdentCli = nroDocIdentCli;
    }

    public String getNroDocIdentCli() {
        return nroDocIdentCli;
    }
}
