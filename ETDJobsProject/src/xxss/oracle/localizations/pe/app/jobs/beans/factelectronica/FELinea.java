package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;

public class FELinea {
    String lineNumber;
    String quantityInvoiced;
    String description;
    String unitSellingPrice;
    String extendedAmount;
    String lineRecoverable;
    String taxRecoverable;
    String uomCode;
    String taxRate;
    String taxClassificationCode;
    String unitSellingPriceNet;
    String tipoAfectIgv;
    String extendedAmountNet;
    String itemNumber;
    String importeTotalTribu;
    String baseImponible;
    String importeExplicito;
    String tasaImpuestoLine;
    String precioVenta;
    String codigoProductoSunat;
    String afectacionIGV;
    String identificacionTributo;
    String nombreTributo;
    String codigoTipoTributo;
    String codigoProducto;
    String tipoPrecioVenta;
    String indicadorTipo;
    String indicador;
    String nivel;
    String afecta_bi;
    String factor_descuento;//ADD DCHUMACERO 170420

    public FELinea() {
        super();
    }

    @XmlElement(name = "LINE_NUMBER")
    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    @XmlElement(name = "QUANTITY_INVOICED")
    public void setQuantityInvoiced(String quantityInvoiced) {
        this.quantityInvoiced = quantityInvoiced;
    }

    public String getQuantityInvoiced() {
        return quantityInvoiced;
    }

    @XmlElement(name = "DESCRIPTION")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement(name = "UNIT_SELLING_PRICE")
    public void setUnitSellingPrice(String unitSellingPrice) {
        this.unitSellingPrice = unitSellingPrice;
    }

    public String getUnitSellingPrice() {
        return (unitSellingPrice == null ? "0.00" : unitSellingPrice);
    }

    @XmlElement(name = "EXTENDED_AMOUNT")
    public void setExtendedAmount(String extendedAmount) {
        this.extendedAmount = extendedAmount;
    }

    public String getExtendedAmount() {
        return (extendedAmount == null ? "0.00" : extendedAmount);
    }

    @XmlElement(name = "LINE_RECOVERABLE")
    public void setLineRecoverable(String lineRecoverable) {
        this.lineRecoverable = lineRecoverable;
    }

    public String getLineRecoverable() {
        return (lineRecoverable == null ? "0.00" : lineRecoverable);
    }

    @XmlElement(name = "TAX_RECOVERABLE")
    public void setTaxRecoverable(String taxRecoverable) {
        this.taxRecoverable = taxRecoverable;
    }

    public String getTaxRecoverable() {
        return (taxRecoverable == null ? "0.00" : taxRecoverable);
    }

    @XmlElement(name = "UOM_CODE")
    public void setUomCode(String uomCode) {
        this.uomCode = uomCode;
    }

    public String getUomCode() {
        return uomCode;
    }

    @XmlElement(name = "TAX_RATE")
    public void setTaxRate(String taxRate) {
        this.taxRate = taxRate;
    }

    public String getTaxRate() {
        return (taxRate == null ? "0" : taxRate);
    }

    @XmlElement(name = "TAX_CLASSIFICATION_CODE")
    public void setTaxClassificationCode(String taxClassificationCode) {
        this.taxClassificationCode = taxClassificationCode;
    }

    public String getTaxClassificationCode() {
        return taxClassificationCode;
    }

    @XmlElement(name = "UNIT_SELLING_PRICE_NET")
    public void setUnitSellingPriceNet(String unitSellingPriceNet) {
        this.unitSellingPriceNet = unitSellingPriceNet;
    }

    public String getUnitSellingPriceNet() {
        return (unitSellingPriceNet == null ? "0.00" : unitSellingPriceNet);
    }

    @XmlElement(name = "TIPO_AFECT_IGV")
    public void setTipoAfectIgv(String tipoAfectIgv) {
        this.tipoAfectIgv = tipoAfectIgv;
    }

    public String getTipoAfectIgv() {
        return tipoAfectIgv;
    }

    @XmlElement(name = "EXTENDED_AMOUNT_NET")
    public void setExtendedAmountNet(String extendedAmountNet) {
        this.extendedAmountNet = extendedAmountNet;
    }
    
   

    public String getExtendedAmountNet() {
        return (extendedAmountNet == null ? "0.00" : extendedAmountNet);
    }

    public String getUnitSellingPrice1() {
        return unitSellingPrice;
    }

    public String getExtendedAmount1() {
        return extendedAmount;
    }

    public String getLineRecoverable1() {
        return lineRecoverable;
    }

    public String getTaxRecoverable1() {
        return taxRecoverable;
    }

    public String getTaxRate1() {
        return taxRate;
    }

    public String getUnitSellingPriceNet1() {
        return unitSellingPriceNet;
    }

    public String getExtendedAmountNet1() {
        return extendedAmountNet;
    }
    
    @XmlElement(name = "ITEM_NUMBER")
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }
    
    public String getItemNumber() {
        return itemNumber;
    }

    @XmlElement(name = "IMPORTE_TOTAL_TRIBU")
        public void setImporteTotalTribu(String importeTotalTribu) {
            this.importeTotalTribu = importeTotalTribu;
        }

        public String getImporteTotalTribu() {
            return importeTotalTribu;
        }
        @XmlElement(name = "BASE_IMPONIBLE")
        public void setBaseImponible(String baseImponible) {
            this.baseImponible = baseImponible;
        }

        public String getBaseImponible() {
            return baseImponible;
        }
        @XmlElement(name = "IMPORTE_EXPLICITO")
        public void setImporteExplicito(String importeExplicito) {
            this.importeExplicito = importeExplicito;
        }

        public String getImporteExplicito() {
            return importeExplicito;
        }
        @XmlElement(name = "TASA_IMPUESTO")
        public void setTasaImpuestoLine(String tasaImpuestoLine) {
            this.tasaImpuestoLine = tasaImpuestoLine;
        }

        public String getTasaImpuestoLine() {
            return tasaImpuestoLine;
        }
        @XmlElement(name = "PRECIO_VENTA")
        public void setPrecioVenta(String precioVenta) {
            this.precioVenta = precioVenta;
        }

        public String getPrecioVenta() {
            return precioVenta;
        }
        @XmlElement(name = "CODIGO_PRODUCTO_SUNAT")
        public void setCodigoProductoSunat(String codigoProductoSunat) {
            this.codigoProductoSunat = codigoProductoSunat;
        }

        public String getCodigoProductoSunat() {
            return codigoProductoSunat;
        }
        @XmlElement(name = "AFECTACION_IGV")
        public void setAfectacionIGV(String afectacionIGV) {
            this.afectacionIGV = afectacionIGV;
        }

        public String getAfectacionIGV() {
            return afectacionIGV;
        }
        @XmlElement(name = "IDENTIFICACION_TRIBUTO")
        public void setIdentificacionTributo(String identificacionTributo) {
            this.identificacionTributo = identificacionTributo;
        }

        public String getIdentificacionTributo() {
            return identificacionTributo;
        }
        @XmlElement(name = "NOMBRE_TRIBUTO")
        public void setNombreTributo(String nombreTributo) {
            this.nombreTributo = nombreTributo;
        }

        public String getNombreTributo() {
            return nombreTributo;
        }
        @XmlElement(name = "CODIGO_TRIBUTO")
        public void setCodigoTipoTributo(String codigoTipoTributo) {
            this.codigoTipoTributo = codigoTipoTributo;
        }

        public String getCodigoTipoTributo() {
            return codigoTipoTributo;
        }
        @XmlElement(name = "ITEM_NUMBER")
        public void setCodigoProducto(String codigoProducto) {
            this.codigoProducto = codigoProducto;
        }

        public String getCodigoProducto() {
            return codigoProducto;
        }
        @XmlElement(name = "TIPO_PRECIO_VENTA")
        public void setTipoPrecioVenta(String tipoPrecioVenta) {
            this.tipoPrecioVenta = tipoPrecioVenta;
        }

        public String getTipoPrecioVenta() {
            return tipoPrecioVenta;
        }

    @XmlElement(name = "ATTRIBUTE28")
    public void setIndicadorTipo(String indicadorTipo) {
        this.indicadorTipo = indicadorTipo;
    }

    public String getIndicadorTipo() {
        return indicadorTipo;
    }

    @XmlElement(name = "INDICADOR")
    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getIndicador() {
        return indicador;
    }

    @XmlElement(name = "NIVEL")
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNivel() {
        return nivel;
    }
    
    @XmlElement(name = "AFECTA_BI")
    public void setAfecta_bi(String afecta_bi) {
        this.afecta_bi = afecta_bi;
    }

    public String getAfecta_bi() {
        return afecta_bi;
    }
    @XmlElement(name = "FACTOR_DESCUENTO")
    public void setFactor_descuento(String factor_descuento) {
        this.factor_descuento = factor_descuento;
    }

    public String getFactor_descuento() {
        return factor_descuento;
    }
}
