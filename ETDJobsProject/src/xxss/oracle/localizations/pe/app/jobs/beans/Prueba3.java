package xxss.oracle.localizations.pe.app.jobs.beans;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import org.apache.commons.io.FilenameUtils;

import org.quartz.JobDataMap;

//import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.ArchivoCorreo;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.EmailsAttach;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesCompressed;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesXml;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesPdf;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesOther;

public class Prueba3 {

    private static String fileExtrName = "";
    private static final String EZIP = "ZIP";
    private static final String ERAR = "RAR";
    private static final String E7Z = "7Z";
    private static final String EXML = "XML";
    private static final String EPDF = "PDF";
    private static final String EOT = "other";


    private static ArrayList<FilesXml> filesXmlList = new ArrayList<FilesXml>();
    private static ArrayList<FilesPdf> filesPdfList = new ArrayList<FilesPdf>();
    private static ArrayList<FilesOther> filesOtherList = new ArrayList<FilesOther>();
    
    private static ArrayList<FilesXml> filesXmlCompList = new ArrayList<FilesXml>();
    private static ArrayList<FilesPdf> filesPdfCompList = new ArrayList<FilesPdf>();
    private static ArrayList<FilesOther> filesOtherCompList = new ArrayList<FilesOther>();


    public Prueba3() {
        super();
    }

    public static void log(String msg) {
        System.out.println(msg);
    }

    public static void main(String[] args) {
        try {
            doAction(null, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void doAction(JobDataMap dataMap, OracleConnection conn) throws SQLException {
        //Parametros
        String pathDirectory = "C:/Users/julio/Downloads/Nueva carpeta/";
        String imapHost = "imap.outlook.com";
        String imapPort = "993";
        String mailStoreType = "imap";
        String username = "consultor1.tecnico@neora.com.pe";//"alberto2503.mori@outlook.com";
        String password = "Barcelona2815";
        // Method: Get unread email and unzip attachment
        getUnreadEmailUnzipAttachment(conn, pathDirectory, imapHost, imapPort, mailStoreType, username, password);
    }

    private static void getUnreadEmailUnzipAttachment(OracleConnection conn, String pathDirectory, String imapHost,
                                                      String imapPort, String mailStoreType, String username,
                                                      String password) {
        // Create properties
        Properties properties = new Properties();
        properties.put("mail.imap.host", imapHost);
        properties.put("mail.imap.port", imapPort);
        properties.put("mail.imap.starttls.enable", "true");
        properties.put("mail.imap.ssl.trust", imapHost);

        // Email Session
        Session emailSession = Session.getDefaultInstance(properties);
        // Enable debug
        //emailSession.setDebug(true);
        //
        JSch jschSSHChannel = new JSch();
        com.jcraft.jsch.Session sesConnection = null;
        //
        // Create the imap store object and connect to the imap server
        try {
            String destFilePath = pathDirectory;
            String extFilePath = pathDirectory;
            Store store = emailSession.getStore("imaps");
            store.connect(imapHost, username, password);
            // Create the inbox object and open it
            Folder inbox = store.getFolder("Inbox");
            inbox.open(Folder.READ_WRITE);
            // Retrieve the messages from the folder in an array and print it. Set False to retrieve Unread Mail
            Message[] messages = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));
            log("Unread Mails " + messages.length);

            //
           /* sesConnection = jschSSHChannel.getSession("usercpe", "168.138.235.72", 22);
            sesConnection.setPassword("U$ercp3123");
            // UNCOMMENT THIS FOR TESTING PURPOSES, BUT DO NOT USE IN PRODUCTION
            sesConnection.setConfig("StrictHostKeyChecking", "no");
            sesConnection.connect(60000);

            System.out.println("Conecto SFTP exitosamente");

            Channel channel = sesConnection.openChannel("sftp");

            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            String pathUploadSfpt = "/u01/CPE/";*/
            //

            for (int i = 0, n = messages.length; i < n; i++) {
                String emsg = "";
                int emailNum = (i + 1);
                String emailNumTxt = "" + emailNum;
                int at = 0;
                Message message = messages[i];
                // Set email as read. It's not necessary
                 message.setFlag(Flag.SEEN, true);
                log("---------------------------------");
                log("Email Number " + (emailNum));
                log("Subject: " + message.getSubject());
                log("From: " + message.getFrom()[0]);
                log("Text: " + message.getContent().toString());
                log("ContentType: " + message.getContentType());
                log("---------------------------------");
                // Email Parts
                Multipart multiPart = (Multipart) message.getContent();
               
                // Guardar los adjuntos
                EmailsAttach emailAttach = new EmailsAttach();
                emailAttach.setEmailNum(emailNumTxt);
                ArrayList<FilesCompressed> filesCompressedList = new ArrayList<FilesCompressed>();
                

                // Contar adjuntos
                int cntAttach = 0;
                // Contar adjunto zip, rar, 7z
                int cntAttachCompCorrect = 0;
                // Contar adjunto zip, rar, 7z, xml, pdf
                int cntAttachCorrect = 0;
                // Contar xml
                int cntAttachXml = 0;
                // Contar pdf
                int cntAttachPdf = 0;
                
                //Limpiar listas para el proximo correo
                filesXmlList.clear();
                filesPdfList.clear();
                filesOtherList.clear();
                
                filesXmlCompList.clear();
                filesPdfCompList.clear();
                filesOtherCompList.clear();
                
                
                for (int j = 0; j < multiPart.getCount(); j++) {
                    MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(j);
                    if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                        cntAttach++;
                        FilesCompressed filesCompressed = new FilesCompressed();
                        FilesXml filesXml = new FilesXml();
                        FilesPdf filesPdf = new FilesPdf();
                        FilesOther filesOther = new FilesOther();
                        String fileName = part.getFileName();
                        log("Found Attachment");
                        log("Nombre Adjunto original" + fileName);

                        String extension = FilenameUtils.getExtension(fileName);

                        // Save InputStream from Attachment
                        InputStream input = part.getInputStream();


                        switch (extension.toUpperCase()) {
                        case EZIP:
                            cntAttachCompCorrect++;
                            cntAttachCorrect++;
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(extension.toUpperCase()); //EZIP
                            filesCompressed.setFileCompressed(input);
                            filesCompressedList.add(filesCompressed);
                            break;
                        case ERAR:
                            log("Entro if Rar");
                            cntAttachCompCorrect++;
                            cntAttachCorrect++;
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(extension.toUpperCase()); //ERAR
                            filesCompressed.setFileCompressed(input);
                            //Guardar archivos adjuntos
                            try {
                                //descomprimirRar(input, pathUploadSfpt, fileName, sftpChannel, sesConnection);
                                filesCompressed.setFilesXml(filesXmlCompList);
                                filesCompressed.setFilesPdf(filesPdfCompList);
                                filesCompressed.setFilesOther(filesOtherCompList);
                                filesCompressedList.add(filesCompressed);
                           }  catch (Exception e) {
                                // Controlar error. No debe guardarlo
                                log("Entro Exception: " + e.getMessage() + Arrays.toString(e.getStackTrace()));
                            }
                            break;
                        case E7Z:
                            cntAttachCompCorrect++;
                            cntAttachCorrect++;
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(extension.toUpperCase()); //E7Z
                            filesCompressed.setFileCompressed(input);
                            //Guardar archivos adjuntos
                            try {
                               // descomprimirRar(input, pathUploadSfpt, fileName, sftpChannel, sesConnection);
                                filesCompressed.setFilesXml(filesXmlCompList);
                                filesCompressed.setFilesPdf(filesPdfCompList);
                                filesCompressed.setFilesOther(filesOtherCompList);
                                filesCompressedList.add(filesCompressed);
                            } catch (Exception e) {
                                // Controlar error. No debe guardarlo
                            }
                            break;
                        case EXML:
                            cntAttachCorrect++;
                            cntAttachXml++;
                            filesXml.setFileName(fileName);
                            filesXml.setFileExt(extension.toUpperCase()); //EXML
                            filesXml.setFileXml(input);
                            filesXmlList.add(filesXml);
                            break;
                        case EPDF:
                            cntAttachCorrect++;
                            cntAttachPdf++;
                            filesPdf.setFileName(fileName);
                            filesPdf.setFileExt(extension.toUpperCase()); //EPDF
                            filesPdf.setFilePdf(input);
                            filesPdfList.add(filesPdf);
                            break;
                        default:
                            filesOther.setFileName(fileName);
                            filesOther.setFileExt(EOT);
                            filesOtherList.add(filesOther);
                        }
                        
                        //AQUI VA PROCE PARA ELIMINAR ARCHIVO DE SFTP
                        //String command = +"rm -f * ";
                        /*String command = "cd "+pathUploadSfpt+"\n"+
                                       "rm -f *"+"\n";
                        String responseDrop = sendCommand(command, sesConnection);
                        log("Archivos Eliminados: "+responseDrop);*/
                    }

                }

                emailAttach.setFileCompressed(filesCompressedList);
                emailAttach.setFilesXml(filesXmlList);
                emailAttach.setFilesPdf(filesPdfList);
                emailAttach.setFilesOther(filesOtherList);
                
                //cierre conexiones
                /*sftpChannel.exit();
                sftpChannel.disconnect();
                sesConnection.disconnect();
                channel.disconnect();*/

                //sesConnection.disconnect();

                // Validar si hay por lo menos un adjunto
                if (cntAttach == 0) {
                    emsg = "No hay adjuntos en el correo n�mero " + emailNumTxt;
                    System.out.println("Mensaje: " + emsg);
                    continue;
                }
                // Validar si hay rar, zip, 7z, xml y pdf
                if (cntAttachCorrect == 0) {
                    emsg =
                        "En el correo n�mero " + emailNumTxt +
                        " no se encontr� ning�n adjunto con alguna de las siguientes extensiones: *.zip , *.rar , *.7z , *.xml , *.pdf";
                    System.out.println("Mensaje: " + emsg);
                    continue;
                }
                // Validar si hay rar, zip, 7z, xml y pdf
                if (cntAttachCorrect == 1 && cntAttachPdf > 0) {
                    emsg =
                        "En el correo n�mero " + emailNumTxt +
                        " no se encontr� ning�n adjunto con alguna de las siguientes extensiones: *.zip , *.rar , *.7z , *.xml";
                    System.out.println("Mensaje: " + emsg);
                    continue;
                }
                // Validar si hay m�s de un xml
                if (cntAttachXml > 1) {
                    emsg =
                        "En el correo n�mero " + emailNumTxt + " se encontr� m�s de un archivo con la extensi�n *.xml";
                    System.out.println("Mensaje: " + emsg);
                    continue;
                }
                // Validar si hay m�s de un comprimido en los adjuntos
                if (cntAttachCompCorrect>1) {
                    emsg =
                        "En el correo n�mero " + emailNumTxt +
                        " se encontr� m�s de un adjunto comprimido con alguna de las siguientes extensiones: *.zip , *.rar , *.7z";
                    System.out.println("Mensaje: " + emsg);
                    continue;
                }

                //=====================================ESPACIO PARA EL METODO==========================//
                /* RECORRER LOS ARCHIVOS EN LAS CLASES
                 * SE PODRA REALIZAR COUNT DE LOS ARREGLOS SEGUN EL TIPO DE ARCHIVO
                 * Y ASI REALIZAR LAS VALIDACIONES CORRECTAMENTE
                 * Y SEGUN LA VALIDACION SE IRA ENVIANDO O NO, EL XML Y PDF*/
                //log("N�mero de comprimidos adjuntos" + emailAttach.getFileCompressed().size());
                String msg = validateAndSendFileToDb(emailAttach, conn);
                System.out.println("Mensaje: " + msg);
                //=====================================ESPACIO PARA EL METODO==========================//
                //System.out.println("Mensaje: " + emsg);

            }
        } catch (NoSuchProviderException e) {
            log("Error-NoSuchProviderException...");
            e.printStackTrace();
        } catch (MessagingException e) {
            log("Error-MessagingException..." + e.getMessage() + " - " + Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        } catch (IOException e) {
            log("Error-IOException...");
            e.printStackTrace();
        } catch (Exception e) {
        }
    }

    public static void readZip(InputStream reportIs, OracleConnection trans) {
        try {
            String st = "begin NC_CPE_CARGA_BLOB_PKG.PR_CARGAR(pv_nombre => ?, pb_archivo => ?); end;";
            OracleCallableStatement acs = (OracleCallableStatement) trans.prepareCall(st);

            acs.setString(1, "F_Prueba.zip");
            acs.setBlob(2, reportIs);
            //acs.setNUMBER(2, idAttachment);
            //acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
            //acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            //String procStatus = acs.getString(3);
            //String procMessage = acs.getString(4);

            /*if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }*/

        } catch (SQLException exsql) {
            log("Error SQL en readReportOutput(): " + exsql.getMessage());
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage());
        } catch (Exception e) {
            log("Error en readReportOutput(): " + e.getMessage());
            System.out.println("Error en readReportOutput() - " + e.getMessage());
        }

    }


    public static InputStream readFileFromInputStream(ChannelSftp sftpChannel, String rutaNombre) throws SftpException {

        log("Ruta para obtener InputStream de xml o pdf: "+rutaNombre);
        InputStream stream = sftpChannel.get(rutaNombre); //"/upload/XML/pruebaXml3.xml"
        
        if(stream != null){
            
                log("InputStram lleno");
        }


        return stream;
    }

    public static boolean uploadSftpFromInputStream(InputStream localFile, String sftpFile,
                                                    ChannelSftp channelSftp) throws SftpException {

        //channelSftp.cd("");
        if (localFile != null) {
            System.out.println("" + sftpFile);
            channelSftp.put(localFile, sftpFile);
            System.out.println("Upload Complete");
        }
        //channelSftp.exit();
        return true;
    }

    public static String sendCommand(String command, com.jcraft.jsch.Session sesConnection) {
        StringBuilder outputBuffer = new StringBuilder();

        try {
            ChannelExec channel = (ChannelExec) sesConnection.openChannel("exec");
            channel.setCommand(command);
            //Channel channel = sesConnection.openChannel("sftp");
            //((ChannelExec)channel).setCommand(command);
            channel.setInputStream(null);
            //((ChannelExec)channel).setErrStream(System.err);


            InputStream commandOutput = channel.getInputStream();
            channel.connect();
            int readByte = commandOutput.read();

            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }

            channel.disconnect();
        } catch (IOException ioX) {
            //logWarning(ioX.getMessage());
            return null;
        } catch (JSchException jschX) {
            //logWarning(jschX.getMessage());
            return null;
        }

        return outputBuffer.toString();
    }

    private static void descomprimir7z(InputStream localFile, String pathUploadSFTP, String fileNameRar,
                                       ChannelSftp sftpChannel,
                                       com.jcraft.jsch.Session sesConnection) throws IOException, SftpException {
        FilesXml filesXml = new FilesXml();
        FilesPdf filesPdf = new FilesPdf();
        FilesOther filesOther = new FilesOther();
        String outputCommand = "";

        uploadSftpFromInputStream(localFile, pathUploadSFTP + fileNameRar, sftpChannel);
        // Enviar comando para descomprimir
        //outputCommand = sendCommand("unrar", sesConnection);
        // Enviar comando para listar
        //outputCommand = sendCommand("ls", sesConnection);
        // Con la lista de nombre de archivos se debe verificar que extension es y segun esto ir agregando
        //INICIO DE LOOP DE NOMBRES
        /*switch ("XML") {
            case "XML":
                filesXml.setFileName("archivo.xml");
                filesXml.setFileExt(EXML);
                filesXml.setFileXml(readFileFromInputStream(sftpChannel, filesXml.getFileName()));
                filesXmlList.add(filesXml);
                break;
            case "PDF":
                filesPdf.setFileName("archivo.pdf");
                filesPdf.setFileExt(EXML);
                filesPdf.setFilePdf(readFileFromInputStream(sftpChannel, filesPdf.getFileName()));
                filesPdfList.add(filesPdf);
                break;
            default:
                filesOther.setFileName("archivo.jpg");
                filesOther.setFileExt(EOT);
                filesOtherList.add(filesOther);
            }*/
        //FIN DE LOOP DE NOMBRES
        sftpChannel.exit();
        filesOtherList.add(filesOther);
    }

    private static void descomprimirRar(InputStream localFile, String pathUploadSFTP, String fileNameRar,
                                        ChannelSftp sftpChannel,
                                        com.jcraft.jsch.Session sesConnection) throws IOException, SftpException {
        

        FilesXml filesXml = new FilesXml();
        FilesPdf filesPdf = new FilesPdf();
        FilesOther filesOther = new FilesOther();
        String outputCommand = "";
        
        String fileNameRarD = "\""+fileNameRar+"\"";

        log("ruta sftp: " + pathUploadSFTP + fileNameRar);
        //log("unar " + fileNameRar + " " + "" + pathUploadSFTP);
        uploadSftpFromInputStream(localFile, pathUploadSFTP + fileNameRar, sftpChannel);
        // Enviar comando para descomprimir
          String comando = "cd "+pathUploadSFTP+"\n"+
                         "unar -D "+fileNameRarD+"\n";
          outputCommand = sendCommand(comando, sesConnection);
          log("command1: "+outputCommand);
        // Enviar comando para listar
        outputCommand = sendCommand("ls " + pathUploadSFTP, sesConnection);
        String[] loopFile = outputCommand.split("\n");
        List<String> containerFiles = Arrays.asList(loopFile);

        log("size: " + containerFiles.size());
        log("command2: " + outputCommand);
        // Con la lista de nombre de archivos se debe verificar que extension es y segun esto ir agregando
        // INICIO DE LOOP DE ARCHIVOS

        for (int i = 0; i < containerFiles.size(); i++) {
            String nombreArchivo = loopFile[i];
            log("name: " + nombreArchivo);

            String extension = FilenameUtils.getExtension(nombreArchivo);
            log("extension: " + extension);
            
            if(extension.toUpperCase().equals(ERAR) || extension.toUpperCase().equals(E7Z)){
                log("Entro if rar o 7z");
                continue;
            }


            switch (extension.toUpperCase()) {
            case EXML:
                log("Entro case XML");
                
                filesXml.setFileName(nombreArchivo);
                filesXml.setFileExt(EXML);
                filesXml.setFileXml(readFileFromInputStream(sftpChannel, pathUploadSFTP+filesXml.getFileName()));
                filesXmlCompList.add(filesXml);
                break;
            case EPDF:
                log("Entro case PDF");
                filesPdf.setFileName(nombreArchivo);
                filesPdf.setFileExt(EXML);
                filesPdf.setFilePdf(readFileFromInputStream(sftpChannel, pathUploadSFTP+filesPdf.getFileName()));
                filesPdfCompList.add(filesPdf);
                break;
            default:
                filesOther.setFileName("archivo.jpg");
                filesOther.setFileExt(EOT);
                filesOtherCompList.add(filesOther);
            }

        }
        // FIN LOOP DE ARCHIVOS
        // CERRAR CONEXIONES USADAS ACA, NO LA DEL PARAMETRO
    }

    private static String validateAndSendFileToDb(EmailsAttach emailAttach, OracleConnection conn) {
        String result = "";
        FilesCompressed fileCompressed = new FilesCompressed();
        FilesXml fileXml = new FilesXml();
        FilesPdf filePdf = new FilesPdf();
        // Comprimidos
        FilesXml fileXmlComp = new FilesXml();
        FilesPdf filePdfComp = new FilesPdf();
        // Archivos Comprimidos
        ArrayList<FilesCompressed> fileCompressedList = new ArrayList<FilesCompressed>();
        fileCompressedList = emailAttach.getFileCompressed();
        int sizeFileCompressedList = fileCompressedList.size();
        // Archivos XML
        log("nro de xml: "+emailAttach.getFilesXml().size());
        ArrayList<FilesXml> filesXmlList = new ArrayList<FilesXml>();
        filesXmlList = emailAttach.getFilesXml();
        int sizeFilesXmlList = filesXmlList.size();
        // Archivos PDF
        ArrayList<FilesPdf> filesPdfList = new ArrayList<FilesPdf>();
        filesPdfList = emailAttach.getFilesPdf();
        int sizeFilesPdfList = filesPdfList.size();
        // Archivos OTROS
        ArrayList<FilesOther> filesOtherList = new ArrayList<FilesOther>();
        filesOtherList = emailAttach.getFilesOther();
        int sizeFilesOtherList = filesOtherList.size();
        
        // Lista para archivos comprimidos
        ArrayList<FilesXml> filesXmlCompList = new ArrayList<FilesXml>();
        int sizeFilesXmlCompList = 0;
        ArrayList<FilesPdf> filesPdfCompList = new ArrayList<FilesPdf>();
        int sizeFilesPdfCompList = 0;
        
        // Archivos
        InputStream isFileXml = null;
        InputStream isFilePdf = null;
        String nameFileXml = "";
        String nameFilePdf = "";
        // Validar: Si tiene un XML no comprimido, ya no considera el adjunto
        if (sizeFilesXmlList == 1) {
            log("Entro Validaci�n Si tiene un XML no comprimido, ya no considera el adjunto");
            Iterator<FilesXml> itrFilesXml = filesXmlList.iterator();
            while (itrFilesXml.hasNext()) {
                fileXml = new FilesXml();
                fileXml = itrFilesXml.next();
                isFileXml = fileXml.getFileXml();
                nameFileXml = fileXml.getFileName();
            }
            // Validar: Si tiene m�s de un archivo PDF, no env�a los PDF, solo env�a el XML
            if (sizeFilesPdfList == 1) {
                Iterator<FilesPdf> itrFilesPdf = filesPdfList.iterator();
                while (itrFilesPdf.hasNext()) {
                    filePdf = new FilesPdf();
                    filePdf = itrFilesPdf.next();
                    isFilePdf = filePdf.getFilePdf();
                    nameFilePdf = filePdf.getFileName();
                }
            }
        } else {
            // No hay xml, hay Comprimidos y/o Pdf y/u Otros archivos
            // Si hay comprimidos: Obtener el xml y pdf
            if (sizeFileCompressedList > 0) {
                Iterator<FilesCompressed> itrFileCompressed = fileCompressedList.iterator();
                while (itrFileCompressed.hasNext()) {
                    fileCompressed = new FilesCompressed();
                    fileCompressed = itrFileCompressed.next();
                    if (fileCompressed.getFileExt().equals(EZIP)) {
                        //readZip(fileCompressed.getFileCompressed(), conn);// Se validaria en la bd los archivos que contiene
                        result = "Nota: Zip enviado a la BD";
                        return (result);
                    }
                    filesXmlCompList = fileCompressed.getFilesXml();
                    sizeFilesXmlCompList = filesXmlCompList.size();
                    filesPdfCompList = fileCompressed.getFilesPdf();
                    sizeFilesPdfCompList = filesPdfCompList.size();
                    
                    if (sizeFilesXmlCompList == 0) {
                        result = "En el adjunto no se encontr� un archivo con la extensi�n *.xml";
                        return (result);
                    }
                    if (sizeFilesXmlCompList > 1) {
                        result = "En el adjunto se encontr� m�s de un archivo con la extensi�n *.xml";
                        return (result);
                    }
                    if (sizeFilesPdfCompList == 0) {
                        result = "Nota: En el adjunto no se encontr� un archivo con la extensi�n *.pdf";
                    }
                    if (sizeFilesPdfCompList > 1) {
                        result = "Nota: En el adjunto se encontr� m�s un archivo con la extensi�n *.pdf";
                    }
                    // Obtener XML comprimido
                    Iterator<FilesXml> itrFilesXmlComp = filesXmlCompList.iterator();
                    while (itrFilesXmlComp.hasNext()) {
                        fileXmlComp = new FilesXml();
                        fileXmlComp = itrFilesXmlComp.next();
                        isFileXml = fileXmlComp.getFileXml();
                        nameFileXml = fileXmlComp.getFileName();
                    }
                    // Obtener PDF comprimido
                    if (sizeFilesPdfCompList == 1) {
                        Iterator<FilesPdf> itrFilesPdfComp = filesPdfCompList.iterator();
                        while (itrFilesPdfComp.hasNext()) {
                            filePdfComp = new FilesPdf();
                            filePdfComp = itrFilesPdfComp.next();
                            isFilePdf = filePdfComp.getFilePdf();
                            nameFilePdf =  filePdfComp.getFileName();
                        }
                    }
                }
            } else {
                result = "En el adjunto no se encontr� un archivo comprimido con la extensi�n *.zip, *.7z, *.rar";
                return (result);
            }
            if ((sizeFilesPdfList+sizeFilesPdfCompList)>1) {
                isFilePdf = null;
                nameFilePdf = "";
            }
        }
        result = sendXmlAndPdfToDb(isFileXml, isFilePdf, nameFileXml, nameFilePdf);
        return (result);
    }

    private static String sendXmlAndPdfToDb(InputStream isFileXml, InputStream isFilePdf, String nameFileXml, String nameFilePdf) {
        
        log("Entro proc sendXmlAndPdfToDb");
        if(isFileXml !=null){
                log("Tiene InputStream xml lleno");
            }
        if(isFilePdf !=null){
                log("Tiene InputStream pdf lleno");
            }
        
        String result = "";
        return (result);
    }
}
