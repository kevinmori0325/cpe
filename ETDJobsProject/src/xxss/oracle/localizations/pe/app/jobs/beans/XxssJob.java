package xxss.oracle.localizations.pe.app.jobs.beans;

import java.sql.SQLException;

import java.util.Arrays;
import java.util.Date;

import javax.naming.InitialContext;

import javax.naming.NamingException;

import javax.sql.DataSource;

import oracle.jdbc.OracleConnection;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import xxss.oracle.localizations.pe.app.jobs.view.listeners.JobResult;

public abstract class XxssJob implements Job {
    protected static Logger log = Logger.getLogger(XxssJob.class);
    protected StringBuilder jobLog;
    protected String newLine;
    protected JobResult jobResult;
    protected String implementClassName;
    
    public XxssJob() {
        super();
    }
    
    abstract public void doAction(JobDataMap dataMap, OracleConnection conn) throws Exception;
    
    protected void log(String message) {
        jobLog.append(message + newLine);
    }
    
    protected void logWithTimestamp(String message) {
        Date date = new Date();
        this.log(date + ": " + message);
    }

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        jobResult = new JobResult();
        jobLog = new StringBuilder();        
        newLine = System.getProperty("line.separator"); // System.lineSeparator() for java > 1.6
        
        jobResult.setJobLog(jobLog);
        jobExecutionContext.setResult(jobResult);
        
        this.logWithTimestamp("Hello " + this.implementClassName);
        
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        
        try {
            ctx = new InitialContext();
            //ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            ds = (DataSource)ctx.lookup("jdbc/XXSSPELOCDBCS");
            conn = (OracleConnection)ds.getConnection();

            conn.setAutoCommit(false);
            
            doAction(dataMap, conn);
            
            conn.close();
            
            jobResult.setStatus(JobResult.SUCCESS);
            this.logWithTimestamp("Bye " + this.implementClassName);
        } catch (Exception ex) {
            jobResult.setStatus(JobResult.ERROR);
            log.error("Error on execute(): " + ex.getMessage(), ex);
            
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    jobResult.setStatus(JobResult.ERROR);
                    log.error("Error on execute(): " + sqlex.getMessage(), sqlex);
                    this.logWithTimestamp("Bye SQLException"+sqlex.getStackTrace() + this.implementClassName);
                    throw new JobExecutionException(sqlex);
                }
            }
            
            this.logWithTimestamp("Bye Exception"+ex.getStackTrace() + this.implementClassName);            
            throw new JobExecutionException(ex);
        } 
    }
}
