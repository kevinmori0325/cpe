package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;

public class CPEDetracciones {
    String paramName;
    String paramValue;
    String attribute1;
    String attribute2;
    String attribute3;
    
    public CPEDetracciones() {
        super();
    }

    @XmlElement(name = "PARAM_NAME")
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamName() {
        return paramName;
    }

    @XmlElement(name = "PARAM_VALUE")
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getParamValue() {
        return paramValue;
    }

    @XmlElement(name = "SEGMENT1")
    public void setAttribute1(String attribute1) {
        this.attribute1 = attribute1;
    }

    public String getAttribute1() {
        return attribute1;
    }

    @XmlElement(name = "SEGMENT2")
    public void setAttribute2(String attribute2) {
        this.attribute2 = attribute2;
    }

    public String getAttribute2() {
        return attribute2;
    }

    @XmlElement(name = "SEGMENT3")
    public void setAttribute3(String attribute3) {
        this.attribute3 = attribute3;
    }

    public String getAttribute3() {
        return attribute3;
    }
}
