package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CPEHeader {
    long cashReceiptId;
    long legalEntityId;
    Date receiptDate;
    String receiptNumber;
    double amount;
    String currencyCode;
    double exchangeRate;
    CPECustomers[] customers;
    String businessUnit;
    String receiptMethodName;
    
    public CPEHeader() {
        super();
    }

    @XmlElement(name = "CASH_RECEIPT_ID")
    public void setCashReceiptId(long cashReceiptId) {
        this.cashReceiptId = cashReceiptId;
    }

    public long getCashReceiptId() {
        return cashReceiptId;
    }
    
    @XmlElement(name = "LEGAL_ENTITY_ID")
    public void setLegalEntityId(long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public long getLegalEntityId() {
        return legalEntityId;
    }

    @XmlElement(name = "RECEIPT_DATE")
    public void setReceiptDate(Date receiptDate) {
        this.receiptDate = receiptDate;
    }

    public Date getReceiptDate() {
        return receiptDate;
    }

    @XmlElement(name = "RECEIPT_NUMBER")
    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    @XmlElement(name = "AMOUNT")
    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    @XmlElement(name = "CURRENCY_CODE")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @XmlElement(name = "EXCHANGE_RATE")
    public void setExchangeRate(double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }

    @XmlElement(name = "G_CUSTOMERS")
    public void setCustomers(CPECustomers[] customers) {
        this.customers = customers;
    }

    public CPECustomers[] getCustomers() {
        return customers;
    }

    @XmlElement(name = "BUSINESS_UNIT")
    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    @XmlElement(name = "RECEIPT_METHOD_NAME")
    public void setReceiptMethodName(String receiptMethodName) {
        this.receiptMethodName = receiptMethodName;
    }

    public String getReceiptMethodName() {
        return receiptMethodName;
    }
}
