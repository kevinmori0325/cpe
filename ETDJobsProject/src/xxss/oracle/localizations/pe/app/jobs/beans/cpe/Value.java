package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;

public class Value {
    String returnCode;
    String returnMessage;
    
    public Value() {
        super();
    }

    @XmlElement(name = "ReturnCode")
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCode() {
        return returnCode;
    }

    @XmlElement(name = "ReturnMessage")
    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getReturnMessage() {
        return returnMessage;
    }
}
