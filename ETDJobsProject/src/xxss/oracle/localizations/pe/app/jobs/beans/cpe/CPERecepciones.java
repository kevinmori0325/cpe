package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;

public class CPERecepciones
{
  String guia_remision;
  String nro_oc;
  long vendor_id;
  String vendor_site_code;
  String vendor_name;
  long org_id;
  String bu_name;
  long legal_entity_id;
  String legal_entity_name;
  long line_num_oc;
  String item_number;
  String item_description;
  String uom_code;
  long quantity;
  double unit_price;
  String receipt_num;
  long line_num_rcv;
  long quantity_rcv;
  long amount_rcv;
  long receiptId;
  String ruc;
  String itemTypeLookup;
  long ledgerId;
  String vendorNumber;
  String currencyCode;
  String ocOblig;
  
  public CPERecepciones()
  {
    super();
  }

    @XmlElement(name = "OC_OBLIGATORIO")
    public void setOcOblig(String ocOblig) {
        this.ocOblig = ocOblig;
    }

    public String getOcOblig() {
        return ocOblig;
    }

    @XmlElement(name = "CURRENCY_CODE")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @XmlElement(name = "VENDOR_NUMBER")
    public void setVendorNumber(String vendorNumber) {
        this.vendorNumber = vendorNumber;
    }

    public String getVendorNumber() {
        return vendorNumber;
    }

    @XmlElement(name = "PRIMARY_LEDGER_ID")
    public void setLedgerId(long ledgerId) {
        this.ledgerId = ledgerId;
    }

    public long getLedgerId() {
        return ledgerId;
    }

    @XmlElement(name = "ITEM_LOOKUP")
    public void setItemTypeLookup(String itemTypeLookup) {
        this.itemTypeLookup = itemTypeLookup;
    }

    public String getItemTypeLookup() {
        return itemTypeLookup;
    }

    @XmlElement(name = "RUC")
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getRuc() {
        return ruc;
    }

    @XmlElement(name = "RECEIPT_ID")
    public void setReceiptId(long receiptId) {
        this.receiptId = receiptId;
    }

    public long getReceiptId() {
        return receiptId;
    }

    @XmlElement(name = "GUIA_REMISION")
  public void setGuia_remision(String guia_remision)
  {
    this.guia_remision = guia_remision;
  }

  public String getGuia_remision()
  {
    return guia_remision;
  }
  
  @XmlElement(name = "NRO_OC")
  public void setNro_oc(String nro_oc)
  {
    this.nro_oc = nro_oc;
  }
  
  public String getNro_oc()
  {
    return nro_oc;
  }
  
  @XmlElement(name = "VENDOR_ID")
  public void setVendor_id(long vendor_id)
  {
    this.vendor_id = vendor_id;
  }

  public long getVendor_id()
  {
    return vendor_id;
  }
  
  @XmlElement(name = "VENDOR_SITE_CODE")
  public void setVendor_site_code(String vendor_site_code)
  {
    this.vendor_site_code = vendor_site_code;
  }

  public String getVendor_site_code()
  {
    return vendor_site_code;
  }
  
  @XmlElement(name = "VENDOR_NAME")
  public void setVendor_name(String vendor_name)
  {
    this.vendor_name = vendor_name;
  }

  public String getVendor_name()
  {
    return vendor_name;
  }
  
  @XmlElement(name = "ORG_ID")
  public void setOrg_id(long org_id)
  {
    this.org_id = org_id;
  }

  public long getOrg_id()
  {
    return org_id;
  }
  
  @XmlElement(name = "BU_NAME")
  public void setBu_name(String bu_name)
  {
    this.bu_name = bu_name;
  }

  public String getBu_name()
  {
    return bu_name;
  }
  
  @XmlElement(name = "LEGAL_ENTITY_ID")
  public void setLegal_entity_id(long legal_entity_id)
  {
    this.legal_entity_id = legal_entity_id;
  }

  public long getLegal_entity_id()
  {
    return legal_entity_id;
  }
  
  @XmlElement(name = "LEGAL_ENTITY_NAME")
  public void setLegal_entity_name(String legal_entity_name)
  {
    this.legal_entity_name = legal_entity_name;
  }

  public String getLegal_entity_name()
  {
    return legal_entity_name;
  }
  
  @XmlElement(name = "NRO_LINEA_OC")
  public void setLine_num_oc(long line_num_oc)
  {
    this.line_num_oc = line_num_oc;
  }

  public long getLine_num_oc()
  {
    return line_num_oc;
  }
  
  @XmlElement(name = "ARTICULO")
  public void setItem_number(String item_number)
  {
    this.item_number = item_number;
  }

  public String getItem_number()
  {
    return item_number;
  }
  
  @XmlElement(name = "DESCRIPCION")
  public void setItem_description(String item_description)
  {
    this.item_description = item_description;
  }

  public String getItem_description()
  {
    return item_description;
  }
  
  @XmlElement(name = "UNM")
  public void setUom_code(String uom_code)
  {
    this.uom_code = uom_code;
  }

  public String getUom_code()
  {
    return uom_code;
  }
  
  @XmlElement(name = "CANTIDAD")
  public void setQuantity(long quantity)
  {
    this.quantity = quantity;
  }

  public long getQuantity()
  {
    return quantity;
  }
  
  @XmlElement(name = "PRECIO")
  public void setUnit_price(double unit_price)
  {
    this.unit_price = unit_price;
  }

  public double getUnit_price()
  {
    return unit_price;
  }
  
  @XmlElement(name = "NRO_RECEPCION")
  public void setReceipt_num(String receipt_num)
  {
    this.receipt_num = receipt_num;
  }

  public String getReceipt_num()
  {
    return receipt_num;
  }
  
  @XmlElement(name = "NRO_LINEA_RECEPCION")
  public void setLine_num_rcv(long line_num_rcv)
  {
    this.line_num_rcv = line_num_rcv;
  }

  public long getLine_num_rcv()
  {
    return line_num_rcv;
  }
  
  @XmlElement(name = "CANTIDAD_RECIBIDA")
  public void setQuantity_rcv(long quantity_rcv)
  {
    this.quantity_rcv = quantity_rcv;
  }

  public long getQuantity_rcv()
  {
    return quantity_rcv;
  }
  
  @XmlElement(name = "IMPORTE_RECIBIDO")
  public void setAmount_rcv(long amount_rcv)
  {
    this.amount_rcv = amount_rcv;
  }

  public long getAmount_rcv()
  {
    return amount_rcv;
  }

}
