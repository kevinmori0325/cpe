package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class CREMiscDataDS {
    CREPayDoc[] payDocs;
    
    public CREMiscDataDS() {
        super();
    }

    @XmlElement(name = "G_PAY_DOC")
    public void setPayDocs(CREPayDoc[] payDocs) {
        this.payDocs = payDocs;
    }

    public CREPayDoc[] getPayDocs() {
        return payDocs;
    }
}
