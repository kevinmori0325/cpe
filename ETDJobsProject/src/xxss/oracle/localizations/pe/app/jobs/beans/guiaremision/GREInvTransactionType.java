package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GREInvTransactionType {
    long transactionTypeId;
    String transactionTypeName;
    int transactionActionId;
    String transactionActionName;
    String greFlag;
    String greType;
    
    public GREInvTransactionType() {
        super();
    }

    @XmlElement(name = "TRANSACTION_TYPE_ID")
    public void setTransactionTypeId(long transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public long getTransactionTypeId() {
        return transactionTypeId;
    }

    @XmlElement(name = "TRANSACTION_TYPE_NAME")
    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }

    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    @XmlElement(name = "TRANSACTION_ACTION_ID")
    public void setTransactionActionId(int transactionActionId) {
        this.transactionActionId = transactionActionId;
    }

    public int getTransactionActionId() {
        return transactionActionId;
    }

    @XmlElement(name = "TRANSACTION_ACTION_NAME")
    public void setTransactionActionName(String transactionActionName) {
        this.transactionActionName = transactionActionName;
    }

    public String getTransactionActionName() {
        return transactionActionName;
    }

    @XmlElement(name = "GRE_FLAG")
    public void setGreFlag(String greFlag) {
        this.greFlag = greFlag;
    }

    public String getGreFlag() {
        return greFlag;
    }

    @XmlElement(name = "GRE_TYPE")
    public void setGreType(String greType) {
        this.greType = greType;
    }

    public String getGreType() {
        return greType;
    }
}
