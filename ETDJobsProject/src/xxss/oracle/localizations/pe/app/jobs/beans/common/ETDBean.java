package xxss.oracle.localizations.pe.app.jobs.beans.common;

import xxss.oracle.localizations.services.signature.helper.response.ProcessResult;
import xxss.oracle.localizations.services.signature.helper.response.Respuesta;

public class ETDBean {
    String request;
    String response;
    ProcessResult processResult;
    Respuesta respuesta;
    
    public ETDBean() {
        super();
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getRequest() {
        return request;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

    public void setProcessResult(ProcessResult processResult) {
        this.processResult = processResult;
    }

    public ProcessResult getProcessResult() {
        return processResult;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }
}
