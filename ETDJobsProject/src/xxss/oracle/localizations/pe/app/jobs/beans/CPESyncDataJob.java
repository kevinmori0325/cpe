package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URL;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleConnection;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;

import org.xml.sax.InputSource;

import xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica.CREMiscDataDS;
import xxss.oracle.localizations.pe.app.jobs.beans.cpe.CPEDetracciones;
import xxss.oracle.localizations.pe.app.jobs.beans.cpe.CPERecepciones;
import xxss.oracle.localizations.pe.app.jobs.beans.cpe.CPESyncDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREBusinessUnit;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREInvTransactionType;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREInvTransactions;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRELegalEntity;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRELegalEntityBUWarehouseDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREMiscDataDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREReceptorsDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREWarehouse;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil2;

@DisallowConcurrentExecution
public class CPESyncDataJob extends XxssJob
{
  public CPESyncDataJob()
  {
    super();
  }

  @Override
  public void doAction(JobDataMap dataMap, OracleConnection conn)
    throws Exception
  {
    // TODO Implement this method
    String userName = dataMap.getString("USER_NAME"); //dataMap.getString("biUserName");
    String userPass = dataMap.getString("USER_PASS"); //dataMap.getString("biUserPass");
    String instanceName = dataMap.getString("INSTANCE_NAME"); //dataMap.getString("instanceName");
    String dataCenter = dataMap.getString("DATA_CENTER"); //dataMap.getString("dataCenter");
    
    String cpeRecepcionesXMLPath = dataMap.getString("CPE_RECEPCIONES_XML_PATH");
    String cpeDetraccionesXMLPath = dataMap.getString("CPE_DETRACCIONES_XML_PATH");
    String urlRest = dataMap.getString("URL_WS_REST");
    String urlDetraRest = dataMap.getString("URL_WS_DETRA_REST");
      String urlDeleteReceiptRest = dataMap.getString("URL_WS_DELETE");
    //URL_WS_DELETE

    
    byte[] reportOutputBytes;
    InputStream reportIs;
    JAXBContext jaxbContextDataDS;
    Unmarshaller jaxbUnmarshaller;

    ExternalReportWSSService svcReport = ExternalReportWSUtil2.createExternalReportWSSServiceUserToken2(instanceName, dataCenter, userName,
                                                                             userPass);
    
    if (svcReport != null) {
        logWithTimestamp("ExternalReportWS created");
    } else {
        throw new Exception("ExternalReportWS is not created.");
    }
        
    //Extraccion de Recepciones
    reportOutputBytes = this.runReport(svcReport, cpeRecepcionesXMLPath);

    if (reportOutputBytes == null || reportOutputBytes.length == 0) {
        throw new Exception(cpeRecepcionesXMLPath + " report cannot be executed.");
    }
    
    logWithTimestamp(cpeRecepcionesXMLPath + " report was executed. " + reportOutputBytes.length);
    
    reportIs = new ByteArrayInputStream(reportOutputBytes);

    jaxbContextDataDS = JAXBContext.newInstance(CPESyncDS.class);
    jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

    CPESyncDS dataCPERecepcionesDS =
        (CPESyncDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));
    
    this.processCPERecepcionesData(dataCPERecepcionesDS,userName,userPass,urlRest,urlDeleteReceiptRest);
    
      //Extraccion de Detracciones
      reportOutputBytes = this.runReport(svcReport, cpeDetraccionesXMLPath);

      if (reportOutputBytes == null || reportOutputBytes.length == 0) {
          throw new Exception(cpeRecepcionesXMLPath + " report cannot be executed.");
      }
      
      logWithTimestamp(cpeDetraccionesXMLPath + " report was executed. " + reportOutputBytes.length);
      
      reportIs = new ByteArrayInputStream(reportOutputBytes);

      jaxbContextDataDS = JAXBContext.newInstance(CPESyncDS.class);
      jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

      CPESyncDS dataCPEDetraccionesDS =
          (CPESyncDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));
    
    this.processCPEDetraccionesData(dataCPEDetraccionesDS,userName,userPass,urlDetraRest);
    
      logWithTimestamp("Fin Ejecución");

    //conn.commit();    
  }
  
  private void processCPERecepcionesData(CPESyncDS dataDS, String usuario, String password, String url,String urlDelete) throws SQLException, Exception {
      
      
    this.callDeleteReceipt("RECEIPT", urlDelete);   

    CPERecepciones[] txns = dataDS.getRecepciones();
    
    if (txns != null) {
        for (int i=0; i<txns.length; i++) {
            
            this.callInsertReceipt(txns[i], usuario, password, "", "", url);

        }
        

    }
  }
  
    private void processCPEDetraccionesData(CPESyncDS dataDS, String usuario, String password, String url) throws SQLException, Exception {
        
       

      CPEDetracciones[] txns = dataDS.getDetra();
      
      if (txns != null) {
          for (int i=0; i<txns.length; i++) {
              
              this.callInsertDetra(txns[i], usuario, password, "", "", url);

          }
          

      }
    }
    
    public String callDeleteReceipt(String table,String url) throws MalformedURLException, IOException {

        String responseString = "";
        String outputString = "";


       URL oURL = new URL(url);
        
        HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
           

        String jsonInput ="{\n" + 
        "\"nameTable\" : \""+table+"\"\n" + 
        
        "}";
       
        logWithTimestamp("JSON : "+jsonInput); 

        byte[] buffer = new byte[jsonInput.length()];
        buffer = jsonInput.getBytes();

        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
        httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        
        /*String userCredentials = usuario+":"+password;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));      
        httpConn.setRequestProperty("Authorization", basicAuth);*/
        

        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        

        OutputStream out = httpConn.getOutputStream();
        //Write the content of the request to the outputstream of the HTTP Connection.
        out.write(buffer);
        out.flush();
        out.close();
        //Ready with sending the request.

        //Read the response.
        //httpConn.connect();
        logWithTimestamp("Codigo respuesta servicio: "+httpConn.getResponseCode()); 
        if (httpConn.getResponseCode() != 200) {             
          return String.valueOf(httpConn.getResponseCode());//"Failed : HTTP Error code : " + httpConn.getResponseCode();
         }
        
        
        
        InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
        BufferedReader in = new BufferedReader(isr);

        //Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }
        
        in.close();
        httpConn.disconnect();   
        
        return outputString;
    }
    
    
   /* public String callDeleteReceipt(String table,String url) throws MalformedURLException, IOException {

        String responseString = "";
        String outputString = "";


       URL oURL = new URL(url);
        
        HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
    
       
       
        String jsonInput ="{\n" + 
        "\"nameTable\" : \""+table+"\"\n" + 
        
        "}";
       
        logWithTimestamp("JSON : "+jsonInput); 

        byte[] buffer = new byte[jsonInput.length()];
        buffer = jsonInput.getBytes();

        httpConn.setRequestMethod("DELETE");
        //httpConn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
        httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
          

        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        

        //logWithTimestamp("Codigo respuesta servicio: "+httpConn.getResponseCode()); 
        if (httpConn.getResponseCode() != 200) {             
          return String.valueOf(httpConn.getResponseCode());//"Failed : HTTP Error code : " + httpConn.getResponseCode();
         }
        

        //httpConn.disconnect();   
        
        return outputString;
    }
  */

  
    public String callInsertReceipt(CPERecepciones cpeRecep,String usuario,String password,String instanceName, String dataCenter,String url) throws MalformedURLException, IOException {

        String responseString = "";
        String outputString = "";


       URL oURL = new URL(url);
        
        HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
    
        String nroOc = (!"".equals(cpeRecep.getNro_oc()) && cpeRecep.getNro_oc()!= null)?cpeRecep.getNro_oc():"";      
        String itemNumber = (!"".equals(cpeRecep.getItem_number()) && cpeRecep.getItem_number()!= null)?cpeRecep.getItem_number():"";
        String itemDescription = (!"".equals(cpeRecep.getItem_description()) && cpeRecep.getItem_description()!= null)?cpeRecep.getItem_description():"";
        String itemUom = (!"".equals(cpeRecep.getUom_code()) && cpeRecep.getUom_code()!= null)?cpeRecep.getUom_code():"";
        String receiptNumber = (!"".equals(cpeRecep.getReceipt_num()) && cpeRecep.getReceipt_num()!= null)?cpeRecep.getReceipt_num():"";
        String greNumber = (!"".equals(cpeRecep.getGuia_remision()) && cpeRecep.getGuia_remision()!= null)?cpeRecep.getGuia_remision():"";
        String vendorSiteCode = (!"".equals(cpeRecep.getVendor_site_code()) && cpeRecep.getVendor_site_code()!= null)?cpeRecep.getVendor_site_code():"";
        String vendorName = (!"".equals(cpeRecep.getVendor_name()) && cpeRecep.getVendor_name()!= null)?cpeRecep.getVendor_name():"";
        String ruc = (!"".equals(cpeRecep.getRuc()) && cpeRecep.getRuc()!= null)?cpeRecep.getRuc():"";
        String buName = (!"".equals(cpeRecep.getBu_name()) && cpeRecep.getBu_name()!= null)?cpeRecep.getBu_name():"";
        String legalEntityName = (!"".equals(cpeRecep.getLegal_entity_name()) && cpeRecep.getLegal_entity_name()!= null)?cpeRecep.getLegal_entity_name():"";
        String itemLookup = (!"".equals(cpeRecep.getItemTypeLookup()) && cpeRecep.getItemTypeLookup()!= null)?cpeRecep.getItemTypeLookup():"";
       
       
        String jsonInput ="{\n" + 
        "\"poNumber\" : \""+nroOc+"\",\n" + 
        "\"currencyCode\" : \""+cpeRecep.getCurrencyCode()+"\",\n" +                  
        "\"poLineNumber\" : "+cpeRecep.getLine_num_oc()+",\n" + 
        "\"itemNumber\" : \""+itemNumber+"\",\n" + 
        "\"itemDescription\" : \""+itemDescription+"\",\n" + 
        "\"itemUom\" : \""+itemUom+"\",\n" + 
        "\"quantity\" : "+cpeRecep.getQuantity()+",\n" + 
        "\"price\" : "+cpeRecep.getUnit_price()+",\n" + 
        "\"receiptId\" : "+cpeRecep.getReceiptId()+",\n" + 
        "\"receiptNumber\" : \""+receiptNumber+"\",\n" + 
        "\"receiptLineNumber\" : "+cpeRecep.getLine_num_rcv()+",\n" + 
        "\"quantityReceived\" : "+cpeRecep.getQuantity_rcv()+",\n" + 
        "\"amountReceived\" : "+cpeRecep.getAmount_rcv()+",\n" + 
        "\"invoiceNumber\" : \"\",\n" + 
        "\"greNumber\" : \""+greNumber+"\",\n" + 
        "\"vendorId\" : "+cpeRecep.getVendor_id()+",\n" + 
        "\"vendorSiteCode\" : \""+vendorSiteCode+"\",\n" + 
        "\"vendorNumber\" : \""+cpeRecep.getVendorNumber()+"\",\n" +                   
        "\"vendorName\" : \""+vendorName+"\",\n" + 
        "\"ocOblig\" : \""+cpeRecep.getOcOblig()+"\",\n" +                   
        "\"greOblig\" : \""+vendorName+"\",\n" +                   
        "\"ruc\" : \""+ruc+"\",\n" +                   
        "\"orgId\" : "+cpeRecep.getOrg_id()+",\n" + 
        "\"buName\" : \""+buName+"\",\n" + 
        "\"legalEntityId\" : "+cpeRecep.getLegal_entity_id()+",\n" + 
        "\"legalEntityName\" : \""+legalEntityName+"\",\n" + 
        "\"ledgerId\" : "+cpeRecep.getLedgerId()+",\n" +                                  
        "\"itemLookup\" : \""+itemLookup+"\"\n" +                   
        "    \n" + 
        "}";
       
        logWithTimestamp("JSON : "+jsonInput); 

        byte[] buffer = new byte[jsonInput.length()];
        buffer = jsonInput.getBytes();

        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
        httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        
        String userCredentials = usuario+":"+password;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));      
        httpConn.setRequestProperty("Authorization", basicAuth);
        

        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        

        OutputStream out = httpConn.getOutputStream();
        //Write the content of the request to the outputstream of the HTTP Connection.
        out.write(buffer);
        out.flush();
        out.close();
        //Ready with sending the request.

        //Read the response.
        //httpConn.connect();
        logWithTimestamp("Codigo respuesta servicio: "+httpConn.getResponseCode()); 
        if (httpConn.getResponseCode() != 200) {             
          return String.valueOf(httpConn.getResponseCode());//"Failed : HTTP Error code : " + httpConn.getResponseCode();
         }
        
        
        
        InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
        BufferedReader in = new BufferedReader(isr);

        //Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }
        
        in.close();
        httpConn.disconnect();   
        
        return outputString;
    }
    
    public String callInsertDetra(CPEDetracciones cpeDetra,String usuario,String password,String instanceName, String dataCenter,String url) throws MalformedURLException, IOException {

        String responseString = "";
        String outputString = "";


       URL oURL = new URL(url);
        
        HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
    
        String paramValue = (!"".equals(cpeDetra.getParamValue()) && cpeDetra.getParamValue()!= null)?cpeDetra.getParamValue():"";      
        String paramName = (!"".equals(cpeDetra.getParamName()) && cpeDetra.getParamName()!= null)?cpeDetra.getParamName():"";
        String attribute1 = (!"".equals(cpeDetra.getAttribute1()) && cpeDetra.getAttribute1()!= null)?cpeDetra.getAttribute1():"";
        String attribute2 = (!"".equals(cpeDetra.getAttribute2()) && cpeDetra.getAttribute2()!= null)?cpeDetra.getAttribute2():"";
        String attribute3 = (!"".equals(cpeDetra.getAttribute3()) && cpeDetra.getAttribute3()!= null)?cpeDetra.getAttribute3():"";
        

        String jsonInput ="{\n" + 
        "\"paramName\" : \""+paramName+"\",\n" + 
        "\"paramValue\" : \""+paramValue+"\",\n" + 
        "\"attribute1\" : \""+attribute1+"\",\n" + 
        "\"attribute2\" : \""+attribute2+"\",\n" + 
        "\"attribute3\" : \""+attribute3+"\"\n" +                
        "    \n" + 
        "}";
       
        logWithTimestamp("JSON : "+jsonInput); 

        byte[] buffer = new byte[jsonInput.length()];
        buffer = jsonInput.getBytes();

        httpConn.setRequestMethod("POST");
        httpConn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
        httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        
        String userCredentials = usuario+":"+password;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));      
        httpConn.setRequestProperty("Authorization", basicAuth);
        

        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        

        OutputStream out = httpConn.getOutputStream();
        //Write the content of the request to the outputstream of the HTTP Connection.
        out.write(buffer);
        out.flush();
        out.close();
        //Ready with sending the request.

        //Read the response.
        //httpConn.connect();
        logWithTimestamp("Codigo respuesta servicio: "+httpConn.getResponseCode()); 
        if (httpConn.getResponseCode() != 200) {             
          return String.valueOf(httpConn.getResponseCode());//"Failed : HTTP Error code : " + httpConn.getResponseCode();
         }
        
        
        
        InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
        BufferedReader in = new BufferedReader(isr);

        //Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;
        }
        
        in.close();
        httpConn.disconnect();   
        
        return outputString;
    }
    
    
  
  
  
  private byte[] runReport(ExternalReportWSSService svc, String reportPath) throws MalformedURLException, Exception {

      xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest req =
          new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest();
      xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue arrayParam =
          new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue();

      xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue param = null;
      xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString arrayValues = null;

      param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
      arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
      param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
      arrayValues.getItem().add("off");
      param.setValues(arrayValues);
      arrayParam.getItem().add(param);

      param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
      arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
      param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
      arrayValues.getItem().add("off");
      param.setValues(arrayValues);
      arrayParam.getItem().add(param);

      param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
      arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
      param.setName("XDO_DM_DEBUG_FLAG");
      arrayValues.getItem().add("off");
      param.setValues(arrayValues);
      arrayParam.getItem().add(param);


      req.setAttributeLocale("Spanish (Peru)");
      req.setParameterNameValues(arrayParam);
      req.setReportAbsolutePath(reportPath);
      req.setSizeOfDataChunkDownload(-1);
      req.setAttributeFormat("xml");

      xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportResponse resp =
          svc.runReport(req, "");

      return (resp.getReportBytes());

  }
}
