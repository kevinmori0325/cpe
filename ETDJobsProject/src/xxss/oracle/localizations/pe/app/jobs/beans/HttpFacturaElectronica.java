package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.BufferedReader;
import java.io.IOException;

import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.bind.DatatypeConverter;

public class HttpFacturaElectronica {
    public HttpFacturaElectronica() {
        super();
    }

    public static void main(String[] args) throws MalformedURLException,
                                                  IOException {
        
        String trama =
                          "EN|03|B001-00011020||||2017-01-31|PEN|20510713363|6|COLEGIOS PERUANOS S.A.|COLEGIOS PERUANOS S.A.|null|AV. CARLOS VILLARAN NRO. 140 PISO 7 URB. SANTA CATALINA LIMA - LIMA - LA VICTORIA|LIMA|LIMA|SAN BORJA|null|1|GONZALES PRADO MARCOS SEGUNDO|LOS OLIVOS|0.00|0.00|||1000|||null|1|PE|\n" + 
                          "ENEX|2.1|||||2017-02-10||0000|1000\n" + 
                          "DN|1|1000| UN MIL  CON 00/100 SOLES\n" + 
                          "DE|1|1180|EA|1|1000||01|1000|1000||||\n" + 
                          "DEDI|Venta Activos Fijos|||||null||\n" + 
                          "DEDI|1000\n" + 
                          "DEIM|180|1000|180|18.00||null||null|null|null\n" + 
                          "PES|MensajesAt\n" + 
                          "PESD|1|Designado AGENTE DE RETENCI�N de conformidad con lo establecido en el Art. 1 de la R.S. 395-2014\n" + 
                          "PESD|2|SUNAT a partir del 01/02/2015\n" + 
                          "PESD|3|Operaci�n sujeta al Sistema de Pago de Obligaciones Tributarias con el Gobierno Central Cuenta Banco\n" + 
                          "PESD|4|de la Nacion en MN N� 00098044108.\n" + 
                          "PE|Telefono|619-6060\n" + 
                          "PE|PagWeb|http://asp4demos.paperless.com.pe/BoletaFinancieraUno";
        
        String url = "https://facturacioneletronicatst.apiinnovaschools.com/axis2/services/Online?wsdl";
        //String url = "http://dev-gw.efact.pe:8686/connectorcsv_newdoc/TransactionService7ab781c8ce6f11e4b9d61681e6b88ec1?wsdl";
        HttpFacturaElectronica httpFacturaElectronica = new HttpFacturaElectronica();
        httpFacturaElectronica.sendFactura("20510713363", "kevin", "abc123", trama, 1, 1, url);
        //httpFacturaElectronica.getRetention("123", "123", url);
        
        
    }
    
    
    public static String sendFactura(String ruc,String login,String password, String trama, int tipoFol, int tipoReturn,String url) throws MalformedURLException, IOException {
              //PruebaRetenciones pruebaRetenciones = new PruebaRetenciones();
              String responseString = "";
              String outputString = "";
            System.out.println("Entro sendFacturaPaper");
            
         /*     URL oURL =
                  new URL("http://dev-gw.efact.pe:8686/connectorcsv_newdoc/TransactionService7ab781c8ce6f11e4b9d61681e6b88ec1?wsdl");
              */
            URL oURL =
                new URL(url);
              
              HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
              //httpConn.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
            /*  String file =
                  "EN|03|B001-00011019||||2017-01-31|PEN|20510713363|6|COLEGIOS PERUANOS S.A.|COLEGIOS PERUANOS S.A.|null|AV. CARLOS VILLARAN NRO. 140 PISO 7 URB. SANTA CATALINA LIMA - LIMA - LA VICTORIA|LIMA|LIMA|SAN BORJA|null|1|GONZALES PRADO MARCOS SEGUNDO|LOS OLIVOS|0.00|0.00|||1000|||null|1|PE|\n" + 
                  "ENEX|2.1|||||2017-02-10||0000|1000\n" + 
                  "DN|1|1000| UN MIL  CON 00/100 SOLES\n" + 
                  "DE|1|1180|EA|1|1000||01|1000|1000||||\n" + 
                  "DEDI|Venta Activos Fijos|||||null||\n" + 
                  "DEDI|1000\n" + 
                  "DEIM|180|1000|180|18.00||null||null|null|null\n" + 
                  "PES|MensajesAt\n" + 
                  "PESD|1|Designado AGENTE DE RETENCI�N de conformidad con lo establecido en el Art. 1 de la R.S. 395-2014\n" + 
                  "PESD|2|SUNAT a partir del 01/02/2015\n" + 
                  "PESD|3|Operaci�n sujeta al Sistema de Pago de Obligaciones Tributarias con el Gobierno Central Cuenta Banco\n" + 
                  "PESD|4|de la Nacion en MN N� 00098044108.\n" + 
                  "PE|Telefono|619-6060\n" + 
                  "PE|PagWeb|http://asp4demos.paperless.com.pe/BoletaFinancieraUno/";
              */

            System.out.println("Entro Soap");
              String xmlInput =
                  "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ws=\"http://ws.online.asp.core.paperless.cl\">\n" + 
                  "   <soap:Header/>\n" + 
                  "   <soap:Body>\n" + 
                  "      <ws:OnlineGeneration>\n" + 
                  "         <ws:ruc>"+ruc+"</ws:ruc>\n" + 
                  "         <ws:login>"+login+"</ws:login>\n" + 
                  "         <ws:clave>"+ password +"</ws:clave>\n" + 
                  "         <ws:docTxt>"+trama+"</ws:docTxt>\n" + 
                  "         <ws:tipoFoliacion>"+tipoFol+"</ws:tipoFoliacion>\n" + 
                  "         <ws:tipoRetorno>"+tipoReturn+"</ws:tipoRetorno>\n" + 
                  "      </ws:OnlineGeneration>\n" + 
                  "   </soap:Body>\n" + 
                  "</soap:Envelope>";
              System.out.println(xmlInput);
            System.out.println("Fin Soap");
            
              byte[] buffer = new byte[xmlInput.length()];
              buffer = xmlInput.getBytes();
              
              /*bout.write(buffer);
              byte[] b = bout.toByteArray();*/

              // Set the appropriate HTTP parameters.
              //String SOAPAction = "http://service.connector.alfa1lab.efact/TransactionService/sendRetentionRequest";
              
              String SOAPAction = "urn:OnlineGeneration";
              httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
              httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
              //httpConn.setRequestProperty("Content-Type", "application/soap+xml;charset=UTF-8");
              httpConn.setRequestProperty("SOAPAction", SOAPAction);
              httpConn.setRequestMethod("POST");
              httpConn.setDoOutput(true);
              httpConn.setDoInput(true);
              
              //java.io.OutputStreamWriter wr = new java.io.OutputStreamWriter(httpConn.getOutputStream());
              //wr.write(xmlInput);
              //wr.flush();

              OutputStream out = httpConn.getOutputStream();
              //Write the content of the request to the outputstream of the HTTP Connection.
              out.write(buffer);
              out.flush();
              out.close();
              //Ready with sending the request.

              //Read the response.
              //httpConn.connect();
             /* System.out.println("Mensaje Error "+httpConn.getErrorStream());
              System.out.println("Codigo Respuesta "+httpConn.getResponseCode());
              */
              
              InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
              BufferedReader in = new BufferedReader(isr);

              //Write the SOAP message response to a String.
              while ((responseString = in.readLine()) != null) {
                  outputString = outputString + responseString;
              }
              
              System.out.println(outputString);
              System.out.println("Fin sendFactura");
              return outputString;
          }
          
          
    public static String getRetention(String password, String user, String url) throws MalformedURLException, IOException {
              //PruebaRetenciones pruebaRetenciones = new PruebaRetenciones();
              String responseString = "";
              String outputString = "";
            System.out.println("Entro getRetention");
            
         /*     URL oURL =
                  new URL("http://dev-gw.efact.pe:8686/connectorcsv_newdoc/TransactionService7ab781c8ce6f11e4b9d61681e6b88ec1?wsdl");
              */
            URL oURL =
                new URL(url);
              
              HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
              String base64Str = "";

              //ByteArrayOutputStream bout = new ByteArrayOutputStream();
              
             String file =
                  "03/08/2017,R001-000035\n" + 
                  "GrupoRPP SAC,GrupoRPP SAC,20492353214,6,150131,AV. PASEO DE LA REPUBLICA NRO. 3866 SAN ISIDRO - 3866,,LIMA,LIMA,SAN ISIDRO,PE,MODDATOS,moddatos\n" + 
                  "LUJAN TUNQUE JAIME MARTIN,LUJAN TUNQUE JAIME MARTIN,10232719112,6,,CAL. MERCURIO NRO. 101 INT. 2 A.H. STA BARBARA MANZANAYOCC HUANCAVELICA - HUANCAVELICA - HUANCAVELICA,,,,,PE,antonycampana@gmail.com\n" + 
                  "01,3.00,,26.74,PEN,864.26,PEN\n" + 
                  "OCHOCIENTOS SESENTA Y CUATRO CON VEINTISEIS  SOLES\n" + 
                  "DATOS ADICIONALES\n" + 
                  "1,0001-002813,08/05/2017,468.50,PEN,03/08/2017,1,468.50,PEN,14.06,PEN,03/08/2017,454.44,PEN,,,,\n" + 
                  "1,0001-002819,12/06/2017,422.50,PEN,03/08/2017,2,422.50,PEN,12.68,PEN,03/08/2017,409.82,PEN,,\n" + 
                  "FF00FF\n";
             
             
             
              base64Str = DatatypeConverter.printBase64Binary(file.getBytes());
            System.out.println("Entro Soap");
              String xmlInput =
                  "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ser=\"http://service.connector.alfa1lab.efact/\">\n" + 
                  "   <soap:Header>\n" + 
                  "      <ser:Authorization>\n" + 
                  "         <password>"+password+"</password>\n" + 
                  "         <user>"+user+"</user>\n" + 
                  "      </ser:Authorization>\n" + 
                  "   </soap:Header>\n" + 
                  "   <soap:Body>\n" + 
                  "      <ser:sendRetention>\n" + 
                  "         <file>" + base64Str  + "</file>\n" + 
                  "      </ser:sendRetention>\n" + 
                  "   </soap:Body>\n" + 
                  "</soap:Envelope>";
            System.out.println("Fin Soap");
              byte[] buffer = new byte[xmlInput.length()];
              buffer = xmlInput.getBytes();
              
              /*bout.write(buffer);
              byte[] b = bout.toByteArray();*/

              // Set the appropriate HTTP parameters.
              //http://service.connector.alfa1lab.efact/TransactionService/sendRetentionRequest
              String SOAPAction = "sendRetentionRequest";
              httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
              //httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
              httpConn.setRequestProperty("Content-Type", "application/soap+xml;charset=UTF-8");
              httpConn.setRequestProperty("SOAPAction", SOAPAction);
              httpConn.setRequestMethod("POST");
              httpConn.setDoOutput(true);
              httpConn.setDoInput(true);
              
              //java.io.OutputStreamWriter wr = new java.io.OutputStreamWriter(httpConn.getOutputStream());
              //wr.write(xmlInput);
              //wr.flush();

              OutputStream out = httpConn.getOutputStream();
              //Write the content of the request to the outputstream of the HTTP Connection.
              out.write(buffer);
              out.flush();
              out.close();
              //Ready with sending the request.

              //Read the response.
              //httpConn.connect();
              
              InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
              BufferedReader in = new BufferedReader(isr);

              //Write the SOAP message response to a String.
              while ((responseString = in.readLine()) != null) {
                  outputString = outputString + responseString;
              }
              
              System.out.println(outputString);
            System.out.println("Fin GetRetention");
              return outputString;
          }
          
    
    
    
}
