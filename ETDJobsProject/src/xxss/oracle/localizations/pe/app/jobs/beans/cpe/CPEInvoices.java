package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;

public class CPEInvoices {
    
    long invoiceId;
    
    
    public CPEInvoices() {
        super();
    }
    
    
    @XmlElement(name = "ID_FACTURA")
    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public long getInvoiceId() {
        return invoiceId;
    }
}
