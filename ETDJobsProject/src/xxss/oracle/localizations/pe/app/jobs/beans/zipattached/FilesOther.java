package xxss.oracle.localizations.pe.app.jobs.beans.zipattached;

public class FilesOther {
    String fileName;
    String fileExt;
    public FilesOther(){
        super();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getFileExt() {
        return fileExt;
    }
}
