package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "submitInvoiceImportResponse")
public class RespuestaImportAP {
    String result;
    
    public RespuestaImportAP() {
        super();
    }

    @XmlElement(name = "result")
    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
