package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;

public class CPEErrorInterface
{
  String reject_lookup_code;
  String rejection_message;
  long   load_request_id;
  
  public CPEErrorInterface()
  {
    super();
  }


    @XmlElement(name = "LOAD_REQUEST_ID")
    public void setLoad_request_id(long load_request_id) {
        this.load_request_id = load_request_id;
    }

    public long getLoad_request_id() {
        return load_request_id;
    }

    @XmlElement(name = "REJECT_LOOKUP_CODE")
  public void setReject_lookup_code(String reject_lookup_code)
  {
    this.reject_lookup_code = reject_lookup_code;
  }

  public String getReject_lookup_code()
  {
    return reject_lookup_code;
  }

  @XmlElement(name = "REJECTION_MESSAGE")
  public void setRejection_message(String rejection_message)
  {
    this.rejection_message = rejection_message;
  }

  public String getRejection_message()
  {
    return rejection_message;
  }
}
