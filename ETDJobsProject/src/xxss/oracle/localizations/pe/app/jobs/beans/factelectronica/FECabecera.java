package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class FECabecera {
    String customerTrxId;
        Date fechaEmisionInv;
        Date fechaVctoInv;
        String docSerie;
        String trxNumber;
        String tipoComprobantePago;
        String tipoDocIdentCli;
        String nroDocIdentCli;
        String nombreCli;
        String moneda;
        String tipoCambio;
        String countryCli;
        String address1Cli;
        String stateCli;
        String termDescrip;
        String emailAddress;
        String tipoDocRef;
        Date fechaDocRef;
        String serieDocRef;
        String correlDocRef;
        String totalAmountCab;
        String impGravAmountCab;
        String impGravRateCab;
        String baseGravAmountCab;
        String impNoGravExoAmountCab;
        String impNoGravExoRateCab;
        String baseNoGravExoAmountCab;
        String impNoGravInaAmountCab;
        String impNoGravInaRateCab;
        String baseNoGravInaAmountCab;
        String batchSourceName;
        String transactionSetName;
        String codigoRef;
        String razonRef;
        String custTrxTypeName;
        String adjustComments;
        Date adjustApplyDate;
        String adjustmentId;
        String businessUnit;
        String tipoNotaCredito;
        String tipoNotaDebito;
        String sustentoNotaCredito;
        String sustentoNotaDebito;
        String tipoOperacion;
        String codigoServicio;
        String porcetDetraccion;
        String montoDetraccion;
        String bancoNacion;
        String ordenCompra;
        String monedaDetraccion;
        String medioPago;
        int numTax;
        String trxNumberPle;
        String codAsigSunat;
        String sede;
        String concepto;
        String trxNumRef;
        String montoDescuento;
        String montobaseDescuento;
        String factorDescuento;
        String codigoDescuent;
        String indicador;
        String originalCustomerTrxId;
        FELinea[] linea;
        FETax[] tax;
        FERef[] cabRef;
        String nivel;   
        String afecta_bi;
        String desc_lin_desc;   

    public FECabecera() {
        super();
    }

    @XmlElement(name = "CUSTOMER_TRX_ID")
    public void setCustomerTrxId(String customerTrxId) {
        this.customerTrxId = customerTrxId;
    }

    public String getCustomerTrxId() {
        return customerTrxId;
    }

    @XmlElement(name = "FECHA_EMISION_INV")
    public void setFechaEmisionInv(Date fechaEmisionInv) {
        this.fechaEmisionInv = fechaEmisionInv;
    }

    public Date getFechaEmisionInv() {
        return fechaEmisionInv;
    }

    @XmlElement(name = "FECHA_VCTO_INV")
    public void setFechaVctoInv(Date fechaVctoInv) {
        this.fechaVctoInv = fechaVctoInv;
    }

    public Date getFechaVctoInv() {
        return fechaVctoInv;
    }

    @XmlElement(name = "DOC_SERIE")
    public void setDocSerie(String docSerie) {
        this.docSerie = docSerie;
    }

    public String getDocSerie() {
        return docSerie;
    }

    @XmlElement(name = "TRX_NUMBER")
    public void setTrxNumber(String trxNumber) {
        this.trxNumber = trxNumber;
    }

    public String getTrxNumber() {
        return trxNumber;
    }

    @XmlElement(name = "TIPO_COMPROBANTE_PAGO")
    public void setTipoComprobantePago(String tipoComprobantePago) {
        this.tipoComprobantePago = tipoComprobantePago;
    }

    public String getTipoComprobantePago() {
        return tipoComprobantePago;
    }

    @XmlElement(name = "TIPO_DOC_IDENT_CLI")
    public void setTipoDocIdentCli(String tipoDocIdentCli) {
        this.tipoDocIdentCli = tipoDocIdentCli;
    }

    public String getTipoDocIdentCli() {
        return tipoDocIdentCli;
    }

    @XmlElement(name = "NRO_DOC_IDENT_CLI")
    public void setNroDocIdentCli(String nroDocIdentCli) {
        this.nroDocIdentCli = nroDocIdentCli;
    }

    public String getNroDocIdentCli() {
        return nroDocIdentCli;
    }

    @XmlElement(name = "NOMBRE_CLI")
    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    @XmlElement(name = "CODE_MONEDA")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    @XmlElement(name = "TIPO_CAMBIO")
    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    @XmlElement(name = "COUNTRY_CLI")
    public void setCountryCli(String countryCli) {
        this.countryCli = countryCli;
    }

    public String getCountryCli() {
        return countryCli;
    }

    @XmlElement(name = "ADDRESS1_CLI")
    public void setAddress1Cli(String address1Cli) {
        this.address1Cli = address1Cli;
    }

    public String getAddress1Cli() {
        return address1Cli;
    }

    @XmlElement(name = "STATE_CLI")
    public void setStateCli(String stateCli) {
        this.stateCli = stateCli;
    }

    public String getStateCli() {
        return stateCli;
    }

    @XmlElement(name = "TERM_DESCRIP")
    public void setTermDescrip(String termDescrip) {
        this.termDescrip = termDescrip;
    }

    public String getTermDescrip() {
        return termDescrip;
    }

    @XmlElement(name = "EMAIL_ADDRESS")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @XmlElement(name = "TIPO_DOC_REF")
    public void setTipoDocRef(String tipoDocRef) {
        this.tipoDocRef = tipoDocRef;
    }

    public String getTipoDocRef() {
        return tipoDocRef;
    }

    @XmlElement(name = "FECHA_DOC_REF")
        public void setFechaDocRef(Date fechaDocRef) {
            this.fechaDocRef = fechaDocRef;
        }

        public Date getFechaDocRef() {
            return fechaDocRef;
        }

    @XmlElement(name = "SERIE_DOC_REF")
    public void setSerieDocRef(String serieDocRef) {
        this.serieDocRef = serieDocRef;
    }

    public String getSerieDocRef() {
        return serieDocRef;
    }

    @XmlElement(name = "CORREL_DOC_REF")
    public void setCorrelDocRef(String correlDocRef) {
        this.correlDocRef = correlDocRef;
    }

    public String getCorrelDocRef() {
        return correlDocRef;
    }

    @XmlElement(name = "G_DET")
    public void setLinea(FELinea[] linea) {
        this.linea = linea;
    }

    public FELinea[] getLinea() {
        return linea;
    }

    @XmlElement(name = "TOTAL_AMOUNT_CAB")
    public void setTotalAmountCab(String totalAmountCab) {
        this.totalAmountCab = totalAmountCab;
    }

    public String getTotalAmountCab() {
        return totalAmountCab;
    }

    @XmlElement(name = "IMP_GRAV_AMOUNT_CAB")
    public void setImpGravAmountCab(String impGravAmountCab) {
        this.impGravAmountCab = impGravAmountCab;
    }

    public String getImpGravAmountCab() {
        return (impGravAmountCab == null ? "0.00" : impGravAmountCab);
    }

    @XmlElement(name = "IMP_GRAV_RATE_CAB")
    public void setImpGravRateCab(String impGravRateCab) {
        this.impGravRateCab = impGravRateCab;
    }

    public String getImpGravRateCab() {
        return (impGravRateCab == null ? "0.00" : impGravRateCab);
    }

    @XmlElement(name = "IMP_NO_GRAV_EXO_AMOUNT_CAB")
    public void setImpNoGravExoAmountCab(String impNoGravExoAmountCab) {
        this.impNoGravExoAmountCab = impNoGravExoAmountCab;
    }

    public String getImpNoGravExoAmountCab() {
        return (impNoGravExoAmountCab == null ? "0.00" : impNoGravExoAmountCab);
    }

    @XmlElement(name = "IMP_NO_GRAV_EXO_RATE_CAB")
    public void setImpNoGravExoRateCab(String impNoGravExoRateCab) {
        this.impNoGravExoRateCab = impNoGravExoRateCab;
    }

    public String getImpNoGravExoRateCab() {
        return (impNoGravExoRateCab == null ? "0.00" : impNoGravExoRateCab);
    }

    @XmlElement(name = "IMP_NO_GRAV_INA_AMOUNT_CAB")
    public void setImpNoGravInaAmountCab(String impNoGravInaAmountCab) {
        this.impNoGravInaAmountCab = impNoGravInaAmountCab;
    }

    public String getImpNoGravInaAmountCab() {
        return (impNoGravInaAmountCab == null ? "0.00" : impNoGravInaAmountCab);
    }

    @XmlElement(name = "IMP_NO_GRAV_INA_RATE_CAB")
    public void setImpNoGravInaRateCab(String impNoGravInaRateCab) {
        this.impNoGravInaRateCab = impNoGravInaRateCab;
    }

    public String getImpNoGravInaRateCab() {
        return (impNoGravInaRateCab == null ? "0.00" : impNoGravInaRateCab);
    }

    @XmlElement(name = "BASE_GRAV_AMOUNT_CAB")
    public void setBaseGravAmountCab(String baseGravAmountCab) {
        this.baseGravAmountCab = baseGravAmountCab;
    }

    public String getBaseGravAmountCab() {
        return (baseGravAmountCab == null ? "0.00" : baseGravAmountCab);
    }

    @XmlElement(name = "BASE_NO_GRAV_EXO_AMOUNT_CAB")
    public void setBaseNoGravExoAmountCab(String baseNoGravExoAmountCab) {
        this.baseNoGravExoAmountCab = baseNoGravExoAmountCab;
    }

    public String getBaseNoGravExoAmountCab() {
        return (baseNoGravExoAmountCab == null ? "0.00" : baseNoGravExoAmountCab);
    }

    @XmlElement(name = "BASE_NO_GRAV_INA_AMOUNT_CAB")
    public void setBaseNoGravInaAmountCab(String baseNoGravInaAmountCab) {
        this.baseNoGravInaAmountCab = baseNoGravInaAmountCab;
    }

    public String getBaseNoGravInaAmountCab() {
        return (baseNoGravInaAmountCab == null ? "0.00" : baseNoGravInaAmountCab);
    }

    @XmlElement(name = "BATCH_SOURCE_NAME")
    public void setBatchSourceName(String batchSourceName) {
        this.batchSourceName = batchSourceName;
    }

    public String getBatchSourceName() {
        return batchSourceName;
    }

    @XmlElement(name = "TRANSACTION_SET_NAME")
    public void setTransactionSetName(String transactionSetName) {
        this.transactionSetName = transactionSetName;
    }

    public String getTransactionSetName() {
        return transactionSetName;
    }

    @XmlElement(name = "CODIGO_REF")
    public void setCodigoRef(String codigoRef) {
        this.codigoRef = codigoRef;
    }

    public String getCodigoRef() {
        return codigoRef;
    }

    @XmlElement(name = "RAZON_REF")
    public void setRazonRef(String razonRef) {
        this.razonRef = razonRef;
    }

    public String getRazonRef() {
        return razonRef;
    }
    
    @XmlElement(name = "CUST_TRX_TYPE_NAME")
    public void setCustTrxTypeName(String custTrxTypeName) {
        this.custTrxTypeName = custTrxTypeName;
    }

    public String getCustTrxTypeName() {
        return custTrxTypeName;
    }

    @XmlElement(name = "COMMENTS")
    public void setAdjustComments(String adjustComments) {
        this.adjustComments = adjustComments;
    }

    public String getAdjustComments() {
        return adjustComments;
    }

    @XmlElement(name = "APPLY_DATE")
    public void setAdjustApplyDate(Date adjustApplyDate) {
        this.adjustApplyDate = adjustApplyDate;
    }

    public Date getAdjustApplyDate() {
        return adjustApplyDate;
    }

    @XmlElement(name = "ADJUSTMENT_ID")
    public void setAdjustmentId(String adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    public String getAdjustmentId() {
        return adjustmentId;
    }

    @XmlElement(name = "G_REF")
    public void setCabRef(FERef[] cabRef) {
        this.cabRef = cabRef;
    }

    public FERef[] getCabRef() {
        return cabRef;
    }

    @XmlElement(name = "BUSINESS_UNIT")
    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    @XmlElement(name = "TIPO_NC")
    public void setTipoNotaCredito(String tipoNotaCredito) {
        this.tipoNotaCredito = tipoNotaCredito;
    }

    public String getTipoNotaCredito() {
        return tipoNotaCredito;
    }

    @XmlElement(name = "TIPO_ND")
    public void setTipoNotaDebito(String tipoNotaDebito) {
        this.tipoNotaDebito = tipoNotaDebito;
    }

    public String getTipoNotaDebito() {
        return tipoNotaDebito;
    }

    @XmlElement(name = "SUSTENTO_NC")
    public void setSustentoNotaCredito(String sustentoNotaCredito) {
        this.sustentoNotaCredito = sustentoNotaCredito;
    }

    public String getSustentoNotaCredito() {
        return sustentoNotaCredito;
    }

    @XmlElement(name = "SUSTENTO_ND")
    public void setSustentoNotaDebito(String sustentoNotaDebito) {
        this.sustentoNotaDebito = sustentoNotaDebito;
    }

    public String getSustentoNotaDebito() {
        return sustentoNotaDebito;
    }

    @XmlElement(name = "TIPO_OPERACION")
    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    @XmlElement(name = "COD_SERVICIO")
    public void setCodigoServicio(String codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    public String getCodigoServicio() {
        return codigoServicio;
    }

    @XmlElement(name = "PORCT_DETRACCION")
    public void setPorcetDetraccion(String porcetDetraccion) {
        this.porcetDetraccion = porcetDetraccion;
    }

    public String getPorcetDetraccion() {
        return porcetDetraccion;
    }

    @XmlElement(name = "MONTO_DETRACCION")
    public void setMontoDetraccion(String montoDetraccion) {
        this.montoDetraccion = montoDetraccion;
    }

    public String getMontoDetraccion() {
        return montoDetraccion;
    }

    @XmlElement(name = "CTA_BANCO_NACION")
    public void setBancoNacion(String bancoNacion) {
        this.bancoNacion = bancoNacion;
    }

    public String getBancoNacion() {
        return bancoNacion;
    }

    @XmlElement(name = "ORDEN_COMPRA")
    public void setOrdenCompra(String ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public String getOrdenCompra() {
        return ordenCompra;
    }

    @XmlElement(name = "MONEDA_DETRACCION")
    public void setMonedaDetraccion(String monedaDetraccion) {
        this.monedaDetraccion = monedaDetraccion;
    }

    public String getMonedaDetraccion() {
        return monedaDetraccion;
    }

    @XmlElement(name = "MEDIO_PAGO")
    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public String getMedioPago() {
        return medioPago;
    }

    @XmlElement(name = "NUM_TAX")
    public void setNumTax(int numTax) {
        this.numTax = numTax;
    }

    public int getNumTax() {
        return numTax;
    }
    
    @XmlElement(name = "TRX_NUMBER_PLE")
    public void setTrxNumberPle(String trxNumberPle) {
        this.trxNumberPle = trxNumberPle;
    }

    public String getTrxNumberPle() {
        return trxNumberPle;
    }

    @XmlElement(name = "COD_ASIG_SUNAT")
    public void setCodAsigSunat(String codAsigSunat) {
        this.codAsigSunat = codAsigSunat;
    }

    public String getCodAsigSunat() {
        return codAsigSunat;
    }

    @XmlElement(name = "SEDE")
    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getSede() {
        return sede;
    }

    @XmlElement(name="G_TAX")
    public void setTax(FETax[] tax) {
        this.tax = tax;
    }

    public FETax[] getTax() {
        return tax;
    }

    @XmlElement(name = "CONCEPTO")
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getConcepto() {
        return concepto;
    }

    @XmlElement(name = "TRX_NUMBER_REF")
    public void setTrxNumRef(String trxNumRef) {
        this.trxNumRef = trxNumRef;
    }

    public String getTrxNumRef() {
        return trxNumRef;
    }

    @XmlElement(name = "MONTO_DESCUENTO")
    public void setMontoDescuento(String montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public String getMontoDescuento() {
        return montoDescuento;
    }

    @XmlElement(name = "MONTO_BASE_DEL_DESCUENTO")
    public void setMontobaseDescuento(String montobaseDescuento) {
        this.montobaseDescuento = montobaseDescuento;
    }

    public String getMontobaseDescuento() {
        return montobaseDescuento;
    }

    @XmlElement(name = "FACTOR_DESCUENTO")
    public void setFactorDescuento(String factorDescuento) {
        this.factorDescuento = factorDescuento;
    }

    public String getFactorDescuento() {
        return factorDescuento;
    }

    @XmlElement(name = "CODIGO_DESCUENTO")
    public void setCodigoDescuent(String codigoDescuent) {
        this.codigoDescuent = codigoDescuent;
    }

    public String getCodigoDescuent() {
        return codigoDescuent;
    }

    @XmlElement(name = "INDICADOR")
    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getIndicador() {
        return indicador;
    }

    @XmlElement(name = "ORIGINAL_TRX_ID")
    public void setOriginalCustomerTrxId(String originalCustomerTrxId) {
        this.originalCustomerTrxId = originalCustomerTrxId;
    }

    public String getOriginalCustomerTrxId() {
        return originalCustomerTrxId;
    }
    
    @XmlElement(name = "NIVEL")
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNivel() {
        return nivel;
    }
   
    @XmlElement(name = "AFECTA_BI")
    public void setAfecta_bi(String afecta_bi) {
        this.afecta_bi = afecta_bi;
    }

    public String getAfecta_bi() {
        return afecta_bi;
    }
    @XmlElement(name = "DESC_LIN_DESC")
    public void setDesc_lin_desc(String desc_lin_desc) {
        this.desc_lin_desc = desc_lin_desc;
    }

    public String getDesc_lin_desc() {
        return desc_lin_desc;
    }
}
