package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import java.net.MalformedURLException;

import java.sql.SQLException;

import java.sql.Types;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.xml.sax.InputSource;

import xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica.CREMiscDataDS;
import xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica.CREPayDoc;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREBusinessUnit;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRECarrierContacts;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRECarriers;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRECustomers;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREInvMaterialTrxDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREInvTransactionType;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREInvTransactions;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRELegalEntity;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GRELegalEntityBUWarehouseDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREMiscDataDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREReceptorsDS;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREVendors;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.GREWarehouse;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.SunatDocIdType;
import xxss.oracle.localizations.pe.app.jobs.beans.guiaremision.SunatDocType;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.proxy.FndManageImportExportFilesService;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.proxy.ServiceException;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types.DocumentDetails;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil;
import xxss.oracle.localizations.services.util.FndImportExportFileServiceUtil;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil2;

@DisallowConcurrentExecution
public class SyncDataJob extends XxssJob {
    public SyncDataJob() {
        super();
    }
    
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.implementClassName = SyncDataJob.class.getName();
        super.execute(jobExecutionContext);
    }
    
    private void callSyncLegalEntity(GRELegalEntity pLegalEntity,
                                     OracleConnection trans) throws SQLException {

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_legal_entity(?,?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, pLegalEntity.getLegalEntityId());
        acs.setString(2, pLegalEntity.getRegisteredName());
        acs.setString(3, pLegalEntity.getRegistrationNumber());
        acs.setString(4, pLegalEntity.getCountry());
        acs.setString(5, pLegalEntity.getCity());
        acs.setString(6, pLegalEntity.getState());
        acs.setString(7, pLegalEntity.getAddress1());
        acs.setString(8, pLegalEntity.getPostalCode());

        acs.executeUpdate();
        acs.close();
    }

    private void callSyncBU(GREBusinessUnit pBU, OracleConnection trans) throws SQLException {

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_bu(?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, pBU.getBuId());
        acs.setString(2, pBU.getBuName());
        acs.setLong(3, pBU.getLegalEntityId());
        acs.setLong(4, pBU.getDefaultSetId());
        acs.setString(5, pBU.getLedgerName());

        acs.executeUpdate();
        acs.close();
    }

    private void callSyncWarehouse(GREWarehouse pWarehouse, OracleConnection trans) throws SQLException {

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_warehouse(?,?,?,?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, pWarehouse.getOrganizationId());
        acs.setString(2, pWarehouse.getOrganizationCode());
        acs.setString(3, pWarehouse.getOrganizationName());
        acs.setLong(4, pWarehouse.getInvOrgBusinessUnitId());
        acs.setString(5, pWarehouse.getCountry());
        acs.setString(6, pWarehouse.getCity());
        acs.setString(7, pWarehouse.getState());
        acs.setString(8, pWarehouse.getAddress1());
        acs.setString(9, pWarehouse.getPostalCode());
        acs.setLong(10, pWarehouse.getLegalEntityId());

        acs.executeUpdate();
        acs.close();
    }

    private void callSyncInvTrxType(GREInvTransactionType trxType, long pLegalEntityId, OracleConnection trans) throws SQLException {

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_inv_trx_type(?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, trxType.getTransactionTypeId());
        acs.setString(2, trxType.getTransactionTypeName());
        acs.setString(3, trxType.getTransactionActionName());
        acs.setString(4, trxType.getGreFlag());
        acs.setString(5, trxType.getGreType());
        acs.setLong(6, pLegalEntityId);

        acs.executeUpdate();
        acs.close();
    }

    private void callSyncSunatDocType(SunatDocType docType,
                                      OracleConnection trans) throws SQLException, Exception {

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_sunat_doc_type(?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, docType.getFlexValue());
        acs.setString(2, docType.getDescription());

        acs.executeUpdate();
        acs.close();
    }

    private void callSyncSunatDocIdType(SunatDocIdType docIdType,
                                        OracleConnection trans) throws SQLException, Exception {

        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_sunat_doc_id_type(?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, docIdType.getFlexValue());
        acs.setString(2, docIdType.getDescription());

        acs.executeUpdate();
        acs.close();
    }
    
    private void callSyncMaterialTransactions(GREInvTransactions transaction, OracleConnection trans) throws SQLException {
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_material_trx(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, transaction.getTransactionId());
        acs.setLong(2, transaction.getOrganizationId());
        acs.setLong(3, transaction.getTransactionTypeId());
        acs.setDouble(4, transaction.getTransactionQuantity());
        acs.setDouble(5, transaction.getPrimaryQuantity());
        
        if (transaction.getTransactionDate() != null) {
            acs.setDate(6, new java.sql.Date(transaction.getTransactionDate().getTime()));
        } else {
            acs.setNull(6, Types.DATE);
        }
        
        acs.setLong(7, transaction.getInventoryItemId());
        acs.setString(8, transaction.getGreType());
        acs.setString(9, transaction.getItemNumber());
        acs.setString(10, transaction.getItemDescription());
        acs.setDouble(11, transaction.getUnitWeight());
        acs.setString(12, transaction.getTransactionUom());
        acs.setString(13, transaction.getWeightUom());
        acs.setString(14, transaction.getSalesOrderNumber());
        acs.setString(15, transaction.getTransactionSetId());
        acs.setLong(16, transaction.getCustomerPartyId());
        acs.setString(17, transaction.getReceiptNumber());
        acs.setString(18, transaction.getPoNumber());
        acs.setLong(19, transaction.getVendorId());
        acs.setString(20, "ORAERPCLOUD");
        acs.setString(21, transaction.getIsoUomCode());
        acs.setString(22, transaction.getIsoWeightUomCoude());
        acs.setString(23, transaction.getPrimaryUomCode());
        acs.setString(24, transaction.getShipmentNumber());
        acs.setString(25, transaction.getShipmentToAddress());
        
        acs.setLong(26, transaction.getTransferOrganizationId());
        acs.setString(27, transaction.getTransferOrganizationCode());
        acs.setString(28, transaction.getTransferOrganizationName());
        
        acs.setString(29, transaction.getTransferOrder());
        acs.setString(30, transaction.getAsn());
        acs.setString(31, transaction.getClienteOrgDest());
        acs.setDouble(32, transaction.getEnvases());
        acs.setDouble(33, transaction.getPiezas());
        acs.setDouble(34, transaction.getPrecioUnitario());
        acs.setDouble(35, transaction.getPrecioTotal());
        acs.setString(36, transaction.getUomSalesOrder());
        
        if (transaction.getRequestDate() != null) {
            acs.setDate(37, new java.sql.Date(transaction.getRequestDate().getTime()));
        } else {
            acs.setNull(37, Types.DATE);
        }
       

        acs.executeUpdate();
        acs.close();
    }
    
    private void callSyncCustomers(GRECustomers customer, OracleConnection trans) throws SQLException {
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_customer(?,?,?,?,?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, customer.getCustAccountID());
        acs.setString(2, customer.getNroDocIdentCli());
        acs.setString(3, customer.getNombreCli());
        acs.setString(4, customer.getTipoDocIdentCli());
        acs.setString(5, customer.getCountryCli());
        acs.setString(6, customer.getAddress1Cli());
        acs.setString(7, customer.getCityCli());
        acs.setString(8, customer.getStateCli());
        acs.setString(9, customer.getPostalCodeCli());
        acs.setLong(10, customer.getPartyId());
        acs.setString(11, "ORAERPCLOUD");

        acs.executeUpdate();
        acs.close();
    }
    
    private void callSyncVendors(GREVendors vendor, OracleConnection trans) throws SQLException {
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_vendor(?,?,?,?,?,?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, vendor.getVendorId());
        acs.setString(2, vendor.getRepRegistrationNum());
        acs.setString(3, vendor.getNombreProv());
        acs.setString(4, vendor.getTipoDocIdentProv());
        acs.setString(5, vendor.getPaisProv());
        acs.setString(6, vendor.getDireccionProv());
        acs.setString(7, vendor.getCiudadProv());
        acs.setString(8, vendor.getEstadoProv());
        acs.setString(9, vendor.getCodigoPostalProv());
        acs.setLong(10, vendor.getPartyId());
        acs.setString(11, "ORAERPCLOUD");
        acs.setString(12, vendor.getSegment1());

        acs.executeUpdate();
        acs.close();
    }
    
    private void callSyncCarrier(GRECarriers carrier, OracleConnection trans) throws SQLException {
        GRECarrierContacts[] contacts = carrier.getContacts();
        
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_carrier(?,?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, carrier.getCarrierId());
        acs.setString(2, carrier.getCarrierName());
        acs.setString(3, carrier.getTipoDocIdent());
        acs.setString(4, carrier.getNroDoc());
        acs.setString(5, carrier.getRazonSocial());
        acs.setString(6, "Privado");
        acs.setString(7, "ORAERPCLOUD");

        acs.executeUpdate();
        acs.close();
        
        if (contacts != null) {
            for (int i=0; i<contacts.length; i++) {
                this.callSyncCarrierContact(contacts[i], carrier.getCarrierId(), trans);
            }
        }
    }
    
    private void callSyncCarrierContact(GRECarrierContacts contact, long pCarrierId, OracleConnection trans) throws SQLException {
        String st = "begin XXNC_PE_LOC_GUIA_REMISION_PKG.pr_sync_carrier_contact(?,?,?,?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, contact.getPartyId());
        acs.setString(2, contact.getContactLastName() + ", "+ contact.getContactFirstName());
        acs.setString(3, contact.getContactPhone());
        acs.setString(4, contact.getContactEmail());
        acs.setLong(5, pCarrierId);
        acs.setString(6, "ORAERPCLOUD");

        acs.executeUpdate();
        acs.close();
    }
    
    private void callSyncPayDoc(CREPayDoc payDoc, OracleConnection trans) throws SQLException {
        String st = "begin XXNC_PE_LOC_CRE_PKG.pr_sync_pay_doc(?,?,?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)trans.prepareCall(st);

        acs.setLong(1, payDoc.getPaymentDocumentId());
        acs.setString(2, payDoc.getPaymentDocumentName());
        acs.setLong(3, payDoc.getBuId());

        acs.executeUpdate();
        acs.close();
    }
    
    private byte[] createVSFile(List pIdList, String pValueSetName) {
        String separator = "|";
        String newLine = System.getProperty("line.separator"); // System.lineSeparator() for java > 1.6
        String defEnabledFlag = "Y";
        String headerRow = "ValueSetCode|Value|Description|EnabledFlag" + newLine;
        String stringRow;
        byte[] fileBytes;

        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(headerRow);

        for (Object obj : pIdList) {
            stringRow = pValueSetName + separator + (String)obj + separator + "" + separator + defEnabledFlag;
            strBuffer.append(stringRow + newLine);
        }
        
        fileBytes = strBuffer.toString().getBytes();

        return (fileBytes);
    }
    
    private void loadFileForVS(FndManageImportExportFilesService svc, List pIdList,
                                String pValueSetName) throws ServiceException {
        byte[] fileBytes;

        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmssZ");
        String fileName = "filevsimportcom" + format.format(new Date()) + ".txt";

        fileBytes = createVSFile(pIdList, pValueSetName);

        if (fileBytes.length == 0) {
            jobLog.append("Nothing to load on value set " + pValueSetName + newLine);
            return;
        }

        DocumentDetails document = FndImportExportFileServiceUtil.buildVSDocumentDetails(fileName, fileBytes);

        String fileId = svc.uploadFiletoUCM(document);

        if (fileId != null) {
            String vsResponse = svc.valueSetValuesDataLoader(new Long(fileId));
            jobLog.append("ValueSetLoader Result: " + vsResponse + newLine);
        }
    }

    private byte[] runReport(ExternalReportWSSService svc, String reportPath) throws MalformedURLException, Exception {

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest req =
            new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue arrayParam =
            new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);


        req.setAttributeLocale("Spanish (Peru)");
        req.setParameterNameValues(arrayParam);
        req.setReportAbsolutePath(reportPath);
        req.setSizeOfDataChunkDownload(-1);
        req.setAttributeFormat("xml");

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportResponse resp =
            svc.runReport(req, "");

        return (resp.getReportBytes());

    }

    private void processLegalEntityBUWarehouseData(GRELegalEntityBUWarehouseDS dataDS,
                                                   OracleConnection conn) throws SQLException, Exception {

        GRELegalEntity[] legalEntities = dataDS.getEntidadLegal();
        
        if (legalEntities == null) {
            return;
        }
        
        for (int i = 0; i < legalEntities.length; i++) {
            this.callSyncLegalEntity(legalEntities[i], conn);

            GREInvTransactionType[] invTrxTypes = dataDS.getInvTrxTypes();
            
            if (invTrxTypes != null) {
                for (int j = 0; j < invTrxTypes.length; j++) {
                    this.callSyncInvTrxType(invTrxTypes[j],
                                            legalEntities[i].getLegalEntityId(), conn);
                }
            }
            
            GREBusinessUnit[] businessUnits = legalEntities[i].getBusinessUnits();
            
            if (businessUnits != null) {
                for (int j = 0; j < businessUnits.length; j++) {
                    this.callSyncBU(businessUnits[j], conn);

                    GREWarehouse[] warehouses = businessUnits[j].getWarehouses();
                    
                    if (warehouses != null) {
                        for (int k = 0; k < warehouses.length; k++) {
                            this.callSyncWarehouse(warehouses[k], conn);
                        }
                    }                    
                    
                }
            }
            
        }
    }
    
    private void processMiscData(GREMiscDataDS dataDS, OracleConnection conn) throws SQLException, Exception {
        SunatDocType[] docTypes = dataDS.getSunatDocTypes();
        SunatDocIdType[] docIdTypes = dataDS.getSunatDocIdTypes();
        GRECarriers[] carriers = dataDS.getCarriers();
        
        if (docTypes != null) {
            for (int i=0; i<docTypes.length; i++) {
                this.callSyncSunatDocType(docTypes[i], conn);
            }
        }        
        
        if (docIdTypes != null) {
            for (int i=0; i<docIdTypes.length; i++) {
                this.callSyncSunatDocIdType(docIdTypes[i], conn);
            }
        }
        
        if (carriers != null) {
            for (int i=0; i<carriers.length; i++) {
                this.callSyncCarrier(carriers[i], conn);
            }
        }
                
    }
    
    private void processMaterialTrxData(FndManageImportExportFilesService svc,GREInvMaterialTrxDS dataDS, OracleConnection conn, String pValueSetName) throws SQLException,
                                                                                            ServiceException {
        GREInvTransactions[] txns = dataDS.getMaterialTransactions();
        List listIds = new ArrayList();
        
        
        if (txns != null) {
            for (int i=0; i<txns.length; i++) {
                this.callSyncMaterialTransactions(txns[i], conn);
                listIds.add(Long.toString(txns[i].getTransactionId()));
            }
            
            this.loadFileForVS(svc, listIds, pValueSetName);
        }        
    }
    
    private void processReceptorsData(GREReceptorsDS dataDS, OracleConnection conn) throws SQLException {
        GRECustomers[] customers = dataDS.getCustomers();
        GREVendors[] vendors = dataDS.getVendors();
        
        if (customers != null) {
            for (int i=0; i<customers.length; i++) {
                this.callSyncCustomers(customers[i], conn);
            }
        }
        
        if (vendors != null) {
            for (int j=0; j<vendors.length; j++) {
                this.callSyncVendors(vendors[j], conn);
            }
        }
    }
    
    private void processMiscData(CREMiscDataDS dataDS, OracleConnection conn) throws SQLException {
        CREPayDoc[] payDocs = dataDS.getPayDocs();
        
        if (payDocs != null) {
            for (int i=0; i<payDocs.length; i++) {
                this.callSyncPayDoc(payDocs[i], conn);
            }
        }
    }    

    public void doAction(JobDataMap dataMap, OracleConnection conn) throws Exception {
        String userName = dataMap.getString("USER_NAME"); //dataMap.getString("biUserName");
        String userPass = dataMap.getString("USER_PASS"); //dataMap.getString("biUserPass");
        String instanceName = dataMap.getString("INSTANCE_NAME"); //dataMap.getString("instanceName");
        String dataCenter = dataMap.getString("DATA_CENTER"); //dataMap.getString("dataCenter");

        String legalEntityBUWarehouseXMLPath = dataMap.getString("LE_BU_W_XML_PATH"); //dataMap.getString("dataCenter");
        String miscDataXMLPath = dataMap.getString("MISC_DATA_PATH"); //dataMap.getString("dataCenter");
        String materialTrxXMLPath = dataMap.getString("MATERIAL_TRX_DATA_PATH"); //dataMap.getString("dataCenter");
        String receptorXMLPath = dataMap.getString("RECEPTOR_DATA_PATH"); //dataMap.getString("dataCenter");
        String invTrxVSName = dataMap.getString("INV_TRX_VS"); //dataMap.getString("dataCenter");
        String payDocXMLPath = dataMap.getString("PAY_DOC_DATA_PATH"); //dataMap.getString("dataCenter");
        
        byte[] reportOutputBytes;
        InputStream reportIs;
        JAXBContext jaxbContextDataDS;
        Unmarshaller jaxbUnmarshaller;

        ExternalReportWSSService svcReport = ExternalReportWSUtil2.createExternalReportWSSServiceUserToken2(instanceName, dataCenter, userName,
                                                                                 userPass);
        
        if (svcReport != null) {
            logWithTimestamp("ExternalReportWS created");
        } else {
            throw new Exception("ExternalReportWS is not created.");
        }
        /*
        FndManageImportExportFilesService svcManageFiles = FndImportExportFileServiceUtil.createImportServiceUserToken(instanceName, dataCenter, userName,
                                                                                 userPass);
        
        if (svcManageFiles != null) {
            logWithTimestamp("FndManageImportExportFilesService created");
        } else {
            throw new Exception("FndManageImportExportFilesService is not created.");
        }
        */
        //Extraccion de LegalEntity BU Warehouse
        reportOutputBytes = this.runReport(svcReport, legalEntityBUWarehouseXMLPath);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(legalEntityBUWarehouseXMLPath + " report cannot be executed.");
        }
        
        logWithTimestamp(legalEntityBUWarehouseXMLPath + " report was executed. " + reportOutputBytes.length);
        
        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(GRELegalEntityBUWarehouseDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        GRELegalEntityBUWarehouseDS dataEntityBUWarehouseDS =
            (GRELegalEntityBUWarehouseDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));
        
        this.processLegalEntityBUWarehouseData(dataEntityBUWarehouseDS, conn);

        conn.commit();
        
        //Extraccion de Misc Data
        reportOutputBytes = null;
        reportOutputBytes = this.runReport(svcReport, miscDataXMLPath);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(miscDataXMLPath + " report cannot be executed.");
        }
        
        logWithTimestamp(miscDataXMLPath + " report was executed. " + reportOutputBytes.length);

        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(GREMiscDataDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        GREMiscDataDS dataMiscDS = (GREMiscDataDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));

        this.processMiscData(dataMiscDS, conn);

        conn.commit();
      /*  
        //Extraccion de Material Transactions
        reportOutputBytes = null;
        reportOutputBytes = this.runReport(svcReport, materialTrxXMLPath);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(materialTrxXMLPath + " report cannot be executed.");
        }
        
        logWithTimestamp(materialTrxXMLPath + " report was executed. " + reportOutputBytes.length);

        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(GREInvMaterialTrxDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        GREInvMaterialTrxDS materialTrxDS = (GREInvMaterialTrxDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));

        this.processMaterialTrxData(svcManageFiles, materialTrxDS, conn, invTrxVSName);

        conn.commit();
      */  
        //Extraccion de Receptors
        reportOutputBytes = null;
        reportOutputBytes = this.runReport(svcReport, receptorXMLPath);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(receptorXMLPath + " report cannot be executed.");
        }
        
        logWithTimestamp(receptorXMLPath + " report was executed. " + reportOutputBytes.length);

        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(GREReceptorsDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        GREReceptorsDS receptorsDS = (GREReceptorsDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));

        this.processReceptorsData(receptorsDS, conn);

        conn.commit();
        
        //Extraccion de CRE Miscelaneos
        reportOutputBytes = null;
        reportOutputBytes = this.runReport(svcReport, payDocXMLPath);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(payDocXMLPath + " report cannot be executed.");
        }
        log.info(payDocXMLPath + " report was executed. " + reportOutputBytes.length);

        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(CREMiscDataDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        CREMiscDataDS creMiscDS = (CREMiscDataDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));

        this.processMiscData(creMiscDS, conn);

        conn.commit();
        
    }
}
