package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class GREReceptorsDS {
    GRECustomers[] customers;
    GREVendors[] vendors;    
        
    public GREReceptorsDS() {
        super();
    }

    @XmlElement(name = "G_CUSTOMERS")
    public void setCustomers(GRECustomers[] customers) {
        this.customers = customers;
    }

    public GRECustomers[] getCustomers() {
        return customers;
    }

    @XmlElement(name = "G_VENDORS")
    public void setVendors(GREVendors[] vendors) {
        this.vendors = vendors;
    }

    public GREVendors[] getVendors() {
        return vendors;
    }
}
