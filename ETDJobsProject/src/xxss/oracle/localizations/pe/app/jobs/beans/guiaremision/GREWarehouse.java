package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GREWarehouse {
    long organizationId;
    String organizationName;
    String organizationCode;
    long invOrgBusinessUnitId;
    String country;
    String city;
    String state;
    String address1;
    String postalCode;
    long legalEntityId;
    
    public GREWarehouse() {
        super();
    }

    @XmlElement(name = "ORGANIZATION_ID")
    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    @XmlElement(name = "ORGANIZATION_NAME")
    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    @XmlElement(name = "ORGANIZATION_CODE")
    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    @XmlElement(name = "INV_ORG_BUSINESS_UNIT_ID")
    public void setInvOrgBusinessUnitId(long invOrgBusinessUnitId) {
        this.invOrgBusinessUnitId = invOrgBusinessUnitId;
    }

    
    public long getInvOrgBusinessUnitId() {
        return invOrgBusinessUnitId;
    }

    @XmlElement(name = "COUNTRY")
    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @XmlElement(name = "CITY")
    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    @XmlElement(name = "STATE")
    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    @XmlElement(name = "ADDRESS1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }

    @XmlElement(name = "POSTAL_CODE")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    @XmlElement(name = "LEGAL_ENTITY_ID")
    public void setLegalEntityId(long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public long getLegalEntityId() {
        return legalEntityId;
    }
}
