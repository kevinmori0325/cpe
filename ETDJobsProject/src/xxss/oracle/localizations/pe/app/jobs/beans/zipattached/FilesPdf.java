package xxss.oracle.localizations.pe.app.jobs.beans.zipattached;

import java.io.InputStream;

public class FilesPdf {
    String fileName;
    String fileExt;
    InputStream filePdf;
    public FilesPdf(){
        super();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFilePdf(InputStream fileXml) {
        this.filePdf = fileXml;
    }

    public InputStream getFilePdf() {
        return filePdf;
    }
}
