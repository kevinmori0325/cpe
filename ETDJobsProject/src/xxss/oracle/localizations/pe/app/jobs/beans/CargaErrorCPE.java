package xxss.oracle.localizations.pe.app.jobs.beans;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;
import java.io.OutputStream;

import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.PasswordAuthentication;
import java.net.URL;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.Base64;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;

import oracle.jdbc.OracleResultSet;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import org.xml.sax.InputSource;

import xxss.oracle.localizations.pe.app.jobs.beans.factelectronica.FEDataDS;
import xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil3;

@DisallowConcurrentExecution
public class CargaErrorCPE extends XxssJob
{
    
    private SimpleDateFormat targetDateFormat;
    public CargaErrorCPE() {
        super();
        this.targetDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    }
    
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.implementClassName = CargaErrorCPE.class.getName();
        super.execute(jobExecutionContext);
    }


    @Override
    public void doAction(JobDataMap dataMap, OracleConnection conn) throws Exception {
        // TODO Implement this method
        
        String userName = dataMap.getString("USER_NAME");
        String userPass = dataMap.getString("USER_PASS");
        String instanceName = dataMap.getString("INSTANCE_NAME");
        String dataCenter = dataMap.getString("DATA_CENTER");
        String reportPath = dataMap.getString("REPORT_PATH");
        String soapActionUpLoad = dataMap.getString("SOAP_ACTION_WS_UP");
        String soapActionImport = dataMap.getString("SOAP_ACTION_WS_IMP");
        String urlWsInterface = dataMap.getString("URL_WS_INTERFACE");
        
        ExternalReportWSSService svcReport =
            ExternalReportWSUtil3.createExternalReportWSSServiceUserToken3(instanceName, dataCenter, userName,
                                                                           userPass);
        if (svcReport != null) {
            logWithTimestamp("ExternalReportWS created");
        } else {
            logWithTimestamp("ExternalReportWS is not created.");
        }
        
        this.logWithTimestamp(soapActionUpLoad);
        this.logWithTimestamp(soapActionImport);
        this.logWithTimestamp(urlWsInterface);
        
        this.sendInterfaceAp(userName, userPass, instanceName, dataCenter, urlWsInterface, soapActionUpLoad, soapActionImport,  conn);

        /*byte[] reportOutputBytes = runReport(svcReport, reportPath);
        if ((reportOutputBytes == null) || (reportOutputBytes.length == 0)) {
            throw new Exception("FE AR Pending Invoice XML report cannot be executed.");
        }


        InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);

        JAXBContext jaxbContextDataDS = JAXBContext.newInstance(new Class[] { FEDataDS.class });
        Unmarshaller jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        FEDataDS dataDS = (FEDataDS) jaxbUnmarshaller.unmarshal(new InputSource(reportIs));*/
        

    }
    
    private void cargaComprobanteElec(){
        
        
        
        
    }
    
    public String sendInterfaceAp(String userName, String pass, String instanceName, String dataCenter, String url, String soapActionUpLoad, String soapActionImport, OracleConnection trans) 
    throws MalformedURLException,
           SQLException,
           IOException {

        String soap = "";
        StringBuffer strBuffer = new StringBuffer();
        String XmlLines = "";
        long orgId = 0;
        long ledgerId = 0;
        
        this.logWithTimestamp("Inicio send Interface");
        
        String lquery = "select CPE_INVOICE_ID,\n" +
                                        "       SOURCE,\n" +
                                        "       ORG_ID,\n" +
                                        "       LEDGER_ID,\n" +
                                        "       BU_NAME,\n"+
                                        "       VENDOR_ID,\n" +
                                        "       VENDOR_NAME,\n"+
                                        "       VENDOR_SITE_CODE,\n" +
                                        "       ATTRIBUTE14,\n" +
                                        "       ATTRIBUTE15,\n" +
                                        "       ATTRIBUTE_CATEGORY,\n" +
                                        "       DOCUMENT_SUBTYPE,\n" +
                                        "       EXCHANGE_DATE,\n" +
                                        "       EXCHANGE_RATE_TYPE,\n" +
                                        "       GOODS_RECEIVED_DATE,\n" +
                                        "       INVOICE_AMOUNT,\n" +
                                        "       INVOICE_DATE,\n" +
                                        "       INVOICE_CURRENCY_CODE,\n" +
                                        "       INVOICE_NUMBER,\n" +
                                        "       INVOICE_RECEIVED_DATE,\n" +
                                        "       INVOICE_TYPE_LOOKUP_CODE,\n" +
                                        "       LEGAL_ENTITY_NAME,\n" +
                                        "       LEGAL_ENTITY_ID,\n" +
                                        "       PDF_CLOB,\n" +
                                        "       STATUS\n" +
                                        "  from NC_CPE_AP_INVOICE_HEADERS_ALL \n" +
                                        " where 1 = 1\n" +
                                        " and STATUS = 'ERROR_ADMIN'";
        
        
        ResultSet resultado;

                OraclePreparedStatement lStmt =
                    (OraclePreparedStatement)trans.prepareStatement(lquery, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                    OracleResultSet.CONCUR_READ_ONLY);
                //lStmt.setString(1, pIdProcess);
                resultado = lStmt.executeQuery();
                
                
        while (resultado.next()) {
            
            this.logWithTimestamp("While Cab");

                    String message    = "";
                    String status     = "";
                    //int    lineNumber = 0; 
                    //dataExist         = true;
                    
                    long   cpeInvoiceId             = resultado.getLong  ("CPE_INVOICE_ID");
                    String source                   = resultado.getString("SOURCE");
                    orgId                    = resultado.getLong("ORG_ID");
                    ledgerId                 = resultado.getLong("LEDGER_ID");
                    String buName                   = resultado.getNString("BU_NAME");
                    double vendorId                 = resultado.getDouble("VENDOR_ID");
                    String vendorName               = resultado.getString("VENDOR_NAME");
                    String vendorSiteCode           = resultado.getString("VENDOR_SITE_CODE");
                    String attribute14              = resultado.getString("ATTRIBUTE14");
                    String attribute15              = resultado.getString("ATTRIBUTE15");
                    String attributeCategory        = resultado.getString("ATTRIBUTE_CATEGORY");
                    String documentSubtype          = resultado.getString("DOCUMENT_SUBTYPE");
                    Date   exchangeDate             = resultado.getDate  ("EXCHANGE_DATE");
                    String exchangeRateType         = resultado.getString("EXCHANGE_RATE_TYPE");
                    Date   goodsReceivedDate        = resultado.getDate  ("GOODS_RECEIVED_DATE");
                    double invoiceAmount            = resultado.getDouble("INVOICE_AMOUNT");
                    Date   invoiceDate              = resultado.getDate  ("INVOICE_DATE");
                    String invoiceCurrencyCode      = resultado.getString("INVOICE_CURRENCY_CODE");
                    String invoiceNumber            = resultado.getString("INVOICE_NUMBER");
                    Date   invoiceReceivedDate      = resultado.getDate  ("INVOICE_RECEIVED_DATE");
                    String invoiceTypeLookupCode    = resultado.getString("INVOICE_TYPE_LOOKUP_CODE");
                    String legalEntityName          = resultado.getString("LEGAL_ENTITY_NAME");
                    double legalEntityId            = resultado.getDouble("LEGAL_ENTITY_ID");
                    String pdfClob                  = resultado.getString("PDF_CLOB");
                    status                   = resultado.getString("STATUS");
            
            
        String lqueryDet =  "select CPE_INVOICE_ID,\n" +
                                            "       CPE_INVOICE_LINE_ID,\n" +
                                            "       AMOUNT,\n" +
                                            "       LINE_TYPE_LOOKUP_CODE,\n" +
                                            "       LINE_NUMBER,\n" +
                                            "       PO_NUMBER,\n" +
                                            "       PO_LINE_NUMBER,\n" +
                                            "       INVOICE_QUANTITY,\n" +
                                            "       RECEIPT_LINE_NUMBER,\n" +
                                            "       RECEIPT_NUMBER,\n" +
                                            "       TAX_CLASSIFICATION_CODE,\n" +
                                            "       CURRENCY_CODE,\n" +
                                            "       PRODUCT_TYPE,\n" +
                                            "       USER_DEFINED_FISC_CLASS,\n" +
                                            "       UNIT_CODE\n"+
                                            "  from NC_CPE_INVOICE_LINES_ALL \n" +
                                            " where CPE_INVOICE_ID = ? " ;

        ResultSet resultadoDet = null;

                OraclePreparedStatement lStmtDet =
                    (OraclePreparedStatement)trans.prepareStatement(lqueryDet, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                    OracleResultSet.CONCUR_READ_ONLY);
                lStmtDet.setLong  (1, cpeInvoiceId);
                resultadoDet = lStmtDet.executeQuery();
            
            
        while (resultadoDet.next()) {

                    //String message    = "";
                    //int    lineNumber = 0; 
                    //dataExist         = true;
            
                    this.logWithTimestamp("While Det");
                    
                    //long   cpeInvoiceId             = resultadoDet.getLong  ("CPE_INVOICE_ID");
                    //long cpeInvoiceLineId         = resultadoDet.getLong("CPE_INVOICE_LINE_ID");
                    double amount                   = resultadoDet.getDouble("AMOUNT");
                    String lineTypeLookupCode       = resultadoDet.getString("LINE_TYPE_LOOKUP_CODE");
                    double lineNumber               = resultadoDet.getDouble("LINE_NUMBER");
                    double poNumber                 = resultadoDet.getDouble("PO_NUMBER");
                    double poLineNumber             = resultadoDet.getDouble("PO_LINE_NUMBER");
                    double invoiceQuantity          = resultadoDet.getDouble("INVOICE_QUANTITY");
                    double receiptLineNumber        = resultadoDet.getDouble("RECEIPT_LINE_NUMBER");
                    double receiptNumber            = resultadoDet.getDouble("RECEIPT_NUMBER");
                    String taxClassificationCode    = resultadoDet.getString("TAX_CLASSIFICATION_CODE");
                    String currencyCode             = resultadoDet.getString("CURRENCY_CODE");
                    String productType              = resultadoDet.getString("PRODUCT_TYPE");
                    String userDefinedFiscClass     = resultadoDet.getString("USER_DEFINED_FISC_CLASS");
                    String unitCode                 = resultadoDet.getString("UNIT_CODE");
            
                
            
                 XmlLines = "<inv:InvoiceInterfaceLine>\n" + 
                            "               <inv:Amount currencyCode=\""+currencyCode+"\">"+amount+"</inv:Amount>\n" + 
                            "               <inv:LineTypeLookupCode>"+lineTypeLookupCode+"</inv:LineTypeLookupCode>\n" + 
                            "               <inv:PONumber>"+poNumber+"</inv:PONumber>\n" + 
                            "               <inv:POLineNumber>"+poLineNumber+"</inv:POLineNumber>\n" + 
                            "               <inv:ProductType>"+productType+"</inv:ProductType>              \n" + 
                            "               <inv:InvoicedQuantity unitCode=\""+unitCode+"\">"+invoiceQuantity+"</inv:InvoicedQuantity>\n" + 
                            "               <inv:ReceiptLineNumber>"+receiptLineNumber+"</inv:ReceiptLineNumber>\n" + 
                            "               <inv:ReceiptNumber>"+receiptNumber+"</inv:ReceiptNumber>              \n" + 
                            "               <inv:TaxClassificationCode>"+taxClassificationCode+"</inv:TaxClassificationCode>              \n" + 
                            "               <inv:UserDefinedFiscClass>"+userDefinedFiscClass+"</inv:UserDefinedFiscClass>      \n" + 
                            "            </inv:InvoiceInterfaceLine>\n";
            
                strBuffer.append(XmlLines);
            
            }
            
            lStmtDet.close();
            
            String xmlInput =
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\" xmlns:inv=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/\">\n" + 
                "   <soapenv:Header/>\n" + 
                "   <soapenv:Body>\n" + 
                "      <typ:createInvoiceInterfaceWithAttachments>\n" + 
                "         <typ:invoiceInterfaceHeader>\n" + 
                "            <inv:Source>"+source+"</inv:Source>\n" + 
                "            <inv:OperatingUnit>"+buName+"</inv:OperatingUnit>\n" + 
                "            <inv:VendorName>"+vendorName+"</inv:VendorName>\n" + 
                "            <inv:VendorSiteCode>"+vendorSiteCode+"</inv:VendorSiteCode>   \n" + 
                "            <inv:Attribute14>"+attribute14+"</inv:Attribute14>\n" + 
                "            <inv:Attribute15>"+attribute15+"</inv:Attribute15>          \n" + 
                "            <inv:AttributeCategory>"+attributeCategory+"</inv:AttributeCategory>           \n" + 
                "            <inv:DocumentSubType>"+documentSubtype+"</inv:DocumentSubType>\n" + 
                "            <!--<inv:ExchangeDate>"+exchangeDate+"</inv:ExchangeDate>-->\n" + 
                "            <!--<inv:ExchangeRateType>"+exchangeRateType+"</inv:ExchangeRateType>-->            \n" + 
                "            <inv:InvoiceAmount currencyCode=\""+invoiceCurrencyCode+"\">"+invoiceAmount+"</inv:InvoiceAmount>\n" + 
                "            <inv:InvoiceCurrencyCode>"+invoiceCurrencyCode+"</inv:InvoiceCurrencyCode>\n" + 
                "            <inv:InvoiceDate>"+invoiceDate+"</inv:InvoiceDate>\n" + 
                "            <inv:InvoiceNumber>"+invoiceNumber+"</inv:InvoiceNumber>\n" + 
                "            <inv:InvoiceReceivedDate>"+this.targetDateFormat.format(invoiceReceivedDate)+"</inv:InvoiceReceivedDate>\n" + 
                "            <inv:InvoiceTypeLookupCode>"+invoiceTypeLookupCode+"</inv:InvoiceTypeLookupCode>\n" + 
                "            <inv:LegalEntityName>"+legalEntityName+"</inv:LegalEntityName>\n" + 
                strBuffer.toString()+
                "         </typ:invoiceInterfaceHeader>\n" + 
                "      </typ:createInvoiceInterfaceWithAttachments>\n" + 
                "   </soapenv:Body>\n" + 
                "</soapenv:Envelope>\n";

            this.logWithTimestamp(xmlInput);
            
            
            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\" xmlns:inv=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/\">\n" + 
            "   <soapenv:Header/>\n" + 
            "   <soapenv:Body>\n" + 
            "      <typ:createInvoiceInterfaceWithAttachments>\n" + 
            "         <typ:invoiceInterfaceHeader>\n" + 
            "            <inv:Source>ELECTRONIC INVOICE</inv:Source>\n" + 
            "            <inv:OperatingUnit>NEORA BU</inv:OperatingUnit>\n" + 
            "            <inv:VendorName>SOLUTIONS SAGE S.A.C.</inv:VendorName>\n" + 
            "            <inv:VendorSiteCode>FACTURA-REL</inv:VendorSiteCode>   \n" + 
            "            <inv:Attribute14>5</inv:Attribute14>\n" + 
            "            <inv:Attribute15>01</inv:Attribute15>          \n" + 
            "            <inv:AttributeCategory>SP Peru</inv:AttributeCategory>           \n" + 
            "            <inv:DocumentSubType>PE DET/12%/22</inv:DocumentSubType>\n" + 
            "            <!--<inv:ExchangeDate>2020-12-01</inv:ExchangeDate>-->\n" + 
            "            <!--<inv:ExchangeRateType>Venta</inv:ExchangeRateType>-->            \n" + 
            "            <inv:InvoiceAmount currencyCode=\"PEN\">94.4</inv:InvoiceAmount>\n" + 
            "            <inv:InvoiceCurrencyCode>PEN</inv:InvoiceCurrencyCode>\n" + 
            "            <inv:InvoiceDate>2020-12-01</inv:InvoiceDate>\n" + 
            "            <inv:InvoiceNumber>F001-100</inv:InvoiceNumber>\n" + 
            "            <inv:InvoiceReceivedDate>2020-12-01</inv:InvoiceReceivedDate>\n" + 
            "            <inv:InvoiceTypeLookupCode>STANDARD</inv:InvoiceTypeLookupCode>\n" + 
            "            <inv:LegalEntityName>HSP S.A.C.</inv:LegalEntityName>\n" + 
            "            <inv:InvoiceInterfaceLine>\n" + 
            "            <inv:Amount currencyCode=\"PEN\">80</inv:Amount>\n" + 
            "               <inv:LineTypeLookupCode>ITEM</inv:LineTypeLookupCode>\n" + 
            "               <inv:PONumber>10008</inv:PONumber>\n" + 
            "               <inv:POLineNumber>1</inv:POLineNumber>\n" + 
            "               <inv:ProductType>SERVICES</inv:ProductType>              \n" + 
            "               <inv:InvoicedQuantity unitCode=\"H\">1</inv:InvoicedQuantity>\n" + 
            "               <inv:ReceiptLineNumber>1</inv:ReceiptLineNumber>\n" + 
            "               <inv:ReceiptNumber>1362</inv:ReceiptNumber>              \n" + 
            "               <inv:TaxClassificationCode>PE_GRAVADO</inv:TaxClassificationCode>              \n" + 
            "               <inv:UserDefinedFiscClass>SP_GRAVADO</inv:UserDefinedFiscClass>      \n" + 
            "            </inv:InvoiceInterfaceLine>\n" + 
            "         </typ:invoiceInterfaceHeader>\n" + 
            "      </typ:createInvoiceInterfaceWithAttachments>\n" + 
            "   </soapenv:Body>\n" + 
            "</soapenv:Envelope>";
            
            this.logWithTimestamp(xml);
            
            this.executeApInterfaceWs(xmlInput,
                                        url,
                                        userName,
                                        pass,
                                        soapActionUpLoad);
            
            this.logWithTimestamp(soap); 
            
            
            
            
            
        }    
        
        String xmlImport = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:typ=\"http://xmlns.oracle.com/apps/financials/payables/invoices/quickInvoices/invoiceInterfaceService/types/\">\n" + 
        "   <soapenv:Header/>\n" + 
        "   <soapenv:Body>\n" + 
        "      <typ:submitInvoiceImport>\n" + 
        "         <typ:ledgerId>"+ledgerId+"</typ:ledgerId>\n" + 
        "         <typ:orgId>"+orgId+"</typ:orgId>\n" + 
        "         <typ:importSet>"+""+"</typ:importSet>\n" + 
        "         <typ:groupName>"+""+"</typ:groupName>\n" + 
        "         <typ:accountingDate>"+""+"</typ:accountingDate>\n" + 
        "         <typ:hold>"+""+"</typ:hold>\n" + 
        "         <typ:holdReason>"+""+"</typ:holdReason>\n" + 
        "         <typ:summarizeReport>"+""+"</typ:summarizeReport>\n" + 
        "      </typ:submitInvoiceImport>\n" + 
        "   </soapenv:Body>\n" + 
        "</soapenv:Envelope>";
        this.logWithTimestamp(xmlImport);
        
        this.executeApInterfaceWs(xmlImport,
                                    url,
                                    userName,
                                    pass,
                                    soapActionImport);
        
        this.logWithTimestamp("Fin");
        
        lStmt.close();
        

        
        return soap;
    }
    
    public String executeApInterfaceWs(String requestPayload, String url, String userName, String pass, String soapAction) throws MalformedURLException, IOException {
        
       
       String soap = "";

        
       URL oURL = new URL(url);

       HttpURLConnection httpConn = (HttpURLConnection) oURL.openConnection();


       byte[] request = new byte[requestPayload.length()];
       request = requestPayload.getBytes();

       String userCredentials = userName + ":" + pass;
       String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

       /* Authenticator.setDefault(new Authenticator() {
           @Override
           protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(userName, pass.toCharArray());
           }
       });*/

       httpConn.setRequestProperty("Authorization", basicAuth);
       httpConn.setRequestProperty("Content-Length", String.valueOf(request.length));
       httpConn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
       httpConn.setRequestProperty("SOAPAction", soapAction);
       httpConn.setRequestMethod("POST");
       httpConn.setDoOutput(true);
       httpConn.setDoInput(true);


       OutputStream out = httpConn.getOutputStream();
       //Write the content of the request to the outputstream of the HTTP Connection.
       out.write(request);
       out.flush();
       out.close();
       //Ready with sending the request.
       //Read the response.
       //httpConn.connect();

       int responseCode = httpConn.getResponseCode();

       StringBuilder builder = new StringBuilder();
       InputStreamReader in = new InputStreamReader(httpConn.getInputStream(), "UTF-8");
       BufferedReader br = new BufferedReader(in);
       String output;
       if (responseCode == HttpURLConnection.HTTP_OK) {
           //Write the SOAP message response to a String.
           while ((output = br.readLine()) != null) {
               builder.append(output).append("\n");
           }
       }
       soap = builder.toString();
       System.out.println(soap);
       br.close();
       httpConn.disconnect();   
        
     return soap;   
    }
    
    private byte[] runReport(xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService svc,
                             String reportPath) throws MalformedURLException, Exception {

        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ReportRequest req =
            new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfParamNameValue arrayParam =
            new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice3.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);


        req.setParameterNameValues(arrayParam);
        req.setReportAbsolutePath(reportPath);
        req.setSizeOfDataChunkDownload(-1);
        req.setAttributeFormat("xml");

        xxss.oracle.localizations.services.fusion.externalreportservice3.types.ReportResponse resp =
            svc.runReport(req, "");

        return (resp.getReportBytes());

    }
}
