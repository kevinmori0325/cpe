package xxss.oracle.localizations.pe.app.jobs.beans;

import java.sql.SQLException;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@DisallowConcurrentExecution
public class CleanRequests extends XxssJob {
    public CleanRequests() {
        super();
    }

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        this.implementClassName = CleanRequests.class.getName();
        super.execute(jobExecutionContext);
    }
    
    public void callCleanRequests(int pDays, OracleConnection conn) throws SQLException {
        String st = "begin XXSS_PE_LOC_JOB_PKG.pr_clean_request(?); end;";
        OracleCallableStatement acs = (OracleCallableStatement)conn.prepareCall(st);

        acs.setInt(1, pDays);        

        acs.executeUpdate();
        acs.close();
    }

    public void doAction(JobDataMap dataMap, OracleConnection conn) throws SQLException {
        String daysStr = dataMap.getString("NUM_DAYS"); //dataMap.getString("biUserName");
        int days = Integer.valueOf(daysStr);
        this.callCleanRequests(days, conn);
    }
}
