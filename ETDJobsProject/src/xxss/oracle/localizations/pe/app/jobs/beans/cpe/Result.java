package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;

public class Result {
    Value value;
    
    public Result() {
        super();
    }

    @XmlElement(name = "Value")
    public void setValue(Value value) {
        this.value = value;
    }

    public Value getValue() {
        return value;
    }
}
