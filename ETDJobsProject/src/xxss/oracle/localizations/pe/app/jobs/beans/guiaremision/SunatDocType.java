package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class SunatDocType {
    String flexValue;
    String description;
    
    public SunatDocType() {
        super();
    }

    @XmlElement(name = "FLEX_VALUE")
    public void setFlexValue(String flexValue) {
        this.flexValue = flexValue;
    }

    public String getFlexValue() {
        return flexValue;
    }

    @XmlElement(name = "DESCRIPTION")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
