package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CPELines {
    long receivableApplicationId;
    double amountApplied;
    double amountAppliedFrom;
    String trxNumber;
    Date trxDate;
    long appliedCustomerTrxId;
    double totalAmount;
    String tipoComprPago;
    String docSerie;
    String docNumero;
    double lineApplied;
    double taxApplied;
    String invoiceCurrencyCode;
    double funcPayAmount;
    double origPayAmount;
    
    public CPELines() {
        super();
    }

    @XmlElement(name = "RECEIVABLE_APPLICATION_ID")
    public void setReceivableApplicationId(long receivableApplicationId) {
        this.receivableApplicationId = receivableApplicationId;
    }

    public long getReceivableApplicationId() {
        return receivableApplicationId;
    }

    @XmlElement(name = "AMOUNT_APPLIED")
    public void setAmountApplied(double amountApplied) {
        this.amountApplied = amountApplied;
    }

    public double getAmountApplied() {
        return amountApplied;
    }

    @XmlElement(name = "AMOUNT_APPLIED_FROM")
    public void setAmountAppliedFrom(double amountAppliedFrom) {
        this.amountAppliedFrom = amountAppliedFrom;
    }

    public double getAmountAppliedFrom() {
        return amountAppliedFrom;
    }

    @XmlElement(name = "TRX_NUMBER")
    public void setTrxNumber(String trxNumber) {
        this.trxNumber = trxNumber;
    }

    public String getTrxNumber() {
        return trxNumber;
    }

    @XmlElement(name = "TRX_DATE")
    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    @XmlElement(name = "APPLIED_CUSTOMER_TRX_ID")
    public void setAppliedCustomerTrxId(long appliedCustomerTrxId) {
        this.appliedCustomerTrxId = appliedCustomerTrxId;
    }

    public long getAppliedCustomerTrxId() {
        return appliedCustomerTrxId;
    }

    @XmlElement(name = "TOTAL_AMOUNT")
    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    @XmlElement(name = "TIPO_COMPROBANTE_PAGO")
    public void setTipoComprPago(String tipoComprPago) {
        this.tipoComprPago = tipoComprPago;
    }

    public String getTipoComprPago() {
        return tipoComprPago;
    }

    @XmlElement(name = "DOC_SERIE")
    public void setDocSerie(String docSerie) {
        this.docSerie = docSerie;
    }

    public String getDocSerie() {
        return docSerie;
    }

    @XmlElement(name = "DOC_NUMERO")
    public void setDocNumero(String docNumero) {
        this.docNumero = docNumero;
    }

    public String getDocNumero() {
        return docNumero;
    }

    @XmlElement(name = "LINE_APPLIED")
    public void setLineApplied(double lineApplied) {
        this.lineApplied = lineApplied;
    }

    public double getLineApplied() {
        return lineApplied;
    }

    @XmlElement(name = "TAX_APPLIED")
    public void setTaxApplied(double taxApplied) {
        this.taxApplied = taxApplied;
    }

    public double getTaxApplied() {
        return taxApplied;
    }

    @XmlElement(name = "INVOICE_CURRENCY_CODE")
    public void setInvoiceCurrencyCode(String invoiceCurrencyCode) {
        this.invoiceCurrencyCode = invoiceCurrencyCode;
    }

    public String getInvoiceCurrencyCode() {
        return invoiceCurrencyCode;
    }

    @XmlElement(name = "FUNC_PAY_AMOUNT")
    public void setFuncPayAmount(double funcPayAmount) {
        this.funcPayAmount = funcPayAmount;
    }

    public double getFuncPayAmount() {
        return funcPayAmount;
    }

    @XmlElement(name = "ORIG_PAY_AMOUNT")
    public void setOrigPayAmount(double origPayAmount) {
        this.origPayAmount = origPayAmount;
    }

    public double getOrigPayAmount() {
        return origPayAmount;
    }
}
