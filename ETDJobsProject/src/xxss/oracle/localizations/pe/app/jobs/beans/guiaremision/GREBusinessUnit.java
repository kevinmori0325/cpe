package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GREBusinessUnit {
    long buId;
    String buName;
    long legalEntityId;
    long primaryLedgerId;
    String shortCode;
    long defaultSetId;
    String ledgerName;
    GREWarehouse[] warehouses;
    
    public GREBusinessUnit() {
        super();
    }

    @XmlElement(name = "BU_ID")
    public void setBuId(long buId) {
        this.buId = buId;
    }

    public long getBuId() {
        return buId;
    }

    @XmlElement(name = "BU_NAME")
    public void setBuName(String buName) {
        this.buName = buName;
    }

    public String getBuName() {
        return buName;
    }

    @XmlElement(name = "PRIMARY_LEDGER_ID")
    public void setPrimaryLedgerId(long primaryLedgerId) {
        this.primaryLedgerId = primaryLedgerId;
    }

    public long getPrimaryLedgerId() {
        return primaryLedgerId;
    }

    @XmlElement(name = "SHORT_CODE")
    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getShortCode() {
        return shortCode;
    }

    @XmlElement(name = "G_WAREHOUSE")
    public void setWarehouses(GREWarehouse[] warehouses) {
        this.warehouses = warehouses;
    }

    public GREWarehouse[] getWarehouses() {
        return warehouses;
    }

    @XmlElement(name = "LEGAL_ENTITY_ID")
    public void setLegalEntityId(long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public long getLegalEntityId() {
        return legalEntityId;
    }
    
    @XmlElement(name = "DEFAULT_SET_ID")
    public void setDefaultSetId(long defaultSetId) {
        this.defaultSetId = defaultSetId;
    }

    public long getDefaultSetId() {
        return defaultSetId;
    }

    @XmlElement(name = "LEDGER_NAME")
    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getLedgerName() {
        return ledgerName;
    }
}
