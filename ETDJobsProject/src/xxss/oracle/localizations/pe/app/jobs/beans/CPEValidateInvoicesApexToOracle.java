package xxss.oracle.localizations.pe.app.jobs.beans;

import com.bea.httppubsub.json.JSONArray;
import com.bea.httppubsub.json.JSONException;
import com.bea.httppubsub.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;

import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.io.OutputStream;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URL;

import java.sql.Blob;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.xml.bind.JAXBContext;

import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;

import org.quartz.JobDataMap;

import org.xml.sax.InputSource;

import xxss.oracle.localizations.pe.app.jobs.beans.cpe.CPESyncDS;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.proxy.ErpObjectAttachmentService;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.proxy.ServiceException;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.types.AttachmentDetails;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.util.ErpObjAttachWSUtil;
import xxss.oracle.localizations.services.util.ExternalReportWSUtil2;


public class CPEValidateInvoicesApexToOracle extends XxssJob {
    public CPEValidateInvoicesApexToOracle() {
        super();
    }

    public void doAction(JobDataMap dataMap, OracleConnection conn) throws Exception {
        // TODO Implement this method
        
        String userName = dataMap.getString("USER_NAME");
        String userPass = dataMap.getString("USER_PASS");
        String instanceName = dataMap.getString("INSTANCE_NAME");
        String dataCenter = dataMap.getString("DATA_CENTER");
        String reportPath = dataMap.getString("REPORT_PATH");
        String url = dataMap.getString("URL_WS_INVOICES_GET");
        String urlUpdate = dataMap.getString("URL_WS_UPDATE");
        
        /*byte[] reportOutputBytes;
        InputStream reportIs;
        JAXBContext jaxbContextDataDS;
        Unmarshaller jaxbUnmarshaller;*/
        
        ExternalReportWSSService svcReport = ExternalReportWSUtil2.createExternalReportWSSServiceUserToken2(instanceName, dataCenter, userName,
                                                                                 userPass);
        
        if (svcReport != null) {
            logWithTimestamp("ExternalReportWS created");
        } else {
            throw new Exception("ExternalReportWS is not created.");
        }
        
        ErpObjectAttachmentService svcAttach = ErpObjAttachWSUtil.createErpObjectAttachmentService2(instanceName, dataCenter, userName, userPass);
        
        if (svcAttach != null) {
            log.info("ErpObjAttachWS created");
        } else {
            throw new Exception("ErpObjAttachWS is not created.");
        }
        
        //this.getInvoicesAp(userName, userPass, url, urlUpdate, reportPath, svcReport, svcAttach);
        this.getInvoicesBD(userName, userPass, url, urlUpdate, reportPath, svcReport, svcAttach, conn);
        
        
        
        

    }
    
    
    public void getInvoicesBD(String usuario,String password,String url, String urlUpdate, String reportPath, ExternalReportWSSService svcReport,ErpObjectAttachmentService svcAttach,OracleConnection trans){
        
            String message = "";   
        
            try {    
            String lqueryDet =  "select CPE_INVOICE_ID,\n" +
                                                "       INVOICE_NUMBER,\n" +
                                                "       BU_NAME,\n" +
                                                "       VENDOR_NUMBER,\n" +     
                                                "       PDF_CLOB,\n" +  
                                                "       ORG_ID,\n"+
                                                "       VENDOR_ID,\n"+
                                                "       PDF_BLOB\n" +  
                                                "  from NC_CPE_AP_INVOICE_HEADERS_ALL \n" +
                                                " where 1 = 1 \n" +
                                                "and status = 'EXECUTE_IMPORT' \n"+
                                                " order by INVOICE_NUMBER";

            ResultSet resultadoDet = null;

                    OraclePreparedStatement lStmtDet =
                        (OraclePreparedStatement)trans.prepareStatement(lqueryDet, OracleResultSet.TYPE_FORWARD_ONLY,
                                                                        OracleResultSet.CONCUR_READ_ONLY);
                    //Long  (1, cpeInvoiceId);
                    resultadoDet = lStmtDet.executeQuery();
                
            StringBuffer  strBuffer = new StringBuffer(); ;   
            while (resultadoDet.next()) {

                       
                        //this.logWithTimestamp("While Det");
                        long cpeInvoiceId       = resultadoDet.getLong("CPE_INVOICE_ID");
                        String invoiceNumber       = resultadoDet.getString("INVOICE_NUMBER");
                        String buName                 = resultadoDet.getString("BU_NAME");
                        String vendorNumber            = resultadoDet.getString("VENDOR_NUMBER");
                        String base64Pdf2                  = resultadoDet.getString("PDF_CLOB");
                        long buId = resultadoDet.getLong("ORG_ID");
                        long vendorId = resultadoDet.getLong("VENDOR_ID");
 
                        Blob blobPdf2                  = resultadoDet.getBlob("PDF_BLOB");
                
                String base64Pdf = "";
                
                if(blobPdf2 != null){
                        byte[] blobPdf = blobPdf2.getBytes(1,(int)blobPdf2.length());
                
                        base64Pdf = new String(Base64.getEncoder().encode(blobPdf));//Base64..encodeBase64String(blobPdf);
                
                }
                
                        String urlUpdate2 =  urlUpdate + cpeInvoiceId;

     
                    this.validateInvoice(invoiceNumber, buId, vendorId, reportPath, base64Pdf, buName, vendorNumber,
                                         cpeInvoiceId, urlUpdate2, svcReport, svcAttach, trans);


                // blobPdf.getBytes();
                
                       
               // base64Pdf = this.quitarSaltos(base64Pdf);
                
                   // this.logWithTimestamp("base64 : "+base64Pdf);
                
                
                /*String[] userKeys = new String[5];
                userKeys[0] = buName; //Bu Name;
                userKeys[1] = invoiceNumber; //Invoice Number;
                userKeys[2] = vendorNumber; //Supplier Number;
                userKeys[3] = "#NULL";
                userKeys[4] = "#NULL";
                
                
                this.addAttachmentInvoicePDF(svc, "AP_INVOICES_ALL", "AP_SUPPORTING_DOC", userKeys, invoiceNumber,base64Pdf);*/


                    //this.addAttachmentInvoicePDF(svc, entityName, categoryName, userKeys, textString, base64Pdf);



            }
                
                lStmtDet.close();
        
            } catch (SQLException e) {    
             
                message = "Error SQLException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
                this.logWithTimestamp(message);
             
            }catch (ServiceException e) {
                message = "Error ServiceException: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
                this.logWithTimestamp(message);
            }catch (Exception e) {
                message = "Error Exception: " + e.getMessage() + "-" + Arrays.toString(e.getStackTrace());
                this.logWithTimestamp(message);
            }

        
        
        }
    
    public String quitarSaltos(String cadena) {
      // Para el reemplazo usamos un string vac�o 
      return cadena.replaceAll("\\r\\n", "\n"); 
    }
    
    private byte[] runReport(xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService svc, String reportPath, String invoiceNum, long proveedor, long bu) throws MalformedURLException, Exception {

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest req =
            new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue arrayParam =
            new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        param =  new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
              arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
              param.setName("P_INVOICE_NUM");
              arrayValues.getItem().add(invoiceNum);
              param.setValues(arrayValues);
              arrayParam.getItem().add(param);
              
              param =  new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
              arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
              param.setName("P_PROV");
              arrayValues.getItem().add(String.valueOf(proveedor));
              param.setValues(arrayValues);
              arrayParam.getItem().add(param);

              param =  new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
              arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
              param.setName("P_BU");
              arrayValues.getItem().add(String.valueOf(bu));
              param.setValues(arrayValues);
              arrayParam.getItem().add(param);


        req.setAttributeLocale("Spanish (Peru)");
        req.setParameterNameValues(arrayParam);
        req.setReportAbsolutePath(reportPath);
        req.setSizeOfDataChunkDownload(-1);
        req.setAttributeFormat("xml");

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportResponse resp =
            svc.runReport(req, "");

        return (resp.getReportBytes());

    }
    
    public String getInvoicesAp(String usuario,String password,String url, String urlUpdate, String reportPath, ExternalReportWSSService svcReport,ErpObjectAttachmentService svcAttach, OracleConnection trans) throws MalformedURLException, IOException, JSONException, Exception {

            String responseString = "";
            String outputString = "";


           URL oURL = new URL(url);
           
            this.logWithTimestamp(url);
           
            HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
                     

            httpConn.setRequestMethod("GET");      
            /*String userCredentials = usuario+":"+password;
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));      
            httpConn.setRequestProperty("Authorization", basicAuth);*/
           

            int responseCode = httpConn.getResponseCode();
           
            // System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                 BufferedReader in = new BufferedReader(new InputStreamReader(
                 httpConn.getInputStream()));
                 String inputLine;
                 StringBuffer response = new StringBuffer();

                 while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                 }
                 in.close();

               
                 outputString = response.toString();
                 
                this.logWithTimestamp("outputString: "+ outputString);
                 
            } else {
                String msj = "GET request not worked ";
                this.logWithTimestamp(msj);
                return msj;
            }
           
           
           
            JSONObject objetoJson = new JSONObject(outputString);
            JSONArray arregloJson = objetoJson.getJSONArray("items");
            //System.out.println(outputString);
           
            //ArrayList<Item> items = new ArrayList<>();
            // Iterar
            for (int indice = 0; indice < arregloJson.length(); indice++) {
                // Obtener objeto a trav�s del �ndice
                JSONObject object = arregloJson.getJSONObject(indice);
                String invoiceNumber = object.getString("invoice_number");
                long buId = object.getLong("bu_id");
                long vendorId = object.getLong("vendor_id");
                long cpeInvoiceId = object.getLong("cpe_invoice_id");
                String base64Pdf = object.getString("pdf_clob");
                
                String buName =  object.getString("bu_name");
                String vendorNumber = object.getString("vendor_number");    
               
                this.logWithTimestamp("Factura: "+invoiceNumber);
                this.logWithTimestamp("Unidad de Negocio: "+buName+"-"+buId);
                this.logWithTimestamp("Proveedor: "+vendorNumber+"-"+vendorId);
                this.logWithTimestamp("Id Factura: "+cpeInvoiceId);
                String urlUpdate2 =  urlUpdate + cpeInvoiceId;
                this.logWithTimestamp("Url: "+urlUpdate2);
                
                this.validateInvoice(invoiceNumber, buId, vendorId, reportPath, base64Pdf, buName, vendorNumber, cpeInvoiceId, urlUpdate2, svcReport,svcAttach,trans);
                
               
            }

           
           
            return outputString;
            
            
        }   
    
    public void validateInvoice(String invoiceNumber, long buId, long vendorId, String reportPath,String base64Pdf, String buName, String vendorNumber, long cpeInvoiceId, String urlUpdate, ExternalReportWSSService svcReport,  ErpObjectAttachmentService svcAttach, OracleConnection trans) throws Exception {
        
        byte[] reportOutputBytes;
        InputStream reportIs;
        JAXBContext jaxbContextDataDS;
        Unmarshaller jaxbUnmarshaller;
        
        reportOutputBytes = this.runReport(svcReport, reportPath, invoiceNumber, vendorId, buId);

        if (reportOutputBytes == null || reportOutputBytes.length == 0) {
            throw new Exception(reportPath + " report cannot be executed.");
        }
        
        logWithTimestamp(reportPath + " report was executed. " + reportOutputBytes.length);
        
        reportIs = new ByteArrayInputStream(reportOutputBytes);

        jaxbContextDataDS = JAXBContext.newInstance(CPESyncDS.class);
        jaxbUnmarshaller = jaxbContextDataDS.createUnmarshaller();

        CPESyncDS dataCPEInvoicesDS =
            (CPESyncDS)jaxbUnmarshaller.unmarshal(new InputSource(reportIs));
        
        if (dataCPEInvoicesDS.getInvoices() != null && dataCPEInvoicesDS.getInvoices().length > 0){
            
            this.callUpdateStatusDB(cpeInvoiceId, "PROCESS", "", "", 0, "", "", "", dataCPEInvoicesDS.getInvoices()[0].getInvoiceId(), trans);
            //this.callUpdateStatus("PROCESS", "", "", 0, "", "", "", dataCPEInvoicesDS.getInvoices()[0].getInvoiceId(), urlUpdate);
            
            String[] userKeys = new String[5];
            
            userKeys[0] = buName; //Bu Name;
            userKeys[1] = invoiceNumber; //Invoice Number;
            userKeys[2] = vendorNumber; //Supplier Number;
            userKeys[3] = "#NULL";
            userKeys[4] = "#NULL";
            
            
            this.addAttachmentInvoicePDF(svcAttach, "AP_INVOICES_ALL", "AP_SUPPORTING_DOC", userKeys, invoiceNumber,base64Pdf);
            
            
        }
        else {
            if(dataCPEInvoicesDS.getErrorInterface() != null && dataCPEInvoicesDS.getErrorInterface().length > 0){
                logWithTimestamp("Entro a reporte error");
                this.callUpdateStatusDB(cpeInvoiceId, "ERROR_INT", dataCPEInvoicesDS.getErrorInterface()[0].getReject_lookup_code(), dataCPEInvoicesDS.getErrorInterface()[0].getRejection_message(), dataCPEInvoicesDS.getErrorInterface()[0].getLoad_request_id(), "", "", "", 0, trans);

                //this.callUpdateStatus("ERROR_INT", dataCPEInvoicesDS.getErrorInterface()[0].getReject_lookup_code(), dataCPEInvoicesDS.getErrorInterface()[0].getRejection_message(), dataCPEInvoicesDS.getErrorInterface()[0].getLoad_request_id(), "", "", "", 0, urlUpdate);
            }
            else{
                logWithTimestamp("No encontro error en la tabla de interface. Revisar reporte en OTBI");
                
                this.callUpdateStatusDB(cpeInvoiceId, "IN_PROCESS", "", "", 0, "", "", "", 0, trans);

                
            }
        }
        
        
    }
    
    private String addAttachmentInvoicePDF(ErpObjectAttachmentService svc, String entityName, String categoryName, String[] userKeys, String textString, String base64Pdf) throws ServiceException {
          String returnValue = "";

        
        if (base64Pdf != null) {
            List<AttachmentDetails> attachments = new ArrayList<AttachmentDetails>();
            AttachmentDetails attach = new AttachmentDetails();


            xxss.oracle.localizations.services.fusion.erpobjattachmentservice.types.ObjectFactory objfactory = new xxss.oracle.localizations.services.fusion.erpobjattachmentservice.types.ObjectFactory();
            
            
            logWithTimestamp("Entity Name: "+entityName);
            logWithTimestamp("Category Name: "+categoryName);
            logWithTimestamp("BU: "+userKeys[0]);
            logWithTimestamp("Invoice: "+userKeys[1]);
            logWithTimestamp("Supplier Number: "+userKeys[2]);
            logWithTimestamp("Nombre Archivo: "+textString);
           // logWithTimestamp("Base64 Archivo: "+base64Pdf);
            
            attach.setTitle(objfactory.createAttachmentDetailsTitle(textString + ".pdf"));
            attach.setAttachmentType(objfactory.createAttachmentDetailsAttachmentType("FILE"));
            attach.setUserKeyA(objfactory.createAttachmentDetailsUserKeyA(userKeys[0]));
            attach.setUserKeyB(objfactory.createAttachmentDetailsUserKeyB(userKeys[1]));
            attach.setUserKeyC(objfactory.createAttachmentDetailsUserKeyC(userKeys[2]));
            attach.setUserKeyD(objfactory.createAttachmentDetailsUserKeyD(userKeys[3]));
            attach.setUserKeyE(objfactory.createAttachmentDetailsUserKeyE(userKeys[4]));
            attach.setContent(objfactory.createAttachmentDetailsContent(base64Pdf));
            
            
            
            attachments.add(attach);
            
            returnValue = svc.uploadAttachment(entityName, categoryName, "yes", attachments);
            
            logWithTimestamp("Response Attach Service: "+returnValue);
        }
          
          return(returnValue);
      }
    
    
    public  void callUpdateStatusDB(long cpeInvoiceId,
                                    String status,
                                    String errorCode, 
                                    String errorDescription,
                                    long loadRequestId, 
                                    String errorPayload,
                                    String requestPayload,
                                    String sunatRequestPayload,
                                    long invoiceId,
                                    OracleConnection trans) {
        try {
            String st = "begin NC_CPE_CARGA_FACTURAS_PKG.PR_UPDATE_STATUS(pn_cpe_invoice_id => ?," +
                "pv_status => ?," +
                "pv_error_code => ?," +
                "pv_error_description => ?," +
                "pn_load_request_id => ?," +
                "pv_error_payload => ?," +
                "pv_request_payload => ?," +
                "pv_sunat_request_payload => ?," +
                "pn_invoice_id => ?); end;";
            
            OracleCallableStatement acs = (OracleCallableStatement) trans.prepareCall(st);

            acs.setLong(1, cpeInvoiceId);
            acs.setString(2, status);
            acs.setString(3, errorCode);
            acs.setString(4, errorDescription);
            acs.setLong(5, loadRequestId);
            acs.setString(6, errorPayload);
            acs.setString(7, requestPayload);
            acs.setString(8, sunatRequestPayload);
            acs.setLong(9, invoiceId);
        

            acs.executeUpdate();
            acs.close();

    

        } catch (SQLException exsql) {
            this.logWithTimestamp("Error SQLException: " + exsql.getMessage());
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage()+" - "+Arrays.toString(exsql.getStackTrace()));
        } catch (Exception e) {
            this.logWithTimestamp("Error Exception: " + e.getMessage());
            System.out.println("Error en readReportOutput() - " + e.getMessage()+" - "+Arrays.toString(e.getStackTrace()));
        }

    }
    
    
    public String callUpdateStatusRest(String status, 
                                    String errorCode, 
                                    String errorDescription,
                                    long   loadRequestId, 
                                    String errorPayload,
                                    String requestPayload,
                                    String sunatRequestPayload,
                                    long   invoiceId,
                                    String urlUpdate) throws MalformedURLException, IOException {

            String responseString = "";
            String outputString = "";


           URL oURL = new URL(urlUpdate);
           
            HttpURLConnection httpConn = (HttpURLConnection)oURL.openConnection();
       
           
           
            String jsonInput ="{\n" + 
            "\"status_in\" : \""+status+"\",\n" + 
            "\"errorCode\" : \""+errorCode+"\",\n" + 
            "\"errorDescription\" : \""+errorDescription+"\",\n" + 
            "\"loadRequestId\" : "+loadRequestId+",\n" + 
            "\"errorPayload\" : \""+errorPayload+"\",\n" + 
            "\"requestPayload\" : \""+requestPayload+"\",\n" + 
            "\"sunatRequestPayload\" : \""+sunatRequestPayload+"\",\n" +
            "\"invoiceApId\" : "+invoiceId+"\n" +                   
            "\n" + 
            "}";
           
            logWithTimestamp("JSON : "+jsonInput);

            byte[] buffer = new byte[jsonInput.length()];
            buffer = jsonInput.getBytes();

            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("X-HTTP-Method-Override", "PATCH");
            httpConn.setRequestProperty("Content-Length", String.valueOf(buffer.length));
            httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
           
            /*String userCredentials = usuario+":"+password;
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));      
            httpConn.setRequestProperty("Authorization", basicAuth);*/
           

            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
           

            OutputStream out = httpConn.getOutputStream();
            //Write the content of the request to the outputstream of the HTTP Connection.
            out.write(buffer);
            out.flush();
            out.close();
            //Ready with sending the request.

            //Read the response.
            //httpConn.connect();
            logWithTimestamp("Codigo respuesta servicio: "+httpConn.getResponseCode());
            if (httpConn.getResponseCode() != 200) {            
              return String.valueOf(httpConn.getResponseCode());//"Failed : HTTP Error code : " + httpConn.getResponseCode();
             }
           
           
           
            InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            //Write the SOAP message response to a String.
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }
            
            this.logWithTimestamp(outputString);
           
            in.close();
            httpConn.disconnect();  
           
            return outputString;
        }
    
}
