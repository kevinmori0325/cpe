package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class FEDataDS {
    FEEntidadLegal[] entidadLegal;
        
    public FEDataDS() {
        super();
    }

    @XmlElement(name = "G_LE")
    public void setEntidadLegal(FEEntidadLegal[] entidadLegal) {
        this.entidadLegal = entidadLegal;
    }

    public FEEntidadLegal[] getEntidadLegal() {
        return entidadLegal;
    }
}
