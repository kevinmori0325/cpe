package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CREHeader {
    long legalEntityId;
    long idPago;
    String tipoDocProv;
    String rucProv;
    String nombreProv;
    String nombreAltProv;
    String direccionProv;
    String ciudadProv;
    String distritoProv;
    String paisProv;
    String codigoPostalProv;
    String emailProv;
    String monedaPago;
    String codigoTipoMonto;
    String tasaConcepto;
    String montoBase;
    String montoConcepto;
    String checkNumber;
    Date voidDate;
    String voidReason;
    Date checkDate;
    String serieCRE;
    String numeroCRE;
    long paymentDocumentId;
    long orgId;
    long paymentReferenceNumber;
    String provinciaProv;
    CRELines[] lines;
    
    public CREHeader() {
        super();
    }

    @XmlElement(name = "LEGAL_ENTITY")
    public void setLegalEntityId(long legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public long getLegalEntityId() {
        return legalEntityId;
    }

    @XmlElement(name = "ID_PAGO")
    public void setIdPago(long idPago) {
        this.idPago = idPago;
    }

    public long getIdPago() {
        return idPago;
    }

    @XmlElement(name = "TIPO_DOC_PROV")
    public void setTipoDocProv(String tipoDocProv) {
        this.tipoDocProv = tipoDocProv;
    }

    public String getTipoDocProv() {
        return tipoDocProv;
    }

    @XmlElement(name = "RUC_PROV")
    public void setRucProv(String rucProv) {
        this.rucProv = rucProv;
    }

    public String getRucProv() {
        return rucProv;
    }

    @XmlElement(name = "NOMBRE_PROV")
    public void setNombreProv(String nombreProv) {
        this.nombreProv = nombreProv;
    }

    public String getNombreProv() {
        return nombreProv;
    }

    @XmlElement(name = "NOMBRE_ALT_PROV")
    public void setNombreAltProv(String nombreAltProv) {
        this.nombreAltProv = nombreAltProv;
    }

    public String getNombreAltProv() {
        return nombreAltProv;
    }

    @XmlElement(name = "DIRECCION_PROV")
    public void setDireccionProv(String direccionProv) {
        this.direccionProv = direccionProv;
    }

    public String getDireccionProv() {
        return direccionProv;
    }

    @XmlElement(name = "CIUDAD_PROV")
    public void setCiudadProv(String ciudadProv) {
        this.ciudadProv = ciudadProv;
    }

    public String getCiudadProv() {
        return ciudadProv;
    }

    @XmlElement(name = "DISTRITO_PROV")
    public void setDistritoProv(String distritoProv) {
        this.distritoProv = distritoProv;
    }

    public String getDistritoProv() {
        return distritoProv;
    }

    @XmlElement(name = "PAIS_PROV")
    public void setPaisProv(String paisProv) {
        this.paisProv = paisProv;
    }

    public String getPaisProv() {
        return paisProv;
    }

    @XmlElement(name = "CODIGO_POSTAL_PROV")
    public void setCodigoPostalProv(String codigoPostalProv) {
        this.codigoPostalProv = codigoPostalProv;
    }

    public String getCodigoPostalProv() {
        return codigoPostalProv;
    }

    @XmlElement(name = "EMAIL_PROV")
    public void setEmailProv(String emailProv) {
        this.emailProv = emailProv;
    }

    public String getEmailProv() {
        return emailProv;
    }

    @XmlElement(name = "MONEDA_PAGO")
    public void setMonedaPago(String monedaPago) {
        this.monedaPago = monedaPago;
    }

    public String getMonedaPago() {
        return monedaPago;
    }

    @XmlElement(name = "CODIGO_TIPO_MONTO")
    public void setCodigoTipoMonto(String codigoTipoMonto) {
        this.codigoTipoMonto = codigoTipoMonto;
    }

    public String getCodigoTipoMonto() {
        return codigoTipoMonto;
    }

    @XmlElement(name = "TASA_CONCEPTO")
    public void setTasaConcepto(String tasaConcepto) {
        this.tasaConcepto = tasaConcepto;
    }

    public String getTasaConcepto() {
        return tasaConcepto;
    }

    @XmlElement(name = "MONTO_BASE")
    public void setMontoBase(String montoBase) {
        this.montoBase = montoBase;
    }

    public String getMontoBase() {
        return montoBase;
    }

    @XmlElement(name = "MONTO_CONCEPTO")
    public void setMontoConcepto(String montoConcepto) {
        this.montoConcepto = montoConcepto;
    }

    public String getMontoConcepto() {
        return montoConcepto;
    }

    @XmlElement(name = "CHECK_NUMBER")
    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    @XmlElement(name = "G_LINES")
    public void setLines(CRELines[] lines) {
        this.lines = lines;
    }

    public CRELines[] getLines() {
        return lines;
    }

    @XmlElement(name = "PAYMENT_DOCUMENT_ID")
    public void setPaymentDocumentId(long paymentDocumentId) {
        this.paymentDocumentId = paymentDocumentId;
    }

    public long getPaymentDocumentId() {
        return paymentDocumentId;
    }

    @XmlElement(name = "ORG_ID")
    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public long getOrgId() {
        return orgId;
    }

    @XmlElement(name = "VOID_DATE")
    public void setVoidDate(Date voidDate) {
        this.voidDate = voidDate;
    }

    public Date getVoidDate() {
        return voidDate;
    }

    @XmlElement(name = "VOID_REASON")
    public void setVoidReason(String voidReason) {
        this.voidReason = voidReason;
    }

    public String getVoidReason() {
        return voidReason;
    }

    @XmlElement(name = "CHECK_DATE")
    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    @XmlElement(name = "SERIE_CR")
    public void setSerieCRE(String serieCRE) {
        this.serieCRE = serieCRE;
    }

    public String getSerieCRE() {
        return serieCRE;
    }

    @XmlElement(name = "NUMERO_CR")
    public void setNumeroCRE(String numeroCRE) {
        this.numeroCRE = numeroCRE;
    }

    public String getNumeroCRE() {
        return numeroCRE;
    }

    @XmlElement(name = "PAYMENT_REFERENCE_NUMBER")
    public void setPaymentReferenceNumber(long paymentReferenceNumber) {
        this.paymentReferenceNumber = paymentReferenceNumber;
    }

    public long getPaymentReferenceNumber() {
        return paymentReferenceNumber;
    }

    @XmlElement(name = "PROVINCIA_PROV")
    public void setProvinciaProv(String provinciaProv) {
        this.provinciaProv = provinciaProv;
    }

    public String getProvinciaProv() {
        return provinciaProv;
    }
}
