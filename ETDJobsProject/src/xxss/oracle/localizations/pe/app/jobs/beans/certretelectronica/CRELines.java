package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CRELines {
    String numeroPagoFact;
    Date fechaElabFact;
    Date fechaVencFact;
    String montoFact;
    String monedaFact;
    String tipoCambioFact;
    double montoBrutoFact;
    double montoRetFact;
    double montoNetoPagadoFact;
    String tipoDocFact;
    String serieFact;
    String numeroFact;
    String idPago;
    String invoiceId;
    String invoiceTypeLookupCode;
    Date fechaTipoCambio;
    
    public CRELines() {
        super();
    }

    @XmlElement(name = "NUMERO_PAGO_FACT")
    public void setNumeroPagoFact(String numeroPagoFact) {
        this.numeroPagoFact = numeroPagoFact;
    }

    public String getNumeroPagoFact() {
        return numeroPagoFact;
    }

    @XmlElement(name = "FECHA_ELAB_FACT")
    public void setFechaElabFact(Date fechaElabFact) {
        this.fechaElabFact = fechaElabFact;
    }

    public Date getFechaElabFact() {
        return fechaElabFact;
    }

    @XmlElement(name = "FECHA_VENC_FACT")
    public void setFechaVencFact(Date fechaVencFact) {
        this.fechaVencFact = fechaVencFact;
    }

    public Date getFechaVencFact() {
        return fechaVencFact;
    }

    @XmlElement(name = "MONTO_FACT")
    public void setMontoFact(String montoFact) {
        this.montoFact = montoFact;
    }

    public String getMontoFact() {
        return montoFact;
    }

    @XmlElement(name = "MONEDA_FACT")
    public void setMonedaFact(String monedaFact) {
        this.monedaFact = monedaFact;
    }

    public String getMonedaFact() {
        return monedaFact;
    }

    @XmlElement(name = "TIPO_CAMBIO_FACT")
    public void setTipoCambioFact(String tipoCambioFact) {
        this.tipoCambioFact = tipoCambioFact;
    }

    public String getTipoCambioFact() {
        return tipoCambioFact;
    }

    @XmlElement(name = "MONTO_BRUTO_FACT")
    public void setMontoBrutoFact(double montoBrutoFact) {
        this.montoBrutoFact = montoBrutoFact;
    }

    public double getMontoBrutoFact() {
        return montoBrutoFact;
    }

    @XmlElement(name = "MONTO_RET_FACT")
    public void setMontoRetFact(double montoRetFact) {
        this.montoRetFact = montoRetFact;
    }

    public double getMontoRetFact() {
        return montoRetFact;
    }

    @XmlElement(name = "MONTO_NETO_PAGADO_FACT")
    public void setMontoNetoPagadoFact(double montoNetoPagadoFact) {
        this.montoNetoPagadoFact = montoNetoPagadoFact;
    }

    public double getMontoNetoPagadoFact() {
        return montoNetoPagadoFact;
    }

    @XmlElement(name = "SERIE_FACT")
    public void setSerieFact(String serieFact) {
        this.serieFact = serieFact;
    }

    public String getSerieFact() {
        return serieFact;
    }

    @XmlElement(name = "NUMERO_FACT")
    public void setNumeroFact(String numeroFact) {
        this.numeroFact = numeroFact;
    }

    public String getNumeroFact() {
        return numeroFact;
    }

    @XmlElement(name = "ID_PAGO")
    public void setIdPago(String idPago) {
        this.idPago = idPago;
    }

    public String getIdPago() {
        return idPago;
    }

    @XmlElement(name = "INVOICE_ID")
    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    @XmlElement(name = "TIPO_DOC_FACT")
    public void setTipoDocFact(String tipoDocFact) {
        this.tipoDocFact = tipoDocFact;
    }

    public String getTipoDocFact() {
        return tipoDocFact;
    }

    @XmlElement(name = "INVOICE_TYPE_LOOKUP_CODE")
    public void setInvoiceTypeLookupCode(String invoiceTypeLookupCode) {
        this.invoiceTypeLookupCode = invoiceTypeLookupCode;
    }

    public String getInvoiceTypeLookupCode() {
        return invoiceTypeLookupCode;
    }

    @XmlElement(name = "FECHA_TIPO_CAMBIO")
    public void setFechaTipoCambio(Date fechaTipoCambio) {
        this.fechaTipoCambio = fechaTipoCambio;
    }

    public Date getFechaTipoCambio() {
        return fechaTipoCambio;
    }
}
