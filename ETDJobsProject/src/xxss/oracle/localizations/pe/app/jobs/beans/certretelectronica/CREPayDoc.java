package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

import javax.xml.bind.annotation.XmlElement;

public class CREPayDoc {
    long paymentDocumentId;
    String paymentDocumentName;
    long buId;
    
    public CREPayDoc() {
        super();
    }

    @XmlElement(name = "PAYMENT_DOCUMENT_ID")
    public void setPaymentDocumentId(long paymentDocumentId) {
        this.paymentDocumentId = paymentDocumentId;
    }

    public long getPaymentDocumentId() {
        return paymentDocumentId;
    }

    @XmlElement(name = "PAYMENT_DOCUMENT_NAME")
    public void setPaymentDocumentName(String paymentDocumentName) {
        this.paymentDocumentName = paymentDocumentName;
    }

    public String getPaymentDocumentName() {
        return paymentDocumentName;
    }

    @XmlElement(name = "ORG_ID")
    public void setBuId(long buId) {
        this.buId = buId;
    }

    public long getBuId() {
        return buId;
    }
}
