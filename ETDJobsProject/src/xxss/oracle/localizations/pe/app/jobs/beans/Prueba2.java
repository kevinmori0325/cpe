package xxss.oracle.localizations.pe.app.jobs.beans;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import org.apache.commons.io.FilenameUtils;

import org.quartz.JobDataMap;

import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.EmailsAttach;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesCompressed;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesXml;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesPdf;
import xxss.oracle.localizations.pe.app.jobs.beans.zipattached.FilesOther;

public class Prueba2 {

    private static String fileExtrName = "";
    private static final String EZIP = ".ZIP";
    private static final String ERAR = ".RAR";
    private static final String E7Z = ".7Z";
    private static final String EXML = ".XML";
    private static final String EPDF = ".PDF";
    private static final String EOT = "other";


    private static ArrayList<FilesXml> filesXmlList = new ArrayList<FilesXml>();
    private static ArrayList<FilesPdf> filesPdfList = new ArrayList<FilesPdf>();
    private static ArrayList<FilesOther> filesOtherList = new ArrayList<FilesOther>();


    public Prueba2() {
        super();
    }

    public static void log(String msg) {
        System.out.println(msg);
    }

    public static void main(String[] args) {
        try {
            doAction(null, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void doAction(JobDataMap dataMap, OracleConnection conn) throws SQLException {
        //Parametros
        String pathDirectory = "C:/Users/julio/Downloads/Nueva carpeta/";
        String imapHost = "imap.gmail.com";
        String imapPort = "993";
        String mailStoreType = "imap";
        String username = "julioludenadev@gmail.com";
        String password = "Oracle2020";
        // Method: Get unread email and unzip attachment
        getUnreadEmailUnzipAttachment(conn, pathDirectory, imapHost, imapPort, mailStoreType, username, password);
    }

    private static void getUnreadEmailUnzipAttachment(OracleConnection conn, String pathDirectory, String imapHost,
                                                      String imapPort, String mailStoreType, String username,
                                                      String password) {
        // Create properties
        Properties properties = new Properties();
        properties.put("mail.imap.host", imapHost);
        properties.put("mail.imap.port", imapPort);
        properties.put("mail.imap.starttls.enable", "true");
        properties.put("mail.imap.ssl.trust", imapHost);

        // Email Session
        Session emailSession = Session.getDefaultInstance(properties);
        // Enable debug
        //emailSession.setDebug(true);
        //
        JSch jschSSHChannel = new JSch();
        com.jcraft.jsch.Session sesConnection = null;
        //
        // Create the imap store object and connect to the imap server
        try {
            String destFilePath = pathDirectory;
            String extFilePath = pathDirectory;
            Store store = emailSession.getStore("imaps");
            store.connect(imapHost, username, password);
            // Create the inbox object and open it
            Folder inbox = store.getFolder("Inbox");
            inbox.open(Folder.READ_WRITE);
            // Retrieve the messages from the folder in an array and print it. Set False to retrieve Unread Mail
            Message[] messages = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));
            log("Unread Mails" + messages.length);

            //
            sesConnection = jschSSHChannel.getSession("usercpe", "168.138.235.72", 22);
            sesConnection.setPassword("U$ercp3123");
            // UNCOMMENT THIS FOR TESTING PURPOSES, BUT DO NOT USE IN PRODUCTION
            sesConnection.setConfig("StrictHostKeyChecking", "no");
            sesConnection.connect(60000);

            System.out.println("Conecto SFTP exitosamente");

            Channel channel = sesConnection.openChannel("sftp");
            channel.connect();
            
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            String pathUploadSfpt = "/u01/CPE/";
            sftpChannel.cd(pathUploadSfpt);
            
            //

            for (int i = 0, n = messages.length; i < n; i++) {
                String emsg = "";
                int emailNum = (i + 1);
                String emailNumTxt = "" + emailNum;
                int at = 0;
                Message message = messages[i];
                // Set email as read. It's not necessary
                // message.setFlag(Flag.SEEN, true);
                log("---------------------------------");
                log("Email Number " + (emailNum));
                log("Subject: " + message.getSubject());
                log("From: " + message.getFrom()[0]);
                log("Text: " + message.getContent().toString());
                log("ContentType: " + message.getContentType());
                log("---------------------------------");
                // Email Parts
                Multipart multiPart = (Multipart) message.getContent();
                /*
                if((multiPart.getCount()-1) == 1){
                    for (int j = 0; j < multiPart.getCount(); j++) {
                        MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(j);
                        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                            log("Found Attachment");
                            log("Nombre Adjunto original"+part.getFileName());

                            // Save InputStream from Attachment
                            InputStream input = part.getInputStream();


                            if(part.getFileName().contains(EZIP)){
                                readZip(input, conn);// Se validaria en la bd los archivos que contiene
                            }else if(part.getFileName().contains(ERAR)){


                            }else if(part.getFileName().contains(E7Z)){



                            }else if(part.getFileName().contains(EXML)){

                            } else {
                                emsg = "No se carga - No tiene archivo XML o comprimido (ZIP, RAR o 7z)";
                                break;
                            }

                        }
                    }

                }else if((multiPart.getCount()-1) > 1){

                }*/
                // Guardar los adjuntos
                EmailsAttach emailAttach = new EmailsAttach();
                emailAttach.setEmailNum(emailNumTxt);
                ArrayList<FilesCompressed> filesCompressedList = new ArrayList<FilesCompressed>();
                ArrayList<FilesXml> filesXmlList = new ArrayList<FilesXml>();
                ArrayList<FilesPdf> filesPdfList = new ArrayList<FilesPdf>();
                ArrayList<FilesOther> filesOtherList = new ArrayList<FilesOther>();

                for (int j = 0; j < multiPart.getCount(); j++) {
                    MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(j);
                    if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                        FilesCompressed filesCompressed = new FilesCompressed();
                        FilesXml filesXml = new FilesXml();
                        FilesPdf filesPdf = new FilesPdf();
                        FilesOther filesOther = new FilesOther();
                        String fileName = part.getFileName();
                        log("Found Attachment");
                        log("Nombre Adjunto original" + fileName);
                        
                        String extension = FilenameUtils.getExtension(fileName);

                        // Save InputStream from Attachment
                        InputStream input = part.getInputStream();
                    
                        
                        switch (extension.toUpperCase()) {
                        case "ZIP":
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(extension.toUpperCase()); //EZIP
                            filesCompressed.setFileCompressed(input);
                            filesCompressedList.add(filesCompressed);
                            
                            break;
                        case "RAR":
                            log("Entro if Rar");
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(extension.toUpperCase()); //ERAR
                            filesCompressed.setFileCompressed(input);
                            //Guardar archivos adjuntos
                            try {
                                descomprimirRar(input, pathUploadSfpt, fileName, sftpChannel, sesConnection);
                                filesCompressed.setFilesXml(filesXmlList);
                                filesCompressed.setFilesPdf(filesPdfList);
                                filesCompressed.setFilesOther(filesOtherList);
                                filesCompressedList.add(filesCompressed);
                            } catch (SftpException e) {
                                // Controlar error. No debe guardarlo
                                log("Entro Exception: "+e.getMessage()+ Arrays.toString(e.getStackTrace()));
                            }
                            break;
                        case "7Z":
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(extension.toUpperCase()); //E7Z
                            filesCompressed.setFileCompressed(input);
                            //Guardar archivos adjuntos
                            try {
                                descomprimir7z(input, pathUploadSfpt, fileName, sftpChannel, sesConnection);
                                filesCompressed.setFilesXml(filesXmlList);
                                filesCompressed.setFilesPdf(filesPdfList);
                                filesCompressed.setFilesOther(filesOtherList);
                                filesCompressedList.add(filesCompressed);
                            } catch (SftpException e) {
                                // Controlar error. No debe guardarlo
                            }
                            break;
                        case "XML":
                            filesXml.setFileName(fileName);
                            filesXml.setFileExt(extension.toUpperCase()); //EXML
                            filesXml.setFileXml(input);
                            filesXmlList.add(filesXml);
                            break;
                        case "PDF":
                            filesPdf.setFileName(fileName);
                            filesPdf.setFileExt(extension.toUpperCase()); //EPDF
                            filesPdf.setFilePdf(input);
                            filesPdfList.add(filesPdf);
                            break;
                        default:
                            filesOther.setFileName(fileName);
                            filesOther.setFileExt(EOT);
                            filesOtherList.add(filesOther);
                        }

                       /* if (part.getFileName()
                                .toUpperCase()
                                .contains(EZIP)) {
                            filesCompressed.setFileName(fileName);
                            filesCompressed.setFileExt(EZIP);
                            filesCompressed.setFileCompressed(input);
                            filesCompressedList.add(filesCompressed);
                        } else {
                            if (part.getFileName()
                                    .toUpperCase()
                                    .contains(ERAR)) {
                                
                                log("Entro if Rar");
                                filesCompressed.setFileName(fileName);
                                filesCompressed.setFileExt(ERAR);
                                filesCompressed.setFileCompressed(input);
                                //Guardar archivos adjuntos
                                try {
                                    descomprimirRar(input, pathUploadSfpt, fileName, sftpChannel, sesConnection);
                                    filesCompressed.setFilesXml(filesXmlList);
                                    filesCompressed.setFilesPdf(filesPdfList);
                                    filesCompressed.setFilesOther(filesOtherList);
                                    filesCompressedList.add(filesCompressed);
                                } catch (SftpException e) {
                                    // Controlar error. No debe guardarlo
                                    log("Entro Exception: "+e.getMessage()+ Arrays.toString(e.getStackTrace()));
                                }
                            } else {
                                if (part.getFileName()
                                        .toUpperCase()
                                        .contains(E7Z)) {
                                    filesCompressed.setFileName(fileName);
                                    filesCompressed.setFileExt(E7Z);
                                    filesCompressed.setFileCompressed(input);
                                    //Guardar archivos adjuntos
                                    try {
                                        descomprimir7z(input, pathUploadSfpt, fileName, sftpChannel, sesConnection);
                                        filesCompressed.setFilesXml(filesXmlList);
                                        filesCompressed.setFilesPdf(filesPdfList);
                                        filesCompressed.setFilesOther(filesOtherList);
                                        filesCompressedList.add(filesCompressed);
                                    } catch (SftpException e) {
                                        // Controlar error. No debe guardarlo
                                    }
                                } else {
                                    if (part.getFileName()
                                            .toUpperCase()
                                            .contains(EXML)) {
                                        filesXml.setFileName(fileName);
                                        filesXml.setFileExt(EXML);
                                        filesXml.setFileXml(input);
                                        filesXmlList.add(filesXml);
                                    } else {
                                        if (part.getFileName()
                                                .toUpperCase()
                                                .contains(EPDF)) {
                                            filesPdf.setFileName(fileName);
                                            filesPdf.setFileExt(EPDF);
                                            filesPdf.setFilePdf(input);
                                            filesPdfList.add(filesPdf);
                                        } else {
                                            filesOther.setFileName(fileName);
                                            filesOther.setFileExt(EOT);
                                            filesOtherList.add(filesOther);
                                        }
                                    }
                                }
                            }
                        } 
                        */

                    }
                    
                    //AQUI VA PROCE PARA ELIMINAR ARCHIVO DE SFTP
                    
                }
                emailAttach.setFileCompressed(filesCompressedList);
                emailAttach.setFilesXml(filesXmlList);
                emailAttach.setFilesPdf(filesPdfList);
                emailAttach.setFilesOther(filesOtherList);
                
                sftpChannel.exit();
                sftpChannel.disconnect();
                
                sesConnection.disconnect();
                channel.disconnect();
                // ELIMINAR TODOS LOS ARCHIVOS DEL SERVIDOR
                //=====================================ESPACIO PARA EL METODO==========================//
                /* RECORRER LOS ARCHIVOS EN LAS CLASES 
                 * SE PODRA REALIZAR COUNT DE LOS ARREGLOS SEGUN EL TIPO DE ARCHIVO
                 * Y ASI REALIZAR LAS VALIDACIONES CORRECTAMENTE 
                 * Y SEGUN LA VALIDACION SE IRA ENVIANDO O NO, EL XML Y PDF*/
                log("N�mero de comprimidos adjuntos"+emailAttach.getFileCompressed().size());
                
                /*switch (emailAttach.getFileCompressed().size()){
                case 1:
                    
                        
                        switch ( ){
                        case "ZIP":
                            
                            
                            break;
                        case "RAR":
                            
                            break;
                        case "7Z":
                            
                            break;
                        case "XML":
                            
                            break;
                        case "PDF":
                            
                            break;
                        default:
                            
                        }
                
                    
            
                break;
                case 2:
                ;
                break;
                default:
                ;
                }
                
               */
                
                
                
                
                //=====================================ESPACIO PARA EL METODO==========================//
                System.out.println("Mensaje: " + emsg);

            }
        } catch (NoSuchProviderException e) {
            log("Error-NoSuchProviderException...");
            e.printStackTrace();
        } catch (MessagingException e) {
            log("Error-MessagingException..." + e.getMessage() + " - " + Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        } catch (IOException e) {
            log("Error-IOException...");
            e.printStackTrace();
        } catch (JSchException e) {
        } catch (SftpException e) {
        }
    }

 /*   private static ArrayList<ArchivoCorreo> extractOnlyFile(String path) throws IOException {
        ArchivoCorreo arcCor = new ArchivoCorreo();
        ArrayList<ArchivoCorreo> arcCorList = new ArrayList<ArchivoCorreo>();
        ZipFile zf;
        zf = new ZipFile(path);
        Enumeration e = zf.entries();
        while (e.hasMoreElements()) {
            arcCor = new ArchivoCorreo();
            // Your only file
            ZipEntry entry = (ZipEntry) e.nextElement();
            // Get File Name
            arcCor.setFilename(entry.getName());
            arcCor.setFileInputStream(zf.getInputStream(entry));
            arcCorList.add(arcCor);
        }
        return arcCorList;
    }*/

    private static void savePathFileNameExecuted(OracleConnection conn, String pathFileName) {
        // Save in a table the pathFileName to remove it later






    }

  /*  private static void descomprimirZip(String destFilePath, String extFilePath, String pathDirectory, byte[] buffer,
                                        int byteRead) throws IOException {
        ArchivoCorreo arcCor = new ArchivoCorreo();
        ArrayList<ArchivoCorreo> arcCorList = new ArrayList<ArchivoCorreo>();
        // Get InputStream from the File zipped
        arcCorList = extractOnlyFile(destFilePath);
        Iterator<ArchivoCorreo> itrArcCor = arcCorList.iterator();
        while (itrArcCor.hasNext()) {
            arcCor = new ArchivoCorreo();
            arcCor = itrArcCor.next();
            // Save an extracted file from a Zip to a file (Optional)
            if (1 == 1) { //Disabled
                extFilePath = pathDirectory;
                extFilePath += arcCor.getFilename();
                FileOutputStream outputEx = new FileOutputStream(extFilePath);
                byteRead = 0;
                while ((byteRead = arcCor.getFileInputStream().read(buffer)) != -1) {
                    outputEx.write(buffer, 0, byteRead);
                }
                outputEx.close();
            }
        }
    }
*/
    private static void descomprimirRarWin(String destFilePath, String extFilePath, String pathDirectory, byte[] buffer,
                                           int byteRead) throws IOException {

        Process p;
        String execcomand = "";
        execcomand = "cmd /c mkdir \"C:/Users/julio/Downloads/Nueva carpeta/Test\"";
        System.out.println(execcomand);
        p = Runtime.getRuntime().exec(execcomand);
        execcomand =
            "\"C:\\Program Files\\WinRAR\\unrar\" e \"C:\\Users\\julio\\Downloads\\Nueva carpeta\\F011-00008.rar\" \"C:\\Users\\julio\\Downloads\\Nueva carpeta\\Test\"";
        System.out.println(execcomand);
        p = Runtime.getRuntime().exec(execcomand);
        execcomand = "cmd /c rmdir \"C:/Users/julio/Downloads/Nueva carpeta/Test\"";
        System.out.println(execcomand);
        p = Runtime.getRuntime().exec(execcomand);
    }

    public static void readZip(InputStream reportIs, OracleConnection trans) {
        try {
            String st = "begin NC_CPE_CARGA_BLOB_PKG.PR_CARGAR(pv_nombre => ?, pb_archivo => ?); end;";
            OracleCallableStatement acs = (OracleCallableStatement) trans.prepareCall(st);

            acs.setString(1, "F_Prueba.zip");
            acs.setBlob(2, reportIs);
            //acs.setNUMBER(2, idAttachment);
            //acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
            //acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            //String procStatus = acs.getString(3);
            //String procMessage = acs.getString(4);

            /*if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }*/

        } catch (SQLException exsql) {
            /*status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();*/
            log("Error SQL en readReportOutput(): " + exsql.getMessage());
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage());
        } catch (Exception e) {
            /*status = ERROR;
            message =
                    "Error en XxssPeLocAgenteRetProcessAttach: " + e.getMessage();*/
            log("Error en readReportOutput(): " + e.getMessage());
            System.out.println("Error en readReportOutput() - " + e.getMessage());
        }

    }


    public static InputStream readFileFromInputStream(ChannelSftp sftpChannel, String rutaNombre) throws SftpException {

        InputStream stream = sftpChannel.get(rutaNombre); //"/upload/XML/pruebaXml3.xml"


        return stream;
    }

    public static boolean uploadSftpFromInputStream(InputStream localFile, String sftpFile,
                                                    ChannelSftp channelSftp) throws SftpException {

        //channelSftp.cd("");
        if (localFile != null){
        System.out.println(""+sftpFile);
        channelSftp.put(localFile, sftpFile);
        System.out.println("Upload Complete");
        }
        //channelSftp.exit();
        return true;
    }

    public static String sendCommand(String command, com.jcraft.jsch.Session sesConnection) {
        StringBuilder outputBuffer = new StringBuilder();

        try {
            ChannelExec channel = (ChannelExec) sesConnection.openChannel("exec");
            channel.setCommand(command);
            //Channel channel = sesConnection.openChannel("sftp");
            //((ChannelExec)channel).setCommand(command);
            channel.setInputStream(null);
            //((ChannelExec)channel).setErrStream(System.err);


            InputStream commandOutput = channel.getInputStream();
            channel.connect();
            int readByte = commandOutput.read();

            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }

            channel.disconnect();
        } catch (IOException ioX) {
            //logWarning(ioX.getMessage());
            return null;
        } catch (JSchException jschX) {
            //logWarning(jschX.getMessage());
            return null;
        }

        return outputBuffer.toString();
    }

    private static void descomprimir7z(InputStream localFile, String pathUploadSFTP, String fileNameRar,
                                       ChannelSftp sftpChannel,
                                       com.jcraft.jsch.Session sesConnection) throws IOException, SftpException {
        FilesXml filesXml = new FilesXml();
        FilesPdf filesPdf = new FilesPdf();
        FilesOther filesOther = new FilesOther();
        String outputCommand = "";

        uploadSftpFromInputStream(localFile, pathUploadSFTP + fileNameRar, sftpChannel);
        // Enviar comando para descomprimir
           //outputCommand = sendCommand("unrar", sesConnection);
        // Enviar comando para listar
           //outputCommand = sendCommand("ls", sesConnection);
        // Con la lista de nombre de archivos se debe verificar que extension es y segun esto ir agregando
        //INICIO DE LOOP DE NOMBRES
            /*switch ("XML") {
            case "XML":
                filesXml.setFileName("archivo.xml");
                filesXml.setFileExt(EXML);
                filesXml.setFileXml(readFileFromInputStream(sftpChannel, filesXml.getFileName()));
                filesXmlList.add(filesXml);
                break;
            case "PDF":
                filesPdf.setFileName("archivo.pdf");
                filesPdf.setFileExt(EXML);
                filesPdf.setFilePdf(readFileFromInputStream(sftpChannel, filesPdf.getFileName()));
                filesPdfList.add(filesPdf);
                break;
            default:
                filesOther.setFileName("archivo.jpg");
                filesOther.setFileExt(EOT);
                filesOtherList.add(filesOther);
            }*/
        //FIN DE LOOP DE NOMBRES
        sftpChannel.exit();
        filesOtherList.add(filesOther);
    }

    private static void descomprimirRar(InputStream localFile, String pathUploadSFTP, String fileNameRar,
                                        ChannelSftp sftpChannel,
                                        com.jcraft.jsch.Session sesConnection) throws IOException, SftpException {
        FilesXml filesXml = new FilesXml();
        FilesPdf filesPdf = new FilesPdf();
        FilesOther filesOther = new FilesOther();
        String outputCommand = "";
        
        log("ruta sftp: "+pathUploadSFTP + fileNameRar);
        //log("unar "+fileNameRar);
        //Cargar archivos del correo a servidor sftp
         uploadSftpFromInputStream(localFile, pathUploadSFTP + fileNameRar, sftpChannel);
        //Colocarse en el directorio donde estan los archivos del correo
         String comando = "cd "+pathUploadSFTP+"\n"+
                          "unar "+fileNameRar+"\n";
        //log("command0: "+outputCommand);
        //Descomprimir rar
           outputCommand = sendCommand(comando, sesConnection);
        log("command1: "+outputCommand);
        //Eliminar comprimido
          //sftpChannel.rm(pathUploadSFTP + fileNameRar);
        // Enviar comando para listar
           outputCommand = sendCommand("ls "+pathUploadSFTP, sesConnection);
           String[] loopFile  = outputCommand.split("\n");
           List<String> containerFiles = Arrays.asList(loopFile);
           
        log("size: "+containerFiles.size());
        log("command2: "+outputCommand);
        // Con la lista de nombre de archivos se debe verificar que extension es y segun esto ir agregando
        // INICIO DE LOOP DE ARCHIVOS
        
        for(int i = 0; i < containerFiles.size(); i++){
         String nombreArchivo = loopFile[i];
            log("name: "+nombreArchivo);
            
            String extension = FilenameUtils.getExtension(nombreArchivo);
            log("extension: "+extension);
            
            switch (extension.toUpperCase()) {
            case "XML":
                filesXml.setFileName(nombreArchivo);
                filesXml.setFileExt(extension.toUpperCase());
                //filesXml.setFileXml(readFileFromInputStream(sftpChannel, filesXml.getFileName()));
                filesXmlList.add(filesXml);
                break;
            case "PDF":
                filesPdf.setFileName(nombreArchivo);
                filesPdf.setFileExt(extension.toUpperCase());
                filesPdf.setFilePdf(readFileFromInputStream(sftpChannel, filesPdf.getFileName()));
                filesPdfList.add(filesPdf);
                break;
            default:
                filesOther.setFileName("archivo.jpg");
                filesOther.setFileExt(EOT);
                filesOtherList.add(filesOther);
            }
            
        }
        // FIN LOOP DE ARCHIVOS
        // CERRAR CONEXIONES USADAS ACA, NO LA DEL PARAMETRO
        //sftpChannel.exit();
        //sftpChannel.disconnect();
    }
}
