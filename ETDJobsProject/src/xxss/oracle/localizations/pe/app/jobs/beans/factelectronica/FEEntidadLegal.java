package xxss.oracle.localizations.pe.app.jobs.beans.factelectronica;

import javax.xml.bind.annotation.XmlElement;

public class FEEntidadLegal {
    String registeredName;
    String registrationId;
    String registrationNumber;
    //String partyId;
    String country;
    String address1;
    String juridictionCode;
    String registrationCodeLE;
    String state;
    String legalEntityId;
    String postalCode;
    String city;
    String province;
    String tipoIdent;
    FECabecera[] cabecera;
    
    public FEEntidadLegal() {
        super();
    }

    @XmlElement(name = "REGISTERED_NAME")
    public void setRegisteredName(String registeredName) {
        this.registeredName = registeredName;
    }

    public String getRegisteredName() {
        return registeredName;
    }

    @XmlElement(name = "REGISTRATION_ID")
    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    @XmlElement(name = "REGISTRATION_NUMBER")
    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @XmlElement(name = "COUNTRY")
    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    @XmlElement(name = "ADDRESS1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1() {
        return address1;
    }

    @XmlElement(name = "JURISDICTION_CODE")
    public void setJuridictionCode(String juridictionCode) {
        this.juridictionCode = juridictionCode;
    }

    public String getJuridictionCode() {
        return juridictionCode;
    }

    @XmlElement(name = "REGISTRATION_CODE_LE")
    public void setRegistrationCodeLE(String registrationCodeLE) {
        this.registrationCodeLE = registrationCodeLE;
    }

    public String getRegistrationCodeLE() {
        return registrationCodeLE;
    }

    @XmlElement(name = "STATE")
    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    @XmlElement(name = "LEGAL_ENTITY_ID")
    public void setLegalEntityId(String legalEntityId) {
        this.legalEntityId = legalEntityId;
    }

    public String getLegalEntityId() {
        return legalEntityId;
    }

    @XmlElement(name = "G_CAB")
    public void setCabecera(FECabecera[] cabecera) {
        this.cabecera = cabecera;
    }

    public FECabecera[] getCabecera() {
        return cabecera;
    }

    @XmlElement(name = "POSTAL_CODE")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    @XmlElement(name = "CITY")
    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    @XmlElement(name = "PROVINCE")
    public void setProvince(String province) {
        this.province = province;
    }

    public String getProvince() {
        return province;
    }

    @XmlElement(name = "TIPO_IDENT")
    public void setTipoIdent(String tipoIdent) {
        this.tipoIdent = tipoIdent;
    }

    public String getTipoIdent() {
        return tipoIdent;
    }
}
