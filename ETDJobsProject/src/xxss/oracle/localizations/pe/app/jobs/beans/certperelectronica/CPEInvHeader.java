package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CPEInvHeader {
    long customerTrxId;
    Date fechaEmisionInv;
    Date fechaVctoInv;
    String docSerie;
    String trxNumber;
    String tipoComprobantePago;
    String tipoDocIdentCli;
    String nroDocIdentCli;
    String nombreCli;
    String moneda;
    String tipoCambio;
    String countryCli;
    String address1Cli;
    String stateCli;
    String postalCodeCli;
    String emailAddress;
    String batchSourceName;
    String transactionSetName;
    String custTrxTypeName;
    String adjustComments;
    Date adjustApplyDate;
    String adjustmentId;
    String businessUnit;
    CPEInvLines[] linea;
    
    public CPEInvHeader() {
        super();
    }

    @XmlElement(name = "CUSTOMER_TRX_ID")
    public void setCustomerTrxId(long customerTrxId) {
        this.customerTrxId = customerTrxId;
    }

    public long getCustomerTrxId() {
        return customerTrxId;
    }

    @XmlElement(name = "FECHA_EMISION_INV")
    public void setFechaEmisionInv(Date fechaEmisionInv) {
        this.fechaEmisionInv = fechaEmisionInv;
    }

    public Date getFechaEmisionInv() {
        return fechaEmisionInv;
    }

    @XmlElement(name = "FECHA_VCTO_INV")
    public void setFechaVctoInv(Date fechaVctoInv) {
        this.fechaVctoInv = fechaVctoInv;
    }

    public Date getFechaVctoInv() {
        return fechaVctoInv;
    }

    @XmlElement(name = "DOC_SERIE")
    public void setDocSerie(String docSerie) {
        this.docSerie = docSerie;
    }

    public String getDocSerie() {
        return docSerie;
    }

    @XmlElement(name = "TRX_NUMBER")
    public void setTrxNumber(String trxNumber) {
        this.trxNumber = trxNumber;
    }

    public String getTrxNumber() {
        return trxNumber;
    }

    @XmlElement(name = "TIPO_COMPROBANTE_PAGO")
    public void setTipoComprobantePago(String tipoComprobantePago) {
        this.tipoComprobantePago = tipoComprobantePago;
    }

    public String getTipoComprobantePago() {
        return tipoComprobantePago;
    }

    @XmlElement(name = "TIPO_DOC_IDENT_CLI")
    public void setTipoDocIdentCli(String tipoDocIdentCli) {
        this.tipoDocIdentCli = tipoDocIdentCli;
    }

    public String getTipoDocIdentCli() {
        return tipoDocIdentCli;
    }

    @XmlElement(name = "NRO_DOC_IDENT_CLI")
    public void setNroDocIdentCli(String nroDocIdentCli) {
        this.nroDocIdentCli = nroDocIdentCli;
    }

    public String getNroDocIdentCli() {
        return nroDocIdentCli;
    }

    @XmlElement(name = "NOMBRE_CLI")
    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    @XmlElement(name = "CODE_MONEDA")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    @XmlElement(name = "TIPO_CAMBIO")
    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    @XmlElement(name = "COUNTRY_CLI")
    public void setCountryCli(String countryCli) {
        this.countryCli = countryCli;
    }

    public String getCountryCli() {
        return countryCli;
    }

    @XmlElement(name = "ADDRESS1_CLI")
    public void setAddress1Cli(String address1Cli) {
        this.address1Cli = address1Cli;
    }

    public String getAddress1Cli() {
        return address1Cli;
    }

    @XmlElement(name = "STATE_CLI")
    public void setStateCli(String stateCli) {
        this.stateCli = stateCli;
    }

    public String getStateCli() {
        return stateCli;
    }

    @XmlElement(name = "EMAIL_ADDRESS")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    @XmlElement(name = "BATCH_SOURCE_NAME")
    public void setBatchSourceName(String batchSourceName) {
        this.batchSourceName = batchSourceName;
    }

    public String getBatchSourceName() {
        return batchSourceName;
    }

    @XmlElement(name = "TRANSACTION_SET_NAME")
    public void setTransactionSetName(String transactionSetName) {
        this.transactionSetName = transactionSetName;
    }

    public String getTransactionSetName() {
        return transactionSetName;
    }
    
    @XmlElement(name = "CUST_TRX_TYPE_NAME")
    public void setCustTrxTypeName(String custTrxTypeName) {
        this.custTrxTypeName = custTrxTypeName;
    }

    public String getCustTrxTypeName() {
        return custTrxTypeName;
    }

    @XmlElement(name = "COMMENTS")
    public void setAdjustComments(String adjustComments) {
        this.adjustComments = adjustComments;
    }

    public String getAdjustComments() {
        return adjustComments;
    }

    @XmlElement(name = "APPLY_DATE")
    public void setAdjustApplyDate(Date adjustApplyDate) {
        this.adjustApplyDate = adjustApplyDate;
    }

    public Date getAdjustApplyDate() {
        return adjustApplyDate;
    }

    @XmlElement(name = "ADJUSTMENT_ID")
    public void setAdjustmentId(String adjustmentId) {
        this.adjustmentId = adjustmentId;
    }

    public String getAdjustmentId() {
        return adjustmentId;
    }

    @XmlElement(name = "BUSINESS_UNIT")
    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    @XmlElement(name = "G_DET")
    public void setLinea(CPEInvLines[] linea) {
        this.linea = linea;
    }

    public CPEInvLines[] getLinea() {
        return linea;
    }

    @XmlElement(name = "POSTAL_CODE_CLI")
    public void setPostalCodeCli(String postalCodeCli) {
        this.postalCodeCli = postalCodeCli;
    }

    public String getPostalCodeCli() {
        return postalCodeCli;
    }
}
