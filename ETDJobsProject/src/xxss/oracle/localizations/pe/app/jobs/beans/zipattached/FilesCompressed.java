package xxss.oracle.localizations.pe.app.jobs.beans.zipattached;

import java.io.InputStream;

import java.util.ArrayList;

public class FilesCompressed {
    String fileName;
    String fileExt;
    InputStream fileCompressed;
    ArrayList<FilesXml> filesXml;
    ArrayList<FilesPdf> filesPdf;
    ArrayList<FilesOther> filesOther;
    public FilesCompressed(){
        super();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileExt(String fileExt) {
        this.fileExt = fileExt;
    }

    public String getFileExt() {
        return fileExt;
    }

    public void setFilesXml(ArrayList<FilesXml> filesXml) {
        this.filesXml = filesXml;
    }

    public ArrayList<FilesXml> getFilesXml() {
        return filesXml;
    }

    public void setFilesPdf(ArrayList<FilesPdf> filesPdf) {
        this.filesPdf = filesPdf;
    }

    public ArrayList<FilesPdf> getFilesPdf() {
        return filesPdf;
    }

    public void setFilesOther(ArrayList<FilesOther> filesOther) {
        this.filesOther = filesOther;
    }

    public ArrayList<FilesOther> getFilesOther() {
        return filesOther;
    }

    public void setFileCompressed(InputStream fileCompressed) {
        this.fileCompressed = fileCompressed;
    }

    public InputStream getFileCompressed() {
        return fileCompressed;
    }
}
