package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class CREDataDS {
    CRELegalEntity[] legalEntities;
    
    public CREDataDS() {
        super();
    }

    @XmlElement(name = "G_LEGAL_ENTITY")
    public void setLegalEntities(CRELegalEntity[] legalEntities) {
        this.legalEntities = legalEntities;
    }

    public CRELegalEntity[] getLegalEntities() {
        return legalEntities;
    }
}
