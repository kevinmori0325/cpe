package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;

public class GRECarrierContacts {
    long partyId;
    String contactFirstName;
    String contactLastName;
    String contactEmail;
    String contactPhone;
    
    public GRECarrierContacts() {
        super();
        contactFirstName = "";
        contactLastName = "";
        contactEmail = "";
        contactPhone = "";
    }

    @XmlElement(name = "PARTY_ID")
    public void setPartyId(long partyId) {
        this.partyId = partyId;
    }

    public long getPartyId() {
        return partyId;
    }

    @XmlElement(name = "CONTACT_FIRST_NAME")
    public void setContactFirstName(String contactFirstName) {
        this.contactFirstName = contactFirstName;
    }

    public String getContactFirstName() {
        return contactFirstName;
    }

    @XmlElement(name = "CONTACT_LAST_NAME")
    public void setContactLastName(String contactLastName) {
        this.contactLastName = contactLastName;
    }

    public String getContactLastName() {
        return contactLastName;
    }

    @XmlElement(name = "CONTACT_EMAIL")
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    @XmlElement(name = "CONTACT_PHONE_NUMBER")
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactPhone() {
        return contactPhone;
    }
}
