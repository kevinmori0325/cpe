package xxss.oracle.localizations.pe.app.jobs.beans.common;

public class ValidationStatusBean {
    String status;
    String message;
    
    public ValidationStatusBean() {
        super();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
