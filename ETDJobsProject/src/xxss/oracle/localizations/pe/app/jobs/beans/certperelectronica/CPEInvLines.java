package xxss.oracle.localizations.pe.app.jobs.beans.certperelectronica;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;

public class CPEInvLines {
    long lineNumber;
    double extendedAmount;
    long appliedCustomerTrxId;
    long cashReceiptId;
    
    public CPEInvLines() {
        super();
    }

    @XmlElement(name = "LINE_NUMBER")
    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    @XmlElement(name = "EXTENDED_AMOUNT")
    public void setExtendedAmount(double extendedAmount) {
        this.extendedAmount = extendedAmount;
    }

    public double getExtendedAmount() {
        return extendedAmount;
    }

    @XmlElement(name = "APPLIED_CUSTOMER_TRX_ID")
    public void setAppliedCustomerTrxId(long appliedCustomerTrxId) {
        this.appliedCustomerTrxId = appliedCustomerTrxId;
    }

    public long getAppliedCustomerTrxId() {
        return appliedCustomerTrxId;
    }

    @XmlElement(name = "CASH_RECEIPT_ID")
    public void setCashReceiptId(long cashReceiptId) {
        this.cashReceiptId = cashReceiptId;
    }

    public long getCashReceiptId() {
        return cashReceiptId;
    }
}
