package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class GREMiscDataDS {
    GREInvTransactionType[] invTrxTypes;
    SunatDocType[] sunatDocTypes;
    SunatDocIdType[] sunatDocIdTypes;
    GRECarriers[] carriers;    
        
    public GREMiscDataDS() {
        super();
    }

    @XmlElement(name = "G_TRX_TYPES")
    public void setInvTrxTypes(GREInvTransactionType[] invTrxTypes) {
        this.invTrxTypes = invTrxTypes;
    }

    public GREInvTransactionType[] getInvTrxTypes() {
        return invTrxTypes;
    }

    @XmlElement(name = "G_SUNAT_DOC_TYPES")
    public void setSunatDocTypes(SunatDocType[] sunatDocTypes) {
        this.sunatDocTypes = sunatDocTypes;
    }

    public SunatDocType[] getSunatDocTypes() {
        return sunatDocTypes;
    }

    @XmlElement(name = "G_SUNAT_DOC_ID_TYPES")
    public void setSunatDocIdTypes(SunatDocIdType[] sunatDocIdTypes) {
        this.sunatDocIdTypes = sunatDocIdTypes;
    }

    public SunatDocIdType[] getSunatDocIdTypes() {
        return sunatDocIdTypes;
    }

    @XmlElement(name = "G_CARRIERS")
    public void setCarriers(GRECarriers[] carriers) {
        this.carriers = carriers;
    }

    public GRECarriers[] getCarriers() {
        return carriers;
    }
}
