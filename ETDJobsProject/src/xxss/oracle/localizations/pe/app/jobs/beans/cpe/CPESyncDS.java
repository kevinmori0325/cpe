package xxss.oracle.localizations.pe.app.jobs.beans.cpe;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class CPESyncDS
{
  CPERecepciones[] recepciones;
  CPEDetracciones[] detra;
  CPEInvoices[] invoices;
  CPEErrorInterface[] errorInterface;
  
  public CPESyncDS()
  {
    super();
  }

  @XmlElement(name = "G_RECEPCIONES")
  public void setRecepciones(CPERecepciones[] recepciones)
  {
    this.recepciones = recepciones;
  }

  public CPERecepciones[] getRecepciones()
  {
    return recepciones;
  }

    @XmlElement(name = "G_DETRA")
    public void setDetra(CPEDetracciones[] detra) {
        this.detra = detra;
    }

    public CPEDetracciones[] getDetra() {
        return detra;
    }
    
    @XmlElement(name = "G_FACTURAS")
        public void setInvoices(CPEInvoices[] invoices) 
        {
            this.invoices = invoices;
        }

        public CPEInvoices[] getInvoices() 
        {
            return invoices;
        }
		
 @XmlElement(name = "G_ERRORS")
  public void setErrorInterface(CPEErrorInterface[] errorInterface)
  {
    this.errorInterface = errorInterface;
  }

  public CPEErrorInterface[] getErrorInterface()
  {
    return errorInterface;
  }		

}
