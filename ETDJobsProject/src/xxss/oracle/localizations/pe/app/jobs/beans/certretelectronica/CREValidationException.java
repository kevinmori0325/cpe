package xxss.oracle.localizations.pe.app.jobs.beans.certretelectronica;

public class CREValidationException extends Exception {
    public CREValidationException(Throwable throwable) {
        super(throwable);
    }

    public CREValidationException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public CREValidationException(String string) {
        super(string);
    }

    public CREValidationException() {
        super();
    }
}
