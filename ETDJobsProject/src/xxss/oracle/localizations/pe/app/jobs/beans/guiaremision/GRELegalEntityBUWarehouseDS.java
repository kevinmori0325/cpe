package xxss.oracle.localizations.pe.app.jobs.beans.guiaremision;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "DATA_DS")
public class GRELegalEntityBUWarehouseDS {
    GRELegalEntity[] entidadLegal;
    GREInvTransactionType[] invTrxTypes;
        
    public GRELegalEntityBUWarehouseDS() {
        super();
    }

    @XmlElement(name = "G_LEGAL_ENTITY")
    public void setEntidadLegal(GRELegalEntity[] entidadLegal) {
        this.entidadLegal = entidadLegal;
    }

    public GRELegalEntity[] getEntidadLegal() {
        return entidadLegal;
    }

    @XmlElement(name = "G_TRX_TYPES")
    public void setInvTrxTypes(GREInvTransactionType[] invTrxTypes) {
        this.invTrxTypes = invTrxTypes;
    }

    public GREInvTransactionType[] getInvTrxTypes() {
        return invTrxTypes;
    }
    
}
