package xxss.oracle.localizations.pe.app.utils;

import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;

import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

public class WebServiceUtils {
    private static Logger log = Logger.getLogger(WebServiceUtils.class);
    public WebServiceUtils() {
        super();
    }
    
    public static boolean checkConnection(String pUrl) throws MalformedURLException, IOException {
        try {    
            if (pUrl.toUpperCase().indexOf("HTTPS") > -1) {
                HttpsURLConnection connection = (HttpsURLConnection) new URL(pUrl).openConnection();
                
                connection.setConnectTimeout(60000);
                connection.setReadTimeout(60000);
                
                if (connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {
                    return(true);
                } else {
                    return(false);
                }
            } else {
                HttpURLConnection connection = (HttpURLConnection) new URL(pUrl).openConnection();
                connection.setConnectTimeout(60000);
                connection.setReadTimeout(60000);
                
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    return(true);
                } else {
                    return(false);
                }
            }
        } catch (SocketTimeoutException e) {
            log.error("Error on checkConnection(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            return(false);
        } 
    }
}
