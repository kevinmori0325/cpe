package xxss.oracle.localizations.pe.app.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import java.util.Enumeration;

public class InetUtils {
    public InetUtils() {
        super();
    }

    public static String getServerIpAddress() {
        InetAddress ip;
        String ipAddress = "";
        try {

            //ip = InetAddress.getLocalHost();
            //ipAddress = ip.getHostAddress();
            //System.out.println("Current IP address : " + ip.getHostAddress());
            
            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
            for (; n.hasMoreElements(); ) {
                NetworkInterface e = n.nextElement();

                Enumeration<InetAddress> a = e.getInetAddresses();
                for (; a.hasMoreElements(); ) {
                    InetAddress addr = a.nextElement();
                    if (!(addr.getHostAddress().length() > 15) && !e.isLoopback()) {
                        ipAddress = ipAddress + ", " + e.getDisplayName() + ": " + addr.getHostAddress();
                    }
                   
                }
            }
            
        } catch (SocketException e) {
            e.printStackTrace();
        }

        return(ipAddress);
    }
}
