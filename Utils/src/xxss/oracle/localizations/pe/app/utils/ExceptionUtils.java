package xxss.oracle.localizations.pe.app.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ExceptionUtils {
    public ExceptionUtils() {
        super();
    }
    
    public static String stackTraceToString(Exception exception) {
        String stackTraceStr = "";
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();        
        PrintStream pt = new PrintStream(bos);
        exception.printStackTrace(pt);
        pt.flush();
        pt.close();
        
        stackTraceStr = new String(bos.toByteArray());        
        
        return(stackTraceStr);
    }
}
