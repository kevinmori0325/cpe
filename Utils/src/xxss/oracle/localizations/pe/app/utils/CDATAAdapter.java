package xxss.oracle.localizations.pe.app.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CDATAAdapter extends XmlAdapter<String, String> {
    public CDATAAdapter() {
        super();
    }

    public String unmarshal(String v) throws Exception {
        return v;
    }

    public String marshal(String v) throws Exception {
        return "<![CDATA[" + v + "]]>";
    }
}
