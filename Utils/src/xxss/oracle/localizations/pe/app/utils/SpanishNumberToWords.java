package xxss.oracle.localizations.pe.app.utils;

import java.text.DecimalFormat;

import java.util.ResourceBundle;

public class SpanishNumberToWords {
    private static final String[] hundreds =
    { "Cien", "Ciento", "Doscientos", "Trescientos", "Cuatrocientos", "Quinientos", "Seiscientos", "Setecientos",
      "Ochocientos", "Novecientos" };

    private static final String[] tens =
    { "", "Diez", "Veinte", "Treinta", "Cuarenta", "Cincuenta", "Sesenta", "Setenta", "Ochenta", "Noventa" };

    private static final String[] units =
    { "Uno", "Un", "Dos", "Tres", "Cuatro", "Cinco", "Seis", "Siete", "Ocho", "Nueve", "Cero", "Once", "Doce", "Trece",
      "Catorce", "Quince", "Dieciseis", "Diecisiete", "Dieciocho", "Diecinueve" };

    private static final String[] thousandMillionBillion = { "Mil", "Millon", "Billon" };
    private static final String plural = "es";
    private static final String AND = " CON ";
    private static final String DECIMALSUFFIX = "/100";

    public SpanishNumberToWords() {
        super();
    }

    private static String amountToText(double number, int opt) {
        double numeral;
        double centena = Math.pow(10, 2);
        double decena = Math.pow(10, 1);
        double unidad = Math.pow(10, 0);

        double currentSegment;

        String numberText = "";
        String segmentText = "";
        String cand = "";

        numeral = number;

        // Excepcion de Cien o Ciento
        if (numeral == 100) {
            segmentText = hundreds[0];
            numberText = segmentText;
        } else {
            //System.out.println("numeral: " + numeral);
            //System.out.println("centena: " + centena);
            
            currentSegment = (int)numeral/(int)centena; //Math.rint(numeral / centena);
            //currentSegment = numeral < centena ? 0.0 : currentSegment;
            numeral = numeral - (currentSegment * centena);
            
            //System.out.println("currentSegment: " + (int)currentSegment);

            if (currentSegment != 0) {
                segmentText = hundreds[(int)currentSegment];
                numberText = segmentText;
            }

            // Excepciones de la regla de Decenas
            if (numeral > 10 && numeral < 20) {
                segmentText = units[(int)numeral];
                numberText = numberText + " " + segmentText;
            } else {
                //System.out.println("numeral: " + numeral);
                //System.out.println("decena: " + decena);
                
                currentSegment = (int)numeral/(int)decena; //Math.rint(numeral / decena);
                //currentSegment = numeral < decena ? 0.0 : currentSegment;
                numeral = numeral - (currentSegment * decena);
                
                //System.out.println("currentSegment: " + (int)currentSegment);

                if (currentSegment != 0) {
                    segmentText = tens[(int)currentSegment];
                    numberText = numberText + " " + segmentText;
                    cand = "Y";
                }

                // Unidades
                currentSegment = (int)numeral/(int)unidad; //Math.rint(numeral / unidad);
                //currentSegment = numeral < unidad ? 0.0 : currentSegment;
                numeral = numeral - (currentSegment * unidad);

                if (currentSegment != 0) {
                    if (currentSegment == 1 && opt == 1) {
                        segmentText = units[0];
                    } else {
                        segmentText = units[(int)currentSegment];
                    }

                    if (!"".equals(cand)) {
                        numberText = numberText + " " + cand + " " + segmentText;
                    } else {
                        numberText = numberText + " " + segmentText;
                    }
                }
            }
        }

        return (numberText);
    }

    private static String allAmountText(double number) {
        double numeral = Math.abs(number);
        String numberText = "";
        double currentSegment;
        double thousand = Math.pow(10, 3);
        double million = Math.pow(10, 6);
        double billion = Math.pow(10, 9);

        if (numeral == 0) {
            return (" " + units[10]);
        }

        currentSegment = (int)numeral/(int)billion;  //Math.rint(numeral / billion);
        numeral = numeral - (currentSegment * billion);

        if (currentSegment != 0) {
            numberText = numberText + amountToText(currentSegment, 0) + " " + thousandMillionBillion[0] + " ";
        }

        currentSegment = (int)numeral/(int)million;//Math.rint(numeral / million);
        numeral = numeral - (currentSegment * million);

        if (currentSegment != 0) {
            numberText = numberText + amountToText(currentSegment, 0) + " " + thousandMillionBillion[1];
            numberText = numberText + (currentSegment == 1 ? "" : plural) + " ";
        }
        
        currentSegment = (int)numeral/(int)thousand;//Math.rint(numeral / thousand);
        numeral = numeral - (currentSegment * thousand);
        
        if (currentSegment != 0) {
            numberText = numberText + amountToText(currentSegment, 0) + " " + thousandMillionBillion[0] + " ";
        }
        
        if (numeral != 0) {
            numberText = numberText + amountToText(numeral, 1);
        }
        
        return(numberText);
    }
    
    private static String centToText(double pAmount) {
        double decimal = pAmount - Math.floor(pAmount);
        String decimalText;
        
        String mask = "#####00";
        DecimalFormat df = new DecimalFormat(mask);
        
        decimalText = df.format(decimal * 100) + DECIMALSUFFIX;
        return(decimalText);
    }
    
    public static String convertNumberToText(double pAmount, String pCurrencyCode) {
        String text = allAmountText(pAmount) + AND + centToText(pAmount);
        
        ResourceBundle rb = ResourceBundle.getBundle("xxss.oracle.localizations.pe.app.utils.CurrencyBundle");
        String textCurrency = rb.getString(pCurrencyCode);
        text = text + (!"".equals(textCurrency) ? " " + textCurrency : "");
        
        return(text);
    }
    
    public static void main(String[] args) {
        for (double i=900; i<1000; i++) {
            System.out.println(SpanishNumberToWords.convertNumberToText(i + 1, "PEN").toUpperCase().trim());
        }
        
    }
}
