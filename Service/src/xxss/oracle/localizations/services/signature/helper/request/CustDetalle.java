package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"custDetNroLin","custDetAFN_01", "custDetAFN_02", "custDetAFN_03", "custDetAFN_04", "custDetAFN_05", "custDetAFN_06"})
public class CustDetalle {
    
    String custDetNroLin;
    String custDetAFN_01;
    String custDetAFN_02;
    String custDetAFN_03;
    String custDetAFN_04;
    String custDetAFN_05;
    String custDetAFN_06;
    
    public CustDetalle() {
        super();
    }

    @XmlElement(name = "CustDetNroLin")
    public void setCustDetNroLin(String custDetNroLin) {
        this.custDetNroLin = custDetNroLin;
    }

    public String getCustDetNroLin() {
        return custDetNroLin;
    }
    @XmlElement(name = "CustDetAFN_01")
    public void setCustDetAFN_01(String custDetAFN_01) {
        this.custDetAFN_01 = custDetAFN_01;
    }

    public String getCustDetAFN_01() {
        return custDetAFN_01;
    }
    @XmlElement(name = "CustDetAFN_02")
    public void setCustDetAFN_02(String custDetAFN_02) {
        this.custDetAFN_02 = custDetAFN_02;
    }

    public String getCustDetAFN_02() {
        return custDetAFN_02;
    }
    @XmlElement(name = "CustDetAFN_03")
    public void setCustDetAFN_03(String custDetAFN_03) {
        this.custDetAFN_03 = custDetAFN_03;
    }

    public String getCustDetAFN_03() {
        return custDetAFN_03;
    }
    @XmlElement(name = "CustDetAFN_04")
    public void setCustDetAFN_04(String custDetAFN_04) {
        this.custDetAFN_04 = custDetAFN_04;
    }

    public String getCustDetAFN_04() {
        return custDetAFN_04;
    }
    @XmlElement(name = "CustDetAFN_05")
    public void setCustDetAFN_05(String custDetAFN_05) {
        this.custDetAFN_05 = custDetAFN_05;
    }

    public String getCustDetAFN_05() {
        return custDetAFN_05;
    }
    @XmlElement(name = "CustDetAFN_06")
    public void setCustDetAFN_06(String custDetAFN_06) {
        this.custDetAFN_06 = custDetAFN_06;
    }

    public String getCustDetAFN_06() {
        return custDetAFN_06;
    }
}
