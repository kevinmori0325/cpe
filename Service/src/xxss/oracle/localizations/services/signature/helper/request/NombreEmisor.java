package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"primerNombre"})
public class NombreEmisor {
    String primerNombre;
    
    public NombreEmisor() {
        super();
    }

    @XmlElement(name = "PrimerNombre")
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }
}
