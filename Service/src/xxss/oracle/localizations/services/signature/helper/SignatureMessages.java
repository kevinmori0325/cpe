package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "SignatureMessages")
public class SignatureMessages {
    SignatureMessage[] signatureMessages;
    
    public SignatureMessages() {
        super();
    }
    
    @XmlElement(name = "SignatureMessage")
    public void setSignatureMessages(SignatureMessage[] signatureMessages) {
        this.signatureMessages = signatureMessages;
    }

    public SignatureMessage[] getSignatureMessages() {
        return signatureMessages;
    }
}
