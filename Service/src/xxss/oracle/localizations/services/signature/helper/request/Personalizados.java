package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"docPersonalizado"})
public class Personalizados {
    DocPersonalizado docPersonalizado;
    
    public Personalizados() {
        super();
    }

    @XmlElement(name = "DocPersonalizado")
    public void setDocPersonalizado(DocPersonalizado docPersonalizado) {
        this.docPersonalizado = docPersonalizado;
    }

    public DocPersonalizado getDocPersonalizado() {
        return docPersonalizado;
    }
}
