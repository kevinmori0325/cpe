package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tipoImp", "tasaImp", "montoImp"})
public class Impuestos {
    String tipoImp;
    String tasaImp;
    String montoImp;
    
    public Impuestos() {
        super();
    }

    @XmlElement(name = "TipoImp")
    public void setTipoImp(String tipoImp) {
        this.tipoImp = tipoImp;
    }

    public String getTipoImp() {
        return tipoImp;
    }

    @XmlElement(name = "TasaImp")
    public void setTasaImp(String tasaImp) {
        this.tasaImp = tasaImp;
    }

    public String getTasaImp() {
        return tasaImp;
    }

    @XmlElement(name = "MontoImp")
    public void setMontoImp(String montoImp) {
        this.montoImp = montoImp;
    }

    public String getMontoImp() {
        return montoImp;
    }
}
