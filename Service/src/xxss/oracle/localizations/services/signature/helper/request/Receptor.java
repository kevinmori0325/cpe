package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"docRecep", "nmbRecep", "nombreRecep", "contacto", "domFiscalRcp"})
public class Receptor {
    DocRecep docRecep;
    String nmbRecep;
    NombreRecep nombreRecep;
    String contacto;
    DomFiscalRcp domFiscalRcp;
    
    public Receptor() {
        super();
    }

    @XmlElement(name = "DocRecep")
    public void setDocRecep(DocRecep docRecep) {
        this.docRecep = docRecep;
    }

    public DocRecep getDocRecep() {
        return docRecep;
    }

    @XmlElement(name = "NmbRecep")
    public void setNmbRecep(String nmbRecep) {
        this.nmbRecep = nmbRecep;
    }

    public String getNmbRecep() {
        return nmbRecep;
    }

    @XmlElement(name = "NombreRecep")
    public void setNombreRecep(NombreRecep nombreRecep) {
        this.nombreRecep = nombreRecep;
    }

    public NombreRecep getNombreRecep() {
        return nombreRecep;
    }

    @XmlElement(name = "Contacto")
    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getContacto() {
        return contacto;
    }

    @XmlElement(name = "DomFiscalRcp")
    public void setDomFiscalRcp(DomFiscalRcp domFiscalRcp) {
        this.domFiscalRcp = domFiscalRcp;
    }

    public DomFiscalRcp getDomFiscalRcp() {
        return domFiscalRcp;
    }
}
