package xxss.oracle.localizations.services.signature.helper.request;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"ambiente", "tipoEmision", "contenidoTC", "tipo", "serie", "numero", "estado", "numeroInterno", "fechaEmis","tipoCambio","fechaVen","tipoNotaCredito","tipoNotaDebito","sustentoNotaCredito","sustentoNotaDebito","tipoOperacion","codigoServicio","porcetDetraccion","montoDetraccion","bancoNacion","ordenCompra","monedaDetraccion","medioPago","numTax","numeroPle","codAsigSunat","sede","batchSource","concepto","trxNumberRef","montoDescuento","montobaseDescuento","factorDescuento","codigoDescuento","indicador","trxType","nivel","afecta_bi","moneda","desc_lin_desc","factor_descuento"})
public class IdDoc {
    private String ambiente;
        private String tipoEmision;
        private String contenidoTC;
        private String tipo;
        private String serie;
        private String numero;
        private String estado;
        private String numeroInterno;
        private String fechaEmis;
        private String tipoCambio;
        private String fechaVen;
        private String tipoNotaCredito;
        private String tipoNotaDebito;
        private String sustentoNotaCredito;
        private String sustentoNotaDebito;
        private String tipoOperacion;
        private String codigoServicio;
        private String porcetDetraccion;
        private String montoDetraccion;
        private String bancoNacion;
        private String ordenCompra;
        private String monedaDetraccion;
        private String medioPago;
        private int numTax;
        private String numeroPle;
        private String codAsigSunat;
        private String sede;
        private String batchSource;
        private String concepto;
        private String trxNumberRef;
        private String montoDescuento;
        private String montobaseDescuento;
        private String factorDescuento;
        private String codigoDescuento;
        private String indicador;
        private String trxType;
        private String nivel;
        private String afecta_bi;
        private String desc_lin_desc;
        private String moneda;
        private String factor_descuento;//ADD DCHUMACERO 170420    


    public IdDoc() {
        super();
    }
    
    @XmlElement(name = "Ambiente")
    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getAmbiente() {
        return ambiente;
    }

    @XmlElement(name = "TipoEmision")
    public void setTipoEmision(String tipoEmision) {
        this.tipoEmision = tipoEmision;
    }

    public String getTipoEmision() {
        return tipoEmision;
    }

    @XmlElement(name = "ContenidoTC")
    public void setContenidoTC(String contenidoTC) {
        this.contenidoTC = contenidoTC;
    }

    public String getContenidoTC() {
        return contenidoTC;
    }

    @XmlElement(name = "Tipo")
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    @XmlElement(name = "Serie")
    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    @XmlElement(name = "Numero")
    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    @XmlElement(name = "Estado")
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    @XmlElement(name = "NumeroInterno")
    public void setNumeroInterno(String numeroInterno) {
        this.numeroInterno = numeroInterno;
    }

    public String getNumeroInterno() {
        return numeroInterno;
    }

    @XmlElement(name = "FechaEmis")
    public void setFechaEmis(String fechaEmis) {
        this.fechaEmis = fechaEmis;
    }

    public String getFechaEmis() {
        return fechaEmis;
    }

    @XmlElement(name = "TipoCambio")
    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    @XmlElement(name = "FechaVen")
    public void setFechaVen(String fechaVen) {
        this.fechaVen = fechaVen;
    }

    public String getFechaVen() {
        return fechaVen;
    }

    @XmlElement(name = "TipoNotaCredito")
    public void setTipoNotaCredito(String tipoNotaCredito) {
        this.tipoNotaCredito = tipoNotaCredito;
    }

    public String getTipoNotaCredito() {
        return tipoNotaCredito;
    }

    @XmlElement(name = "TipoNotaDebito")
    public void setTipoNotaDebito(String tipoNotaDebito) {
        this.tipoNotaDebito = tipoNotaDebito;
    }

    public String getTipoNotaDebito() {
        return tipoNotaDebito;
    }

    @XmlElement(name = "SustentoNotaCredito")
    public void setSustentoNotaCredito(String sustentoNotaCredito) {
        this.sustentoNotaCredito = sustentoNotaCredito;
    }

    public String getSustentoNotaCredito() {
        return sustentoNotaCredito;
    }

    @XmlElement(name = "SustentoNotaDebito")
    public void setSustentoNotaDebito(String sustentoNotaDebito) {
        this.sustentoNotaDebito = sustentoNotaDebito;
    }

    public String getSustentoNotaDebito() {
        return sustentoNotaDebito;
    }

    @XmlElement(name = "TipoOperacion")
    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getTipoOperacion() {
        return tipoOperacion;
    }

    @XmlElement(name = "CodigoServicio")
    public void setCodigoServicio(String codigoServicio) {
        this.codigoServicio = codigoServicio;
    }

    public String getCodigoServicio() {
        return codigoServicio;
    }

    @XmlElement(name = "PorcetDetraccion")
    public void setPorcetDetraccion(String porcetDetraccion) {
        this.porcetDetraccion = porcetDetraccion;
    }

    public String getPorcetDetraccion() {
        return porcetDetraccion;
    }

    @XmlElement(name = "MontoDetraccion")
    public void setMontoDetraccion(String montoDetraccion) {
        this.montoDetraccion = montoDetraccion;
    }

    public String getMontoDetraccion() {
        return montoDetraccion;
    }

    @XmlElement(name = "BancoNacion")
    public void setBancoNacion(String bancoNacion) {
        this.bancoNacion = bancoNacion;
    }

    public String getBancoNacion() {
        return bancoNacion;
    }

    @XmlElement(name = "OrdenCompra")
    public void setOrdenCompra(String ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public String getOrdenCompra() {
        return ordenCompra;
    }

    @XmlElement(name = "MonedaDetraccion")
    public void setMonedaDetraccion(String monedaDetraccion) {
        this.monedaDetraccion = monedaDetraccion;
    }

    public String getMonedaDetraccion() {
        return monedaDetraccion;
    }

    @XmlElement(name = "MedioPago")
    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public String getMedioPago() {
        return medioPago;
    }

    @XmlElement(name = "NumTax")
    public void setNumTax(int numTax) {
        this.numTax = numTax;
    }

    public int getNumTax() {
        return numTax;
    }

    @XmlElement(name = "NumeroPle")
    public void setNumeroPle(String numeroPle) {
        this.numeroPle = numeroPle;
    }

    public String getNumeroPle() {
        return numeroPle;
    }

    @XmlElement(name = "CodAsigSunat")
    public void setCodAsigSunat(String codAsigSunat) {
        this.codAsigSunat = codAsigSunat;
    }

    public String getCodAsigSunat() {
        return codAsigSunat;
    }

    @XmlElement(name = "Sede")
    public void setSede(String sede) {
        this.sede = sede;
    }

    public String getSede() {
        return sede;
    }

    @XmlElement(name = "BatchSource")
    public void setBatchSource(String batchSource) {
        this.batchSource = batchSource;
    }

    public String getBatchSource() {
        return batchSource;
    }

    @XmlElement(name = "Concepto")
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getConcepto() {
        return concepto;
    }

    @XmlElement(name = "TrxNumberRef")
    public void setTrxNumberRef(String trxNumberRef) {
        this.trxNumberRef = trxNumberRef;
    }

    public String getTrxNumberRef() {
        return trxNumberRef;
    }

    @XmlElement(name = "MontoDescuento")
    public void setMontoDescuento(String montoDescuento) {
        this.montoDescuento = montoDescuento;
    }

    public String getMontoDescuento() {
        return montoDescuento;
    }

    @XmlElement(name = "MontobaseDescuento")
    public void setMontobaseDescuento(String montobaseDescuento) {
        this.montobaseDescuento = montobaseDescuento;
    }

    public String getMontobaseDescuento() {
        return montobaseDescuento;
    }

    @XmlElement(name = "FactorDescuento")
    public void setFactorDescuento(String factorDescuento) {
        this.factorDescuento = factorDescuento;
    }

    public String getFactorDescuento() {
        return factorDescuento;
    }

    @XmlElement(name = "CodigoDescuento")
    public void setCodigoDescuento(String codigoDescuento) {
        this.codigoDescuento = codigoDescuento;
    }

    public String getCodigoDescuento() {
        return codigoDescuento;
    }

    @XmlElement(name = "Indicador")
    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getIndicador() {
        return indicador;
    }

    @XmlElement(name = "TrxType")
    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxType() {
        return trxType;
    }
    
    @XmlElement(name = "Nivel")
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNivel() {
        return nivel;
    }

    @XmlElement(name = "Afecta_bi")
    public void setAfecta_bi(String afecta_bi) {
        this.afecta_bi = afecta_bi;
    }

    public String getAfecta_bi() {
        return afecta_bi;
    }


    @XmlElement(name = "Desc_lin_desc")
    public void setDesc_lin_desc(String desc_lin_desc) {
        this.desc_lin_desc = desc_lin_desc;
    }

    public String getDesc_lin_desc() {
        return desc_lin_desc;
    }

    @XmlElement(name = "Moneda")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }
    @XmlElement(name = "Factor_descuento")
    public void setFactor_descuento(String factor_descuento) {
        this.factor_descuento = factor_descuento;
    }

    public String getFactor_descuento() {
        return factor_descuento;
    }
}
