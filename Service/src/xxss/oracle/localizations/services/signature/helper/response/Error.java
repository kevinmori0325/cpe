package xxss.oracle.localizations.services.signature.helper.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

//@XmlType(propOrder={"code", "description", "uiMessage"})
public class Error {
    String code;
    String description;
    String uiMessage;
    
    public Error() {
        super();
    }

    @XmlElement(name = "Code")
    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @XmlElement(name = "Description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement(name = "UIMessage")
    public void setUIMessage(String UIMessage) {
        this.uiMessage = UIMessage;
    }

    public String getUIMessage() {
        return uiMessage;
    }
}
