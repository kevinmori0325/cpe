package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder={"taxClassificationCode", "nombreTributo", "identTributo", "codTributo"})
public class Tax {
    String taxClassificationCode;
    String nombreTributo;
    String identTributo;
    String codTributo;
    public Tax() {
        super();
    }

    @XmlElement(name = "TaxClassificationCode")
    public void setTaxClassificationCode(String taxClassificationCode) {
        this.taxClassificationCode = taxClassificationCode;
    }

    public String getTaxClassificationCode() {
        return taxClassificationCode;
    }

    @XmlElement(name = "NombreTributo")
    public void setNombreTributo(String nombreTributo) {
        this.nombreTributo = nombreTributo;
    }

    public String getNombreTributo() {
        return nombreTributo;
    }

    @XmlElement(name = "IdentTributo")
    public void setIdentTributo(String identTributo) {
        this.identTributo = identTributo;
    }

    public String getIdentTributo() {
        return identTributo;
    }

    @XmlElement(name = "CodTributo")
    public void setCodTributo(String codTributo) {
        this.codTributo = codTributo;
    }

    public String getCodTributo() {
        return codTributo;
    }
}
