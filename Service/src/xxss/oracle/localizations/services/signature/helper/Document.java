package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

//@XmlType(propOrder={"sender", "receiver", "type", "serie", "number", "date"})
public class Document {
    String id;
    String sender;
    String receiver;
    String type;
    String serie;
    String number;
    String date;
    
    public Document() {
        super();
    }

    @XmlAttribute(name = "Id")
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @XmlElement(name = "Sender")
    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    @XmlElement(name = "Receiver")
    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiver() {
        return receiver;
    }

    @XmlElement(name = "Type")
    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @XmlElement(name = "Serie")
    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    @XmlElement(name = "Number")
    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    @XmlElement(name = "Date")
    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
