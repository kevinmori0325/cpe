package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tipo", "codTipoMonto", "tasaConcepto", "montoBase","montoTotal" ,"montoConcepto"})
public class TotSubMonto {
    String tipo;
    String codTipoMonto;
    String tasaConcepto;
    String montoBase;
    String montoTotal;
    String montoConcepto;
    
    
    
    public TotSubMonto() {
        super();
    }

    @XmlElement(name = "Tipo")
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    @XmlElement(name = "CodTipoMonto")
    public void setCodTipoMonto(String codTipoMonto) {
        this.codTipoMonto = codTipoMonto;
    }

    public String getCodTipoMonto() {
        return codTipoMonto;
    }

    @XmlElement(name = "TasaConcepto")
    public void setTasaConcepto(String tasaConcepto) {
        this.tasaConcepto = tasaConcepto;
    }

    public String getTasaConcepto() {
        return tasaConcepto;
    }

    @XmlElement(name = "MontoBase")
    public void setMontoBase(String montoBase) {
        this.montoBase = montoBase;
    }

    public String getMontoBase() {
        return montoBase;
    }

    @XmlElement(name = "MontoTotal")
    public void setMontoTotal(String montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getMontoTotal() {
        return montoTotal;
    }

    @XmlElement(name = "MontoConcepto")
    public void setMontoConcepto(String montoConcepto) {
        this.montoConcepto = montoConcepto;
    }

    public String getMontoConcepto() {
        return montoConcepto;
    }
}
