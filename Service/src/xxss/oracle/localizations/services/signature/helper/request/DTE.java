package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "DTE")
@XmlType(propOrder={"documento", "personalizados"})
public class DTE {
    Documento documento;
    Personalizados personalizados;
    
    public DTE() {
        super();
    }
    
    @XmlElement(name = "Documento")
    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Documento getDocumento() {
        return documento;
    }

    @XmlElement(name = "Personalizados")
    public void setPersonalizados(Personalizados personalizados) {
        this.personalizados = personalizados;
    }

    public Personalizados getPersonalizados() {
        return personalizados;
    }

    
}
