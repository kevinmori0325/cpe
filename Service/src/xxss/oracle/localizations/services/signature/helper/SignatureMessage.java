package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//@XmlType(propOrder={"document", "storeInfo", "authorization", "status", "extensions"})
public class SignatureMessage {
    Document document;
    StoreInfo storeInfo;
    Authorization authorization;
    Status[] status;
    Extensions extensions;
    
    public SignatureMessage() {
        super();
    }

    @XmlElement(name = "Document")
    public void setDocument(Document document) {
        this.document = document;
    }

    public Document getDocument() {
        return document;
    }

    @XmlElement(name = "StoreInfo")
    public void setStoreInfo(StoreInfo storeInfo) {
        this.storeInfo = storeInfo;
    }

    public StoreInfo getStoreInfo() {
        return storeInfo;
    }

    @XmlElement(name = "Authorization")
    public void setAuthorization(Authorization authorization) {
        this.authorization = authorization;
    }

    public Authorization getAuthorization() {
        return authorization;
    }

    @XmlElement(name = "Status")
    public void setStatus(Status[] status) {
        this.status = status;
    }

    public Status[] getStatus() {
        return status;
    }

    @XmlElement(name = "Extensions")
    public void setExtensions(Extensions extensions) {
        this.extensions = extensions;
    }

    public Extensions getExtensions() {
        return extensions;
    }
}
