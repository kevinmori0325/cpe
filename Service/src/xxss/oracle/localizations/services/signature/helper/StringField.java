package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

public class StringField {
    String name;
    String value;
    
    public StringField() {
        super();
    }

    @XmlAttribute(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @XmlValue
    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
