package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"nroLinDet", "cdgItem", "dscItem", "qtyItem", "fchElabor", "fchVencim", "otrMnda", "unmdItem", "prcBrutoItem", "prcNetoItem", "impuestosDet", "descuentoPct", "descuentoMonto", "montoBrutoItem", "montoNetoItem", "montoTotalItem", "tpoDocRef", "serieRef", "numeroRef"})
public class Detalle {
    String nroLinDet;
    CdgItem[] cdgItem;
    String dscItem;
    String qtyItem;
    String fchElabor;
    String fchVencim;
    OtrMnda otrMnda;
    String unmdItem;
    String prcBrutoItem;
    String prcNetoItem;
    ImpuestosDet[] impuestosDet;
    String descuentoPct;
    String descuentoMonto;
    String montoBrutoItem;
    String montoNetoItem;
    String montoTotalItem;
    String tpoDocRef;
    String serieRef;
    String numeroRef;
    String fchTipoCamb;
    double montoBrutoItemDbl;
    double montoNetoItemDbl;
    double montoTotalItemDbl; 
    double montoTotal;
    String taxClassificationCode;
    String importeTotalTribu;
    String baseImponible;
    String importeExplicito;
    String tasaImpuestoLine;
    String precioVenta;
    String codigoProductoSunat;
    String afectacionIGV;
    String identificacionTributo;
    String nombreTributo;
    String codigoTipoTributo;
    String tipoPrecioVenta;
    String indicadorTipo;
    String indicador;
    String nivel;
    String afecta_bi;
    String factor_descuento;//ADD DCHUMACERO 170420
        
        
    public Detalle() {
        super();
    }

    @XmlElement(name = "NroLinDet")
    public void setNroLinDet(String nroLinDet) {
        this.nroLinDet = nroLinDet;
    }

    public String getNroLinDet() {
        return nroLinDet;
    }

    @XmlElement(name = "CdgItem")
    public void setCdgItem(CdgItem[] cdgItem) {
        this.cdgItem = cdgItem;
    }

    public CdgItem[] getCdgItem() {
        return cdgItem;
    }

    @XmlElement(name = "DscItem")
    public void setDscItem(String dscItem) {
        this.dscItem = dscItem;
    }

    public String getDscItem() {
        return dscItem;
    }

    @XmlElement(name = "QtyItem")
    public void setQtyItem(String qtyItem) {
        this.qtyItem = qtyItem;
    }

    public String getQtyItem() {
        return qtyItem;
    }

    @XmlElement(name = "UnmdItem")
    public void setUnmdItem(String unmdItem) {
        this.unmdItem = unmdItem;
    }

    public String getUnmdItem() {
        return unmdItem;
    }

    @XmlElement(name = "PrcBrutoItem")
    public void setPrcBrutoItem(String prcBrutoItem) {
        this.prcBrutoItem = prcBrutoItem;
    }

    public String getPrcBrutoItem() {
        return prcBrutoItem;
    }

    @XmlElement(name = "PrcNetoItem")
    public void setPrcNetoItem(String prcNetoItem) {
        this.prcNetoItem = prcNetoItem;
    }

    public String getPrcNetoItem() {
        return prcNetoItem;
    }

    @XmlElement(name = "ImpuestosDet")
    public void setImpuestosDet(ImpuestosDet[] impuestosDet) {
        this.impuestosDet = impuestosDet;
    }

    public ImpuestosDet[] getImpuestosDet() {
        return impuestosDet;
    }

    @XmlElement(name = "DescuentoPct")
    public void setDescuentoPct(String descuentoPct) {
        this.descuentoPct = descuentoPct;
    }

    public String getDescuentoPct() {
        return descuentoPct;
    }

    @XmlElement(name = "DescuentoMonto")
    public void setDescuentoMonto(String descuentoMonto) {
        this.descuentoMonto = descuentoMonto;
    }

    public String getDescuentoMonto() {
        return descuentoMonto;
    }

    @XmlElement(name = "MontoBrutoItem")
    public void setMontoBrutoItem(String montoBrutoItem) {
        this.montoBrutoItem = montoBrutoItem;
    }

    public String getMontoBrutoItem() {
        return montoBrutoItem;
    }

    @XmlElement(name = "MontoNetoItem")
    public void setMontoNetoItem(String montoNetoItem) {
        this.montoNetoItem = montoNetoItem;
    }

    public String getMontoNetoItem() {
        return montoNetoItem;
    }

    @XmlElement(name = "MontoTotalItem")
    public void setMontoTotalItem(String montoTotalItem) {
        this.montoTotalItem = montoTotalItem;
    }

    public String getMontoTotalItem() {
        return montoTotalItem;
    }

    @XmlElement(name = "FchElabor")
    public void setFchElabor(String fchElabor) {
        this.fchElabor = fchElabor;
    }

    public String getFchElabor() {
        return fchElabor;
    }

    @XmlElement(name = "FchVencim")
    public void setFchVencim(String fchVencim) {
        this.fchVencim = fchVencim;
    }

    public String getFchVencim() {
        return fchVencim;
    }

    @XmlElement(name = "OtrMnda")
    public void setOtrMnda(OtrMnda otrMnda) {
        this.otrMnda = otrMnda;
    }

    public OtrMnda getOtrMnda() {
        return otrMnda;
    }

    @XmlElement(name = "TpoDocRef")
    public void setTpoDocRef(String tpoDocRef) {
        this.tpoDocRef = tpoDocRef;
    }

    public String getTpoDocRef() {
        return tpoDocRef;
    }

    @XmlElement(name = "SerieRef")
    public void setSerieRef(String serieRef) {
        this.serieRef = serieRef;
    }

    public String getSerieRef() {
        return serieRef;
    }

    @XmlElement(name = "NumeroRef")
    public void setNumeroRef(String numeroRef) {
        this.numeroRef = numeroRef;
    }

    public String getNumeroRef() {
        return numeroRef;
    }

    @XmlTransient
    public void setFchTipoCamb(String fchTipoCamb) {
        this.fchTipoCamb = fchTipoCamb;
    }

    public String getFchTipoCamb() {
        return fchTipoCamb;
    }

    @XmlTransient
    public void setMontoBrutoItemDbl(double montoBrutoItemDbl) {
        this.montoBrutoItemDbl = montoBrutoItemDbl;
    }

    public double getMontoBrutoItemDbl() {
        return montoBrutoItemDbl;
    }

    @XmlTransient
    public void setMontoNetoItemDbl(double montoNetoItemDbl) {
        this.montoNetoItemDbl = montoNetoItemDbl;
    }

    public double getMontoNetoItemDbl() {
        return montoNetoItemDbl;
    }

    @XmlTransient
    public void setMontoTotalItemDbl(double montoTotalItemDbl) {
        this.montoTotalItemDbl = montoTotalItemDbl;
    }

    public double getMontoTotalItemDbl() {
        return montoTotalItemDbl;
    }
    @XmlTransient
    public void setMontoTotal(double montoTotal) {
        this.montoTotal = montoTotal;
    }

    public double getMontoTotal() {
        return montoTotal;
    }
    
    @XmlTransient
    public void setTaxClassificationCode(String taxClassificationCode) {
        this.taxClassificationCode = taxClassificationCode;
    }

    public String getTaxClassificationCode() {
        return taxClassificationCode;
    }

    @XmlTransient
    public void setImporteTotalTribu(String importeTotalTribu) {
        this.importeTotalTribu = importeTotalTribu;
    }

    public String getImporteTotalTribu() {
        return importeTotalTribu;
    }

    @XmlTransient
    public void setBaseImponible(String baseImponible) {
        this.baseImponible = baseImponible;
    }

    public String getBaseImponible() {
        return baseImponible;
    }

    @XmlTransient
    public void setImporteExplicito(String importeExplicito) {
        this.importeExplicito = importeExplicito;
    }

    public String getImporteExplicito() {
        return importeExplicito;
    }

    @XmlTransient
    public void setTasaImpuestoLine(String tasaImpuestoLine) {
        this.tasaImpuestoLine = tasaImpuestoLine;
    }

    public String getTasaImpuestoLine() {
        return tasaImpuestoLine;
    }

    @XmlTransient
    public void setPrecioVenta(String precioVenta) {
        this.precioVenta = precioVenta;
    }

    public String getPrecioVenta() {
        return precioVenta;
    }

    @XmlTransient
    public void setCodigoProductoSunat(String codigoProductoSunat) {
        this.codigoProductoSunat = codigoProductoSunat;
    }

    public String getCodigoProductoSunat() {
        return codigoProductoSunat;
    }

    @XmlTransient
    public void setAfectacionIGV(String afectacionIGV) {
        this.afectacionIGV = afectacionIGV;
    }

    public String getAfectacionIGV() {
        return afectacionIGV;
    }

    @XmlTransient
    public void setIdentificacionTributo(String identificacionTributo) {
        this.identificacionTributo = identificacionTributo;
    }

    public String getIdentificacionTributo() {
        return identificacionTributo;
    }

    @XmlTransient
    public void setNombreTributo(String nombreTributo) {
        this.nombreTributo = nombreTributo;
    }

    public String getNombreTributo() {
        return nombreTributo;
    }

    @XmlTransient
    public void setCodigoTipoTributo(String codigoTipoTributo) {
        this.codigoTipoTributo = codigoTipoTributo;
    }

    public String getCodigoTipoTributo() {
        return codigoTipoTributo;
    }

    @XmlTransient
    public void setTipoPrecioVenta(String tipoPrecioVenta) {
        this.tipoPrecioVenta = tipoPrecioVenta;
    }

    public String getTipoPrecioVenta() {
        return tipoPrecioVenta;
    }

    @XmlTransient
    public void setIndicadorTipo(String indicadorTipo) {
        this.indicadorTipo = indicadorTipo;
    }

    public String getIndicadorTipo() {
        return indicadorTipo;
    }

    @XmlTransient
    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }
    
    public String getIndicador() {
        return indicador;
    }
    @XmlTransient
    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNivel() {
        return nivel;
    }

    @XmlTransient
    public void setAfecta_bi(String afecta_bi) {
        this.afecta_bi = afecta_bi;
    }

    public String getAfecta_bi() {
        return afecta_bi;
    }
    @XmlTransient
    public void setFactor_descuento(String factor_descuento) {
        this.factor_descuento = factor_descuento;
    }

    public String getFactor_descuento() {
        return factor_descuento;
    }
}
