package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlElement;

public class Authorization {
    Status[] status;
    
    public Authorization() {
        super();
    }

    @XmlElement(name = "Status")
    public void setStatus(Status[] status) {
        this.status = status;
    }

    public Status[] getStatus() {
        return status;
    }
}
