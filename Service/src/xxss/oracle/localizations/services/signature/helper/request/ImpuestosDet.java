package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tipoImp", "codTasaImp", "tasaImp", "montoImp"})
public class ImpuestosDet {
    String tipoImp;
    String codTasaImp;
    String tasaImp;
    String montoImp;
    
    public ImpuestosDet() {
        super();
    }

    @XmlElement(name = "TipoImp")
    public void setTipoImp(String tipoImp) {
        this.tipoImp = tipoImp;
    }

    public String getTipoImp() {
        return tipoImp;
    }

    @XmlElement(name = "CodTasaImp")
    public void setCodTasaImp(String codTasaImp) {
        this.codTasaImp = codTasaImp;
    }

    public String getCodTasaImp() {
        return codTasaImp;
    }

    @XmlElement(name = "TasaImp")
    public void setTasaImp(String tasaImp) {
        this.tasaImp = tasaImp;
    }

    public String getTasaImp() {
        return tasaImp;
    }

    @XmlElement(name = "MontoImp")
    public void setMontoImp(String montoImp) {
        this.montoImp = montoImp;
    }

    public String getMontoImp() {
        return montoImp;
    }
}
