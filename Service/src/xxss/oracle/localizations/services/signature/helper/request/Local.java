package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tipoLoc", "codigoLoc", "nombreLoc"})
public class Local {
    String tipoLoc;
    String codigoLoc;
    String nombreLoc;
    
    public Local() {
        super();
    }

    @XmlElement(name = "TipoLoc")
    public void setTipoLoc(String tipoLoc) {
        this.tipoLoc = tipoLoc;
    }

    public String getTipoLoc() {
        return tipoLoc;
    }

    @XmlElement(name = "CodigoLoc")
    public void setCodigoLoc(String codigoLoc) {
        this.codigoLoc = codigoLoc;
    }

    public String getCodigoLoc() {
        return codigoLoc;
    }

    @XmlElement(name = "NombreLoc")
    public void setNombreLoc(String nombreLoc) {
        this.nombreLoc = nombreLoc;
    }

    public String getNombreLoc() {
        return nombreLoc;
    }
}
