package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"idDoc", "tipo", "numeroInterno", "fechaRef", "fechaEmis", "emisor", "receptor", "transporte", "totales", "impuestos", "local", "traslado"})
public class Encabezado {
    IdDoc idDoc;
    String tipo;
    String numeroInterno;
    String fechaRef;
    String fechaEmis;
    Emisor emisor;
    Receptor receptor;
    Totales totales;
    Impuestos[] impuestos;
    Transporte transporte;
    Local local;
    Traslado traslado;
    
    public Encabezado() {
        super();
    }

    @XmlElement(name = "IdDoc")
    public void setIdDoc(IdDoc idDoc) {
        this.idDoc = idDoc;
    }

    public IdDoc getIdDoc() {
        return idDoc;
    }

    @XmlElement(name = "Emisor")
    public void setEmisor(Emisor emisor) {
        this.emisor = emisor;
    }

    public Emisor getEmisor() {
        return emisor;
    }

    @XmlElement(name = "Receptor")
    public void setReceptor(Receptor receptor) {
        this.receptor = receptor;
    }

    public Receptor getReceptor() {
        return receptor;
    }

    @XmlElement(name = "Totales")
    public void setTotales(Totales totales) {
        this.totales = totales;
    }

    public Totales getTotales() {
        return totales;
    }

    @XmlElement(name = "Impuestos")
    public void setImpuestos(Impuestos[] impuestos) {
        this.impuestos = impuestos;
    }

    public Impuestos[] getImpuestos() {
        return impuestos;
    }

    @XmlElement(name = "Tipo")
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    @XmlElement(name = "NumeroInterno")
    public void setNumeroInterno(String numeroInterno) {
        this.numeroInterno = numeroInterno;
    }

    public String getNumeroInterno() {
        return numeroInterno;
    }

    @XmlElement(name = "FechaRef")
    public void setFechaRef(String fechaRef) {
        this.fechaRef = fechaRef;
    }

    public String getFechaRef() {
        return fechaRef;
    }

    @XmlElement(name = "FechaEmis")
    public void setFechaEmis(String fechaEmis) {
        this.fechaEmis = fechaEmis;
    }

    public String getFechaEmis() {
        return fechaEmis;
    }

    @XmlElement(name = "Transporte")
    public void setTransporte(Transporte transporte) {
        this.transporte = transporte;
    }

    public Transporte getTransporte() {
        return transporte;
    }

    @XmlElement(name = "Local")
    public void setLocal(Local local) {
        this.local = local;
    }

    public Local getLocal() {
        return local;
    }

    @XmlElement(name = "Traslado")
    public void setTraslado(Traslado traslado) {
        this.traslado = traslado;
    }

    public Traslado getTraslado() {
        return traslado;
    }
}
