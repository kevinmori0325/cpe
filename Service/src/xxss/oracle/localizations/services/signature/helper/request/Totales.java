package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


@XmlType(propOrder={"moneda", "mntDcto", "mntRcgo", "vlrPagar", "vlrPalabras", "totSubMonto", "indLista", "tipolista", "tipoQtyItem"})
public class Totales {
    String moneda;
    String mntDcto;
    String mntRcgo;
    String vlrPagar;
    String vlrPalabras;
    TotSubMonto[] totSubMonto;
    String indLista;
    String tipolista;
    String tipoQtyItem;
    String baseGravAmountCab;
    String baseNoGravInaAmountCab;
    String baseNoGravExoAmountCab;
    
    public Totales() {
        super();
    }

    @XmlElement(name = "Moneda")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    @XmlElement(name = "MntDcto")
    public void setMntDcto(String mntDcto) {
        this.mntDcto = mntDcto;
    }

    public String getMntDcto() {
        return mntDcto;
    }

    @XmlElement(name = "MntRcgo")
    public void setMntRcgo(String mntRcgo) {
        this.mntRcgo = mntRcgo;
    }

    public String getMntRcgo() {
        return mntRcgo;
    }

    @XmlElement(name = "VlrPagar")
    public void setVlrPagar(String vlrPagar) {
        this.vlrPagar = vlrPagar;
    }

    public String getVlrPagar() {
        return vlrPagar;
    }

    @XmlElement(name = "VlrPalabras")
    public void setVlrPalabras(String vlrPalabras) {
        this.vlrPalabras = vlrPalabras;
    }

    public String getVlrPalabras() {
        return vlrPalabras;
    }

    @XmlElement(name = "TotSubMonto")
    public void setTotSubMonto(TotSubMonto[] totSubMonto) {
        this.totSubMonto = totSubMonto;
    }

    public TotSubMonto[] getTotSubMonto() {
        return totSubMonto;
    }
    
    @XmlElement(name = "IndLista")
    public void setIndLista(String indLista) {
        this.indLista = indLista;
    }

    public String getIndLista() {
        return indLista;
    }

    @XmlElement(name = "TipoLista")
    public void setTipolista(String tipolista) {
        this.tipolista = tipolista;
    }

    public String getTipolista() {
        return tipolista;
    }

    @XmlElement(name = "TotQtyItem")
    public void setTipoQtyItem(String tipoQtyItem) {
        this.tipoQtyItem = tipoQtyItem;
    }

    public String getTipoQtyItem() {
        return tipoQtyItem;
    }

    @XmlTransient
    public void setBaseGravAmountCab(String baseGravAmountCab) {
        this.baseGravAmountCab = baseGravAmountCab;
    }

    public String getBaseGravAmountCab() {
        return baseGravAmountCab;
    }
    @XmlTransient
    public void setBaseNoGravInaAmountCab(String baseNoGravInaAmountCab) {
        this.baseNoGravInaAmountCab = baseNoGravInaAmountCab;
    }

    public String getBaseNoGravInaAmountCab() {
        return baseNoGravInaAmountCab;
    }
    @XmlTransient
    public void setBaseNoGravExoAmountCab(String baseNoGravExoAmountCab) {
        this.baseNoGravExoAmountCab = baseNoGravExoAmountCab;
    }

    public String getBaseNoGravExoAmountCab() {
        return baseNoGravExoAmountCab;
    }
}
