package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"nroLinDet", "tipoDoc", "serie", "numero", "dscRzonAnulacion"})
public class DetalleDocAnulado {
    String nroLinDet;
    String tipoDoc;
    String serie;
    String numero;
    String dscRzonAnulacion;
    
    public DetalleDocAnulado() {
        super();
    }

    @XmlElement(name = "NroLinDet")
    public void setNroLinDet(String nroLinDet) {
        this.nroLinDet = nroLinDet;
    }

    public String getNroLinDet() {
        return nroLinDet;
    }

    @XmlElement(name = "TipoDoc")
    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    @XmlElement(name = "Serie")
    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSerie() {
        return serie;
    }

    @XmlElement(name = "Numero")
    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    @XmlElement(name = "DscRzonAnulacion")
    public void setDscRzonAnulacion(String dscRzonAnulacion) {
        this.dscRzonAnulacion = dscRzonAnulacion;
    }

    public String getDscRzonAnulacion() {
        return dscRzonAnulacion;
    }
}
