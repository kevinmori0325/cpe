package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tipoDocRecep", "nroDocRecep"})
public class DocRecep {
    String tipoDocRecep;
    String nroDocRecep;
    
    public DocRecep() {
        super();
    }

    @XmlElement(name = "TipoDocRecep")
    public void setTipoDocRecep(String tipoDocRecep) {
        this.tipoDocRecep = tipoDocRecep;
    }

    public String getTipoDocRecep() {
        return tipoDocRecep;
    }

    @XmlElement(name = "NroDocRecep")
    public void setNroDocRecep(String nroDocRecep) {
        this.nroDocRecep = nroDocRecep;
    }

    public String getNroDocRecep() {
        return nroDocRecep;
    }
}
