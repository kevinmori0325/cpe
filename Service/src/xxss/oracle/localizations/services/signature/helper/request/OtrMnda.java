package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"prcOtrMon", "moneda", "fctConv"})
public class OtrMnda {
    String prcOtrMon;
    String moneda;
    String fctConv;
    
    public OtrMnda() {
        super();
    }

    @XmlElement(name = "PrcOtrMon")
    public void setPrcOtrMon(String prcOtrMon) {
        this.prcOtrMon = prcOtrMon;
    }

    public String getPrcOtrMon() {
        return prcOtrMon;
    }

    @XmlElement(name = "Moneda")
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getMoneda() {
        return moneda;
    }

    @XmlElement(name = "FctConv")
    public void setFctConv(String fctConv) {
        this.fctConv = fctConv;
    }

    public String getFctConv() {
        return fctConv;
    }
}
