package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlElement;

public class Extensions {
    StringField[] stringField;
    
    public Extensions() {
        super();
    }

    @XmlElement(name = "StringField")
    public void setStringField(StringField[] stringField) {
        this.stringField = stringField;
    }

    public StringField[] getStringField() {
        return stringField;
    }
    
    public String findValue(String pName) {
        String value = "";
        
        if (stringField != null && stringField.length > 0) {
            for (StringField field : stringField) {
                if (pName.equals(field.getName())) {
                    value = field.getValue();
                    break;
                }
            }
        }
        
        return(value);
    }
}
