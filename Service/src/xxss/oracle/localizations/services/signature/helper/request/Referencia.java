package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"nroLinRef", "tpoDocRef", "serieRef", "numeroRef", "fechaRef", "codRef", "razonRef"})
public class Referencia {
    String nroLinRef;
    String tpoDocRef;
    String serieRef;
    String numeroRef;
    String fechaRef;
    String codRef;
    String razonRef;
        
    public Referencia() {
        super();
    }

    @XmlElement(name = "NroLinRef")
    public void setNroLinRef(String nroLinRef) {
        this.nroLinRef = nroLinRef;
    }

    public String getNroLinRef() {
        return nroLinRef;
    }

    @XmlElement(name = "TpoDocRef")
    public void setTpoDocRef(String tpoDocRef) {
        this.tpoDocRef = tpoDocRef;
    }

    public String getTpoDocRef() {
        return tpoDocRef;
    }

    @XmlElement(name = "NumeroRef")
    public void setNumeroRef(String numeroRef) {
        this.numeroRef = numeroRef;
    }

    public String getNumeroRef() {
        return numeroRef;
    }

    @XmlElement(name = "SerieRef")
    public void setSerieRef(String serieRef) {
        this.serieRef = serieRef;
    }

    public String getSerieRef() {
        return serieRef;
    }

    @XmlElement(name = "FechaRef")
    public void setFechaRef(String fechaRef) {
        this.fechaRef = fechaRef;
    }

    public String getFechaRef() {
        return fechaRef;
    }

    @XmlElement(name = "CodRef")
    public void setCodRef(String codRef) {
        this.codRef = codRef;
    }

    public String getCodRef() {
        return codRef;
    }

    @XmlElement(name = "RazonRef")
    public void setRazonRef(String razonRef) {
        this.razonRef = razonRef;
    }

    public String getRazonRef() {
        return razonRef;
    }
}
