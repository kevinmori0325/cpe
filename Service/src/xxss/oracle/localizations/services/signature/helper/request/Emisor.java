package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"idEmisor", "idEmisorAdd", "nmbEmisor", "nombreEmisor", "domFiscal","tipoIdentEmisor"})
public class Emisor {
    String idEmisor;
    String idEmisorAdd;
    String nmbEmisor;
    NombreEmisor nombreEmisor;
    DomFiscal domFiscal;
    String tipoIdentEmisor;
    public Emisor() {
        super();
    }

    @XmlElement(name = "IDEmisor")
    public void setIdEmisor(String idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getIdEmisor() {
        return idEmisor;
    }

    @XmlElement(name = "NmbEmisor")
    public void setNmbEmisor(String nmbEmisor) {
        this.nmbEmisor = nmbEmisor;
    }

    public String getNmbEmisor() {
        return nmbEmisor;
    }

    @XmlElement(name = "NombreEmisor")
    public void setNombreEmisor(NombreEmisor nombreEmisor) {
        this.nombreEmisor = nombreEmisor;
    }

    public NombreEmisor getNombreEmisor() {
        return nombreEmisor;
    }

    @XmlElement(name = "DomFiscal")
    public void setDomFiscal(DomFiscal domFiscal) {
        this.domFiscal = domFiscal;
    }

    public DomFiscal getDomFiscal() {
        return domFiscal;
    }

    @XmlElement(name = "IDEmisorAdd")
    public void setIdEmisorAdd(String idEmisorAdd) {
        this.idEmisorAdd = idEmisorAdd;
    }

    public String getIdEmisorAdd() {
        return idEmisorAdd;
    }

    @XmlElement(name = "TipoIdentEmiso")
    public void setTipoIdentEmisor(String tipoIdentEmisor) {
        this.tipoIdentEmisor = tipoIdentEmisor;
    }

    public String getTipoIdentEmisor() {
        return tipoIdentEmisor;
    }
}
