package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tipoDocTransp", "nroDocTransp"})
public class DocTransp {
    String tipoDocTransp;
    String nroDocTransp;
    
    public DocTransp() {
        super();
    }

    @XmlElement(name = "TipoDocTransp")
    public void setTipoDocTransp(String tipoDocTransp) {
        this.tipoDocTransp = tipoDocTransp;
    }

    public String getTipoDocTransp() {
        return tipoDocTransp;
    }

    @XmlElement(name = "NroDocTransp")
    public void setNroDocTransp(String nroDocTransp) {
        this.nroDocTransp = nroDocTransp;
    }

    public String getNroDocTransp() {
        return nroDocTransp;
    }
}
