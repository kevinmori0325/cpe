package xxss.oracle.localizations.services.signature.helper.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import xxss.oracle.localizations.pe.app.utils.CDATAAdapter;

@XmlRootElement(name = "ProcessResult")
//@XmlType(propOrder={"process", "hasError", "error", "returnValue", "textData"})
public class ProcessResult {
    Process[] process;
    String hasError;
    Error[] error;
    String returnValue;
    
    String textData;
    
    public ProcessResult() {
        super();
    }

    @XmlElement(name = "Process")
    public void setProcess(Process[] process) {
        this.process = process;
    }

    public Process[] getProcess() {
        return process;
    }

    @XmlElement(name = "HasError")
    public void setHasError(String hasError) {
        this.hasError = hasError;
    }

    public String getHasError() {
        return hasError;
    }

    @XmlElement(name = "Error")
    public void setError(Error[] error) {
        this.error = error;
    }

    public Error[] getError() {
        return error;
    }

    @XmlElement(name = "ReturnValue")
    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public String getReturnValue() {
        return returnValue;
    }

    @XmlElement(name = "TextData")
    @XmlJavaTypeAdapter(value=CDATAAdapter.class)
    public void setTextData(String textData) {
        this.textData = textData;
    }

    public String getTextData() {
        return textData;
    }
}
