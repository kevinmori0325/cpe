package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"tpoCodigo", "vlrCodigo"})
public class CdgItem {
    String tpoCodigo;
    String vlrCodigo;
    
    public CdgItem() {
        super();
    }

    @XmlElement(name = "TpoCodigo")
    public void setTpoCodigo(String tpoCodigo) {
        this.tpoCodigo = tpoCodigo;
    }

    public String getTpoCodigo() {
        return tpoCodigo;
    }

    @XmlElement(name = "VlrCodigo")
    public void setVlrCodigo(String vlrCodigo) {
        this.vlrCodigo = vlrCodigo;
    }

    public String getVlrCodigo() {
        return vlrCodigo;
    }
}
