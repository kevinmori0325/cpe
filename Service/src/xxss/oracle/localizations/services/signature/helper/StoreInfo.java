package xxss.oracle.localizations.services.signature.helper;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

//@XmlType(propOrder={"timeStamp", "url"})
public class StoreInfo {
    String timeStamp;
    String url;
    
    public StoreInfo() {
        super();
    }

    @XmlElement(name = "TimeStamp")
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    @XmlElement(name = "Url")
    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
