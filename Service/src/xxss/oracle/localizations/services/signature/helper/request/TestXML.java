package xxss.oracle.localizations.services.signature.helper.request;

import java.io.StringWriter;

import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class TestXML {
    public TestXML() {
        super();
    }

    public static void main(String[] args) {
        Encabezado encab = new Encabezado();
        
        IdDoc idDoc = new IdDoc();
        idDoc.setAmbiente("01");
        idDoc.setTipoEmision("NORMAL");
        idDoc.setContenidoTC("0");
        idDoc.setTipo("01");
        idDoc.setSerie("FHA1");
        idDoc.setNumero("2");
        idDoc.setEstado("ORIGINAL");
        idDoc.setNumeroInterno("1246982");
        idDoc.setFechaEmis("2016-02-01T00:00:00");
        
        encab.setIdDoc(idDoc);
        
        Emisor emisor = new Emisor();
        emisor.setIdEmisor("20556695548");
        emisor.setNmbEmisor("SOUTH CONSULTING SIGNATURE PERU S.A.C");
        emisor.setNombreEmisor(new NombreEmisor());
        emisor.getNombreEmisor().setPrimerNombre("Laboratorios Roemmers S.A.");
        emisor.setDomFiscal(new DomFiscal());
        emisor.getDomFiscal().setCalle("AV.REPUBLICA DE PANAMA 3570");
        emisor.getDomFiscal().setCiudad("San Isidro, LIMA");
        emisor.getDomFiscal().setCodigoPostal("150131");
        emisor.getDomFiscal().setPais("PE");
        
        encab.setEmisor(emisor);
        
        Receptor receptor = new Receptor();
        receptor.setDocRecep(new DocRecep());
        receptor.getDocRecep().setTipoDocRecep("6");
        receptor.getDocRecep().setNroDocRecep("");
        receptor.setNmbRecep("CUSTOMER");
        receptor.setNombreRecep(new NombreRecep());
        receptor.getNombreRecep().setPrimerNombre("CUSTOMER");
     //   receptor.setContacto("mail@mail.com");
        receptor.setDomFiscalRcp(new DomFiscalRcp());
        receptor.getDomFiscalRcp().setCalle("Av Alfredo Salazar 247 - Mirafllores");
        receptor.getDomFiscalRcp().setCiudad("LIMA");
        receptor.getDomFiscalRcp().setPais("PE");
        
        encab.setReceptor(receptor);
        
        Totales totales = new Totales();
        totales.setMoneda("PEN");
        totales.setMntDcto("0.00");
        totales.setMntRcgo("0.00");
        totales.setVlrPagar("2000.00");
        totales.setVlrPalabras("DOS MIL CON 00/100 NUEVOS SOLES");
        
        //ArrayList<TotSubMonto> totSubmontoArray = new ArrayList<TotSubMonto>();
        //TotSubMonto totSubMonto;
        //totSubmontoArray.add(e);        
        //totales.setTotSubMonto((TotSubMonto[])totSubmontoArray.toArray());
        
        TotSubMonto[] totSubMonto = new TotSubMonto[7];
        totSubMonto[0] = new TotSubMonto();
        totSubMonto[0].setTipo("1001");
        totSubMonto[0].setMontoConcepto("1694.92");
        totSubMonto[1] = new TotSubMonto();
        totSubMonto[1].setTipo("1002");
        totSubMonto[1].setMontoConcepto("0.00");
        totSubMonto[2] = new TotSubMonto();
        totSubMonto[2].setTipo("1003");
        totSubMonto[2].setMontoConcepto("0.00");
        totSubMonto[3] = new TotSubMonto();
        totSubMonto[3].setTipo("1004");
        totSubMonto[3].setMontoConcepto("0.00");
        totSubMonto[4] = new TotSubMonto();
        totSubMonto[4].setTipo("2001");
        totSubMonto[4].setMontoConcepto("0.00");
        totSubMonto[4].setMontoTotal("0.00");
        totSubMonto[5] = new TotSubMonto();
        totSubMonto[5].setTipo("2003");
        totSubMonto[5].setTasaConcepto("0.00");
        totSubMonto[5].setMontoConcepto("0.00");
        totSubMonto[6] = new TotSubMonto();
        totSubMonto[6].setTipo("2005");
        totSubMonto[6].setMontoConcepto("0.00");
        
        totales.setTotSubMonto(totSubMonto);

        encab.setTotales(totales);
        
        Impuestos[] impuestos = new Impuestos[1];
        impuestos[0] = new Impuestos();
        impuestos[0].setTipoImp("1000");
        impuestos[0].setTasaImp("18");
        impuestos[0].setMontoImp("305.08");
        
        encab.setImpuestos(impuestos);
        
        Detalle[] detalle = new Detalle[1];
        detalle[0] = new Detalle();
        detalle[0].setNroLinDet("1");
        detalle[0].setCdgItem(new CdgItem[] {new CdgItem()} );
        detalle[0].getCdgItem()[0].setTpoCodigo("INT");
        detalle[0].getCdgItem()[0].setVlrCodigo("BRK/COMM");
        detalle[0].setDscItem("COMISIONES POR VENTA DE PRODUCTO");
        detalle[0].setQtyItem("2");
        detalle[0].setUnmdItem("MT");
        detalle[0].setPrcBrutoItem("847.46");
        detalle[0].setPrcNetoItem("1000.00");
        detalle[0].setImpuestosDet(new ImpuestosDet[] {new ImpuestosDet()});
        detalle[0].getImpuestosDet()[0].setTipoImp("1000");
        detalle[0].getImpuestosDet()[0].setCodTasaImp("10");
        detalle[0].getImpuestosDet()[0].setTasaImp("18");
        detalle[0].getImpuestosDet()[0].setMontoImp("305.08");
        detalle[0].setDescuentoPct("0.00");
        detalle[0].setMontoBrutoItem("1694.92");
        detalle[0].setMontoNetoItem("1694.92");
        detalle[0].setMontoTotalItem("1694.92");
        
    /*    Referencia[] referencia = new Referencia[2];
        referencia[0] = new Referencia();
        referencia[0].setNroLinRef("1");
        referencia[0].setTpoDocRef("91");
        referencia[0].setNumeroRef("M26693");
        referencia[1] = new Referencia();
        referencia[1].setNroLinRef("2");
        referencia[1].setTpoDocRef("09");
        referencia[1].setSerieRef("0005");
        referencia[1].setNumeroRef("85105");
     */   
        Personalizados personalizados = new Personalizados();
        personalizados.setDocPersonalizado(new DocPersonalizado());
        personalizados.getDocPersonalizado().setDteId("ID0000005");
        
        Campo[] campoString = new Campo[3];
        campoString[0] = new Campo();
        campoString[0].setName("999");
        campoString[0].setValue("Prueba");
        campoString[1] = new Campo();
        campoString[1].setName("Mail");
        campoString[1].setValue("kevin@gmail.com");
     /*   campoString[2] = new Campo();
        campoString[2].setName("Vencimiento");
        campoString[2].setValue("30 d�as");
        */
        personalizados.getDocPersonalizado().setCampoString(campoString);
        
        Documento doc = new Documento();
        doc.setId("ID0000005");
        doc.setEncabezado(encab);
        doc.setDetalle(detalle);
  //      doc.setReferencia(referencia);

        DTE dte = new DTE();
        dte.setDocumento(doc);
        dte.setPersonalizados(personalizados);
        
 /*       Anulacion anulacion = new Anulacion();
        anulacion.setEncabezado(new Encabezado());
        anulacion.getEncabezado().setTipo("10");
        anulacion.getEncabezado().setNumeroInterno("3874985");
        anulacion.getEncabezado().setFechaRef("2015-09-01T00:00:00");
        anulacion.getEncabezado().setFechaEmis("2015-09-04T00:00:00");
        anulacion.getEncabezado().setEmisor(new Emisor());
        anulacion.getEncabezado().getEmisor().setIdEmisor("20100034582");
        anulacion.getEncabezado().getEmisor().setIdEmisorAdd("6");
        anulacion.getEncabezado().getEmisor().setNmbEmisor("INDUSTRIAL ALPAMAYO S A");
        
        anulacion.setDetalleDocAnulado(new DetalleDocAnulado());
        anulacion.getDetalleDocAnulado().setNroLinDet("1");
        anulacion.getDetalleDocAnulado().setTipoDoc("01");
        anulacion.getDetalleDocAnulado().setSerie("F604");
        anulacion.getDetalleDocAnulado().setNumero("131");
        anulacion.getDetalleDocAnulado().setDscRzonAnulacion("Anulacion de la Operacion");
       */ 
        StringWriter swDTE = new StringWriter();
        StringWriter swAnulacion = new StringWriter();

        try {

            JAXBContext jaxbContextDTE = JAXBContext.newInstance(DTE.class);
            Marshaller jaxbMarshallerDTE = jaxbContextDTE.createMarshaller();

            // output pretty printed
            jaxbMarshallerDTE.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshallerDTE.marshal(dte, swDTE);
            
            System.out.println(swDTE.toString());
            System.out.println("");
            
            JAXBContext jaxbContextAnul = JAXBContext.newInstance(Anulacion.class);
            Marshaller jaxbMarshallerAnul = jaxbContextAnul.createMarshaller();

            // output pretty printed
            jaxbMarshallerAnul.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

         /*   jaxbMarshallerAnul.marshal(anulacion, swAnulacion);
            System.out.println(swAnulacion.toString());
            System.out.println("");
*/
            

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
