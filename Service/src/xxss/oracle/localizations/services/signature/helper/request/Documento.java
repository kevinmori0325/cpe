package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"encabezado", "detalle", "referencia", "tax"})
public class Documento {
    String Id;
    Encabezado encabezado;
    Detalle[] detalle;
    Referencia[] referencia;
    Tax[] tax;
    
    public Documento() {
        super();
    }
    
    @XmlElement(name = "Encabezado")
    public void setEncabezado(Encabezado encabezado) {
        this.encabezado = encabezado;
    }

    public Encabezado getEncabezado() {
        return encabezado;
    }
    
    @XmlElement(name = "Detalle")
    public void setDetalle(Detalle[] detalle) {
        this.detalle = detalle;
    }

    public Detalle[] getDetalle() {
        return detalle;
    }

    @XmlElement(name = "Referencia")
    public void setReferencia(Referencia[] referencia) {
        this.referencia = referencia;
    }

    public Referencia[] getReferencia() {
        return referencia;
    }
    
    @XmlAttribute(name = "ID")
    public void setId(String Id) {
        this.Id = Id;
    }

    public String getId() {
        return Id;
    }

    @XmlElement(name = "Tax")
    public void setTax(Tax[] tax) {
        this.tax = tax;
    }

    public Tax[] getTax() {
        return tax;
    }
}
