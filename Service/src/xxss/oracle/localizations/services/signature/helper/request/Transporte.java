package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"docTransp", "nmbTransp", "medioTransporte"})
public class Transporte {
    DocTransp docTransp;
    String nmbTransp;
    MedioTransporte medioTransporte;
    
    public Transporte() {
        super();
    }

    @XmlElement(name = "DocTransp")
    public void setDocTransp(DocTransp docTransp) {
        this.docTransp = docTransp;
    }

    public DocTransp getDocTransp() {
        return docTransp;
    }

    @XmlElement(name = "NmbTransp")
    public void setNmbTransp(String nmbTransp) {
        this.nmbTransp = nmbTransp;
    }

    public String getNmbTransp() {
        return nmbTransp;
    }

    @XmlElement(name = "MedioTransporte")
    public void setMedioTransporte(MedioTransporte medioTransporte) {
        this.medioTransporte = medioTransporte;
    }

    public MedioTransporte getMedioTransporte() {
        return medioTransporte;
    }
}
