package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"calle", "ciudad", "municipio", "distrito", "pais", "codigoPostal","provincia"})
public class DomFiscal {
    String calle;
    String ciudad;
    String municipio;
    String distrito;
    String pais;
    String codigoPostal;
    String provincia;

    
    public DomFiscal() {
        super();
    }

    @XmlElement(name = "Calle")
    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCalle() {
        return calle;
    }

    @XmlElement(name = "Ciudad")
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    @XmlElement(name = "Pais")
    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getPais() {
        return pais;
    }

    @XmlElement(name = "CodigoPostal")
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    @XmlElement(name = "Municipio")
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getMunicipio() {
        return municipio;
    }

    @XmlElement(name = "Distrito")
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDistrito() {
        return distrito;
    }

    @XmlElement(name = "Provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getProvincia() {
        return provincia;
    }
}
