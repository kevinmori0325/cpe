package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"dteId","campoString","campoNumero","campoFecha","custDetalle"})
public class DocPersonalizado {
    String dteId;
    Campo[] campoString; 
    Campo[] campoNumero;
    Campo[] campoFecha;
    CustDetalle[] custDetalle;
    
    public DocPersonalizado() {
        super();
    }

    @XmlAttribute(name = "dteID")
    public void setDteId(String dteId) {
        this.dteId = dteId;
    }

    public String getDteId() {
        return dteId;
    }

    @XmlElement(name = "campoString")
    public void setCampoString(Campo[] campoString) {
        this.campoString = campoString;
    }

    public Campo[] getCampoString() {
        return campoString;
    }

    @XmlElement(name = "campoNumero")
    public void setCampoNumero(Campo[] campoNumero) {
        this.campoNumero = campoNumero;
    }

    public Campo[] getCampoNumero() {
        return campoNumero;
    }

    @XmlElement(name = "campoFecha")
    public void setCampoFecha(Campo[] campoFecha) {
        this.campoFecha = campoFecha;
    }

    public Campo[] getCampoFecha() {
        return campoFecha;
    }
    @XmlElement(name = "CustDetalle")
    public void setCustDetalle(CustDetalle[] custDetalle) {
        this.custDetalle = custDetalle;
    }

    public CustDetalle[] getCustDetalle() {
        return custDetalle;
    }
}
