package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder =
         { "tipo", "motivo", "descripcion", "indicador", "numero", "nroExp", "idOrigen", "origen", "idDestino",
           "destino", "periodoDesde", "qtyTraslado" })
public class Traslado {
    String tipo;
    String motivo;
    String descripcion;
    String indicador;
    String numero;
    String nroExp;
    String idOrigen;
    String origen;
    String idDestino;
    String destino;
    String periodoDesde;
    String qtyTraslado;

    public Traslado() {
        super();
    }

    @XmlElement(name = "Tipo")
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    @XmlElement(name = "Motivo")
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getMotivo() {
        return motivo;
    }

    @XmlElement(name = "Descripcion")
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @XmlElement(name = "Indicador")
    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getIndicador() {
        return indicador;
    }

    @XmlElement(name = "Numero")
    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    @XmlElement(name = "NroExp")
    public void setNroExp(String nroExp) {
        this.nroExp = nroExp;
    }

    public String getNroExp() {
        return nroExp;
    }

    @XmlElement(name = "IDorigen")
    public void setIdOrigen(String idOrigen) {
        this.idOrigen = idOrigen;
    }

    public String getIdOrigen() {
        return idOrigen;
    }

    @XmlElement(name = "Origen")
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getOrigen() {
        return origen;
    }

    @XmlElement(name = "IDdestino")
    public void setIdDestino(String idDestino) {
        this.idDestino = idDestino;
    }

    public String getIdDestino() {
        return idDestino;
    }

    @XmlElement(name = "Destino")
    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getDestino() {
        return destino;
    }

    @XmlElement(name = "PeriodoDesde")
    public void setPeriodoDesde(String periodoDesde) {
        this.periodoDesde = periodoDesde;
    }

    public String getPeriodoDesde() {
        return periodoDesde;
    }

    @XmlElement(name = "QtyTraslado")
    public void setQtyTraslado(String qtyTraslado) {
        this.qtyTraslado = qtyTraslado;
    }

    public String getQtyTraslado() {
        return qtyTraslado;
    }
}
