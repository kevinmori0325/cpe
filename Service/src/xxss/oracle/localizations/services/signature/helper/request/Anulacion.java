package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Anulacion")
@XmlType(propOrder={"encabezado", "detalleDocAnulado"})
public class Anulacion {
    Encabezado encabezado;
    DetalleDocAnulado detalleDocAnulado;
    
    public Anulacion() {
        super();
    }
    
    @XmlElement(name = "Encabezado")
    public void setEncabezado(Encabezado encabezado) {
        this.encabezado = encabezado;
    }

    public Encabezado getEncabezado() {
        return encabezado;
    }
    
    @XmlElement(name = "DetalleDocAnulado")
    public void setDetalleDocAnulado(DetalleDocAnulado detalleDocAnulado) {
        this.detalleDocAnulado = detalleDocAnulado;
    }

    public DetalleDocAnulado getDetalleDocAnulado() {
        return detalleDocAnulado;
    }
}
