package xxss.oracle.localizations.services.signature.helper.request;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"metodoTransp", "nroRefTransp"})
public class MedioTransporte {
    String metodoTransp;
    String nroRefTransp;
    
    public MedioTransporte() {
        super();
    }

    @XmlElement(name = "MetodoTransp")
    public void setMetodoTransp(String metodoTransp) {
        this.metodoTransp = metodoTransp;
    }

    public String getMetodoTransp() {
        return metodoTransp;
    }

    @XmlElement(name = "NroRefTransp")
    public void setNroRefTransp(String nroRefTransp) {
        this.nroRefTransp = nroRefTransp;
    }

    public String getNroRefTransp() {
        return nroRefTransp;
    }
}
