
package xxss.oracle.localizations.services.signaturews.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xxss.oracle.localizations.services.signaturews.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ExecuteReceiveMessageResponse_QNAME = new QName("http://gosocket.ws.pe.localizations.oracle/", "executeReceiveMessageResponse");
    private final static QName _ExecuteConvertDocument_QNAME = new QName("http://gosocket.ws.pe.localizations.oracle/", "executeConvertDocument");
    private final static QName _ExecuteReceiveMessage_QNAME = new QName("http://gosocket.ws.pe.localizations.oracle/", "executeReceiveMessage");
    private final static QName _ExecuteConvertDocumentResponse_QNAME = new QName("http://gosocket.ws.pe.localizations.oracle/", "executeConvertDocumentResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xxss.oracle.localizations.services.signaturews.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteReceiveMessageResponse }
     * 
     */
    public ExecuteReceiveMessageResponse createExecuteReceiveMessageResponse() {
        return new ExecuteReceiveMessageResponse();
    }

    /**
     * Create an instance of {@link ExecuteReceiveMessage }
     * 
     */
    public ExecuteReceiveMessage createExecuteReceiveMessage() {
        return new ExecuteReceiveMessage();
    }

    /**
     * Create an instance of {@link ExecuteConvertDocument }
     * 
     */
    public ExecuteConvertDocument createExecuteConvertDocument() {
        return new ExecuteConvertDocument();
    }

    /**
     * Create an instance of {@link ExecuteConvertDocumentResponse }
     * 
     */
    public ExecuteConvertDocumentResponse createExecuteConvertDocumentResponse() {
        return new ExecuteConvertDocumentResponse();
    }

    /**
     * Create an instance of {@link ConvertDocumentResponse }
     * 
     */
    public ConvertDocumentResponse createConvertDocumentResponse() {
        return new ConvertDocumentResponse();
    }

    /**
     * Create an instance of {@link ConvertDocument }
     * 
     */
    public ConvertDocument createConvertDocument() {
        return new ConvertDocument();
    }

    /**
     * Create an instance of {@link ReceiveMessage }
     * 
     */
    public ReceiveMessage createReceiveMessage() {
        return new ReceiveMessage();
    }

    /**
     * Create an instance of {@link ReceiveMessageResponse }
     * 
     */
    public ReceiveMessageResponse createReceiveMessageResponse() {
        return new ReceiveMessageResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteReceiveMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gosocket.ws.pe.localizations.oracle/", name = "executeReceiveMessageResponse")
    public JAXBElement<ExecuteReceiveMessageResponse> createExecuteReceiveMessageResponse(ExecuteReceiveMessageResponse value) {
        return new JAXBElement<ExecuteReceiveMessageResponse>(_ExecuteReceiveMessageResponse_QNAME, ExecuteReceiveMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteConvertDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gosocket.ws.pe.localizations.oracle/", name = "executeConvertDocument")
    public JAXBElement<ExecuteConvertDocument> createExecuteConvertDocument(ExecuteConvertDocument value) {
        return new JAXBElement<ExecuteConvertDocument>(_ExecuteConvertDocument_QNAME, ExecuteConvertDocument.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteReceiveMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gosocket.ws.pe.localizations.oracle/", name = "executeReceiveMessage")
    public JAXBElement<ExecuteReceiveMessage> createExecuteReceiveMessage(ExecuteReceiveMessage value) {
        return new JAXBElement<ExecuteReceiveMessage>(_ExecuteReceiveMessage_QNAME, ExecuteReceiveMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExecuteConvertDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gosocket.ws.pe.localizations.oracle/", name = "executeConvertDocumentResponse")
    public JAXBElement<ExecuteConvertDocumentResponse> createExecuteConvertDocumentResponse(ExecuteConvertDocumentResponse value) {
        return new JAXBElement<ExecuteConvertDocumentResponse>(_ExecuteConvertDocumentResponse_QNAME, ExecuteConvertDocumentResponse.class, null, value);
    }

}
