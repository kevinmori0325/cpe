package xxss.oracle.localizations.services.signaturews.proxy;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebService(wsdlLocation="http://129.144.31.22/PeGoSocketWS/SignatureWSSoap12HttpPort?wsdl",
  targetNamespace="http://gosocket.ws.pe.localizations.oracle/", name="SignatureWS")
@XmlSeeAlso(
  { xxss.oracle.localizations.services.signaturews.types.ObjectFactory.class })
public interface SignatureWS
{
  @WebMethod
  @Action(input="http://gosocket.ws.pe.localizations.oracle/SignatureWS/executeConvertDocumentRequest",
    output="http://gosocket.ws.pe.localizations.oracle/SignatureWS/executeConvertDocumentResponse")
  @ResponseWrapper(localName="executeConvertDocumentResponse",
    targetNamespace="http://gosocket.ws.pe.localizations.oracle/",
    className="xxss.oracle.localizations.services.signaturews.types.ExecuteConvertDocumentResponse")
  @RequestWrapper(localName="executeConvertDocument", targetNamespace="http://gosocket.ws.pe.localizations.oracle/",
    className="xxss.oracle.localizations.services.signaturews.types.ExecuteConvertDocument")
  @WebResult(targetNamespace="")
  public xxss.oracle.localizations.services.signaturews.types.ConvertDocumentResponse executeConvertDocument(@WebParam(targetNamespace="",
      name="arg0")
    xxss.oracle.localizations.services.signaturews.types.ConvertDocument arg0);

  @WebMethod
  @Action(input="http://gosocket.ws.pe.localizations.oracle/SignatureWS/executeReceiveMessageRequest",
    output="http://gosocket.ws.pe.localizations.oracle/SignatureWS/executeReceiveMessageResponse")
  @ResponseWrapper(localName="executeReceiveMessageResponse",
    targetNamespace="http://gosocket.ws.pe.localizations.oracle/",
    className="xxss.oracle.localizations.services.signaturews.types.ExecuteReceiveMessageResponse")
  @RequestWrapper(localName="executeReceiveMessage", targetNamespace="http://gosocket.ws.pe.localizations.oracle/",
    className="xxss.oracle.localizations.services.signaturews.types.ExecuteReceiveMessage")
  @WebResult(targetNamespace="")
  public xxss.oracle.localizations.services.signaturews.types.ReceiveMessageResponse executeReceiveMessage(@WebParam(targetNamespace="",
      name="arg0")
    xxss.oracle.localizations.services.signaturews.types.ReceiveMessage arg0);
}
