
package xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="content" type="{http://xmlns.oracle.com/adf/svc/types/}base64Binary-DataHandler" minOccurs="0"/>
 *         &lt;element name="documentAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentDetails", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", propOrder = {
    "fileName",
    "contentType",
    "content",
    "documentAccount",
    "documentTitle"
})
public class DocumentDetails {

    @XmlElementRef(name = "fileName", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", type = JAXBElement.class)
    protected JAXBElement<String> fileName;
    @XmlElementRef(name = "contentType", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", type = JAXBElement.class)
    protected JAXBElement<String> contentType;
    @XmlElementRef(name = "content", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", type = JAXBElement.class)
    protected JAXBElement<byte[]> content;
    @XmlElementRef(name = "documentAccount", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", type = JAXBElement.class)
    protected JAXBElement<String> documentAccount;
    @XmlElementRef(name = "documentTitle", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", type = JAXBElement.class)
    protected JAXBElement<String> documentTitle;

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFileName(JAXBElement<String> value) {
        this.fileName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the contentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContentType() {
        return contentType;
    }

    /**
     * Sets the value of the contentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContentType(JAXBElement<String> value) {
        this.contentType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setContent(JAXBElement<byte[]> value) {
        this.content = ((JAXBElement<byte[]> ) value);
    }

    /**
     * Gets the value of the documentAccount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentAccount() {
        return documentAccount;
    }

    /**
     * Sets the value of the documentAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentAccount(JAXBElement<String> value) {
        this.documentAccount = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the documentTitle property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentTitle() {
        return documentTitle;
    }

    /**
     * Sets the value of the documentTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentTitle(JAXBElement<String> value) {
        this.documentTitle = ((JAXBElement<String> ) value);
    }

}
