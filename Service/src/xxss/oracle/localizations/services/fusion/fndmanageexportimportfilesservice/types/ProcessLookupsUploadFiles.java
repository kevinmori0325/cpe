
package xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lookupTypesFileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="lookupCodesFileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="viewApplicationID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lookupTypesFileID",
    "lookupCodesFileID",
    "viewApplicationID"
})
@XmlRootElement(name = "processLookupsUploadFiles", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
public class ProcessLookupsUploadFiles {

    @XmlElement(namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
    protected long lookupTypesFileID;
    @XmlElement(namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
    protected long lookupCodesFileID;
    @XmlElement(namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
    protected long viewApplicationID;

    /**
     * Gets the value of the lookupTypesFileID property.
     * 
     */
    public long getLookupTypesFileID() {
        return lookupTypesFileID;
    }

    /**
     * Sets the value of the lookupTypesFileID property.
     * 
     */
    public void setLookupTypesFileID(long value) {
        this.lookupTypesFileID = value;
    }

    /**
     * Gets the value of the lookupCodesFileID property.
     * 
     */
    public long getLookupCodesFileID() {
        return lookupCodesFileID;
    }

    /**
     * Sets the value of the lookupCodesFileID property.
     * 
     */
    public void setLookupCodesFileID(long value) {
        this.lookupCodesFileID = value;
    }

    /**
     * Gets the value of the viewApplicationID property.
     * 
     */
    public long getViewApplicationID() {
        return viewApplicationID;
    }

    /**
     * Sets the value of the viewApplicationID property.
     * 
     */
    public void setViewApplicationID(long value) {
        this.viewApplicationID = value;
    }

}
