
package xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userProfilesFileID" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userProfilesFileID"
})
@XmlRootElement(name = "processUserProfileValuesFile", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
public class ProcessUserProfileValuesFile {

    @XmlElement(namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
    protected long userProfilesFileID;

    /**
     * Gets the value of the userProfilesFileID property.
     * 
     */
    public long getUserProfilesFileID() {
        return userProfilesFileID;
    }

    /**
     * Sets the value of the userProfilesFileID property.
     * 
     */
    public void setUserProfilesFileID(long value) {
        this.userProfilesFileID = value;
    }

}
