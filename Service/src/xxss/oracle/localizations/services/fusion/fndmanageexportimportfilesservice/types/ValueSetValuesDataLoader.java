
package xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fileIdAtRepository" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fileIdAtRepository"
})
@XmlRootElement(name = "valueSetValuesDataLoader", namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
public class ValueSetValuesDataLoader {

    @XmlElement(namespace = "http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/types/")
    protected long fileIdAtRepository;

    /**
     * Gets the value of the fileIdAtRepository property.
     * 
     */
    public long getFileIdAtRepository() {
        return fileIdAtRepository;
    }

    /**
     * Sets the value of the fileIdAtRepository property.
     * 
     */
    public void setFileIdAtRepository(long value) {
        this.fileIdAtRepository = value;
    }

}
