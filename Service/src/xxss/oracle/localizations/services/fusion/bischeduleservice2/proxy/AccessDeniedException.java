package xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy;

import javax.xml.ws.WebFault;

@WebFault(faultBean="xxss.oracle.localizations.services.fusion.bischeduleservice2.types.AccessDeniedException",
  targetNamespace="http://xmlns.oracle.com/oxp/service/v2", name="fault")
public class AccessDeniedException
  extends Exception
{
  private xxss.oracle.localizations.services.fusion.bischeduleservice2.types.AccessDeniedException faultInfo;

  public AccessDeniedException(String message,
                               xxss.oracle.localizations.services.fusion.bischeduleservice2.types.AccessDeniedException faultInfo)
  {
    super(message);
    this.faultInfo = faultInfo;
  }

  public AccessDeniedException(String message,
                               xxss.oracle.localizations.services.fusion.bischeduleservice2.types.AccessDeniedException faultInfo,
                               Throwable t)
  {
    super(message,t);
    this.faultInfo = faultInfo;
  }

  public xxss.oracle.localizations.services.fusion.bischeduleservice2.types.AccessDeniedException getFaultInfo()
  {
    return faultInfo;
  }

  public void setFaultInfo(xxss.oracle.localizations.services.fusion.bischeduleservice2.types.AccessDeniedException faultInfo)
  {
    this.faultInfo = faultInfo;
  }
}
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)
