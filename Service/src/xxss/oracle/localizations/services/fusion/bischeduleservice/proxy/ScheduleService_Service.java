package xxss.oracle.localizations.services.fusion.bischeduleservice.proxy;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebServiceClient(wsdlLocation="https://ebxu-test.bi.us2.oraclecloud.com/xmlpserver/services/v2/ScheduleService?wsdl",
  targetNamespace="http://xmlns.oracle.com/oxp/service/v2", name="ScheduleService")
public class ScheduleService_Service
  extends Service
{
  private static URL wsdlLocationURL;

  private static Logger logger;
  static
  {
    try
    {
      logger = Logger.getLogger("xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService_Service");
      URL baseUrl = ScheduleService_Service.class.getResource(".");
      if (baseUrl == null)
      {
        wsdlLocationURL =
            ScheduleService_Service.class.getResource("https://ebxu-test.bi.us2.oraclecloud.com/xmlpserver/services/v2/ScheduleService?wsdl");
        if (wsdlLocationURL == null)
        {
          baseUrl = new File(".").toURL();
          wsdlLocationURL =
              new URL(baseUrl, "https://ebxu-test.bi.us2.oraclecloud.com/xmlpserver/services/v2/ScheduleService?wsdl");
        }
      }
      else
      {
                if (!baseUrl.getPath().endsWith("/")) {
         baseUrl = new URL(baseUrl, baseUrl.getPath() + "/");
}
                wsdlLocationURL =
            new URL(baseUrl, "https://ebxu-test.bi.us2.oraclecloud.com/xmlpserver/services/v2/ScheduleService?wsdl");
      }
    }
    catch (MalformedURLException e)
    {
      logger.log(Level.ALL,
          "Failed to create wsdlLocationURL using https://ebxu-test.bi.us2.oraclecloud.com/xmlpserver/services/v2/ScheduleService?wsdl",
          e);
    }
  }

  public ScheduleService_Service()
  {
    super(wsdlLocationURL,
          new QName("http://xmlns.oracle.com/oxp/service/v2",
                    "ScheduleService"));
  }

  public ScheduleService_Service(URL wsdlLocation, QName serviceName)
  {
    super(wsdlLocation, serviceName);
  }

  @WebEndpoint(name="ScheduleService")
  public xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService getScheduleService()
  {
    return (xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService) super.getPort(new QName("http://xmlns.oracle.com/oxp/service/v2",
                                                                                                                       "ScheduleService"),
                                                                                                             xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService.class);
  }

  @WebEndpoint(name="ScheduleService")
  public xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService getScheduleService(WebServiceFeature... features)
  {
    return (xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService) super.getPort(new QName("http://xmlns.oracle.com/oxp/service/v2",
                                                                                                                       "ScheduleService"),
                                                                                                             xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService.class,
                                                                                                             features);
  }
}
