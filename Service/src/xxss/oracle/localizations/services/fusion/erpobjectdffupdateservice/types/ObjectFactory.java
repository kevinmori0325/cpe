
package xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Fault_QNAME = new QName("http://xmlns.oracle.com/oracleas/schema/oracle-fault-11_0", "Fault");
    private final static QName _DocumentDetails_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "documentDetails");
    private final static QName _FindCriteria_QNAME = new QName("http://xmlns.oracle.com/adf/svc/types/", "findCriteria");
    private final static QName _Types_QNAME = new QName("commonj.sdo", "types");
    private final static QName _Type_QNAME = new QName("commonj.sdo", "type");
    private final static QName _FindControl_QNAME = new QName("http://xmlns.oracle.com/adf/svc/types/", "findControl");
    private final static QName _DataObject_QNAME = new QName("commonj.sdo", "dataObject");
    private final static QName _ServiceErrorMessage_QNAME = new QName("http://xmlns.oracle.com/adf/svc/errors/", "ServiceErrorMessage");
    private final static QName _ProcessControl_QNAME = new QName("http://xmlns.oracle.com/adf/svc/types/", "processControl");
    private final static QName _ErpObjectDetails_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "erpObjectDetails");
    private final static QName _Datagraph_QNAME = new QName("commonj.sdo", "datagraph");
    private final static QName _UpdateDffEntityDetailsAsyncObject_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", "object");
    private final static QName _UpdateDffEntityDetailsAsyncCallbackURL_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", "callbackURL");
    private final static QName _UpdateDffEntityDetailsAsyncNotificationCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", "notificationCode");
    private final static QName _UpdateDffEntityDetailsAsyncDocument_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", "document");
    private final static QName _DocumentDetailsDocumentAccount_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DocumentAccount");
    private final static QName _DocumentDetailsContentType_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "ContentType");
    private final static QName _DocumentDetailsDocumentId_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DocumentId");
    private final static QName _DocumentDetailsDocumentName_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DocumentName");
    private final static QName _DocumentDetailsDocumentAuthor_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DocumentAuthor");
    private final static QName _DocumentDetailsDocumentSecurityGroup_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DocumentSecurityGroup");
    private final static QName _DocumentDetailsDocumentTitle_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DocumentTitle");
    private final static QName _ErpObjectDetailsUserKeyB_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "UserKeyB");
    private final static QName _ErpObjectDetailsUserKeyC_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "UserKeyC");
    private final static QName _ErpObjectDetailsEntityName_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "EntityName");
    private final static QName _ErpObjectDetailsUserKeyA_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "UserKeyA");
    private final static QName _ErpObjectDetailsDFFAttributes_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "DFFAttributes");
    private final static QName _ErpObjectDetailsContextValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "ContextValue");
    private final static QName _ErpObjectDetailsUserKeyE_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "UserKeyE");
    private final static QName _ErpObjectDetailsUserKeyD_QNAME = new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "UserKeyD");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FaultType }
     * 
     */
    public FaultType createFaultType() {
        return new FaultType();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link UpdateDffEntityDetailsAsync }
     * 
     */
    public UpdateDffEntityDetailsAsync createUpdateDffEntityDetailsAsync() {
        return new UpdateDffEntityDetailsAsync();
    }

    /**
     * Create an instance of {@link DocumentDetails }
     * 
     */
    public DocumentDetails createDocumentDetails() {
        return new DocumentDetails();
    }

    /**
     * Create an instance of {@link ErpObjectDetails }
     * 
     */
    public ErpObjectDetails createErpObjectDetails() {
        return new ErpObjectDetails();
    }

    /**
     * Create an instance of {@link UpdateDffEntityDetails }
     * 
     */
    public UpdateDffEntityDetails createUpdateDffEntityDetails() {
        return new UpdateDffEntityDetails();
    }

    /**
     * Create an instance of {@link UpdateDffEntityDetailsResponse }
     * 
     */
    public UpdateDffEntityDetailsResponse createUpdateDffEntityDetailsResponse() {
        return new UpdateDffEntityDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateDffEntityDetailsAsyncResponse }
     * 
     */
    public UpdateDffEntityDetailsAsyncResponse createUpdateDffEntityDetailsAsyncResponse() {
        return new UpdateDffEntityDetailsAsyncResponse();
    }

    /**
     * Create an instance of {@link FindControl }
     * 
     */
    public FindControl createFindControl() {
        return new FindControl();
    }

    /**
     * Create an instance of {@link ProcessControl }
     * 
     */
    public ProcessControl createProcessControl() {
        return new ProcessControl();
    }

    /**
     * Create an instance of {@link FindCriteria }
     * 
     */
    public FindCriteria createFindCriteria() {
        return new FindCriteria();
    }

    /**
     * Create an instance of {@link ChildFindCriteria }
     * 
     */
    public ChildFindCriteria createChildFindCriteria() {
        return new ChildFindCriteria();
    }

    /**
     * Create an instance of {@link BigDecimalResult }
     * 
     */
    public BigDecimalResult createBigDecimalResult() {
        return new BigDecimalResult();
    }

    /**
     * Create an instance of {@link SortOrder }
     * 
     */
    public SortOrder createSortOrder() {
        return new SortOrder();
    }

    /**
     * Create an instance of {@link SortAttribute }
     * 
     */
    public SortAttribute createSortAttribute() {
        return new SortAttribute();
    }

    /**
     * Create an instance of {@link AmountType }
     * 
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link ServiceViewInfo }
     * 
     */
    public ServiceViewInfo createServiceViewInfo() {
        return new ServiceViewInfo();
    }

    /**
     * Create an instance of {@link DataObjectResult }
     * 
     */
    public DataObjectResult createDataObjectResult() {
        return new DataObjectResult();
    }

    /**
     * Create an instance of {@link BooleanResult }
     * 
     */
    public BooleanResult createBooleanResult() {
        return new BooleanResult();
    }

    /**
     * Create an instance of {@link MeasureType }
     * 
     */
    public MeasureType createMeasureType() {
        return new MeasureType();
    }

    /**
     * Create an instance of {@link MethodResult }
     * 
     */
    public MethodResult createMethodResult() {
        return new MethodResult();
    }

    /**
     * Create an instance of {@link TimeResult }
     * 
     */
    public TimeResult createTimeResult() {
        return new TimeResult();
    }

    /**
     * Create an instance of {@link DateResult }
     * 
     */
    public DateResult createDateResult() {
        return new DateResult();
    }

    /**
     * Create an instance of {@link ViewCriteriaRow }
     * 
     */
    public ViewCriteriaRow createViewCriteriaRow() {
        return new ViewCriteriaRow();
    }

    /**
     * Create an instance of {@link BytesResult }
     * 
     */
    public BytesResult createBytesResult() {
        return new BytesResult();
    }

    /**
     * Create an instance of {@link CtrlHint }
     * 
     */
    public CtrlHint createCtrlHint() {
        return new CtrlHint();
    }

    /**
     * Create an instance of {@link DoubleResult }
     * 
     */
    public DoubleResult createDoubleResult() {
        return new DoubleResult();
    }

    /**
     * Create an instance of {@link ViewCriteriaItem }
     * 
     */
    public ViewCriteriaItem createViewCriteriaItem() {
        return new ViewCriteriaItem();
    }

    /**
     * Create an instance of {@link ShortResult }
     * 
     */
    public ShortResult createShortResult() {
        return new ShortResult();
    }

    /**
     * Create an instance of {@link BigIntegerResult }
     * 
     */
    public BigIntegerResult createBigIntegerResult() {
        return new BigIntegerResult();
    }

    /**
     * Create an instance of {@link ObjAttrHints }
     * 
     */
    public ObjAttrHints createObjAttrHints() {
        return new ObjAttrHints();
    }

    /**
     * Create an instance of {@link DataHandlerResult }
     * 
     */
    public DataHandlerResult createDataHandlerResult() {
        return new DataHandlerResult();
    }

    /**
     * Create an instance of {@link IntegerResult }
     * 
     */
    public IntegerResult createIntegerResult() {
        return new IntegerResult();
    }

    /**
     * Create an instance of {@link FloatResult }
     * 
     */
    public FloatResult createFloatResult() {
        return new FloatResult();
    }

    /**
     * Create an instance of {@link StringResult }
     * 
     */
    public StringResult createStringResult() {
        return new StringResult();
    }

    /**
     * Create an instance of {@link ByteResult }
     * 
     */
    public ByteResult createByteResult() {
        return new ByteResult();
    }

    /**
     * Create an instance of {@link LongResult }
     * 
     */
    public LongResult createLongResult() {
        return new LongResult();
    }

    /**
     * Create an instance of {@link AttrCtrlHints }
     * 
     */
    public AttrCtrlHints createAttrCtrlHints() {
        return new AttrCtrlHints();
    }

    /**
     * Create an instance of {@link ViewCriteria }
     * 
     */
    public ViewCriteria createViewCriteria() {
        return new ViewCriteria();
    }

    /**
     * Create an instance of {@link TimestampResult }
     * 
     */
    public TimestampResult createTimestampResult() {
        return new TimestampResult();
    }

    /**
     * Create an instance of {@link ServiceErrorMessage }
     * 
     */
    public ServiceErrorMessage createServiceErrorMessage() {
        return new ServiceErrorMessage();
    }

    /**
     * Create an instance of {@link ServiceMessage }
     * 
     */
    public ServiceMessage createServiceMessage() {
        return new ServiceMessage();
    }

    /**
     * Create an instance of {@link ServiceDMLErrorMessage }
     * 
     */
    public ServiceDMLErrorMessage createServiceDMLErrorMessage() {
        return new ServiceDMLErrorMessage();
    }

    /**
     * Create an instance of {@link ServiceRowValErrorMessage }
     * 
     */
    public ServiceRowValErrorMessage createServiceRowValErrorMessage() {
        return new ServiceRowValErrorMessage();
    }

    /**
     * Create an instance of {@link ServiceAttrValErrorMessage }
     * 
     */
    public ServiceAttrValErrorMessage createServiceAttrValErrorMessage() {
        return new ServiceAttrValErrorMessage();
    }

    /**
     * Create an instance of {@link JavaInfo }
     * 
     */
    public JavaInfo createJavaInfo() {
        return new JavaInfo();
    }

    /**
     * Create an instance of {@link DataGraphType }
     * 
     */
    public DataGraphType createDataGraphType() {
        return new DataGraphType();
    }

    /**
     * Create an instance of {@link Type }
     * 
     */
    public Type createType() {
        return new Type();
    }

    /**
     * Create an instance of {@link Types }
     * 
     */
    public Types createTypes() {
        return new Types();
    }

    /**
     * Create an instance of {@link ModelsType }
     * 
     */
    public ModelsType createModelsType() {
        return new ModelsType();
    }

    /**
     * Create an instance of {@link Property }
     * 
     */
    public Property createProperty() {
        return new Property();
    }

    /**
     * Create an instance of {@link XSDType }
     * 
     */
    public XSDType createXSDType() {
        return new XSDType();
    }

    /**
     * Create an instance of {@link ChangeSummaryType }
     * 
     */
    public ChangeSummaryType createChangeSummaryType() {
        return new ChangeSummaryType();
    }

    /**
     * Create an instance of {@link XMLInfo }
     * 
     */
    public XMLInfo createXMLInfo() {
        return new XMLInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/oracleas/schema/oracle-fault-11_0", name = "Fault")
    public JAXBElement<FaultType> createFault(FaultType value) {
        return new JAXBElement<FaultType>(_Fault_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "documentDetails")
    public JAXBElement<DocumentDetails> createDocumentDetails(DocumentDetails value) {
        return new JAXBElement<DocumentDetails>(_DocumentDetails_QNAME, DocumentDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/types/", name = "findCriteria")
    public JAXBElement<FindCriteria> createFindCriteria(FindCriteria value) {
        return new JAXBElement<FindCriteria>(_FindCriteria_QNAME, FindCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Types }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "types")
    public JAXBElement<Types> createTypes(Types value) {
        return new JAXBElement<Types>(_Types_QNAME, Types.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "type")
    public JAXBElement<Type> createType(Type value) {
        return new JAXBElement<Type>(_Type_QNAME, Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindControl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/types/", name = "findControl")
    public JAXBElement<FindControl> createFindControl(FindControl value) {
        return new JAXBElement<FindControl>(_FindControl_QNAME, FindControl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "dataObject")
    public JAXBElement<Object> createDataObject(Object value) {
        return new JAXBElement<Object>(_DataObject_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceErrorMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/errors/", name = "ServiceErrorMessage")
    public JAXBElement<ServiceErrorMessage> createServiceErrorMessage(ServiceErrorMessage value) {
        return new JAXBElement<ServiceErrorMessage>(_ServiceErrorMessage_QNAME, ServiceErrorMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessControl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/types/", name = "processControl")
    public JAXBElement<ProcessControl> createProcessControl(ProcessControl value) {
        return new JAXBElement<ProcessControl>(_ProcessControl_QNAME, ProcessControl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErpObjectDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "erpObjectDetails")
    public JAXBElement<ErpObjectDetails> createErpObjectDetails(ErpObjectDetails value) {
        return new JAXBElement<ErpObjectDetails>(_ErpObjectDetails_QNAME, ErpObjectDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataGraphType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "datagraph")
    public JAXBElement<DataGraphType> createDatagraph(DataGraphType value) {
        return new JAXBElement<DataGraphType>(_Datagraph_QNAME, DataGraphType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErpObjectDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "object", scope = UpdateDffEntityDetailsAsync.class)
    public JAXBElement<ErpObjectDetails> createUpdateDffEntityDetailsAsyncObject(ErpObjectDetails value) {
        return new JAXBElement<ErpObjectDetails>(_UpdateDffEntityDetailsAsyncObject_QNAME, ErpObjectDetails.class, UpdateDffEntityDetailsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "callbackURL", scope = UpdateDffEntityDetailsAsync.class)
    public JAXBElement<String> createUpdateDffEntityDetailsAsyncCallbackURL(String value) {
        return new JAXBElement<String>(_UpdateDffEntityDetailsAsyncCallbackURL_QNAME, String.class, UpdateDffEntityDetailsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "notificationCode", scope = UpdateDffEntityDetailsAsync.class)
    public JAXBElement<String> createUpdateDffEntityDetailsAsyncNotificationCode(String value) {
        return new JAXBElement<String>(_UpdateDffEntityDetailsAsyncNotificationCode_QNAME, String.class, UpdateDffEntityDetailsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "document", scope = UpdateDffEntityDetailsAsync.class)
    public JAXBElement<DocumentDetails> createUpdateDffEntityDetailsAsyncDocument(DocumentDetails value) {
        return new JAXBElement<DocumentDetails>(_UpdateDffEntityDetailsAsyncDocument_QNAME, DocumentDetails.class, UpdateDffEntityDetailsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DocumentAccount", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsDocumentAccount(String value) {
        return new JAXBElement<String>(_DocumentDetailsDocumentAccount_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "ContentType", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsContentType(String value) {
        return new JAXBElement<String>(_DocumentDetailsContentType_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DocumentId", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsDocumentId(String value) {
        return new JAXBElement<String>(_DocumentDetailsDocumentId_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DocumentName", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsDocumentName(String value) {
        return new JAXBElement<String>(_DocumentDetailsDocumentName_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DocumentAuthor", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsDocumentAuthor(String value) {
        return new JAXBElement<String>(_DocumentDetailsDocumentAuthor_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DocumentSecurityGroup", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsDocumentSecurityGroup(String value) {
        return new JAXBElement<String>(_DocumentDetailsDocumentSecurityGroup_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DocumentTitle", scope = DocumentDetails.class)
    public JAXBElement<String> createDocumentDetailsDocumentTitle(String value) {
        return new JAXBElement<String>(_DocumentDetailsDocumentTitle_QNAME, String.class, DocumentDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErpObjectDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "object", scope = UpdateDffEntityDetails.class)
    public JAXBElement<ErpObjectDetails> createUpdateDffEntityDetailsObject(ErpObjectDetails value) {
        return new JAXBElement<ErpObjectDetails>(_UpdateDffEntityDetailsAsyncObject_QNAME, ErpObjectDetails.class, UpdateDffEntityDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "callbackURL", scope = UpdateDffEntityDetails.class)
    public JAXBElement<String> createUpdateDffEntityDetailsCallbackURL(String value) {
        return new JAXBElement<String>(_UpdateDffEntityDetailsAsyncCallbackURL_QNAME, String.class, UpdateDffEntityDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "notificationCode", scope = UpdateDffEntityDetails.class)
    public JAXBElement<String> createUpdateDffEntityDetailsNotificationCode(String value) {
        return new JAXBElement<String>(_UpdateDffEntityDetailsAsyncNotificationCode_QNAME, String.class, UpdateDffEntityDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", name = "document", scope = UpdateDffEntityDetails.class)
    public JAXBElement<DocumentDetails> createUpdateDffEntityDetailsDocument(DocumentDetails value) {
        return new JAXBElement<DocumentDetails>(_UpdateDffEntityDetailsAsyncDocument_QNAME, DocumentDetails.class, UpdateDffEntityDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "UserKeyB", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsUserKeyB(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsUserKeyB_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "UserKeyC", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsUserKeyC(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsUserKeyC_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "EntityName", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsEntityName(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsEntityName_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "UserKeyA", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsUserKeyA(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsUserKeyA_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "DFFAttributes", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsDFFAttributes(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsDFFAttributes_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "ContextValue", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsContextValue(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsContextValue_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "UserKeyE", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsUserKeyE(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsUserKeyE_QNAME, String.class, ErpObjectDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", name = "UserKeyD", scope = ErpObjectDetails.class)
    public JAXBElement<String> createErpObjectDetailsUserKeyD(String value) {
        return new JAXBElement<String>(_ErpObjectDetailsUserKeyD_QNAME, String.class, ErpObjectDetails.class, value);
    }

}
