
package xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ErpObjectDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ErpObjectDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EntityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContextValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserKeyA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserKeyB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserKeyC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserKeyD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserKeyE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DFFAttributes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErpObjectDetails", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", propOrder = {
    "entityName",
    "contextValue",
    "userKeyA",
    "userKeyB",
    "userKeyC",
    "userKeyD",
    "userKeyE",
    "dffAttributes"
})
public class ErpObjectDetails {

    @XmlElementRef(name = "EntityName", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> entityName;
    @XmlElementRef(name = "ContextValue", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> contextValue;
    @XmlElementRef(name = "UserKeyA", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> userKeyA;
    @XmlElementRef(name = "UserKeyB", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> userKeyB;
    @XmlElementRef(name = "UserKeyC", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> userKeyC;
    @XmlElementRef(name = "UserKeyD", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> userKeyD;
    @XmlElementRef(name = "UserKeyE", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> userKeyE;
    @XmlElementRef(name = "DFFAttributes", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", type = JAXBElement.class)
    protected JAXBElement<String> dffAttributes;

    /**
     * Gets the value of the entityName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEntityName() {
        return entityName;
    }

    /**
     * Sets the value of the entityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEntityName(JAXBElement<String> value) {
        this.entityName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the contextValue property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContextValue() {
        return contextValue;
    }

    /**
     * Sets the value of the contextValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContextValue(JAXBElement<String> value) {
        this.contextValue = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userKeyA property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserKeyA() {
        return userKeyA;
    }

    /**
     * Sets the value of the userKeyA property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserKeyA(JAXBElement<String> value) {
        this.userKeyA = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userKeyB property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserKeyB() {
        return userKeyB;
    }

    /**
     * Sets the value of the userKeyB property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserKeyB(JAXBElement<String> value) {
        this.userKeyB = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userKeyC property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserKeyC() {
        return userKeyC;
    }

    /**
     * Sets the value of the userKeyC property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserKeyC(JAXBElement<String> value) {
        this.userKeyC = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userKeyD property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserKeyD() {
        return userKeyD;
    }

    /**
     * Sets the value of the userKeyD property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserKeyD(JAXBElement<String> value) {
        this.userKeyD = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the userKeyE property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserKeyE() {
        return userKeyE;
    }

    /**
     * Sets the value of the userKeyE property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserKeyE(JAXBElement<String> value) {
        this.userKeyE = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the dffAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDFFAttributes() {
        return dffAttributes;
    }

    /**
     * Sets the value of the dffAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDFFAttributes(JAXBElement<String> value) {
        this.dffAttributes = ((JAXBElement<String> ) value);
    }

}
