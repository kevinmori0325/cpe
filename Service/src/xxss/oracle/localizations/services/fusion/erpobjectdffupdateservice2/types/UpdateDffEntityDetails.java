
package xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice2.types;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="document" type="{http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/}DocumentDetails" minOccurs="0"/>
 *         &lt;element name="operationMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="object" type="{http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/}ErpObjectDetails" minOccurs="0"/>
 *         &lt;element name="notificationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="callbackURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "document",
    "operationMode",
    "object",
    "notificationCode",
    "callbackURL"
})
@XmlRootElement(name = "updateDffEntityDetails", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/")
public class UpdateDffEntityDetails {

    @XmlElementRef(name = "document", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", type = JAXBElement.class)
    protected JAXBElement<DocumentDetails> document;
    @XmlElementRef(name = "operationMode", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", type = JAXBElement.class)
    protected JAXBElement<String> operationMode;
    @XmlElementRef(name = "object", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", type = JAXBElement.class)
    protected JAXBElement<ErpObjectDetails> object;
    @XmlElementRef(name = "notificationCode", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", type = JAXBElement.class)
    protected JAXBElement<String> notificationCode;
    @XmlElementRef(name = "callbackURL", namespace = "http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/types/", type = JAXBElement.class)
    protected JAXBElement<String> callbackURL;

    /**
     * Gets the value of the document property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocumentDetails }{@code >}
     *     
     */
    public JAXBElement<DocumentDetails> getDocument() {
        return document;
    }

    /**
     * Sets the value of the document property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocumentDetails }{@code >}
     *     
     */
    public void setDocument(JAXBElement<DocumentDetails> value) {
        this.document = ((JAXBElement<DocumentDetails> ) value);
    }

    /**
     * Gets the value of the operationMode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOperationMode() {
        return operationMode;
    }

    /**
     * Sets the value of the operationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOperationMode(JAXBElement<String> value) {
        this.operationMode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the object property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ErpObjectDetails }{@code >}
     *     
     */
    public JAXBElement<ErpObjectDetails> getObject() {
        return object;
    }

    /**
     * Sets the value of the object property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ErpObjectDetails }{@code >}
     *     
     */
    public void setObject(JAXBElement<ErpObjectDetails> value) {
        this.object = ((JAXBElement<ErpObjectDetails> ) value);
    }

    /**
     * Gets the value of the notificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotificationCode() {
        return notificationCode;
    }

    /**
     * Sets the value of the notificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotificationCode(JAXBElement<String> value) {
        this.notificationCode = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the callbackURL property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCallbackURL() {
        return callbackURL;
    }

    /**
     * Sets the value of the callbackURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCallbackURL(JAXBElement<String> value) {
        this.callbackURL = ((JAXBElement<String> ) value);
    }

}
