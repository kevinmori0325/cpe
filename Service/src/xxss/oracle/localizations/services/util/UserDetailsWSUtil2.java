package xxss.oracle.localizations.services.util;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;

import javax.xml.ws.BindingProvider;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.userdetailsservice2.proxy.UserDetailsService;
import xxss.oracle.localizations.services.fusion.userdetailsservice2.proxy.UserDetailsService_Service;

public class UserDetailsWSUtil2 {
    public UserDetailsWSUtil2() {
        super();
    }
    
    public static UserDetailsService createExternalReportWSSService(String instanceName,
                                                                                       String dataCenter, String userName,
                                                                                       String userPass) throws MalformedURLException {
                UserDetailsService_Service service_Service;
                UserDetailsService service = null;

                service_Service = new UserDetailsService_Service();

                if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                    return service;
                }

          
                SecurityPolicyFeature[] secFeatures =
                { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

                
                service =
                        service_Service.getUserDetailsServiceSoapHttpPort(secFeatures);
    
                if (service == null) {
                    System.out.println("UserDetailsService is null");
                } else {
                    System.out.println("UserDetailsService created.");
                }


                // add JWT auth map to HTTP header
                WSBindingProvider wsbp = (WSBindingProvider)service;
                wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
                wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
                wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com/hcmPeopleRolesV2/UserDetailsService");
                wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

                return service;
    }
}
