package xxss.oracle.localizations.services.util;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.signature.proxy.Core;
import xxss.oracle.localizations.services.signature.proxy.CoreSoap;
import xxss.oracle.localizations.services.signaturews.proxy.SignatureWS;
import xxss.oracle.localizations.services.signaturews.proxy.SignatureWSService;

public class GoSocketSignatureWSUtil {
    public GoSocketSignatureWSUtil() {
        super();
    }

    public static SignatureWS createCoreSoap(String url) throws MalformedURLException {
        SignatureWSService service_Service;
        SignatureWS service = null;

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
            new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service =
                new SignatureWSService(new URL(url), new QName("http://gosocket.ws.pe.localizations.oracle/",
                    "SignatureWSService"));
        service =
                service_Service.getSignatureWSSoap12HttpPort(secFeatures);
        //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
        if (service == null) {
            System.out.println("SignatureService is null");
        } else {
            System.out.println("SignatureService created.");
        }

        // add JWT auth map to HTTP header
        /*BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap =
            new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS,
                                   authMap);*/

        return service;
    }
}
