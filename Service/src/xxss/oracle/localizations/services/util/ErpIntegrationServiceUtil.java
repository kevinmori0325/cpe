package xxss.oracle.localizations.services.util;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;
import xxss.oracle.localizations.services.fusion.erpintegrationservice.proxy.ErpIntegrationService;
import xxss.oracle.localizations.services.fusion.erpintegrationservice.proxy.ErpIntegrationService_Service;
import xxss.oracle.localizations.services.fusion.erpintegrationservice.types.DocumentDetails;
import xxss.oracle.localizations.services.fusion.erpintegrationservice.types.ObjectFactory;
import xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService_Service;


public class ErpIntegrationServiceUtil {
    public ErpIntegrationServiceUtil() {
      
    }
    
    public static ErpIntegrationService createIntegrationServiceJwt(String jwt, String instanceName, String dataCenter)
            throws MalformedURLException
        {
            ErpIntegrationService service = null;
            if(jwt == null || "".equals(jwt))
                return service;
            SecurityPolicyFeature secFeatures[] = {
                new SecurityPolicyFeature("")
            };
            ErpIntegrationService_Service service_Service = new ErpIntegrationService_Service(new URL((new StringBuilder()).append("https://").append(instanceName).append(".fs.").append(dataCenter).append(".oraclecloud.com/fndAppCoreServices/FndManageImportExportFilesService").toString()), new QName("http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/", "FndManageImportExportFilesService"));
            service = service_Service.getErpIntegrationServiceSoapHttpPort(secFeatures);
            if(service == null)
                System.out.println("ErpIntegrationService is null");
            else
                System.out.println("ErpIntegrationService created.");
            System.out.println((new StringBuilder()).append("XxssPeLocalizaciones: jwt=").append(jwt).toString());
            BindingProvider bp = (BindingProvider)service;
            Map authMap = new HashMap();
            List authZlist = new ArrayList();
            authZlist.add((new StringBuilder()).append("Bearer ").append(jwt).toString());
            authMap.put("Authorization", authZlist);
            bp.getRequestContext().put("javax.xml.ws.http.request.headers", authMap);
            return service;
        }
    
    public static ErpIntegrationService createIntegrationServiceUser(String userName, String userPass, String instanceName, String dataCenter )
        throws MalformedURLException
    {
        ErpIntegrationService service = null;
        ErpIntegrationService_Service service_Service = null;
        
        service_Service =
                new ErpIntegrationService_Service();
            
        if(userName == null || "".equals(userName) || userPass == null || "".equals(userPass))
            return service;
        SecurityPolicyFeature secFeatures[] = {
            new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy")
        };                               
        

        service = service_Service.getErpIntegrationServiceSoapHttpPort(secFeatures);
        
        
        if(service == null)
            System.out.println("ErpIntegrationService is null");
        else
            System.out.println("ErpIntegrationService created.");
        
        
        // add JWT auth map to HTTP header
        WSBindingProvider wsbp = (WSBindingProvider)service;
        wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
        wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
        wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://" + instanceName + ".fa." + dataCenter + ".oraclecloud.com/fscmService/ErpIntegrationService");
        wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);
        return service;
    }
    
   /* public static ErpIntegrationService createIntegrationServiceUser(String instanceName,
                                                                                       String dataCenter, String userName,
                                                                                       String userPass) throws MalformedURLException {
            ErpIntegrationService_Service service_Service;
            ErpIntegrationService service = null;
            
            service_Service =
                    new ErpIntegrationService_Service();

            if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                return service;
            }

            SecurityPolicyFeature[] secFeatures =
            { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

            
            service = service_Service.getErpIntegrationServiceSoapHttpPort(secFeatures);

            if (service == null) {
                System.out.println("ExternalReportWSSService is null");
            } else {
                System.out.println("ExternalReportWSSService created.");
            }



            // add JWT auth map to HTTP header
            WSBindingProvider wsbp = (WSBindingProvider)service;
            wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
            wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
            wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com/xmlpserver/services/ExternalReportWSSService");
            wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

            return service;
        }*/
    

        public static ErpIntegrationService createIntegrationServiceUserToken(String instanceName, String dataCenter, String userName, String userPass)
            throws MalformedURLException
        {
            ErpIntegrationService service = null;
            if(userName == null || "".equals(userName) || userPass == null || "".equals(userPass))
                return service;
            SecurityPolicyFeature secFeatures[] = {
                new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy")
            };                                                                           //   https://efuk.fin.us6.oraclecloud.com/publicFinancialCommonErpIntegration/ErpIntegrationService?WSDL         //http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/              http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/
            ErpIntegrationService_Service service_Service = new ErpIntegrationService_Service(new URL((new StringBuilder()).append("https://").append(instanceName).append(".fin.").append(dataCenter).append(".oraclecloud.com/publicFinancialCommonErpIntegration/ErpIntegrationService").toString()), new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "ErpIntegrationService"));
            service = service_Service.getErpIntegrationServiceSoapHttpPort(secFeatures);
            if(service == null)
                System.out.println("ErpIntegrationService is null");
            else
                System.out.println("ErpIntegrationService created.");
            WSBindingProvider wsbp = (WSBindingProvider)service;
            wsbp.getRequestContext().put("javax.xml.ws.security.auth.username", userName);
            wsbp.getRequestContext().put("javax.xml.ws.security.auth.password", userPass);
            return service;
        }

        public static DocumentDetails buildVSDocumentDetails(String fileName, byte fileBytes[], String acount,String type, String title)
        {
            DocumentDetails document = new DocumentDetails();
            ObjectFactory objfactory = new ObjectFactory();
            document.setFileName(fileName);
            document.setDocumentAccount(objfactory.createDocumentDetailsDocumentAccount(acount));
            document.setDocumentTitle(objfactory.createDocumentDetailsDocumentTitle(title));
            document.setContentType(objfactory.createDocumentDetailsContentType(type));
            byte content[] = fileBytes;
            document.setContent(content);
             return document;
        }

        public static boolean isNumber(String s)
        {
            double n;
            try
            {
                n = Double.parseDouble(s);
            }
            catch(NumberFormatException e)
            {
                return false;
            }
            return true;
        }

}
