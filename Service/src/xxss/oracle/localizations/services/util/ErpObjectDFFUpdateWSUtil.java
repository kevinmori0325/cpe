package xxss.oracle.localizations.services.util;

import com.sun.xml.ws.developer.WSBindingProvider;
import com.sun.xml.ws.client.BindingProviderProperties;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice.proxy.ErpObjectDFFUpdateService;
import xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice.proxy.ErpObjectDFFUpdateService_Service;


public class ErpObjectDFFUpdateWSUtil {
    public ErpObjectDFFUpdateWSUtil() {
        super();
    }
    
    public static ErpObjectDFFUpdateService createErpObjDFFUpdateWSSServiceUserToken(String instanceName,
                                                                                       String dataCenter, String userName,
                                                                                       String userPass) throws MalformedURLException {
            ErpObjectDFFUpdateService_Service service_Service;
            ErpObjectDFFUpdateService service = null;

            service_Service =
                        new ErpObjectDFFUpdateService_Service();

            if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                return service;
            }

            SecurityPolicyFeature[] secFeatures =
            { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

            service = service_Service.getErpObjectDFFUpdateServiceSoapHttpPort(secFeatures);

            if (service == null) {
                System.out.println("ErpObjDFFUpdateWSSService is null");
            } else {
                System.out.println("ErpObjDFFUpdateWSSService created.");
            }

            // add JWT auth map to HTTP header
            WSBindingProvider wsbp = (WSBindingProvider)service;
            wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
            wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
            wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com:443/fscmService/ErpObjectDFFUpdateService");
            wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

            return service;
        }

   /* public static ErpObjectDFFUpdateService createErpObjDFFUpdateWSSServiceUserToken(String instanceName,
                                                                                   String dataCenter, String userName,
                                                                                   String userPass) throws MalformedURLException {
        ErpObjectDFFUpdateService_Service service_Service;
        ErpObjectDFFUpdateService service = null;

        //String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

        if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
        { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

        service_Service =
                new ErpObjectDFFUpdateService_Service(new URL("https://" + instanceName + ".fin." + dataCenter +
                                                             ".oraclecloud.com/publicFinancialCommonErpIntegration/ErpObjectDFFUpdateService"),
                                                     new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/",
                                                               "ErpObjectDFFUpdateService"));
        service = service_Service.getErpObjectDFFUpdateServiceSoapHttpPort(secFeatures);

        if (service == null) {
            System.out.println("ErpObjDFFUpdateWSSService is null");
        } else {
            System.out.println("ErpObjDFFUpdateWSSService created.");
        }

        // add JWT auth map to HTTP header
        WSBindingProvider wsbp = (WSBindingProvider)service;
        wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
        wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);

        return service;
    }
*/
    public static ErpObjectDFFUpdateService createErpObjDFFUpdateWSSServiceJwt(String jwt, String instanceName,
                                                                          String dataCenter) throws MalformedURLException {
        ErpObjectDFFUpdateService_Service service_Service;
        ErpObjectDFFUpdateService service = null;

        //String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

        if (jwt == null || "".equals(jwt)) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures = new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service =
                new ErpObjectDFFUpdateService_Service(new URL("https://" + instanceName + ".fin." + dataCenter +
                                                             ".oraclecloud.com/publicFinancialCommonErpIntegration/ErpObjectDFFUpdateService"),
                                                    new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/",
                                                              "ErpObjectDFFUpdateService"));
        service = service_Service.getErpObjectDFFUpdateServiceSoapHttpPort(secFeatures);
        //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
        if (service == null) {
            System.out.println("ErpObjDFFUpdateWSSService is null");
        } else {
            System.out.println("ErpObjDFFUpdateWSSService created.");
        }

        System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

        // add JWT auth map to HTTP header
        BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap = new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, authMap);

        return service;
    }
}
