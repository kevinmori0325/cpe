package xxss.oracle.localizations.services.util;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;

import javax.xml.ws.BindingProvider;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice2.proxy.ErpObjectDFFUpdateService;
import xxss.oracle.localizations.services.fusion.erpobjectdffupdateservice2.proxy.ErpObjectDFFUpdateService_Service;


public class ErpObjectDFFUpdateWSUtil2 {
    public ErpObjectDFFUpdateWSUtil2() {
        super();
    }
    
    public static ErpObjectDFFUpdateService createErpObjDFFUpdateWSSServiceUserToken2(String instanceName,
                                                                                       String dataCenter, String userName,
                                                                                       String userPass) throws MalformedURLException {
            ErpObjectDFFUpdateService_Service service_Service;
            ErpObjectDFFUpdateService service = null;

            service_Service =
                        new ErpObjectDFFUpdateService_Service();

            if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                return service;
            }

            SecurityPolicyFeature[] secFeatures =
            { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

            service = service_Service.getErpObjectDFFUpdateServiceSoapHttpPort(secFeatures);

            if (service == null) {
                System.out.println("ErpObjDFFUpdateWSSService is null");
            } else {
                System.out.println("ErpObjDFFUpdateWSSService created.");
            }

            // add JWT auth map to HTTP header
            WSBindingProvider wsbp = (WSBindingProvider)service;
            wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
            wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
            wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com/fscmService/ErpObjectDFFUpdateService");
            wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

            return service;
        }
    
    
}
