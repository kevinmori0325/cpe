package xxss.oracle.localizations.services.util;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;

import javax.xml.ws.BindingProvider;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice3.proxy.ExternalReportWSSService_Service;


public class ExternalReportWSUtil3 {
    public ExternalReportWSUtil3() {
        super();
    }
    
    public static ExternalReportWSSService createExternalReportWSSServiceUserToken3(String instanceName,
                                                                                       String dataCenter, String userName,
                                                                                       String userPass) throws MalformedURLException {
            ExternalReportWSSService_Service service_Service;
            ExternalReportWSSService service = null;
            
            service_Service =
                    new ExternalReportWSSService_Service();

            if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                return service;
            }

            SecurityPolicyFeature[] secFeatures =
            { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

            
            service = service_Service.getExternalReportWSSService(secFeatures);

            if (service == null) {
                System.out.println("ExternalReportWSSService is null");
            } else {
                System.out.println("ExternalReportWSSService created.");
            }


            // add JWT auth map to HTTP header
            WSBindingProvider wsbp = (WSBindingProvider)service;
            wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
            wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
            wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com/xmlpserver/services/ExternalReportWSSService");
            wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

            return service;
        }
}
