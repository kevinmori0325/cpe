package xxss.oracle.localizations.services.util;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.signature.proxy.Core;
import xxss.oracle.localizations.services.signature.proxy.CoreSoap;

public class SignatureWSUtil {
    public SignatureWSUtil() {
        super();
    }

    public static CoreSoap createCoreSoap(String url) throws MalformedURLException {
        Core service_Service;
        CoreSoap service = null;

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
            new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service =
                new Core(new URL(url), new QName("http://tempuri.org/", "Core"));
        /*service =
                service_Service.getCoreSoap12(secFeatures);*/
        service =
                service_Service.getCoreSoap12();
        //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
        if (service == null) {
            System.out.println("SignatureService is null");
        } else {
            System.out.println("SignatureService created.");
        }

        // add JWT auth map to HTTP header
        /*BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap =
            new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS,
                                   authMap);*/

        return service;
    }
}
