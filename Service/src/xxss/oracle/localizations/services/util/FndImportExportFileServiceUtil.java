package xxss.oracle.localizations.services.util;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.proxy.FndManageImportExportFilesService;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.proxy.FndManageImportExportFilesService_Service;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types.DocumentDetails;
import xxss.oracle.localizations.services.fusion.fndmanageexportimportfilesservice.types.ObjectFactory;

public class FndImportExportFileServiceUtil {

    public FndImportExportFileServiceUtil() {
        super();
    }

    // static util method

    public static FndManageImportExportFilesService createImportServiceJwt(String jwt, String instanceName,
                                                                           String dataCenter) throws MalformedURLException {

        FndManageImportExportFilesService_Service service_Service;
        FndManageImportExportFilesService service = null;

        if (jwt == null || "".equals(jwt)) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures = new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service =
                new FndManageImportExportFilesService_Service(new URL("https://" + instanceName + ".fa." + dataCenter + ".oraclecloud.com/fndAppCoreServices/FndManageImportExportFilesService"),
                                                              new QName("http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/",
                                                                        "FndManageImportExportFilesService"));
        service = service_Service.getFndManageImportExportFilesServiceSoapHttpPort(secFeatures);
        //service_Service.getFndManageImportExportFilesServiceSoapHttpPort();
        if (service == null) {
            System.out.println("FndImportExportFileService is null");
        } else {
            System.out.println("FndImportExportFileService created.");
        }

        System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

        // add JWT auth map to HTTP header
        BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap = new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, authMap);

        return service;

    }
    
    public static FndManageImportExportFilesService createImportServiceUserToken(String instanceName,
                                                                                   String dataCenter, String userName,
                                                                                   String userPass) throws MalformedURLException {

        FndManageImportExportFilesService_Service service_Service;
        FndManageImportExportFilesService service = null;

            if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                return service;
            }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
        { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

        service_Service =
                new FndManageImportExportFilesService_Service(new URL("https://" + instanceName + ".fa." + dataCenter + ".oraclecloud.com/fndAppCoreServices/FndManageImportExportFilesService"),
                                                              new QName("http://xmlns.oracle.com/oracle/apps/fnd/applcore/webservices/",
                                                                        "FndManageImportExportFilesService"));
        service = service_Service.getFndManageImportExportFilesServiceSoapHttpPort(secFeatures);
        
        if (service == null) {
            System.out.println("FndImportExportFileService is null");
        } else {
            System.out.println("FndImportExportFileService created.");
        }

        // add JWT auth map to HTTP header
        WSBindingProvider wsbp = (WSBindingProvider)service;
        wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
        wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);

        return service;

    }
    
    public static FndManageImportExportFilesService createManageImportExportFilesServiceUserToken2(String instanceName,
                                                                                   String dataCenter, String userName,
                                                                                   String userPass) throws MalformedURLException {
        FndManageImportExportFilesService_Service service_Service;
        FndManageImportExportFilesService service = null;

        service_Service =
                new FndManageImportExportFilesService_Service();

        if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
        { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };


        service = service_Service.getFndManageImportExportFilesServiceSoapHttpPort(secFeatures);

        if (service == null) {
            System.out.println("FndManageImportExportFilesService is null");
        } else {
            System.out.println("FndManageImportExportFilesService created.");
        }

        // add JWT auth map to HTTP header
        WSBindingProvider wsbp = (WSBindingProvider)service;
        wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
        wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
        wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://" + instanceName + ".fa." + dataCenter + ".oraclecloud.com/fndAppCoreServices/FndManageImportExportFilesService");
        wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

        return service;
    }

        

    public static DocumentDetails buildVSDocumentDetails(String fileName, byte[] fileBytes) {

        //top level
        DocumentDetails document = new DocumentDetails();
        ObjectFactory objfactory = new ObjectFactory();

        // build the SOAP payload

        document.setFileName(objfactory.createDocumentDetailsFileName(fileName));

        // Provide UCM repository - if repository is fin/tax/import then suffix each value with $ as mentioned below
        document.setDocumentAccount(objfactory.createDocumentDetailsDocumentAccount("fin$/tax$/import$"));
        document.setDocumentTitle(objfactory.createDocumentDetailsDocumentTitle("VS"));
        document.setContentType(objfactory.createDocumentDetailsContentType("plain/text"));

        //try {
        // Provide location of bulk import data file in prescribed format
        byte[] content = fileBytes;
        //org.apache.commons.io.FileUtils.readFileToByteArray(new File("/tmp/" +
        //                                                             fileName));

        //System.out.println("File content:" + new String(content, "UTF-8"));
        document.setContent(objfactory.createDocumentDetailsContent(content));
        /*} catch (IOException ex) {
            log.error("Error on read file WS: " + ex.getMessage(), ex);
        } catch (Exception e) {
            log.error("Error calling DocumentDetails WS: " + e.getMessage(),
                      e);
        }*/

        return document;
    }
    
    public static DocumentDetails buildVSDocumentDetails2(String fileName, byte[] fileBytes) {

        //top level
        DocumentDetails document = new DocumentDetails();
        ObjectFactory objfactory = new ObjectFactory();

        // build the SOAP payload

        document.setFileName(objfactory.createDocumentDetailsFileName(fileName));

        // Provide UCM repository - if repository is fin/tax/import then suffix each value with $ as mentioned below
        document.setDocumentAccount(objfactory.createDocumentDetailsDocumentAccount("scm$/inventoryTransaction$/import$"));
        document.setDocumentTitle(objfactory.createDocumentDetailsDocumentTitle("VS"));
        document.setContentType(objfactory.createDocumentDetailsContentType("plain/text"));

        //try {
        // Provide location of bulk import data file in prescribed format
        byte[] content = fileBytes;
        //org.apache.commons.io.FileUtils.readFileToByteArray(new File("/tmp/" +
        //                                                             fileName));

        //System.out.println("File content:" + new String(content, "UTF-8"));
        document.setContent(objfactory.createDocumentDetailsContent(content));
        /*} catch (IOException ex) {
            log.error("Error on read file WS: " + ex.getMessage(), ex);
        } catch (Exception e) {
            log.error("Error calling DocumentDetails WS: " + e.getMessage(),
                      e);
        }*/

        return document;
    }

    public static boolean isNumber(String s) {
        try {
            double n = Double.parseDouble(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
