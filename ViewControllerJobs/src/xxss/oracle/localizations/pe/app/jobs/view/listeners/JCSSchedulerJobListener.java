package xxss.oracle.localizations.pe.app.jobs.view.listeners;

import java.io.StringReader;

import java.sql.SQLException;

import java.sql.Types;

import java.util.Arrays;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleConnection;

import oracle.sql.CLOB;

import org.apache.log4j.Logger;

import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import xxss.oracle.localizations.pe.app.utils.ExceptionUtils;

public class JCSSchedulerJobListener implements JobListener {
    private static Logger log = Logger.getLogger(JCSSchedulerJobListener.class);
    String name;
    
    public JCSSchedulerJobListener() {        
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {
        //System.out.println("jobToBeExecuted - getScheduledFireTime: " + jobExecutionContext.getScheduledFireTime());
        //System.out.println("jobToBeExecuted - getFireTime: " + jobExecutionContext.getFireTime());
        //System.out.println("jobToBeExecuted - getFireInstanceId: " + jobExecutionContext.getFireInstanceId());
        //System.out.println("jobToBeExecuted - getName: " + jobExecutionContext.getJobDetail().getKey().getName());
        
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        
        try {
            ctx = new InitialContext();
            //ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            ds = (DataSource)ctx.lookup("jdbc/XXSSPELOCDBCS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
            
            int jobId = dataMap.getInt("JobId");
            String requestedBy = dataMap.getString("RequestedBy");
            
            int requestId =  this.callInsertJobReq(jobId, requestedBy, conn);
            
            dataMap.put("RequestId", requestId);
        } catch (Exception ex) {
            log.error("Error on jobToBeExecuted(): " + ex.getMessage(), ex);
            log.error("Detalle: " + ExceptionUtils.stackTraceToString(ex));
            log.error(Arrays.toString(ex.getStackTrace()));
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on jobToBeExecuted(): " + sqlex.getMessage(), sqlex);
                    log.error("Detalle: " + ExceptionUtils.stackTraceToString(sqlex));
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
                
            }
        }
    }

    public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        
        try {
            ctx = new InitialContext();
            //ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            ds = (DataSource)ctx.lookup("jdbc/XXSSPELOCDBCS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
            
            int jobId = dataMap.getInt("JobId");
            String requestedBy = dataMap.getString("RequestedBy");
            
            int requestId =  this.callInsertJobReq(jobId, requestedBy, conn);
            
            dataMap.put("RequestId", requestId);
        } catch (Exception ex) {
            log.error("Error on jobExecutionVetoed(): " + ex.getMessage(), ex);
            log.error("Detalle: " + ExceptionUtils.stackTraceToString(ex));
            log.error(Arrays.toString(ex.getStackTrace()));
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on jobExecutionVetoed(): " + sqlex.getMessage(), sqlex);
                    log.error("Detalle: " + ExceptionUtils.stackTraceToString(sqlex));
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
                
            }
        }
    }

    public void jobWasExecuted(JobExecutionContext jobExecutionContext,
                               JobExecutionException jobExecutionException) {
        //System.out.println("jobWasExecuted - getScheduledFireTime: " + jobExecutionContext.getScheduledFireTime());
        //System.out.println("jobWasExecuted - getFireTime: " + jobExecutionContext.getFireTime());
        //System.out.println("jobWasExecuted - getFireInstanceId: " + jobExecutionContext.getFireInstanceId());
        //System.out.println("jobWasExecuted - getName: " + jobExecutionContext.getJobDetail().getKey().getName());
        //System.out.println("jobWasExecuted - getJobRunTime: " + jobExecutionContext.getJobRunTime());
        
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        String newLine = System.getProperty("line.separator"); // System.lineSeparator() for java > 1.6
        
        try {
            ctx = new InitialContext();
            //ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            ds = (DataSource)ctx.lookup("jdbc/XXSSPELOCDBCS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
            int requestId = dataMap.getInt("RequestId");
            JobResult jobResult = (JobResult)jobExecutionContext.getResult();
            
            String jobResultStr = "";
            StringBuilder sbLog = null;
            
            if (jobResult != null && jobResult.getStatus() != null) {
                jobResultStr = jobResult.getStatus();
                sbLog = jobResult.getJobLog();
            } else {
                jobResultStr = "U";
                sbLog = new StringBuilder();
            }
            
            //Clob logfileClob = conn.createClob();
            //CLOB logFileCLOB = CLOB.createTemporary(conn, false, CLOB.DURATION_SESSION); //new CLOB(conn);
            
            if (!"S".equals(jobResultStr)) {
                if (jobExecutionException != null && !jobExecutionException.getMessage().equals("")) {
                    sbLog.append(jobExecutionException.getMessage() + newLine);
                    sbLog.append(Arrays.toString(jobExecutionException.getStackTrace()));
                    sbLog.append(ExceptionUtils.stackTraceToString(jobExecutionException));
                    //logfileClob.setString(1, sb.toString());
                    //logFileCLOB.setString(1, sb.toString());
                }
            }
            
            this.callUpdateJobReq(requestId, sbLog, jobResultStr, conn);
        } catch (Exception ex) {
            log.error("Error on jobWasExecuted(): " + ex.getMessage(), ex);
            log.error("Detalle: " + ExceptionUtils.stackTraceToString(ex));
            log.error(Arrays.toString(ex.getStackTrace()));
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on jobWasExecuted(): " + sqlex.getMessage(), sqlex);
                    log.error("Detalle: " + ExceptionUtils.stackTraceToString(sqlex));
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
                
            }
        }
    }
    
    private int callInsertJobReq(int pJobId, String pRequestedBy, OracleConnection trans) throws SQLException {

        String st =
            "begin XXSS_PE_LOC_JOB_PKG.pr_insert_job_exec(?,?,?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setInt(1, pJobId);
        acs.setString(2, pRequestedBy);
        acs.registerOutParameter(3, Types.NUMERIC);

        acs.executeUpdate();
        
        int requestId = acs.getInt(3);
        
        return(requestId);
    }
    
    private void callUpdateJobReq(int pRequestId, StringBuilder pLogFile, String pRequestStatus, OracleConnection trans) throws SQLException {

        String st =
            "begin XXSS_PE_LOC_JOB_PKG.pr_update_job_exec(?,?,?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setInt(1, pRequestId);        
        
        StringReader sr = new StringReader(pLogFile.toString());
        acs.setClob(2, sr);
        acs.setString(3, pRequestStatus);

        acs.executeUpdate();
    }    
}
