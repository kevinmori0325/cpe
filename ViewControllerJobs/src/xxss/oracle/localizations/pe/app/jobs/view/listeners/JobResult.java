package xxss.oracle.localizations.pe.app.jobs.view.listeners;

public class JobResult {
    public final static String SUCCESS = "S";
    public final static String ERROR = "E";
    
    String status;
    StringBuilder jobLog;
    
    public JobResult() {
        super();
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setJobLog(StringBuilder jobLog) {
        this.jobLog = jobLog;
    }

    public StringBuilder getJobLog() {
        return jobLog;
    }
}
