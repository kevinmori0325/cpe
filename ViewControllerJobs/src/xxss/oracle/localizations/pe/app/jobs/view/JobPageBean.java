package xxss.oracle.localizations.pe.app.jobs.view;

import java.io.IOException;
import java.io.OutputStream;

import java.util.Arrays;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;

import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.ClobDomain;
import oracle.jbo.domain.Number;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import xxss.oracle.localizations.pe.app.utils.ADFUtil;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;

public class JobPageBean {
    private static Logger log = Logger.getLogger(JobPageBean.class);
    private String schedulerStatus;
    
    private RichPopup processMainPopup;
    private RichTable processTable;
    private RichPopup scheduleProcessPopup;
    private RichPopup paramProcessPopup;
    private RichPopup executionHistJobPopup;

    public JobPageBean() {
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void startButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding processIter = bindings.findIteratorBinding("XxssPeLocJobsVO1Iterator");

        ViewObject processVo = processIter.getViewObject();
        Row processRow = processVo.getCurrentRow();

        if (processRow != null) {
            DCIteratorBinding processParamIter = bindings.findIteratorBinding("XxssPeLocJobsParamsVO1Iterator");
            ViewObject paramsVo = processParamIter.getViewObject();
            RowSetIterator rsIterPara = paramsVo.createRowSetIterator(null);

            JobDataMap dataMap = new JobDataMap();

            while (rsIterPara.hasNext()) {
                Row paramsRow = rsIterPara.next();
                dataMap.put((String)paramsRow.getAttribute("ParamName"), (String)paramsRow.getAttribute("ParamValue"));
            }

            String className = (String)processRow.getAttribute("JobClassName");
            String jobName = (String)processRow.getAttribute("JobName");
            String cronExpression = (String)processRow.getAttribute("CronExpression");
            int jobId = ((Number)processRow.getAttribute("JobId")).intValue();
            String userName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.userName}");
            
            dataMap.put("JobId", jobId);
            
            if (userName != null && !"".equals(userName)) {
                dataMap.put("RequestedBy", userName);
            }

            if (cronExpression != null && !"".equals(cronExpression)) {
                try {
                    Class<Job> classVar = (Class<Job>)Job.class.getClassLoader().loadClass(className);
                    JobDetail job = JobBuilder.newJob(classVar).withIdentity(jobName).usingJobData(dataMap).build();

                    CronTrigger triggerJob =
                        TriggerBuilder.newTrigger().withIdentity(jobName + "Trigger").withSchedule(CronScheduleBuilder.cronSchedule(cronExpression)).startNow().build();

                    Scheduler schedulerJob = new StdSchedulerFactory().getScheduler();
                    schedulerJob.scheduleJob(job, triggerJob);

                    processVo.executeQuery();

                    AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());

                } catch (ClassNotFoundException ex) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    log.error("startButtonAction(): " + ex.getMessage(), ex);
                } catch (SchedulerException e) {
                    FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    log.error("startButtonAction(): " + e.getMessage(), e);
                }
            } else {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Debe completar la expresion Cron para iniciar el proceso.",
                                     "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
        } else {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar un proceso para continuar.", "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void stopButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding processIter =
            bindings.findIteratorBinding("XxssPeLocJobsVO1Iterator");
        
        ViewObject processVo = processIter.getViewObject();
        Row processRow = processVo.getCurrentRow();
        
        if (processRow != null) {
            String jobName = (String)processRow.getAttribute("JobName");
            
            try {
                Scheduler scheduler = new StdSchedulerFactory().getScheduler();
                scheduler.unscheduleJob(new TriggerKey(jobName + "Trigger"));
                
                processVo.executeQuery();
                
                AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
                
            } catch (SchedulerException e) {
                FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                                 "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                log.error("startAdminAction(): " + e.getMessage(), e);
            }
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar un proceso para continuar.",
                                             "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void startAdminAction(ActionEvent actionEvent) {
        // Add event code here...
        Scheduler scheduler;
        
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
        } catch (SchedulerException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                             "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            log.error("startAdminAction(): " + e.getMessage(), e);
        }
    }

    public void stopAdminAction(ActionEvent actionEvent) {
        // Add event code here...
        Scheduler scheduler;
        
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.shutdown(false);
        } catch (SchedulerException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                             "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            log.error("stopAdminAction(): " + e.getMessage(), e);
        }
    }

    public void createProcessAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }
        
        RichPopup popup = this.getProcessMainPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
        
        
    }

    public void setProcessMainPopup(RichPopup processMainPopup) {
        this.processMainPopup = processMainPopup;
    }

    public RichPopup getProcessMainPopup() {
        return processMainPopup;
    }

    public void okProcessMainPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getProcessMainPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
    }

    public void cancelProcessMainPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getProcessMainPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
    }

    public void setProcessTable(RichTable processTable) {
        this.processTable = processTable;
    }

    public RichTable getProcessTable() {
        return processTable;
    }

    public void okSchedProcPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getScheduleProcessPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
    }

    public void cancelSchedProcPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getProcessMainPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
    }

    public void setScheduleProcessPopup(RichPopup scheduleProcessPopup) {
        this.scheduleProcessPopup = scheduleProcessPopup;
    }

    public RichPopup getScheduleProcessPopup() {
        return scheduleProcessPopup;
    }

    public void scheduleButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        
        RichPopup popup = this.getScheduleProcessPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }

    public void updateProcessAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding processIter =
            bindings.findIteratorBinding("XxssPeLocJobsVO1Iterator");
        
        ViewObject processVo = processIter.getViewObject();
        Row processRow = processVo.getCurrentRow();
        
        if (processRow != null) {
            processVo.setCurrentRow(processRow);
            
            RichPopup popup = this.getProcessMainPopup();
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            popup.show(hints);
            
        } else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar un proceso para continuar.",
                                             "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public String scheduleProcessAction() {
        // Add event code here...
        RichPopup popup = this.getScheduleProcessPopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
        
        return null;
    }

    public void setSchedulerStatus(String schedulerStatus) {
        this.schedulerStatus = schedulerStatus;
    }

    public String getSchedulerStatus() {
        
        Scheduler scheduler;
        
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            if (scheduler.isStarted()) {
                schedulerStatus = "Activo";
            } else {
                schedulerStatus = "Inactivo";
            }
        } catch (SchedulerException e) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                             "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            log.error("stopAdminAction(): " + e.getMessage(), e);
        } 
        
        return schedulerStatus;
    }

    public void okParamPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getParamProcessPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
    }

    public void cancelParamPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getParamProcessPopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getProcessTable());
    }

    public void setParamProcessPopup(RichPopup paramProcessPopup) {
        this.paramProcessPopup = paramProcessPopup;
    }

    public RichPopup getParamProcessPopup() {
        return paramProcessPopup;
    }

    public void setExecutionHistJobPopup(RichPopup executionHistJobPopup) {
        this.executionHistJobPopup = executionHistJobPopup;
    }

    public RichPopup getExecutionHistJobPopup() {
        return executionHistJobPopup;
    }

    public void JobLogDownloadActionListener(FacesContext facesContext, OutputStream outputStream) {
        // Add event code here...
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("LogFile");
        if (attr == null) {
            log.error("Can't find OutputFile binding...");
            return;
        }
        
        ClobDomain clob = (ClobDomain)attr.getInputValue();
        
        try {
            IOUtils.copy(clob.getInputStream(), outputStream);
            
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            clob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en JobLogDownloadActionListener(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
    }

    public void deleteProcessAction(ActionEvent actionEvent) {
        // Add event code here...
        Object result;
        BindingContainer bindings = getBindings();
        OperationBinding operationBindingDelete =
            bindings.getOperationBinding("Delete");
        result = operationBindingDelete.execute();
        if (!operationBindingDelete.getErrors().isEmpty()) {
            return;
        }

        OperationBinding operationBindingCommit =
            bindings.getOperationBinding("Commit");
        result = operationBindingCommit.execute();
        if (!operationBindingCommit.getErrors().isEmpty()) {
            return;
        }
    }
}
