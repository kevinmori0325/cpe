
package com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioninterfacelinktodff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioninterfacelinktodff package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InterfaceLineLinkToFLEXDOO_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "interfaceLineLinkToFLEXDOO");
    private final static QName _InterfaceLineLinkToFLEX_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "interfaceLineLinkToFLEX");
    private final static QName _GbPcContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "gbPcContext");
    private final static QName _InterfaceLineLinkToFLEXFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "__FLEX_Context");
    private final static QName _InterfaceLineLinkToFLEXFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_FLEX_NumOfSegments");
    private final static QName _InterfaceLineLinkToFLEXInterfaceLineGuid_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "InterfaceLineGuid");
    private final static QName _InterfaceLineLinkToFLEXFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "__FLEX_Context_DisplayValue");
    private final static QName _InterfaceLineLinkToFLEXDOOPriceAdjustmentID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Price__Adjustment__ID");
    private final static QName _InterfaceLineLinkToFLEXDOOBillOfLadingNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Bill__of__Lading__Number");
    private final static QName _InterfaceLineLinkToFLEXDOOProfitCenterBusinessUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Profit__Center__Business__Unit");
    private final static QName _InterfaceLineLinkToFLEXDOOWayBillNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_WayBill__Number");
    private final static QName _InterfaceLineLinkToFLEXDOOFulfillLineSplitReference_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Fulfill__Line__Split__Reference");
    private final static QName _InterfaceLineLinkToFLEXDOODOOOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_DOO__Order__Number");
    private final static QName _InterfaceLineLinkToFLEXDOOFulfillmentLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Fulfillment__Line__Number");
    private final static QName _InterfaceLineLinkToFLEXDOOSourceOrderSystem_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Source__Order__System");
    private final static QName _InterfaceLineLinkToFLEXDOODeliveryName_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Delivery__Name");
    private final static QName _InterfaceLineLinkToFLEXDOOSourceOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Source__Order__Number");
    private final static QName _InterfaceLineLinkToFLEXDOOFulfillmentLineID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Fulfillment__Line__ID");
    private final static QName _InterfaceLineLinkToFLEXDOOPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "period");
    private final static QName _InterfaceLineLinkToFLEXDOOCustomerItem_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Customer__Item");
    private final static QName _InterfaceLineLinkToFLEXDOOSourceScheduleNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "_Source__Schedule__Number");
    private final static QName _GbPcContextNoLinea_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "noLinea");
    private final static QName _GbPcContextAgencia_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "agencia");
    private final static QName _GbPcContextFecha_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "fecha");
    private final static QName _GbPcContextTipoDeTransaccion_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "tipoDeTransaccion");
    private final static QName _GbPcContextNoTrx_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", "noTrx");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioninterfacelinktodff
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GbPcContext }
     * 
     */
    public GbPcContext createGbPcContext() {
        return new GbPcContext();
    }

    /**
     * Create an instance of {@link InterfaceLineLinkToFLEXDOO }
     * 
     */
    public InterfaceLineLinkToFLEXDOO createInterfaceLineLinkToFLEXDOO() {
        return new InterfaceLineLinkToFLEXDOO();
    }

    /**
     * Create an instance of {@link InterfaceLineLinkToFLEX }
     * 
     */
    public InterfaceLineLinkToFLEX createInterfaceLineLinkToFLEX() {
        return new InterfaceLineLinkToFLEX();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterfaceLineLinkToFLEXDOO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "interfaceLineLinkToFLEXDOO")
    public JAXBElement<InterfaceLineLinkToFLEXDOO> createInterfaceLineLinkToFLEXDOO(InterfaceLineLinkToFLEXDOO value) {
        return new JAXBElement<InterfaceLineLinkToFLEXDOO>(_InterfaceLineLinkToFLEXDOO_QNAME, InterfaceLineLinkToFLEXDOO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InterfaceLineLinkToFLEX }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "interfaceLineLinkToFLEX")
    public JAXBElement<InterfaceLineLinkToFLEX> createInterfaceLineLinkToFLEX(InterfaceLineLinkToFLEX value) {
        return new JAXBElement<InterfaceLineLinkToFLEX>(_InterfaceLineLinkToFLEX_QNAME, InterfaceLineLinkToFLEX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GbPcContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "gbPcContext")
    public JAXBElement<GbPcContext> createGbPcContext(GbPcContext value) {
        return new JAXBElement<GbPcContext>(_GbPcContext_QNAME, GbPcContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "__FLEX_Context", scope = InterfaceLineLinkToFLEX.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXFLEXContext(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXFLEXContext_QNAME, String.class, InterfaceLineLinkToFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_FLEX_NumOfSegments", scope = InterfaceLineLinkToFLEX.class)
    public JAXBElement<Integer> createInterfaceLineLinkToFLEXFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_InterfaceLineLinkToFLEXFLEXNumOfSegments_QNAME, Integer.class, InterfaceLineLinkToFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "InterfaceLineGuid", scope = InterfaceLineLinkToFLEX.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXInterfaceLineGuid(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXInterfaceLineGuid_QNAME, String.class, InterfaceLineLinkToFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "__FLEX_Context_DisplayValue", scope = InterfaceLineLinkToFLEX.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXFLEXContextDisplayValue_QNAME, String.class, InterfaceLineLinkToFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Price__Adjustment__ID", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOPriceAdjustmentID(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOPriceAdjustmentID_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Bill__of__Lading__Number", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOBillOfLadingNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOBillOfLadingNumber_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Profit__Center__Business__Unit", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOProfitCenterBusinessUnit(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOProfitCenterBusinessUnit_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_WayBill__Number", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOWayBillNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOWayBillNumber_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Fulfill__Line__Split__Reference", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOFulfillLineSplitReference(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOFulfillLineSplitReference_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_DOO__Order__Number", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOODOOOrderNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOODOOOrderNumber_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Fulfillment__Line__Number", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOFulfillmentLineNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOFulfillmentLineNumber_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Source__Order__System", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOSourceOrderSystem(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOSourceOrderSystem_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Delivery__Name", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOODeliveryName(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOODeliveryName_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Source__Order__Number", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOSourceOrderNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOSourceOrderNumber_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Fulfillment__Line__ID", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOFulfillmentLineID(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOFulfillmentLineID_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "period", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOPeriod(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOPeriod_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Customer__Item", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOCustomerItem(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOCustomerItem_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "_Source__Schedule__Number", scope = InterfaceLineLinkToFLEXDOO.class)
    public JAXBElement<String> createInterfaceLineLinkToFLEXDOOSourceScheduleNumber(String value) {
        return new JAXBElement<String>(_InterfaceLineLinkToFLEXDOOSourceScheduleNumber_QNAME, String.class, InterfaceLineLinkToFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "noLinea", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextNoLinea(String value) {
        return new JAXBElement<String>(_GbPcContextNoLinea_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "agencia", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextAgencia(String value) {
        return new JAXBElement<String>(_GbPcContextAgencia_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "fecha", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextFecha(String value) {
        return new JAXBElement<String>(_GbPcContextFecha_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "tipoDeTransaccion", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextTipoDeTransaccion(String value) {
        return new JAXBElement<String>(_GbPcContextTipoDeTransaccion_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionInterfaceLinkToDff/", name = "noTrx", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextNoTrx(String value) {
        return new JAXBElement<String>(_GbPcContextNoTrx_QNAME, String.class, GbPcContext.class, value);
    }

}
