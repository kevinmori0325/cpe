
package com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactionsalescreditinterfacelinedff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactionsalescreditinterfacelinedff package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OraCurriculumFees_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "oraCurriculumFees");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXGLOBAL_5FPROCUREMENT");
    private final static QName _CPQCloud_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "cPQCloud");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINVOICES_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXCONTRACT__INVOICES");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANY_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXINTERCOMPANY");
    private final static QName _OraGeneralFees_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "oraGeneralFees");
    private final static QName _RecurringBill_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "recurringBill");
    private final static QName _TransactionInterfaceLineFLEX_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEX");
    private final static QName _GbPcContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "gbPcContext");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXCONTRACT__INTERNAL__INVOICES");
    private final static QName _TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXINTERNAL_5FALLOCATIONS");
    private final static QName _TransactionInterfaceLineFLEXDOO_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXDOO");
    private final static QName _GbInicial_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "gbInicial");
    private final static QName _TransactionInterfaceLineFLEXFOS_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionInterfaceLineFLEXFOS");
    private final static QName _TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSBatchNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_batch_number");
    private final static QName _TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSTrxId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_trx_id");
    private final static QName _TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSBatchId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_batch_id");
    private final static QName _TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSLineId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_line_id");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractOrganization_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Contract__organization");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESReceivingProjectNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Receiving__Project__Number");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESDraftInvoiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Draft__Invoice__Number");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Contract__Id");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESType_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Type");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Contract__Number");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESReceivingBusinessUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Receiving__Business__Unit");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESLineId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Line__Id");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPOLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_PO__Line__Number");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPOLineLocationID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_PO__Line__Location__ID");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPurchasingOperatingUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Purchasing__Operating__Unit");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReceivingOperatingUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Receiving__Operating__Unit");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPONumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_PO__Number");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTCreateAPInvoice_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Create__AP__Invoice");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReference_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Reference");
    private final static QName _TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReceivingInventoryOrg_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Receiving__Inventory__Org");
    private final static QName _TransactionInterfaceLineFLEXCONTRACTINVOICESContractOrganization_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Contract__Organization");
    private final static QName _RecurringBillBillPlanName_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "billPlanName");
    private final static QName _RecurringBillLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "lineNumber");
    private final static QName _RecurringBillPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "period");
    private final static QName _OraGeneralFeesFeeId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "feeId");
    private final static QName _OraGeneralFeesAcadPeriodDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "acadPeriod_Display");
    private final static QName _OraGeneralFeesInstitutionDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "institution_Display");
    private final static QName _OraGeneralFeesTransactionPost_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "transactionPost");
    private final static QName _OraGeneralFeesDiscId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "discId");
    private final static QName _OraGeneralFeesAcadPeriod_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "acadPeriod");
    private final static QName _OraGeneralFeesReference_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "reference");
    private final static QName _OraGeneralFeesInstitution_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "institution");
    private final static QName _OraGeneralFeesDiscIdDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "discId_Display");
    private final static QName _OraGeneralFeesFeeIdDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "feeId_Display");
    private final static QName _TransactionInterfaceLineFLEXFOSProfitCenterBusinessUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Profit__Center__Business__Unit");
    private final static QName _TransactionInterfaceLineFLEXFOSFinancialTradeRelationship_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Financial__Trade__Relationship");
    private final static QName _TransactionInterfaceLineFLEXFOSEventType_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Event__Type");
    private final static QName _TransactionInterfaceLineFLEXFOSFOSFlowInstanceID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_FOS__Flow__Instance__ID");
    private final static QName _TransactionInterfaceLineFLEXFOSEventHeaderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Event__Header__Number__");
    private final static QName _TransactionInterfaceLineFLEXFOSPrimaryTradeRelationship_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Primary__Trade__Relationship");
    private final static QName _TransactionInterfaceLineFLEXFOSAgreementNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Agreement__Number");
    private final static QName _TransactionInterfaceLineFLEXFOSEventID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Event__ID");
    private final static QName _TransactionInterfaceLineFLEXDOODeliveryName_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Delivery__Name");
    private final static QName _TransactionInterfaceLineFLEXDOOSourceOrderSystem_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Source__Order__System");
    private final static QName _TransactionInterfaceLineFLEXDOOFulfillmentLineID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Fulfillment__Line__ID");
    private final static QName _TransactionInterfaceLineFLEXDOOSourceOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Source__Order__Number");
    private final static QName _TransactionInterfaceLineFLEXDOOCustomerItem_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Customer__Item");
    private final static QName _TransactionInterfaceLineFLEXDOOSourceScheduleNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Source__Schedule__Number");
    private final static QName _TransactionInterfaceLineFLEXDOOPriceAdjustmentID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Price__Adjustment__ID");
    private final static QName _TransactionInterfaceLineFLEXDOOBillOfLadingNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Bill__of__Lading__Number");
    private final static QName _TransactionInterfaceLineFLEXDOOWayBillNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_WayBill__Number");
    private final static QName _TransactionInterfaceLineFLEXDOOFulfillLineSplitReference_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Fulfill__Line__Split__Reference");
    private final static QName _TransactionInterfaceLineFLEXDOOFulfillmentLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Fulfillment__Line__Number");
    private final static QName _TransactionInterfaceLineFLEXDOODOOOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_DOO__Order__Number");
    private final static QName _CPQCloudServiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "serviceNumber");
    private final static QName _CPQCloudOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "orderNumber");
    private final static QName _CPQCloudOrderLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "orderLineNumber");
    private final static QName _GbPcContextAgencia_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "agencia");
    private final static QName _GbPcContextNoLinea_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "noLinea");
    private final static QName _GbPcContextFecha_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "fecha");
    private final static QName _GbPcContextTipoDeTransaccion_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "tipoDeTransaccion");
    private final static QName _GbPcContextNoTrx_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "noTrx");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYOrderLineID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Order__Line__ID");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYShippingWarehouse_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Shipping__Warehouse");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYOrderLineNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Order__Line__Number");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYOrderOrgID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Order__Org__ID");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYOrderNumber_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Order__Number");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYPriceAdjustmentID2FOrderID_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Price__Adjustment__ID_2FOrder__ID");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYSellingOperatingUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Selling__Operating__Unit");
    private final static QName _TransactionInterfaceLineFLEXINTERCOMPANYShippingOperatingUnit_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_Shipping__Operating__Unit");
    private final static QName _TransactionInterfaceLineFLEXFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "__FLEX_Context");
    private final static QName _TransactionInterfaceLineFLEXFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "_FLEX_NumOfSegments");
    private final static QName _TransactionInterfaceLineFLEXInterfaceSalescreditGuid_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "InterfaceSalescreditGuid");
    private final static QName _TransactionInterfaceLineFLEXFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "__FLEX_Context_DisplayValue");
    private final static QName _OraCurriculumFeesCurrIdDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "currId_Display");
    private final static QName _OraCurriculumFeesAdjReason_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "adjReason");
    private final static QName _OraCurriculumFeesStdntCurrIdDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "stdntCurrId_Display");
    private final static QName _OraCurriculumFeesCurrId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "currId");
    private final static QName _OraCurriculumFeesAdjCalDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "adjCal_Display");
    private final static QName _OraCurriculumFeesAdjReasonDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "adjReason_Display");
    private final static QName _OraCurriculumFeesAdjCal_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "adjCal");
    private final static QName _OraCurriculumFeesStdntCurrId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "stdntCurrId");
    private final static QName _GbInicialCiPaisTipo_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "ciPaisTipo");
    private final static QName _GbInicialCiNroTransaccion_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "ciNroTransaccion");
    private final static QName _GbInicialCiTipoLinea_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", "ciTipoLinea");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactionsalescreditinterfacelinedff
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GbPcContext }
     * 
     */
    public GbPcContext createGbPcContext() {
        return new GbPcContext();
    }

    /**
     * Create an instance of {@link GbInicial }
     * 
     */
    public GbInicial createGbInicial() {
        return new GbInicial();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXFOS }
     * 
     */
    public TransactionInterfaceLineFLEXFOS createTransactionInterfaceLineFLEXFOS() {
        return new TransactionInterfaceLineFLEXFOS();
    }

    /**
     * Create an instance of {@link RecurringBill }
     * 
     */
    public RecurringBill createRecurringBill() {
        return new RecurringBill();
    }

    /**
     * Create an instance of {@link OraCurriculumFees }
     * 
     */
    public OraCurriculumFees createOraCurriculumFees() {
        return new OraCurriculumFees();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS }
     * 
     */
    public TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS createTransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS() {
        return new TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEX }
     * 
     */
    public TransactionInterfaceLineFLEX createTransactionInterfaceLineFLEX() {
        return new TransactionInterfaceLineFLEX();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXDOO }
     * 
     */
    public TransactionInterfaceLineFLEXDOO createTransactionInterfaceLineFLEXDOO() {
        return new TransactionInterfaceLineFLEXDOO();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXINTERCOMPANY }
     * 
     */
    public TransactionInterfaceLineFLEXINTERCOMPANY createTransactionInterfaceLineFLEXINTERCOMPANY() {
        return new TransactionInterfaceLineFLEXINTERCOMPANY();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES }
     * 
     */
    public TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES() {
        return new TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES();
    }

    /**
     * Create an instance of {@link CPQCloud }
     * 
     */
    public CPQCloud createCPQCloud() {
        return new CPQCloud();
    }

    /**
     * Create an instance of {@link OraGeneralFees }
     * 
     */
    public OraGeneralFees createOraGeneralFees() {
        return new OraGeneralFees();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXCONTRACTINVOICES }
     * 
     */
    public TransactionInterfaceLineFLEXCONTRACTINVOICES createTransactionInterfaceLineFLEXCONTRACTINVOICES() {
        return new TransactionInterfaceLineFLEXCONTRACTINVOICES();
    }

    /**
     * Create an instance of {@link TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT }
     * 
     */
    public TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT() {
        return new TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OraCurriculumFees }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "oraCurriculumFees")
    public JAXBElement<OraCurriculumFees> createOraCurriculumFees(OraCurriculumFees value) {
        return new JAXBElement<OraCurriculumFees>(_OraCurriculumFees_QNAME, OraCurriculumFees.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXGLOBAL_5FPROCUREMENT")
    public JAXBElement<TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT(TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT value) {
        return new JAXBElement<TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT_QNAME, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CPQCloud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "cPQCloud")
    public JAXBElement<CPQCloud> createCPQCloud(CPQCloud value) {
        return new JAXBElement<CPQCloud>(_CPQCloud_QNAME, CPQCloud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXCONTRACTINVOICES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXCONTRACT__INVOICES")
    public JAXBElement<TransactionInterfaceLineFLEXCONTRACTINVOICES> createTransactionInterfaceLineFLEXCONTRACTINVOICES(TransactionInterfaceLineFLEXCONTRACTINVOICES value) {
        return new JAXBElement<TransactionInterfaceLineFLEXCONTRACTINVOICES>(_TransactionInterfaceLineFLEXCONTRACTINVOICES_QNAME, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXINTERCOMPANY }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXINTERCOMPANY")
    public JAXBElement<TransactionInterfaceLineFLEXINTERCOMPANY> createTransactionInterfaceLineFLEXINTERCOMPANY(TransactionInterfaceLineFLEXINTERCOMPANY value) {
        return new JAXBElement<TransactionInterfaceLineFLEXINTERCOMPANY>(_TransactionInterfaceLineFLEXINTERCOMPANY_QNAME, TransactionInterfaceLineFLEXINTERCOMPANY.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OraGeneralFees }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "oraGeneralFees")
    public JAXBElement<OraGeneralFees> createOraGeneralFees(OraGeneralFees value) {
        return new JAXBElement<OraGeneralFees>(_OraGeneralFees_QNAME, OraGeneralFees.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecurringBill }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "recurringBill")
    public JAXBElement<RecurringBill> createRecurringBill(RecurringBill value) {
        return new JAXBElement<RecurringBill>(_RecurringBill_QNAME, RecurringBill.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEX }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEX")
    public JAXBElement<TransactionInterfaceLineFLEX> createTransactionInterfaceLineFLEX(TransactionInterfaceLineFLEX value) {
        return new JAXBElement<TransactionInterfaceLineFLEX>(_TransactionInterfaceLineFLEX_QNAME, TransactionInterfaceLineFLEX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GbPcContext }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "gbPcContext")
    public JAXBElement<GbPcContext> createGbPcContext(GbPcContext value) {
        return new JAXBElement<GbPcContext>(_GbPcContext_QNAME, GbPcContext.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXCONTRACT__INTERNAL__INVOICES")
    public JAXBElement<TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES(TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES value) {
        return new JAXBElement<TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES_QNAME, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXINTERNAL_5FALLOCATIONS")
    public JAXBElement<TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS> createTransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS(TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS value) {
        return new JAXBElement<TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS>(_TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS_QNAME, TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXDOO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXDOO")
    public JAXBElement<TransactionInterfaceLineFLEXDOO> createTransactionInterfaceLineFLEXDOO(TransactionInterfaceLineFLEXDOO value) {
        return new JAXBElement<TransactionInterfaceLineFLEXDOO>(_TransactionInterfaceLineFLEXDOO_QNAME, TransactionInterfaceLineFLEXDOO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GbInicial }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "gbInicial")
    public JAXBElement<GbInicial> createGbInicial(GbInicial value) {
        return new JAXBElement<GbInicial>(_GbInicial_QNAME, GbInicial.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionInterfaceLineFLEXFOS }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionInterfaceLineFLEXFOS")
    public JAXBElement<TransactionInterfaceLineFLEXFOS> createTransactionInterfaceLineFLEXFOS(TransactionInterfaceLineFLEXFOS value) {
        return new JAXBElement<TransactionInterfaceLineFLEXFOS>(_TransactionInterfaceLineFLEXFOS_QNAME, TransactionInterfaceLineFLEXFOS.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_batch_number", scope = TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSBatchNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSBatchNumber_QNAME, String.class, TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_trx_id", scope = TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSTrxId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSTrxId_QNAME, String.class, TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_batch_id", scope = TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSBatchId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSBatchId_QNAME, String.class, TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_line_id", scope = TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSLineId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONSLineId_QNAME, String.class, TransactionInterfaceLineFLEXINTERNAL5FALLOCATIONS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Contract__organization", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractOrganization(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractOrganization_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Receiving__Project__Number", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESReceivingProjectNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESReceivingProjectNumber_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Draft__Invoice__Number", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESDraftInvoiceNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESDraftInvoiceNumber_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Contract__Id", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractId_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Type", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESType(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESType_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Contract__Number", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractNumber_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Receiving__Business__Unit", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESReceivingBusinessUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESReceivingBusinessUnit_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Line__Id", scope = TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESLineId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESLineId_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_PO__Line__Number", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPOLineNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPOLineNumber_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_PO__Line__Location__ID", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPOLineLocationID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPOLineLocationID_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Purchasing__Operating__Unit", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPurchasingOperatingUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPurchasingOperatingUnit_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Receiving__Operating__Unit", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReceivingOperatingUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReceivingOperatingUnit_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_PO__Number", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPONumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTPONumber_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Create__AP__Invoice", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTCreateAPInvoice(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTCreateAPInvoice_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Reference", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReference(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReference_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Receiving__Inventory__Org", scope = TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReceivingInventoryOrg(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReceivingInventoryOrg_QNAME, String.class, TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENT.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Draft__Invoice__Number", scope = TransactionInterfaceLineFLEXCONTRACTINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINVOICESDraftInvoiceNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESDraftInvoiceNumber_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Contract__Id", scope = TransactionInterfaceLineFLEXCONTRACTINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINVOICESContractId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractId_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Contract__Organization", scope = TransactionInterfaceLineFLEXCONTRACTINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINVOICESContractOrganization(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINVOICESContractOrganization_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Type", scope = TransactionInterfaceLineFLEXCONTRACTINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINVOICESType(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESType_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Contract__Number", scope = TransactionInterfaceLineFLEXCONTRACTINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINVOICESContractNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESContractNumber_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Line__Id", scope = TransactionInterfaceLineFLEXCONTRACTINVOICES.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXCONTRACTINVOICESLineId(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXCONTRACTINTERNALINVOICESLineId_QNAME, String.class, TransactionInterfaceLineFLEXCONTRACTINVOICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "billPlanName", scope = RecurringBill.class)
    public JAXBElement<String> createRecurringBillBillPlanName(String value) {
        return new JAXBElement<String>(_RecurringBillBillPlanName_QNAME, String.class, RecurringBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "lineNumber", scope = RecurringBill.class)
    public JAXBElement<String> createRecurringBillLineNumber(String value) {
        return new JAXBElement<String>(_RecurringBillLineNumber_QNAME, String.class, RecurringBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "period", scope = RecurringBill.class)
    public JAXBElement<String> createRecurringBillPeriod(String value) {
        return new JAXBElement<String>(_RecurringBillPeriod_QNAME, String.class, RecurringBill.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "feeId", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesFeeId(String value) {
        return new JAXBElement<String>(_OraGeneralFeesFeeId_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "acadPeriod_Display", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesAcadPeriodDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesAcadPeriodDisplay_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "institution_Display", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesInstitutionDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesInstitutionDisplay_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionPost", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesTransactionPost(String value) {
        return new JAXBElement<String>(_OraGeneralFeesTransactionPost_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "discId", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesDiscId(String value) {
        return new JAXBElement<String>(_OraGeneralFeesDiscId_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "acadPeriod", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesAcadPeriod(String value) {
        return new JAXBElement<String>(_OraGeneralFeesAcadPeriod_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "reference", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesReference(String value) {
        return new JAXBElement<String>(_OraGeneralFeesReference_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "institution", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesInstitution(String value) {
        return new JAXBElement<String>(_OraGeneralFeesInstitution_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "discId_Display", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesDiscIdDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesDiscIdDisplay_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "feeId_Display", scope = OraGeneralFees.class)
    public JAXBElement<String> createOraGeneralFeesFeeIdDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesFeeIdDisplay_QNAME, String.class, OraGeneralFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Profit__Center__Business__Unit", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSProfitCenterBusinessUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSProfitCenterBusinessUnit_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Financial__Trade__Relationship", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSFinancialTradeRelationship(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSFinancialTradeRelationship_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Event__Type", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSEventType(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSEventType_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_FOS__Flow__Instance__ID", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSFOSFlowInstanceID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSFOSFlowInstanceID_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Event__Header__Number__", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSEventHeaderNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSEventHeaderNumber_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Primary__Trade__Relationship", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSPrimaryTradeRelationship(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSPrimaryTradeRelationship_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Agreement__Number", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSAgreementNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSAgreementNumber_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Event__ID", scope = TransactionInterfaceLineFLEXFOS.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFOSEventID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSEventID_QNAME, String.class, TransactionInterfaceLineFLEXFOS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Delivery__Name", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOODeliveryName(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOODeliveryName_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Source__Order__System", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOSourceOrderSystem(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOSourceOrderSystem_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Fulfillment__Line__ID", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOFulfillmentLineID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOFulfillmentLineID_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Source__Order__Number", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOSourceOrderNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOSourceOrderNumber_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Customer__Item", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOCustomerItem(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOCustomerItem_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Source__Schedule__Number", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOSourceScheduleNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOSourceScheduleNumber_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "period", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOPeriod(String value) {
        return new JAXBElement<String>(_RecurringBillPeriod_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Profit__Center__Business__Unit", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOProfitCenterBusinessUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFOSProfitCenterBusinessUnit_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Price__Adjustment__ID", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOPriceAdjustmentID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOPriceAdjustmentID_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Bill__of__Lading__Number", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOBillOfLadingNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOBillOfLadingNumber_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_WayBill__Number", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOWayBillNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOWayBillNumber_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Fulfill__Line__Split__Reference", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOFulfillLineSplitReference(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOFulfillLineSplitReference_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Fulfillment__Line__Number", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOOFulfillmentLineNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOOFulfillmentLineNumber_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_DOO__Order__Number", scope = TransactionInterfaceLineFLEXDOO.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXDOODOOOrderNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXDOODOOOrderNumber_QNAME, String.class, TransactionInterfaceLineFLEXDOO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "serviceNumber", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudServiceNumber(String value) {
        return new JAXBElement<String>(_CPQCloudServiceNumber_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "orderNumber", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudOrderNumber(String value) {
        return new JAXBElement<String>(_CPQCloudOrderNumber_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "orderLineNumber", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudOrderLineNumber(String value) {
        return new JAXBElement<String>(_CPQCloudOrderLineNumber_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "period", scope = CPQCloud.class)
    public JAXBElement<String> createCPQCloudPeriod(String value) {
        return new JAXBElement<String>(_RecurringBillPeriod_QNAME, String.class, CPQCloud.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "agencia", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextAgencia(String value) {
        return new JAXBElement<String>(_GbPcContextAgencia_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "noLinea", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextNoLinea(String value) {
        return new JAXBElement<String>(_GbPcContextNoLinea_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "fecha", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextFecha(String value) {
        return new JAXBElement<String>(_GbPcContextFecha_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "tipoDeTransaccion", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextTipoDeTransaccion(String value) {
        return new JAXBElement<String>(_GbPcContextTipoDeTransaccion_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "noTrx", scope = GbPcContext.class)
    public JAXBElement<String> createGbPcContextNoTrx(String value) {
        return new JAXBElement<String>(_GbPcContextNoTrx_QNAME, String.class, GbPcContext.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Order__Line__ID", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYOrderLineID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYOrderLineID_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Shipping__Warehouse", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYShippingWarehouse(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYShippingWarehouse_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Order__Line__Number", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYOrderLineNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYOrderLineNumber_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Order__Org__ID", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYOrderOrgID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYOrderOrgID_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Order__Number", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYOrderNumber(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYOrderNumber_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Price__Adjustment__ID_2FOrder__ID", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYPriceAdjustmentID2FOrderID(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYPriceAdjustmentID2FOrderID_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Selling__Operating__Unit", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYSellingOperatingUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYSellingOperatingUnit_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Create__AP__Invoice", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYCreateAPInvoice(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTCreateAPInvoice_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Reference", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYReference(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXGLOBAL5FPROCUREMENTReference_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_Shipping__Operating__Unit", scope = TransactionInterfaceLineFLEXINTERCOMPANY.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXINTERCOMPANYShippingOperatingUnit(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXINTERCOMPANYShippingOperatingUnit_QNAME, String.class, TransactionInterfaceLineFLEXINTERCOMPANY.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "__FLEX_Context", scope = TransactionInterfaceLineFLEX.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFLEXContext_QNAME, String.class, TransactionInterfaceLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "_FLEX_NumOfSegments", scope = TransactionInterfaceLineFLEX.class)
    public JAXBElement<Integer> createTransactionInterfaceLineFLEXFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionInterfaceLineFLEXFLEXNumOfSegments_QNAME, Integer.class, TransactionInterfaceLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "InterfaceSalescreditGuid", scope = TransactionInterfaceLineFLEX.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXInterfaceSalescreditGuid(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXInterfaceSalescreditGuid_QNAME, String.class, TransactionInterfaceLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "__FLEX_Context_DisplayValue", scope = TransactionInterfaceLineFLEX.class)
    public JAXBElement<String> createTransactionInterfaceLineFLEXFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionInterfaceLineFLEXFLEXContextDisplayValue_QNAME, String.class, TransactionInterfaceLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "feeId", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesFeeId(String value) {
        return new JAXBElement<String>(_OraGeneralFeesFeeId_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "institution_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesInstitutionDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesInstitutionDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "currId_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesCurrIdDisplay(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesCurrIdDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "transactionPost", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesTransactionPost(String value) {
        return new JAXBElement<String>(_OraGeneralFeesTransactionPost_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "adjReason", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesAdjReason(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesAdjReason_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "acadPeriod", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesAcadPeriod(String value) {
        return new JAXBElement<String>(_OraGeneralFeesAcadPeriod_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "discId_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesDiscIdDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesDiscIdDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "feeId_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesFeeIdDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesFeeIdDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "acadPeriod_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesAcadPeriodDisplay(String value) {
        return new JAXBElement<String>(_OraGeneralFeesAcadPeriodDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "stdntCurrId_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesStdntCurrIdDisplay(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesStdntCurrIdDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "currId", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesCurrId(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesCurrId_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "adjCal_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesAdjCalDisplay(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesAdjCalDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "adjReason_Display", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesAdjReasonDisplay(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesAdjReasonDisplay_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "adjCal", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesAdjCal(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesAdjCal_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "discId", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesDiscId(String value) {
        return new JAXBElement<String>(_OraGeneralFeesDiscId_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "institution", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesInstitution(String value) {
        return new JAXBElement<String>(_OraGeneralFeesInstitution_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "stdntCurrId", scope = OraCurriculumFees.class)
    public JAXBElement<String> createOraCurriculumFeesStdntCurrId(String value) {
        return new JAXBElement<String>(_OraCurriculumFeesStdntCurrId_QNAME, String.class, OraCurriculumFees.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "ciPaisTipo", scope = GbInicial.class)
    public JAXBElement<String> createGbInicialCiPaisTipo(String value) {
        return new JAXBElement<String>(_GbInicialCiPaisTipo_QNAME, String.class, GbInicial.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "ciNroTransaccion", scope = GbInicial.class)
    public JAXBElement<String> createGbInicialCiNroTransaccion(String value) {
        return new JAXBElement<String>(_GbInicialCiNroTransaccion_QNAME, String.class, GbInicial.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionSalesCreditInterfaceLineDff/", name = "ciTipoLinea", scope = GbInicial.class)
    public JAXBElement<String> createGbInicialCiTipoLinea(String value) {
        return new JAXBElement<String>(_GbInicialCiTipoLinea_QNAME, String.class, GbInicial.class, value);
    }

}
