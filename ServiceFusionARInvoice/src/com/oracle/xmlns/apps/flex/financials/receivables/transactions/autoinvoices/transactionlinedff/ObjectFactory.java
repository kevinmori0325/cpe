
package com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactionlinedff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactionlinedff package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Perupercep_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "perupercep");
    private final static QName _GbInicial_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "gbInicial");
    private final static QName _TransactionLineFLEX_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "transactionLineFLEX");
    private final static QName _PeruPercepcion_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "peruPercepcion");
    private final static QName _PerupercepLocPeArPercCashId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "locPeArPercCashId");
    private final static QName _PerupercepLocPeArPercSerie_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "locPeArPercSerie");
    private final static QName _PerupercepLocPeArPercTrxNum_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "locPeArPercTrxNum");
    private final static QName _PerupercepLocPeArPercTrxId_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "locPeArPercTrxId");
    private final static QName _PerupercepLocPeArPercCorr_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "locPeArPercCorr");
    private final static QName _GbInicialCiTipoLinea_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "ciTipoLinea");
    private final static QName _GbInicialCiPaisTipo_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "ciPaisTipo");
    private final static QName _GbInicialCiNroTransaccion_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "ciNroTransaccion");
    private final static QName _TransactionLineFLEXInterfaceLineGuid_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "InterfaceLineGuid");
    private final static QName _TransactionLineFLEXFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "_FLEX_NumOfSegments");
    private final static QName _TransactionLineFLEXFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionLineFLEXFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", "__FLEX_Context");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactionlinedff
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GbInicial }
     * 
     */
    public GbInicial createGbInicial() {
        return new GbInicial();
    }

    /**
     * Create an instance of {@link PeruPercepcion }
     * 
     */
    public PeruPercepcion createPeruPercepcion() {
        return new PeruPercepcion();
    }

    /**
     * Create an instance of {@link Perupercep }
     * 
     */
    public Perupercep createPerupercep() {
        return new Perupercep();
    }

    /**
     * Create an instance of {@link TransactionLineFLEX }
     * 
     */
    public TransactionLineFLEX createTransactionLineFLEX() {
        return new TransactionLineFLEX();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Perupercep }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "perupercep")
    public JAXBElement<Perupercep> createPerupercep(Perupercep value) {
        return new JAXBElement<Perupercep>(_Perupercep_QNAME, Perupercep.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GbInicial }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "gbInicial")
    public JAXBElement<GbInicial> createGbInicial(GbInicial value) {
        return new JAXBElement<GbInicial>(_GbInicial_QNAME, GbInicial.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLineFLEX }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "transactionLineFLEX")
    public JAXBElement<TransactionLineFLEX> createTransactionLineFLEX(TransactionLineFLEX value) {
        return new JAXBElement<TransactionLineFLEX>(_TransactionLineFLEX_QNAME, TransactionLineFLEX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeruPercepcion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "peruPercepcion")
    public JAXBElement<PeruPercepcion> createPeruPercepcion(PeruPercepcion value) {
        return new JAXBElement<PeruPercepcion>(_PeruPercepcion_QNAME, PeruPercepcion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercCashId", scope = Perupercep.class)
    public JAXBElement<String> createPerupercepLocPeArPercCashId(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercCashId_QNAME, String.class, Perupercep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercSerie", scope = Perupercep.class)
    public JAXBElement<String> createPerupercepLocPeArPercSerie(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercSerie_QNAME, String.class, Perupercep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercTrxNum", scope = Perupercep.class)
    public JAXBElement<String> createPerupercepLocPeArPercTrxNum(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercTrxNum_QNAME, String.class, Perupercep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercTrxId", scope = Perupercep.class)
    public JAXBElement<String> createPerupercepLocPeArPercTrxId(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercTrxId_QNAME, String.class, Perupercep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercCorr", scope = Perupercep.class)
    public JAXBElement<String> createPerupercepLocPeArPercCorr(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercCorr_QNAME, String.class, Perupercep.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "ciTipoLinea", scope = GbInicial.class)
    public JAXBElement<String> createGbInicialCiTipoLinea(String value) {
        return new JAXBElement<String>(_GbInicialCiTipoLinea_QNAME, String.class, GbInicial.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "ciPaisTipo", scope = GbInicial.class)
    public JAXBElement<String> createGbInicialCiPaisTipo(String value) {
        return new JAXBElement<String>(_GbInicialCiPaisTipo_QNAME, String.class, GbInicial.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "ciNroTransaccion", scope = GbInicial.class)
    public JAXBElement<String> createGbInicialCiNroTransaccion(String value) {
        return new JAXBElement<String>(_GbInicialCiNroTransaccion_QNAME, String.class, GbInicial.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "InterfaceLineGuid", scope = TransactionLineFLEX.class)
    public JAXBElement<String> createTransactionLineFLEXInterfaceLineGuid(String value) {
        return new JAXBElement<String>(_TransactionLineFLEXInterfaceLineGuid_QNAME, String.class, TransactionLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "_FLEX_NumOfSegments", scope = TransactionLineFLEX.class)
    public JAXBElement<Integer> createTransactionLineFLEXFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionLineFLEXFLEXNumOfSegments_QNAME, Integer.class, TransactionLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "__FLEX_Context_DisplayValue", scope = TransactionLineFLEX.class)
    public JAXBElement<String> createTransactionLineFLEXFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionLineFLEXFLEXContextDisplayValue_QNAME, String.class, TransactionLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "__FLEX_Context", scope = TransactionLineFLEX.class)
    public JAXBElement<String> createTransactionLineFLEXFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionLineFLEXFLEXContext_QNAME, String.class, TransactionLineFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercCashId", scope = PeruPercepcion.class)
    public JAXBElement<String> createPeruPercepcionLocPeArPercCashId(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercCashId_QNAME, String.class, PeruPercepcion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercSerie", scope = PeruPercepcion.class)
    public JAXBElement<String> createPeruPercepcionLocPeArPercSerie(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercSerie_QNAME, String.class, PeruPercepcion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercTrxNum", scope = PeruPercepcion.class)
    public JAXBElement<String> createPeruPercepcionLocPeArPercTrxNum(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercTrxNum_QNAME, String.class, PeruPercepcion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercTrxId", scope = PeruPercepcion.class)
    public JAXBElement<String> createPeruPercepcionLocPeArPercTrxId(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercTrxId_QNAME, String.class, PeruPercepcion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionLineDff/", name = "locPeArPercCorr", scope = PeruPercepcion.class)
    public JAXBElement<String> createPeruPercepcionLocPeArPercCorr(String value) {
        return new JAXBElement<String>(_PerupercepLocPeArPercCorr_QNAME, String.class, PeruPercepcion.class, value);
    }

}
