
package com.oracle.xmlns.apps.flex.financials.receivables.transactions.autoinvoices.transactioncontingencyinterfacelinedff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GbInicial complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GbInicial">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionContingencyInterfaceLineDff/}TransactionInterfaceLineFLEX">
 *       &lt;sequence>
 *         &lt;element name="ciPaisTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciNroTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ciTipoLinea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GbInicial", propOrder = {
    "ciPaisTipo",
    "ciNroTransaccion",
    "ciTipoLinea"
})
public class GbInicial
    extends TransactionInterfaceLineFLEX
{

    @XmlElementRef(name = "ciPaisTipo", namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionContingencyInterfaceLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> ciPaisTipo;
    @XmlElementRef(name = "ciNroTransaccion", namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionContingencyInterfaceLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> ciNroTransaccion;
    @XmlElementRef(name = "ciTipoLinea", namespace = "http://xmlns.oracle.com/apps/flex/financials/receivables/transactions/autoInvoices/TransactionContingencyInterfaceLineDff/", type = JAXBElement.class)
    protected JAXBElement<String> ciTipoLinea;

    /**
     * Gets the value of the ciPaisTipo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCiPaisTipo() {
        return ciPaisTipo;
    }

    /**
     * Sets the value of the ciPaisTipo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCiPaisTipo(JAXBElement<String> value) {
        this.ciPaisTipo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ciNroTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCiNroTransaccion() {
        return ciNroTransaccion;
    }

    /**
     * Sets the value of the ciNroTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCiNroTransaccion(JAXBElement<String> value) {
        this.ciNroTransaccion = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the ciTipoLinea property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCiTipoLinea() {
        return ciTipoLinea;
    }

    /**
     * Sets the value of the ciTipoLinea property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCiTipoLinea(JAXBElement<String> value) {
        this.ciTipoLinea = ((JAXBElement<String> ) value);
    }

}
