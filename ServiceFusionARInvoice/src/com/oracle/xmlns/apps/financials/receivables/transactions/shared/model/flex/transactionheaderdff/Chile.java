
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Chile complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Chile">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/}TransactionHeaderFLEX">
 *       &lt;sequence>
 *         &lt;element name="detalleError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detalleError3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="detalleError22" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="urlDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estadoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Chile", propOrder = {
    "detalleError",
    "detalleError3",
    "detalleError22",
    "urlDocumento",
    "estadoDocumento",
    "folio"
})
public class Chile
    extends TransactionHeaderFLEX
{

    @XmlElementRef(name = "detalleError", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> detalleError;
    @XmlElementRef(name = "detalleError3", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> detalleError3;
    @XmlElementRef(name = "detalleError22", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> detalleError22;
    @XmlElementRef(name = "urlDocumento", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> urlDocumento;
    @XmlElementRef(name = "estadoDocumento", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> estadoDocumento;
    @XmlElementRef(name = "folio", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> folio;

    /**
     * Gets the value of the detalleError property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDetalleError() {
        return detalleError;
    }

    /**
     * Sets the value of the detalleError property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDetalleError(JAXBElement<String> value) {
        this.detalleError = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the detalleError3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDetalleError3() {
        return detalleError3;
    }

    /**
     * Sets the value of the detalleError3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDetalleError3(JAXBElement<String> value) {
        this.detalleError3 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the detalleError22 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDetalleError22() {
        return detalleError22;
    }

    /**
     * Sets the value of the detalleError22 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDetalleError22(JAXBElement<String> value) {
        this.detalleError22 = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the urlDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUrlDocumento() {
        return urlDocumento;
    }

    /**
     * Sets the value of the urlDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUrlDocumento(JAXBElement<String> value) {
        this.urlDocumento = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the estadoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEstadoDocumento() {
        return estadoDocumento;
    }

    /**
     * Sets the value of the estadoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEstadoDocumento(JAXBElement<String> value) {
        this.estadoDocumento = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the folio property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFolio() {
        return folio;
    }

    /**
     * Sets the value of the folio property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFolio(JAXBElement<String> value) {
        this.folio = ((JAXBElement<String> ) value);
    }

}
