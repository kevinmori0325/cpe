
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheadergdf;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TransactionHeaderGdfJE_5FES_5FMODELO415_5F347 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionHeaderGdfJE_5FES_5FMODELO415_5F347">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/}TransactionHeaderGdf">
 *       &lt;sequence>
 *         &lt;element name="_Transmission__of__Property" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_Transmission__of__Property_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_Transaction__Date" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="_Year__of__Amount__Received__in__Cas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionHeaderGdfJE_5FES_5FMODELO415_5F347", propOrder = {
    "transmissionOfProperty",
    "transmissionOfPropertyDisplay",
    "transactionDate",
    "yearOfAmountReceivedInCas"
})
public class TransactionHeaderGdfJE5FES5FMODELO4155F347
    extends TransactionHeaderGdf
{

    @XmlElementRef(name = "_Transmission__of__Property", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> transmissionOfProperty;
    @XmlElementRef(name = "_Transmission__of__Property_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> transmissionOfPropertyDisplay;
    @XmlElementRef(name = "_Transaction__Date", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> transactionDate;
    @XmlElementRef(name = "_Year__of__Amount__Received__in__Cas", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> yearOfAmountReceivedInCas;

    /**
     * Gets the value of the transmissionOfProperty property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransmissionOfProperty() {
        return transmissionOfProperty;
    }

    /**
     * Sets the value of the transmissionOfProperty property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransmissionOfProperty(JAXBElement<String> value) {
        this.transmissionOfProperty = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the transmissionOfPropertyDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransmissionOfPropertyDisplay() {
        return transmissionOfPropertyDisplay;
    }

    /**
     * Sets the value of the transmissionOfPropertyDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransmissionOfPropertyDisplay(JAXBElement<String> value) {
        this.transmissionOfPropertyDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTransactionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.transactionDate = ((JAXBElement<XMLGregorianCalendar> ) value);
    }

    /**
     * Gets the value of the yearOfAmountReceivedInCas property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getYearOfAmountReceivedInCas() {
        return yearOfAmountReceivedInCas;
    }

    /**
     * Sets the value of the yearOfAmountReceivedInCas property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setYearOfAmountReceivedInCas(JAXBElement<String> value) {
        this.yearOfAmountReceivedInCas = ((JAXBElement<String> ) value);
    }

}
