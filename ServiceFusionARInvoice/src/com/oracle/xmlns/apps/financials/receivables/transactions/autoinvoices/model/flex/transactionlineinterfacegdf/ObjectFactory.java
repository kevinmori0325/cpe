
package com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactionlineinterfacegdf;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactionlineinterfacegdf package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionLineInterfaceGdf_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "transactionLineInterfaceGdf");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "transactionLineInterfaceGdfJE_5FIT_5FESL_5FOF_5FSERVICES");
    private final static QName _TransactionLineInterfaceGdfFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionLineInterfaceGdfFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "_FLEX_NumOfSegments");
    private final static QName _TransactionLineInterfaceGdfInterfaceLineGuid_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "InterfaceLineGuid");
    private final static QName _TransactionLineInterfaceGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "FLEX_PARAM_GLOBAL_COUNTRY_CODE");
    private final static QName _TransactionLineInterfaceGdfFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "__FLEX_Context");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "_Service__Mode_Display");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "reportingExclusionIndicator_Display");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "reportingExclusionIndicator");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "_Service__Code");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "_Service__Mode");
    private final static QName _TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", "_Service__Code_Display");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactionlineinterfacegdf
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionLineInterfaceGdf }
     * 
     */
    public TransactionLineInterfaceGdf createTransactionLineInterfaceGdf() {
        return new TransactionLineInterfaceGdf();
    }

    /**
     * Create an instance of {@link TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES }
     * 
     */
    public TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES() {
        return new TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLineInterfaceGdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "transactionLineInterfaceGdf")
    public JAXBElement<TransactionLineInterfaceGdf> createTransactionLineInterfaceGdf(TransactionLineInterfaceGdf value) {
        return new JAXBElement<TransactionLineInterfaceGdf>(_TransactionLineInterfaceGdf_QNAME, TransactionLineInterfaceGdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "transactionLineInterfaceGdfJE_5FIT_5FESL_5FOF_5FSERVICES")
    public JAXBElement<TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES(TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES value) {
        return new JAXBElement<TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES_QNAME, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "__FLEX_Context_DisplayValue", scope = TransactionLineInterfaceGdf.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfFLEXContextDisplayValue_QNAME, String.class, TransactionLineInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "_FLEX_NumOfSegments", scope = TransactionLineInterfaceGdf.class)
    public JAXBElement<Integer> createTransactionLineInterfaceGdfFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionLineInterfaceGdfFLEXNumOfSegments_QNAME, Integer.class, TransactionLineInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "InterfaceLineGuid", scope = TransactionLineInterfaceGdf.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfInterfaceLineGuid(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfInterfaceLineGuid_QNAME, String.class, TransactionLineInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "FLEX_PARAM_GLOBAL_COUNTRY_CODE", scope = TransactionLineInterfaceGdf.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfFLEXPARAMGLOBALCOUNTRYCODE(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfFLEXPARAMGLOBALCOUNTRYCODE_QNAME, String.class, TransactionLineInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "__FLEX_Context", scope = TransactionLineInterfaceGdf.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfFLEXContext_QNAME, String.class, TransactionLineInterfaceGdf.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "_Service__Mode_Display", scope = TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceModeDisplay_QNAME, String.class, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "reportingExclusionIndicator_Display", scope = TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicatorDisplay_QNAME, String.class, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "reportingExclusionIndicator", scope = TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESReportingExclusionIndicator_QNAME, String.class, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "_Service__Code", scope = TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCode(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCode_QNAME, String.class, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "_Service__Mode", scope = TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceMode(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceMode_QNAME, String.class, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionLineInterfaceGdf/", name = "_Service__Code_Display", scope = TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class)
    public JAXBElement<String> createTransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay(String value) {
        return new JAXBElement<String>(_TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICESServiceCodeDisplay_QNAME, String.class, TransactionLineInterfaceGdfJE5FIT5FESL5FOF5FSERVICES.class, value);
    }

}
