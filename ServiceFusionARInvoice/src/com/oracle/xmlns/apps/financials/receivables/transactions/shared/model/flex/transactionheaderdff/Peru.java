
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Peru complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Peru">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/}TransactionHeaderFLEX">
 *       &lt;sequence>
 *         &lt;element name="locPeArEstadoFe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArUrlFe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArTipoDocSunatRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaEmisionDocReferenciaDdMmY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArSerieCompRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="locPeArDocumentoRelacionado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Peru", propOrder = {
    "locPeArEstadoFe",
    "locPeArUrlFe",
    "locPeArTipoDocSunatRef",
    "fechaEmisionDocReferenciaDdMmY",
    "locPeArSerieCompRef",
    "locPeArDocumentoRelacionado"
})
public class Peru
    extends TransactionHeaderFLEX
{

    @XmlElementRef(name = "locPeArEstadoFe", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArEstadoFe;
    @XmlElementRef(name = "locPeArUrlFe", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArUrlFe;
    @XmlElementRef(name = "locPeArTipoDocSunatRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArTipoDocSunatRef;
    @XmlElementRef(name = "fechaEmisionDocReferenciaDdMmY", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> fechaEmisionDocReferenciaDdMmY;
    @XmlElementRef(name = "locPeArSerieCompRef", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArSerieCompRef;
    @XmlElementRef(name = "locPeArDocumentoRelacionado", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", type = JAXBElement.class)
    protected JAXBElement<String> locPeArDocumentoRelacionado;

    /**
     * Gets the value of the locPeArEstadoFe property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArEstadoFe() {
        return locPeArEstadoFe;
    }

    /**
     * Sets the value of the locPeArEstadoFe property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArEstadoFe(JAXBElement<String> value) {
        this.locPeArEstadoFe = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArUrlFe property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArUrlFe() {
        return locPeArUrlFe;
    }

    /**
     * Sets the value of the locPeArUrlFe property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArUrlFe(JAXBElement<String> value) {
        this.locPeArUrlFe = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArTipoDocSunatRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArTipoDocSunatRef() {
        return locPeArTipoDocSunatRef;
    }

    /**
     * Sets the value of the locPeArTipoDocSunatRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArTipoDocSunatRef(JAXBElement<String> value) {
        this.locPeArTipoDocSunatRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the fechaEmisionDocReferenciaDdMmY property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFechaEmisionDocReferenciaDdMmY() {
        return fechaEmisionDocReferenciaDdMmY;
    }

    /**
     * Sets the value of the fechaEmisionDocReferenciaDdMmY property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFechaEmisionDocReferenciaDdMmY(JAXBElement<String> value) {
        this.fechaEmisionDocReferenciaDdMmY = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArSerieCompRef property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArSerieCompRef() {
        return locPeArSerieCompRef;
    }

    /**
     * Sets the value of the locPeArSerieCompRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArSerieCompRef(JAXBElement<String> value) {
        this.locPeArSerieCompRef = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the locPeArDocumentoRelacionado property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocPeArDocumentoRelacionado() {
        return locPeArDocumentoRelacionado;
    }

    /**
     * Sets the value of the locPeArDocumentoRelacionado property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocPeArDocumentoRelacionado(JAXBElement<String> value) {
        this.locPeArDocumentoRelacionado = ((JAXBElement<String> ) value);
    }

}
