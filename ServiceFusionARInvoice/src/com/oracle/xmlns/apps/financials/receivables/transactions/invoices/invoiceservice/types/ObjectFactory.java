
package com.oracle.xmlns.apps.financials.receivables.transactions.invoices.invoiceservice.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.invoices.invoiceservice.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.invoices.invoiceservice.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProcessInterfaceSalesCredit }
     * 
     */
    public ProcessInterfaceSalesCredit createProcessInterfaceSalesCredit() {
        return new ProcessInterfaceSalesCredit();
    }

    /**
     * Create an instance of {@link CreateSimpleInvoiceResponse }
     * 
     */
    public CreateSimpleInvoiceResponse createCreateSimpleInvoiceResponse() {
        return new CreateSimpleInvoiceResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceSalesCredit }
     * 
     */
    public CreateInterfaceSalesCredit createCreateInterfaceSalesCredit() {
        return new CreateInterfaceSalesCredit();
    }

    /**
     * Create an instance of {@link CreateInterfaceDistributionAsyncResponse }
     * 
     */
    public CreateInterfaceDistributionAsyncResponse createCreateInterfaceDistributionAsyncResponse() {
        return new CreateInterfaceDistributionAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceContingency }
     * 
     */
    public CreateInterfaceContingency createCreateInterfaceContingency() {
        return new CreateInterfaceContingency();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTimeAsync }
     * 
     */
    public GetServiceLastUpdateTimeAsync createGetServiceLastUpdateTimeAsync() {
        return new GetServiceLastUpdateTimeAsync();
    }

    /**
     * Create an instance of {@link CreateInterfaceContingencyResponse }
     * 
     */
    public CreateInterfaceContingencyResponse createCreateInterfaceContingencyResponse() {
        return new CreateInterfaceContingencyResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceDistributionAsync }
     * 
     */
    public ProcessInterfaceDistributionAsync createProcessInterfaceDistributionAsync() {
        return new ProcessInterfaceDistributionAsync();
    }

    /**
     * Create an instance of {@link ProcessInterfaceDistributionResponse }
     * 
     */
    public ProcessInterfaceDistributionResponse createProcessInterfaceDistributionResponse() {
        return new ProcessInterfaceDistributionResponse();
    }

    /**
     * Create an instance of {@link GetEntityListAsyncResponse }
     * 
     */
    public GetEntityListAsyncResponse createGetEntityListAsyncResponse() {
        return new GetEntityListAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceContingencyAsyncResponse }
     * 
     */
    public CreateInterfaceContingencyAsyncResponse createCreateInterfaceContingencyAsyncResponse() {
        return new CreateInterfaceContingencyAsyncResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceLineAsync }
     * 
     */
    public ProcessInterfaceLineAsync createProcessInterfaceLineAsync() {
        return new ProcessInterfaceLineAsync();
    }

    /**
     * Create an instance of {@link ProcessInterfaceSalesCreditAsyncResponse }
     * 
     */
    public ProcessInterfaceSalesCreditAsyncResponse createProcessInterfaceSalesCreditAsyncResponse() {
        return new ProcessInterfaceSalesCreditAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceSalesCreditAsync }
     * 
     */
    public CreateInterfaceSalesCreditAsync createCreateInterfaceSalesCreditAsync() {
        return new CreateInterfaceSalesCreditAsync();
    }

    /**
     * Create an instance of {@link CreateInterfaceDistributionAsync }
     * 
     */
    public CreateInterfaceDistributionAsync createCreateInterfaceDistributionAsync() {
        return new CreateInterfaceDistributionAsync();
    }

    /**
     * Create an instance of {@link ProcessInterfaceSalesCreditAsync }
     * 
     */
    public ProcessInterfaceSalesCreditAsync createProcessInterfaceSalesCreditAsync() {
        return new ProcessInterfaceSalesCreditAsync();
    }

    /**
     * Create an instance of {@link ProcessInterfaceLineResponse }
     * 
     */
    public ProcessInterfaceLineResponse createProcessInterfaceLineResponse() {
        return new ProcessInterfaceLineResponse();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHints }
     * 
     */
    public GetDfltObjAttrHints createGetDfltObjAttrHints() {
        return new GetDfltObjAttrHints();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHintsAsyncResponse }
     * 
     */
    public GetDfltObjAttrHintsAsyncResponse createGetDfltObjAttrHintsAsyncResponse() {
        return new GetDfltObjAttrHintsAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceLineAsync }
     * 
     */
    public CreateInterfaceLineAsync createCreateInterfaceLineAsync() {
        return new CreateInterfaceLineAsync();
    }

    /**
     * Create an instance of {@link CreateInterfaceSalesCreditResponse }
     * 
     */
    public CreateInterfaceSalesCreditResponse createCreateInterfaceSalesCreditResponse() {
        return new CreateInterfaceSalesCreditResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceDistribution }
     * 
     */
    public ProcessInterfaceDistribution createProcessInterfaceDistribution() {
        return new ProcessInterfaceDistribution();
    }

    /**
     * Create an instance of {@link GetEntityListAsync }
     * 
     */
    public GetEntityListAsync createGetEntityListAsync() {
        return new GetEntityListAsync();
    }

    /**
     * Create an instance of {@link CreateInterfaceContingencyAsync }
     * 
     */
    public CreateInterfaceContingencyAsync createCreateInterfaceContingencyAsync() {
        return new CreateInterfaceContingencyAsync();
    }

    /**
     * Create an instance of {@link CreateInterfaceLineAsyncResponse }
     * 
     */
    public CreateInterfaceLineAsyncResponse createCreateInterfaceLineAsyncResponse() {
        return new CreateInterfaceLineAsyncResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceLine }
     * 
     */
    public ProcessInterfaceLine createProcessInterfaceLine() {
        return new ProcessInterfaceLine();
    }

    /**
     * Create an instance of {@link CreateInterfaceLine }
     * 
     */
    public CreateInterfaceLine createCreateInterfaceLine() {
        return new CreateInterfaceLine();
    }

    /**
     * Create an instance of {@link CreateSimpleInvoiceAsyncResponse }
     * 
     */
    public CreateSimpleInvoiceAsyncResponse createCreateSimpleInvoiceAsyncResponse() {
        return new CreateSimpleInvoiceAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateSimpleInvoiceAsync }
     * 
     */
    public CreateSimpleInvoiceAsync createCreateSimpleInvoiceAsync() {
        return new CreateSimpleInvoiceAsync();
    }

    /**
     * Create an instance of {@link CreateInterfaceDistribution }
     * 
     */
    public CreateInterfaceDistribution createCreateInterfaceDistribution() {
        return new CreateInterfaceDistribution();
    }

    /**
     * Create an instance of {@link CreateInterfaceSalesCreditAsyncResponse }
     * 
     */
    public CreateInterfaceSalesCreditAsyncResponse createCreateInterfaceSalesCreditAsyncResponse() {
        return new CreateInterfaceSalesCreditAsyncResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceDistributionAsyncResponse }
     * 
     */
    public ProcessInterfaceDistributionAsyncResponse createProcessInterfaceDistributionAsyncResponse() {
        return new ProcessInterfaceDistributionAsyncResponse();
    }

    /**
     * Create an instance of {@link GetEntityList }
     * 
     */
    public GetEntityList createGetEntityList() {
        return new GetEntityList();
    }

    /**
     * Create an instance of {@link ProcessInterfaceSalesCreditResponse }
     * 
     */
    public ProcessInterfaceSalesCreditResponse createProcessInterfaceSalesCreditResponse() {
        return new ProcessInterfaceSalesCreditResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceContingencyResponse }
     * 
     */
    public ProcessInterfaceContingencyResponse createProcessInterfaceContingencyResponse() {
        return new ProcessInterfaceContingencyResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceLineAsyncResponse }
     * 
     */
    public ProcessInterfaceLineAsyncResponse createProcessInterfaceLineAsyncResponse() {
        return new ProcessInterfaceLineAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceDistributionResponse }
     * 
     */
    public CreateInterfaceDistributionResponse createCreateInterfaceDistributionResponse() {
        return new CreateInterfaceDistributionResponse();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHintsAsync }
     * 
     */
    public GetDfltObjAttrHintsAsync createGetDfltObjAttrHintsAsync() {
        return new GetDfltObjAttrHintsAsync();
    }

    /**
     * Create an instance of {@link ProcessInterfaceContingencyAsync }
     * 
     */
    public ProcessInterfaceContingencyAsync createProcessInterfaceContingencyAsync() {
        return new ProcessInterfaceContingencyAsync();
    }

    /**
     * Create an instance of {@link CreateSimpleInvoice }
     * 
     */
    public CreateSimpleInvoice createCreateSimpleInvoice() {
        return new CreateSimpleInvoice();
    }

    /**
     * Create an instance of {@link GetEntityListResponse }
     * 
     */
    public GetEntityListResponse createGetEntityListResponse() {
        return new GetEntityListResponse();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTimeAsyncResponse }
     * 
     */
    public GetServiceLastUpdateTimeAsyncResponse createGetServiceLastUpdateTimeAsyncResponse() {
        return new GetServiceLastUpdateTimeAsyncResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceContingency }
     * 
     */
    public ProcessInterfaceContingency createProcessInterfaceContingency() {
        return new ProcessInterfaceContingency();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHintsResponse }
     * 
     */
    public GetDfltObjAttrHintsResponse createGetDfltObjAttrHintsResponse() {
        return new GetDfltObjAttrHintsResponse();
    }

    /**
     * Create an instance of {@link CreateInterfaceLineResponse }
     * 
     */
    public CreateInterfaceLineResponse createCreateInterfaceLineResponse() {
        return new CreateInterfaceLineResponse();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTimeResponse }
     * 
     */
    public GetServiceLastUpdateTimeResponse createGetServiceLastUpdateTimeResponse() {
        return new GetServiceLastUpdateTimeResponse();
    }

    /**
     * Create an instance of {@link ProcessInterfaceContingencyAsyncResponse }
     * 
     */
    public ProcessInterfaceContingencyAsyncResponse createProcessInterfaceContingencyAsyncResponse() {
        return new ProcessInterfaceContingencyAsyncResponse();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTime }
     * 
     */
    public GetServiceLastUpdateTime createGetServiceLastUpdateTime() {
        return new GetServiceLastUpdateTime();
    }

}
