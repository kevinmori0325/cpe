
package com.oracle.xmlns.apps.financials.receivables.transactions.autoinvoices.model.flex.transactioninterfacegdf;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for TransactionInterfaceGdfJE_5FES_5FMODELO415_5F347PR complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionInterfaceGdfJE_5FES_5FMODELO415_5F347PR">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/}TransactionInterfaceGdf">
 *       &lt;sequence>
 *         &lt;element name="_Property__Location" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="_Property__Location_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_Transmission__of__Property" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_Transmission__of__Property_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_Transaction__Date" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="_Year__of__Amount__Received__in__Cas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionInterfaceGdfJE_5FES_5FMODELO415_5F347PR", propOrder = {
    "propertyLocation",
    "propertyLocationDisplay",
    "transmissionOfProperty",
    "transmissionOfPropertyDisplay",
    "transactionDate",
    "yearOfAmountReceivedInCas"
})
public class TransactionInterfaceGdfJE5FES5FMODELO4155F347PR
    extends TransactionInterfaceGdf
{

    @XmlElementRef(name = "_Property__Location", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", type = JAXBElement.class)
    protected JAXBElement<BigDecimal> propertyLocation;
    @XmlElementRef(name = "_Property__Location_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", type = JAXBElement.class)
    protected JAXBElement<String> propertyLocationDisplay;
    @XmlElementRef(name = "_Transmission__of__Property", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", type = JAXBElement.class)
    protected JAXBElement<String> transmissionOfProperty;
    @XmlElementRef(name = "_Transmission__of__Property_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", type = JAXBElement.class)
    protected JAXBElement<String> transmissionOfPropertyDisplay;
    @XmlElementRef(name = "_Transaction__Date", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> transactionDate;
    @XmlElementRef(name = "_Year__of__Amount__Received__in__Cas", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/autoInvoices/model/flex/TransactionInterfaceGdf/", type = JAXBElement.class)
    protected JAXBElement<String> yearOfAmountReceivedInCas;

    /**
     * Gets the value of the propertyLocation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getPropertyLocation() {
        return propertyLocation;
    }

    /**
     * Sets the value of the propertyLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setPropertyLocation(JAXBElement<BigDecimal> value) {
        this.propertyLocation = ((JAXBElement<BigDecimal> ) value);
    }

    /**
     * Gets the value of the propertyLocationDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPropertyLocationDisplay() {
        return propertyLocationDisplay;
    }

    /**
     * Sets the value of the propertyLocationDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPropertyLocationDisplay(JAXBElement<String> value) {
        this.propertyLocationDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the transmissionOfProperty property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransmissionOfProperty() {
        return transmissionOfProperty;
    }

    /**
     * Sets the value of the transmissionOfProperty property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransmissionOfProperty(JAXBElement<String> value) {
        this.transmissionOfProperty = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the transmissionOfPropertyDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransmissionOfPropertyDisplay() {
        return transmissionOfPropertyDisplay;
    }

    /**
     * Sets the value of the transmissionOfPropertyDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransmissionOfPropertyDisplay(JAXBElement<String> value) {
        this.transmissionOfPropertyDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTransactionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.transactionDate = ((JAXBElement<XMLGregorianCalendar> ) value);
    }

    /**
     * Gets the value of the yearOfAmountReceivedInCas property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getYearOfAmountReceivedInCas() {
        return yearOfAmountReceivedInCas;
    }

    /**
     * Sets the value of the yearOfAmountReceivedInCas property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setYearOfAmountReceivedInCas(JAXBElement<String> value) {
        this.yearOfAmountReceivedInCas = ((JAXBElement<String> ) value);
    }

}
