
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheadergdf;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Jaxtwtransactions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Jaxtwtransactions">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/}TransactionHeaderGdf">
 *       &lt;sequence>
 *         &lt;element name="wineAndCigarette" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="wineAndCigarette_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deductibleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deductibleType_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportCertificateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportName_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportMethod_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportType_Display" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exportDate" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="originalTransactionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="legacyUniformInvoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Jaxtwtransactions", propOrder = {
    "wineAndCigarette",
    "wineAndCigaretteDisplay",
    "deductibleType",
    "deductibleTypeDisplay",
    "exportCertificateNumber",
    "exportName",
    "exportNameDisplay",
    "exportMethod",
    "exportMethodDisplay",
    "exportType",
    "exportTypeDisplay",
    "exportDate",
    "originalTransactionNumber",
    "legacyUniformInvoice"
})
public class Jaxtwtransactions
    extends TransactionHeaderGdf
{

    @XmlElementRef(name = "wineAndCigarette", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> wineAndCigarette;
    @XmlElementRef(name = "wineAndCigarette_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> wineAndCigaretteDisplay;
    @XmlElementRef(name = "deductibleType", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> deductibleType;
    @XmlElementRef(name = "deductibleType_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> deductibleTypeDisplay;
    @XmlElementRef(name = "exportCertificateNumber", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportCertificateNumber;
    @XmlElementRef(name = "exportName", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportName;
    @XmlElementRef(name = "exportName_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportNameDisplay;
    @XmlElementRef(name = "exportMethod", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportMethod;
    @XmlElementRef(name = "exportMethod_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportMethodDisplay;
    @XmlElementRef(name = "exportType", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportType;
    @XmlElementRef(name = "exportType_Display", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> exportTypeDisplay;
    @XmlElementRef(name = "exportDate", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<XMLGregorianCalendar> exportDate;
    @XmlElementRef(name = "originalTransactionNumber", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> originalTransactionNumber;
    @XmlElementRef(name = "legacyUniformInvoice", namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderGdf/", type = JAXBElement.class)
    protected JAXBElement<String> legacyUniformInvoice;

    /**
     * Gets the value of the wineAndCigarette property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWineAndCigarette() {
        return wineAndCigarette;
    }

    /**
     * Sets the value of the wineAndCigarette property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWineAndCigarette(JAXBElement<String> value) {
        this.wineAndCigarette = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the wineAndCigaretteDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWineAndCigaretteDisplay() {
        return wineAndCigaretteDisplay;
    }

    /**
     * Sets the value of the wineAndCigaretteDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWineAndCigaretteDisplay(JAXBElement<String> value) {
        this.wineAndCigaretteDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the deductibleType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeductibleType() {
        return deductibleType;
    }

    /**
     * Sets the value of the deductibleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeductibleType(JAXBElement<String> value) {
        this.deductibleType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the deductibleTypeDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeductibleTypeDisplay() {
        return deductibleTypeDisplay;
    }

    /**
     * Sets the value of the deductibleTypeDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeductibleTypeDisplay(JAXBElement<String> value) {
        this.deductibleTypeDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportCertificateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportCertificateNumber() {
        return exportCertificateNumber;
    }

    /**
     * Sets the value of the exportCertificateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportCertificateNumber(JAXBElement<String> value) {
        this.exportCertificateNumber = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportName() {
        return exportName;
    }

    /**
     * Sets the value of the exportName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportName(JAXBElement<String> value) {
        this.exportName = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportNameDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportNameDisplay() {
        return exportNameDisplay;
    }

    /**
     * Sets the value of the exportNameDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportNameDisplay(JAXBElement<String> value) {
        this.exportNameDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportMethod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportMethod() {
        return exportMethod;
    }

    /**
     * Sets the value of the exportMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportMethod(JAXBElement<String> value) {
        this.exportMethod = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportMethodDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportMethodDisplay() {
        return exportMethodDisplay;
    }

    /**
     * Sets the value of the exportMethodDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportMethodDisplay(JAXBElement<String> value) {
        this.exportMethodDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportType() {
        return exportType;
    }

    /**
     * Sets the value of the exportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportType(JAXBElement<String> value) {
        this.exportType = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportTypeDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getExportTypeDisplay() {
        return exportTypeDisplay;
    }

    /**
     * Sets the value of the exportTypeDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setExportTypeDisplay(JAXBElement<String> value) {
        this.exportTypeDisplay = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the exportDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getExportDate() {
        return exportDate;
    }

    /**
     * Sets the value of the exportDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setExportDate(JAXBElement<XMLGregorianCalendar> value) {
        this.exportDate = ((JAXBElement<XMLGregorianCalendar> ) value);
    }

    /**
     * Gets the value of the originalTransactionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOriginalTransactionNumber() {
        return originalTransactionNumber;
    }

    /**
     * Sets the value of the originalTransactionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOriginalTransactionNumber(JAXBElement<String> value) {
        this.originalTransactionNumber = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the legacyUniformInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLegacyUniformInvoice() {
        return legacyUniformInvoice;
    }

    /**
     * Sets the value of the legacyUniformInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLegacyUniformInvoice(JAXBElement<String> value) {
        this.legacyUniformInvoice = ((JAXBElement<String> ) value);
    }

}
