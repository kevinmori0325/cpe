
package com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransactionHeaderFLEX_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "transactionHeaderFLEX");
    private final static QName _Peru_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "peru");
    private final static QName _Chile_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "chile");
    private final static QName _ChileFolio_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "folio");
    private final static QName _ChileDetalleError_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "detalleError");
    private final static QName _ChileDetalleError22_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "detalleError22");
    private final static QName _ChileDetalleError3_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "detalleError3");
    private final static QName _ChileUrlDocumento_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "urlDocumento");
    private final static QName _ChileEstadoDocumento_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "estadoDocumento");
    private final static QName _PeruLocPeArUrlFe_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArUrlFe");
    private final static QName _PeruLocPeArDocumentoRelacionado_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArDocumentoRelacionado");
    private final static QName _PeruLocPeArSerieCompRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArSerieCompRef");
    private final static QName _PeruFechaEmisionDocReferenciaDdMmY_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "fechaEmisionDocReferenciaDdMmY");
    private final static QName _PeruLocPeArTipoDocSunatRef_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArTipoDocSunatRef");
    private final static QName _PeruLocPeArEstadoFe_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "locPeArEstadoFe");
    private final static QName _TransactionHeaderFLEXFLEXContext_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "__FLEX_Context");
    private final static QName _TransactionHeaderFLEXFLEXContextDisplayValue_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "__FLEX_Context_DisplayValue");
    private final static QName _TransactionHeaderFLEXFLEXNumOfSegments_QNAME = new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", "_FLEX_NumOfSegments");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.financials.receivables.transactions.shared.model.flex.transactionheaderdff
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Chile }
     * 
     */
    public Chile createChile() {
        return new Chile();
    }

    /**
     * Create an instance of {@link Peru }
     * 
     */
    public Peru createPeru() {
        return new Peru();
    }

    /**
     * Create an instance of {@link TransactionHeaderFLEX }
     * 
     */
    public TransactionHeaderFLEX createTransactionHeaderFLEX() {
        return new TransactionHeaderFLEX();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionHeaderFLEX }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "transactionHeaderFLEX")
    public JAXBElement<TransactionHeaderFLEX> createTransactionHeaderFLEX(TransactionHeaderFLEX value) {
        return new JAXBElement<TransactionHeaderFLEX>(_TransactionHeaderFLEX_QNAME, TransactionHeaderFLEX.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Peru }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "peru")
    public JAXBElement<Peru> createPeru(Peru value) {
        return new JAXBElement<Peru>(_Peru_QNAME, Peru.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Chile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "chile")
    public JAXBElement<Chile> createChile(Chile value) {
        return new JAXBElement<Chile>(_Chile_QNAME, Chile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "folio", scope = Chile.class)
    public JAXBElement<String> createChileFolio(String value) {
        return new JAXBElement<String>(_ChileFolio_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "detalleError", scope = Chile.class)
    public JAXBElement<String> createChileDetalleError(String value) {
        return new JAXBElement<String>(_ChileDetalleError_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "detalleError22", scope = Chile.class)
    public JAXBElement<String> createChileDetalleError22(String value) {
        return new JAXBElement<String>(_ChileDetalleError22_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "detalleError3", scope = Chile.class)
    public JAXBElement<String> createChileDetalleError3(String value) {
        return new JAXBElement<String>(_ChileDetalleError3_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "urlDocumento", scope = Chile.class)
    public JAXBElement<String> createChileUrlDocumento(String value) {
        return new JAXBElement<String>(_ChileUrlDocumento_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "estadoDocumento", scope = Chile.class)
    public JAXBElement<String> createChileEstadoDocumento(String value) {
        return new JAXBElement<String>(_ChileEstadoDocumento_QNAME, String.class, Chile.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArUrlFe", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArUrlFe(String value) {
        return new JAXBElement<String>(_PeruLocPeArUrlFe_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArDocumentoRelacionado", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArDocumentoRelacionado(String value) {
        return new JAXBElement<String>(_PeruLocPeArDocumentoRelacionado_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArSerieCompRef", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArSerieCompRef(String value) {
        return new JAXBElement<String>(_PeruLocPeArSerieCompRef_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "fechaEmisionDocReferenciaDdMmY", scope = Peru.class)
    public JAXBElement<String> createPeruFechaEmisionDocReferenciaDdMmY(String value) {
        return new JAXBElement<String>(_PeruFechaEmisionDocReferenciaDdMmY_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArTipoDocSunatRef", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArTipoDocSunatRef(String value) {
        return new JAXBElement<String>(_PeruLocPeArTipoDocSunatRef_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "locPeArEstadoFe", scope = Peru.class)
    public JAXBElement<String> createPeruLocPeArEstadoFe(String value) {
        return new JAXBElement<String>(_PeruLocPeArEstadoFe_QNAME, String.class, Peru.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "__FLEX_Context", scope = TransactionHeaderFLEX.class)
    public JAXBElement<String> createTransactionHeaderFLEXFLEXContext(String value) {
        return new JAXBElement<String>(_TransactionHeaderFLEXFLEXContext_QNAME, String.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "__FLEX_Context_DisplayValue", scope = TransactionHeaderFLEX.class)
    public JAXBElement<String> createTransactionHeaderFLEXFLEXContextDisplayValue(String value) {
        return new JAXBElement<String>(_TransactionHeaderFLEXFLEXContextDisplayValue_QNAME, String.class, TransactionHeaderFLEX.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/financials/receivables/transactions/shared/model/flex/TransactionHeaderDff/", name = "_FLEX_NumOfSegments", scope = TransactionHeaderFLEX.class)
    public JAXBElement<Integer> createTransactionHeaderFLEXFLEXNumOfSegments(Integer value) {
        return new JAXBElement<Integer>(_TransactionHeaderFLEXFLEXNumOfSegments_QNAME, Integer.class, TransactionHeaderFLEX.class, value);
    }

}
