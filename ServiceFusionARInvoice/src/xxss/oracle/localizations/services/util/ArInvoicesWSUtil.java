package xxss.oracle.localizations.services.util;

import com.oracle.xmlns.apps.financials.receivables.transactions.invoices.invoiceservice.InvoiceService;
import com.oracle.xmlns.apps.financials.receivables.transactions.invoices.invoiceservice.InvoiceService_Service;

import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;

import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

public class ArInvoicesWSUtil {
    public ArInvoicesWSUtil() {
        super();
    }

    public static InvoiceService createInvoiceWSSServiceJwt(String jwt, String instanceName,
                                                            String dataCenter) throws MalformedURLException {
        InvoiceService_Service service_Service;
        InvoiceService service = null;

        if (jwt == null || "".equals(jwt)) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures = new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service =
                new InvoiceService_Service(new URL("https://" + instanceName + ".fin." + dataCenter + ".oraclecloud.com/finArTrxnsInvoices/InvoiceService?wsdl"),
                                           new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/",
                                                     "InvoiceService"));

        service = service_Service.getInvoiceServiceSoapHttpPort(secFeatures);

        if (service == null) {
            System.out.println("InvoiceService is null");
        } else {
            System.out.println("InvoiceService created.");
        }

        System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

        // add JWT auth map to HTTP header
        BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap = new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, authMap);

        return service;
    }

    public static InvoiceService createInvoiceServiceUserToken(String instanceName, String dataCenter, String userName,
                                                               String userPass) throws MalformedURLException {
        InvoiceService_Service service_Service;
        InvoiceService service = null;

        if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
        { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

        service_Service =
                new InvoiceService_Service(new URL("https://" + instanceName + ".fin." + dataCenter + ".oraclecloud.com/finArTrxnsInvoices/InvoiceService?wsdl"),
                                           new QName("http://xmlns.oracle.com/apps/financials/receivables/transactions/invoices/invoiceService/",
                                                     "InvoiceService"));

        service = service_Service.getInvoiceServiceSoapHttpPort(secFeatures);

        if (service == null) {
            System.out.println("InvoiceService is null");
        } else {
            System.out.println("InvoiceService created.");
        }

        // add JWT auth map to HTTP header
        WSBindingProvider wsbp = (WSBindingProvider)service;
        wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
        wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);

        return service;
    }
}
