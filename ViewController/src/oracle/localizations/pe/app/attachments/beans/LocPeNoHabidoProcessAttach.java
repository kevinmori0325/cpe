package oracle.localizations.pe.app.attachments.beans;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.net.MalformedURLException;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Arrays;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jbo.domain.Number;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import oracle.jdbc.OracleConnection;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

import xxss.oracle.localizations.pe.app.attachments.interfaces.IProcessAttachment;
import xxss.oracle.localizations.pe.app.attachments.services.BIScheduleWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.AccessDeniedException;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.InvalidParametersException;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.OperationFailedException;
import xxss.oracle.localizations.services.fusion.externalreportservice.proxy.ExternalReportWSSService;
//import xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfParamNameValue;
//import xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString;
//import xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue;
//import xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportRequest;
//import xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportResponse;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValues;
//import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest;

public class LocPeNoHabidoProcessAttach implements Job {
    private static Logger log = Logger.getLogger(LocPeNoHabidoProcessAttach.class);
    public LocPeNoHabidoProcessAttach() {
        super();
    }

    public void readReportOutput(InputStream reportIs, OracleConnection trans,
                                 Number idAttachment) throws SQLException,
                                                             Exception {
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_read_report_output(pt_report_blob => ?, pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setBlob(1, reportIs);
        acs.setNUMBER(2, idAttachment);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

        acs.executeUpdate();

        String procStatus = acs.getString(3);
        String procMessage = acs.getString(4);

        if (procStatus.equals(IProcessAttachment.ERROR)) {
            throw new Exception(procMessage);
        }

    }
    
    public void cleanPartyClassTable(OracleConnection trans,
                                 String attachmentType) throws SQLException,
                                                             Exception {
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_clear_party_class(pv_attachment_type => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, attachmentType);

        acs.executeUpdate();
    }
    
    public void processClobFile(OracleConnection trans,
                                 Number idAttachment, String attachmentType, String headerLineFlag, 
                                String fileSeparator, String fiscalTypeCode, String fiscalCode) throws SQLException,
                                                             Exception {
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_get_attach_blob(pn_attachment_id => ?, xb_clob_file => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setNUMBER(1, idAttachment);
        acs.registerOutParameter(2, Types.BLOB);

        acs.executeUpdate();
        
        InputStream fileBlob = acs.getBinaryStream(2);
        
        BufferedReader buffer = new BufferedReader(new InputStreamReader(fileBlob));
        String readLine = "";
        int lineCount = 0;
        
        st = "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_insert_clobline(?, ?, ?, ?, ?); end;";
        acs = (OracleCallableStatement)trans.prepareCall(st);
        
        while ((readLine = buffer.readLine()) != null) {
            lineCount++;
            
            if (!(lineCount == 1 && "Y".equals(headerLineFlag))) {
                acs.setString(1, readLine);
                acs.setString(2, attachmentType);
                acs.setString(3, fileSeparator);
                acs.setString(4, fiscalTypeCode);
                acs.setString(5, fiscalCode);
                
                acs.executeUpdate();
            }
        }
        
        buffer.close();
    }

    public void callProcessAttachDB(String pAttachId,
                                    OracleConnection trans) throws SQLException,
                                                                Exception {

        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_process_file_job(pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setNUMBER(1, new Number(pAttachId));
        acs.registerOutParameter(2, Types.VARCHAR, 0, 1);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 4000);

        acs.executeUpdate();

        String procStatus = acs.getString(2);
        String procMessage = acs.getString(3);

        if (procStatus.equals(IProcessAttachment.ERROR)) {
            throw new Exception(procMessage);
        }

        ;
    }
    
    private String executeFiscalClassRep(String concatClassCode, String jwtFusion, String instanceName, String dataCenter, String userEmail, String biUserName, String biUserPass, String report) throws MalformedURLException,
                                                xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.InvalidParametersException,
                                                xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.AccessDeniedException,
                                                xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.OperationFailedException, Exception {
        ScheduleService svcBI = null;
        svcBI = BIScheduleWSUtil.createScheduleService(jwtFusion, instanceName,
                                                       dataCenter);

        if (svcBI != null) {
            System.out.println("ScheduleService created");
        } else {
            throw new Exception("ScheduleService not created");
        }


        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest scheduleRep = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest();

        xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption email = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption();
        email.setEmailAttachmentName("PadronesSunat");
        email.setEmailFrom("no.reply@oraclecloud.com");
        email.setEmailServerName("smtp.oraclecloud.com");
        email.setEmailSubject("Reporte para carga de padrones SUNAT");
        email.setEmailTo(userEmail);

        xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels delivChann = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels();
        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption delivery = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption();
        delivery.getItem().add(email);
        delivChann.setEmailOptions(delivery);
        
        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfParamNameValue arrayParam = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfString();
        param.setName("P_CONCAT_CLASS_CODE");
        arrayValues.getItem().add(concatClassCode);
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValues params = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValues();
        params.setListOfParamNameValues(arrayParam);

        xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest rep = new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest();
        rep.setParameterNameValues(params);
        rep.setAttributeCalendar("Gregorian");
        rep.setAttributeFormat("XLSX");
        rep.setAttributeLocale("English (United States)");
        rep.setAttributeTemplate("Reporte para carga de padrones SUNAT");
        rep.setAttributeTimezone("(UTC-05:00) Lima - Peru Time (PET)");
     // rep.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Reporte para carga de padrones SUNAT.xdo");
        rep.setReportAbsolutePath(report);//KMORI
        rep.setSizeOfDataChunkDownload(-1);

        scheduleRep.setDeliveryChannels(delivChann);
        scheduleRep.setReportRequest(rep);

        String user = biUserName;
        String pass = biUserPass;
        String returnCode = svcBI.scheduleReport(scheduleRep, user, pass);
        
        return(returnCode);
    }
    
    public void updateStatusAttach(String attachId, String status, String message,
                                        OracleConnection trans) {
        try {
            Number nAttachId = new Number(attachId);
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_update_attach_status(?, ?, ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, nAttachId);
            acs.setString(2, status);
            acs.setString(3, message);        

            acs.executeUpdate();
        } catch (SQLException sqlex) {
            log.error("Error en updateStatusAttach(): " + sqlex.getMessage(), sqlex);
            log.error(Arrays.toString(sqlex.getStackTrace()));
        }
    }
    
    private byte[] runTaxReport(String jwt, String instanceName, String dataCenter, String report2) throws MalformedURLException,
                                                xxss.oracle.localizations.services.fusion.externalreportservice.proxy.InvalidParametersException,
                                                xxss.oracle.localizations.services.fusion.externalreportservice.proxy.AccessDeniedException,
                                                xxss.oracle.localizations.services.fusion.externalreportservice.proxy.OperationFailedException, Exception {
        ExternalReportWSSService svc = null;        
        svc = ExternalReportWSUtil.createExternalReportWSSService(jwt, instanceName, dataCenter);
        
        if (svc != null) {
            System.out.println("ExternalReportWS created");
        } else {
            throw new Exception("ExternalReportWS is not created.");
        }
        
        xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportRequest req = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfParamNameValue arrayParam = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);


        req.setParameterNameValues(arrayParam);
   //   req.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Party Tax Classifications.xdo");
        req.setReportAbsolutePath(report2);//KMORI
        req.setSizeOfDataChunkDownload(-1);

        xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportResponse resp = svc.runReport(req, "");
        
        return(resp.getReportBytes());
        
    }

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("entro job");
        
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        JobKey key = jobExecutionContext.getJobDetail().getKey();

        JobDataMap dataMap =
            jobExecutionContext.getJobDetail().getJobDataMap();

        String idAttachment = dataMap.getString("idAttachment");
        /*DBTransaction trans =
            (DBTransaction)dataMap.getWrappedMap().get("trans");*/
        String jwt = dataMap.getString("jwt");
        String biUserName = dataMap.getString("biUserName");
        String biUserPass = dataMap.getString("biUserPass");
        String userEmail = dataMap.getString("userEmail");
        String instanceName = dataMap.getString("instanceName");
        String dataCenter = dataMap.getString("dataCenter");
        String classCode = dataMap.getString("classCode");
        String attachmentType = dataMap.getString("attachmentType");
        String headerLineFlag = dataMap.getString("headerLineFlag");
        String fileSeparator = dataMap.getString("fileSeparator");
        String fiscalClassTypeCode = dataMap.getString("fiscalClassTypeCode");
      String report = dataMap.getString("report");
        String report2 = dataMap.getString("report2");

        // Llamada a WS para ejecutar reporte
        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
            byte[] reportOutputBytes = this.runTaxReport(jwt, instanceName, dataCenter, report2);

            if (reportOutputBytes == null || reportOutputBytes.length == 0) {
                throw new Exception("Party Tax Classification report cannot be executed.");
            }

            InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);
            
            log.info("executing: cleanPartyClassTable()");
            this.cleanPartyClassTable(conn, attachmentType);            
            log.info("executing: readReportOutput()");
            this.readReportOutput(reportIs, conn, new Number(idAttachment));
            log.info("executing: processClobFile()");
            this.processClobFile(conn, new Number(idAttachment), attachmentType, headerLineFlag, fileSeparator, fiscalClassTypeCode, classCode);
            log.info("executing: callProcessAttachDB()");
            this.callProcessAttachDB(idAttachment, conn);
            
            conn.commit();

            // llamando a ws para ejecucion y envio de reporte
            String returnCode = this.executeFiscalClassRep(classCode, jwt, instanceName, dataCenter, userEmail, biUserName, biUserPass, report);

            if(returnCode == null || "".equals(returnCode)) {
                throw new Exception("Error al ejecutar reporte BI.");
            }

        } catch (Exception ex) {
            log.error("Error en execute(): " + ex.getMessage(), ex);
            log.error(Arrays.toString(ex.getStackTrace()));
            System.out.println("Error en execute() - " + ex.getMessage());
            this.updateStatusAttach(idAttachment, "ERROR", ex.getMessage(), conn);
            JobExecutionException e2 = new JobExecutionException(ex);
            // Quartz will automatically unschedule
            // all triggers associated with this job
            // so that it does not run again
            e2.setUnscheduleAllTriggers(true);
            throw e2;
            //ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on execute(): " + sqlex.getMessage(), sqlex);
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
                
            }
        }
    }
}
