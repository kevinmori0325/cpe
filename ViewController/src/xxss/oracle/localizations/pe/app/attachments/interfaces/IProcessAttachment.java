package xxss.oracle.localizations.pe.app.attachments.interfaces;

import oracle.jbo.server.DBTransaction;

public interface IProcessAttachment {
    static final String SUCCESS = "S";
    static final String ERROR = "E";

    void processFile(String idAttachment, String jwt, String biUserName, String biUserPass, String userEmail, String instanceName, String dataCenter, String report, String report2,String classCode);

    String getStatus();

    String getMessage();
}
