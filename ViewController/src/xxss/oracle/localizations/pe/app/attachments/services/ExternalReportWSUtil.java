package xxss.oracle.localizations.pe.app.attachments.services;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.externalreportservice.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice.proxy.ExternalReportWSSService_Service;

public class ExternalReportWSUtil {
    public ExternalReportWSUtil() {
        super();
    }
    
    public static ExternalReportWSSService createExternalReportWSSService(String jwt, String instanceName, String dataCenter) throws MalformedURLException {
        ExternalReportWSSService_Service service_Service;
                ExternalReportWSSService service = null;

                //String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

                if (jwt == null || "".equals(jwt)) {
                    return service;
                }

                // JWT requires no client policy (with this unpatched version of JDev)
                // TODO:  research patch that adds JWT client policy to OWSM on client side

                SecurityPolicyFeature[] secFeatures =
                    new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
                //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

                service_Service = new ExternalReportWSSService_Service(new URL("https://" + instanceName + ".bi." + dataCenter + ".oraclecloud.com/xmlpserver/services/ExternalReportWSSService"), 
                                                                       new QName("http://xmlns.oracle.com/oxp/service/PublicReportService", "ExternalReportWSSService"));
                service =
                        service_Service.getExternalReportWSSService(secFeatures);
                //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
                if (service == null) {
                    System.out.println("ExternalReportWSSService is null");
                } else {
                    System.out.println("ExternalReportWSSService created.");
                }

                System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

                // add JWT auth map to HTTP header
                BindingProvider bp = (BindingProvider)service;
                Map<String, List<String>> authMap =
                    new HashMap<String, List<String>>();
                List<String> authZlist = new ArrayList<String>();
                authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
                authMap.put("Authorization", authZlist);

                bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS,
                                           authMap);

                return service;
    }
}
