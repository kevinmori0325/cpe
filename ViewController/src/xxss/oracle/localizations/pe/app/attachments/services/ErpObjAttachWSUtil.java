package xxss.oracle.localizations.pe.app.attachments.services;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.erpobjattachmentservice.proxy.ErpObjectAttachmentService;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice.proxy.ErpObjectAttachmentService_Service;

public class ErpObjAttachWSUtil {
    public ErpObjAttachWSUtil() {
        super();
    }

    public static ErpObjectAttachmentService createErpObjectAttachmentService(String jwt, String instanceName, String dataCenter) throws MalformedURLException {
        ErpObjectAttachmentService_Service service_Service;
        ErpObjectAttachmentService service = null;

        //String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

        if (jwt == null || "".equals(jwt)) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
            new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service = new ErpObjectAttachmentService_Service(new URL("https://" + instanceName + ".fin." + dataCenter + ".oraclecloud.com/publicFinancialCommonErpIntegration/ErpObjectAttachmentService"), 
        new QName("http://xmlns.oracle.com/apps/financials/commonModules/shared/model/erpIntegrationService/", "ErpObjectAttachmentService"));
        service =
                service_Service.getErpObjectAttachmentServiceSoapHttpPort(secFeatures);
        //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
        if (service == null) {
            System.out.println("ErpObjectAttachmentService is null");
        } else {
            System.out.println("ErpObjectAttachmentService created.");
        }

        System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

        // add JWT auth map to HTTP header
        BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap =
            new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS,
                                   authMap);

        return service;
    }
}
