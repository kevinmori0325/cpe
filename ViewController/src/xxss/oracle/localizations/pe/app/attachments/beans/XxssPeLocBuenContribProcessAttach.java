package xxss.oracle.localizations.pe.app.attachments.beans;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.io.OutputStream;

import java.sql.SQLException;

import java.sql.Types;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.Number;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import oracle.jdbc.OracleConnection;

import org.apache.commons.io.IOUtils;

import org.apache.log4j.Logger;

import xxss.oracle.localizations.pe.app.attachments.interfaces.IProcessAttachment;
import xxss.oracle.localizations.pe.app.attachments.services.BIScheduleWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest;
import xxss.oracle.localizations.services.fusion.externalreportservice.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfParamNameValue;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ArrayOfString;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValue;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValues;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportRequest;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ReportResponse;

public class XxssPeLocBuenContribProcessAttach implements IProcessAttachment {
    private static Logger log = Logger.getLogger(XxssPeLocBuenContribProcessAttach.class);
    private String status;
    private String message;

    public XxssPeLocBuenContribProcessAttach() {
        super();
    }

    private BlobDomain createBlobDomain(InputStream file) {
        // init the internal variables
        InputStream in = null;
        BlobDomain blobDomain = null;
        OutputStream out = null;

        try {
            // Get the input stream representing the data from the client
            in = file; //file.getInputStream();
            // create the BlobDomain datatype to store the data in the db
            blobDomain = new BlobDomain();
            // get the outputStream for hte BlobDomain
            out = blobDomain.getBinaryOutputStream();
            // copy the input stream into the output stream
            /*
                 * IOUtils is a class from the Apache Commons IO Package (http://www.apache.org/)
                 * Here version 2.0.1 is used
                 * please download it directly from http://projects.apache.org/projects/commons_io.html
                 */
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.fillInStackTrace();
        }

        // return the filled BlobDomain
        return blobDomain;
    }

    public void readReportOutput(InputStream reportIs, OracleConnection trans,
                                 Number idAttachment) {
        try {
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_read_report_output(pt_report_blob => ?, pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setBlob(1, reportIs);
            acs.setNUMBER(2, idAttachment);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
            acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            String procStatus = acs.getString(3);
            String procMessage = acs.getString(4);

            if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }

        } catch (SQLException exsql) {
            status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();
            log.error("Error SQL en readReportOutput(): " + exsql.getMessage(), exsql);
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage());
        } catch (Exception e) {
            status = ERROR;
            message =
                    "Error en XxssPeLocBuenContribProcessAttach: " + e.getMessage();
            log.error("Error en readReportOutput(): " + e.getMessage(), e);
            System.out.println("Error en readReportOutput() - " + e.getMessage());
        }
        ;
    }

    public void callProcessAttachDB(String pAttachId, OracleConnection trans) {
        try {
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_process_file(pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, new Number(pAttachId));
            acs.registerOutParameter(2, Types.VARCHAR, 0, 1);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            String procStatus = acs.getString(2);
            String procMessage = acs.getString(3);

            if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }
        } catch (SQLException exsql) {
            status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();
            log.error("Error SQL en callProcessAttachDB(): " + exsql.getMessage(), exsql);
            System.out.println("Error SQL en callProcessAttachDB() - " + exsql.getMessage());
        } catch (Exception e) {
            status = ERROR;
            message =
                    "Error en XxssPeLocBuenContribProcessAttach: " + e.getMessage();
            log.error("Error en callProcessAttachDB(): " + e.getMessage(), e);
            System.out.println("Error en callProcessAttachDB() - " + e.getMessage());
        }
        ;
    }

    public void processFile(String idAttachment, String jwt, String biUserName, String biUserPass, String userEmail, 
                            String instanceName, String dataCenter, String report, String report2,String classCode) {
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        // Llamada a WS para ejecutar reporte
        ExternalReportWSSService svc = null;
        //ScheduleService svcBI = null;
        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
            svc = ExternalReportWSUtil.createExternalReportWSSService(jwt, instanceName, dataCenter);
            //svcBI = BIScheduleWSUtil.createScheduleService(jwt, instanceName, dataCenter);

            if (svc != null) {
                System.out.println("ExternalReportWS created");
            } else {
                this.status = ERROR;
                this.message = "ExternalReportWS is not created";
                log.error(message);
                System.out.println(message);
                return;
            }

            /*if (svcBI != null) {
                System.out.println("ScheduleService created");
            } else {
                this.status = ERROR;
                this.message = "ScheduleService is not created";
                System.out.println(message);
                return;
            }*/

            ReportRequest req = new ReportRequest();
            ArrayOfParamNameValue arrayParam = new ArrayOfParamNameValue();

            ParamNameValue param = null;
            ArrayOfString arrayValues = null;

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_DM_DEBUG_FLAG");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);


            req.setParameterNameValues(arrayParam);
        //    req.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Party Tax Classifications.xdo");
            req.setReportAbsolutePath(report2);//KMORI
            req.setSizeOfDataChunkDownload(-1);

            ReportResponse resp = svc.runReport(req, "");

            byte[] reportOutputBytes = resp.getReportBytes();

            if (reportOutputBytes == null || reportOutputBytes.length == 0) {
                this.status = ERROR;
                this.message =
                        "Party Tax Classification report cannot be executed.";
                log.error(message);
                System.out.println(message);
                return;
            }

            InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);
            this.readReportOutput(reportIs, conn, new Number(idAttachment));

            if (this.status != null && this.status.equals(ERROR)) {
                return;
            }

            this.callProcessAttachDB(idAttachment, conn);
            if (this.status != null && this.status.equals(ERROR)) {
                return;
            }

            // llamando a ws para ejecucion y envio de reporte
            /*String personEmail = userEmail;

            ScheduleRequest scheduleRep = new ScheduleRequest();

            EMailDeliveryOption email = new EMailDeliveryOption();
            email.setEmailAttachmentName("PadronesSunat");
            email.setEmailFrom("noreply@neora.com.pe");
            email.setEmailServerName("smtp.neora.com.pe");
            email.setEmailSubject("Reporte para carga de padrones SUNAT");
            email.setEmailTo(personEmail);

            DeliveryChannels delivChann = new DeliveryChannels();
            ArrayOfEMailDeliveryOption delivery =
                new ArrayOfEMailDeliveryOption();
            delivery.getItem().add(email);
            delivChann.setEmailOptions(delivery);

            xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest rep =
                new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest();
            rep.setAttributeCalendar("Gregorian");
            rep.setAttributeFormat("XLSX");
            rep.setAttributeLocale("English (United States)");
            rep.setAttributeTemplate("Reporte para carga de padrones SUNAT");
            rep.setAttributeTimezone("(UTC-05:00) Lima - Peru Time (PET)");
            rep.setReportAbsolutePath("/Custom/PE Localizaciones/Padrones SUNAT/Reporte para carga de padrones SUNAT.xdo");
            rep.setSizeOfDataChunkDownload(-1);

            scheduleRep.setDeliveryChannels(delivChann);
            scheduleRep.setReportRequest(rep);

            String user = biUserName;
            String pass = biUserPass;
            String returnCode = svcBI.scheduleReport(scheduleRep, user, pass);
            
            if(returnCode == null || "".equals(returnCode)) {
                this.status = ERROR;
                this.message = "Error al ejecutar reporte BI.";
                return;
            }*/

            this.status = SUCCESS;

        } catch (Exception ex) {
            this.status = ERROR;
            this.message = ex.getMessage();
            log.error("Error en processFile(): " + ex.getMessage(), ex);
            System.out.println("Error en processFile() - " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
