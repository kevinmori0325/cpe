package xxss.oracle.localizations.pe.app.attachments.view.listeners;

import oracle.adf.share.weblogic.listeners.ADFApplicationLifecycleListener;

public class LocalizationsADFApplicationLifecycleListener extends ADFApplicationLifecycleListener {
    public LocalizationsADFApplicationLifecycleListener() {
        super();
    }

    public void postStart(weblogic.application.ApplicationLifecycleEvent applicationLifecycleEvent) throws weblogic.application.ApplicationException {
        super.postStart(applicationLifecycleEvent);
        System.out.println("Cerrando aplicacion");
    }

    public void postStop(weblogic.application.ApplicationLifecycleEvent applicationLifecycleEvent) throws weblogic.application.ApplicationException {
        super.postStop(applicationLifecycleEvent);
        System.out.println("Cerrando aplicacion");
    }
    
    public void applicationDestroyed(weblogic.application.ApplicationLifecycleEvent applicationLifecycleEvent) {
        super.applicationDestroyed(applicationLifecycleEvent);
        System.out.println("Destruyendo aplicacion");
    }

}
