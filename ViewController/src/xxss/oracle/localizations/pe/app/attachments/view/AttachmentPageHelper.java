package xxss.oracle.localizations.pe.app.attachments.view;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

import java.net.MalformedURLException;
import java.net.URLConnection;

import java.sql.SQLException;

import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import java.util.zip.ZipOutputStream;

import javax.activation.MimetypesFileTypeMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.faces.event.ValueChangeEvent;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.RichQuery;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.BlobDomain;

import oracle.jbo.domain.ClobDomain;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ApplicationModuleImpl;

import oracle.jbo.server.DBTransaction;

import oracle.jbo.server.SequenceImpl;

import oracle.jdbc.OracleCallableStatement;

import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;

import oracle.jdbc.OracleResultSet;

import oracle.localizations.pe.app.attachments.beans.LocPeNoHabidoProcessAttach;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.event.PollEvent;
import org.apache.myfaces.trinidad.model.BoundedRangeModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import org.quartz.Job;

import xxss.oracle.localizations.pe.app.attachments.beans.AttachmentBean;
import xxss.oracle.localizations.pe.app.attachments.beans.AttachmentTypeInfoBean;
import xxss.oracle.localizations.pe.app.attachments.interfaces.IProcessAttachment;
import xxss.oracle.localizations.pe.app.attachments.services.BIScheduleWSUtil;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.AccessDeniedException;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.InvalidParametersException;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.OperationFailedException;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValues;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfParamNameValue;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfString;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ParamNameValue;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest;


import org.quartz.SimpleScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;

import xxss.oracle.localizations.pe.app.attachments.model.XxssPeLocAttachmentsAMImpl;

public class AttachmentPageHelper extends BoundedRangeModel implements Runnable {
    private RichPanelCollection panelCollection;
    private RichQuery attachQuery;
    private RichTable attachTable;
    private RichPopup createPopup;
    private RichInputFile attachInputFile;
    private int procVal;
    private int pollInterval;
    private RichPopup processPopup;
    private boolean showExitProgressPopup;
    private RichCommandLink downloadFileLink;
    private RichCommandButton hideDownloadFileButton;
    private DBTransaction transPage;
    private Number attachmentIdNum;
    private String attachmentType;
    private String jwtFusion;
    private String statusProcess;
    private String messageProcess;
    private RichCommandLink downloadOutputLink;
    private RichCommandLink downloadOutputLinkReporte;
    private String fileNameOut;
    private String userName;
    private String biUserPass;
    private String userEmail;
    private String instanceName;
    private String dataCenter;
    private List attachmentList;
    private String processFileName;
    
    String rut = "";
    
    private static Logger log = Logger.getLogger(AttachmentPageHelper.class);

    public AttachmentPageHelper() {
        this.procVal = 0;
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    private BlobDomain createBlobDomain(InputStream file) {
        // init the internal variables
        InputStream in = null;
        BlobDomain blobDomain = null;
        OutputStream out = null;

        try {
            // Get the input stream representing the data from the client
            in = file; //file.getInputStream();
            // create the BlobDomain datatype to store the data in the db
            blobDomain = new BlobDomain();
            // get the outputStream for hte BlobDomain
            out = blobDomain.getBinaryOutputStream();
            // copy the input stream into the output stream
            /*
                 * IOUtils is a class from the Apache Commons IO Package (http://www.apache.org/)
                 * Here version 2.0.1 is used
                 * please download it directly from http://projects.apache.org/projects/commons_io.html
                 */
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.fillInStackTrace();
        }

        // return the filled BlobDomain
        return blobDomain;
    }

    public void setPanelCollection(RichPanelCollection panelCollection) {
        this.panelCollection = panelCollection;
    }

    public RichPanelCollection getPanelCollection() {
        return panelCollection;
    }

    public void setAttachQuery(RichQuery attachQuery) {
        this.attachQuery = attachQuery;
    }

    public RichQuery getAttachQuery() {
        return attachQuery;
    }

    public void setAttachTable(RichTable attachTable) {
        this.attachTable = attachTable;
    }

    public RichTable getAttachTable() {
        return attachTable;
    }

    public void createAttachAction(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = this.getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        RichPopup popup = this.getCreatePopup();
        RichPopup.PopupHints hints = new RichPopup.PopupHints();
        popup.show(hints);
    }

    public void setCreatePopup(RichPopup createPopup) {
        this.createPopup = createPopup;
    }

    public RichPopup getCreatePopup() {
        return createPopup;
    }

    public void okCreatePopup(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();

        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        this.getAttachInputFile().resetValue();

        RichPopup popup = this.getCreatePopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAttachTable());
        //FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Archivo Guardado Correctamente.", "");
        //FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void cancelCreatePopup(ActionEvent actionEvent) {
        // Add event code here...
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return;
        }

        this.getAttachInputFile().resetValue();

        RichPopup popup = this.getCreatePopup();
        popup.hide();
        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAttachTable());
    }

    public void uploadAttachFileChanged(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        UploadedFile uploadFile = (UploadedFile)valueChangeEvent.getNewValue();

        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding lBinding =
            bindings.findIteratorBinding("XxssPeLocAttachmentsVO1Iterator");
        Row row = lBinding.getCurrentRow();

        try {
            String fileName = uploadFile.getFilename();
            String contentType = uploadFile.getContentType();
            InputStream inputFile = uploadFile.getInputStream();

            // Valida si archivo es un zip o no
            if (!fileName.substring(fileName.indexOf(".")).toUpperCase().equals(".ZIP")) {
                row.setAttribute("FileName", fileName);
                row.setAttribute("FileType", contentType);
                row.setAttribute("ClobFile", createBlobDomain(inputFile));
            } else {

                ZipInputStream ipzip = new ZipInputStream(inputFile);
                ZipEntry entryZip = ipzip.getNextEntry();

                byte[] entryBytes = new byte[2048];
                int bufferSize = 0;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                while ((bufferSize =
                        ipzip.read(entryBytes, 0, entryBytes.length)) != -1) {
                    bos.write(entryBytes, 0, bufferSize);
                }

                InputStream inputExtractFile =
                    new ByteArrayInputStream(bos.toByteArray());
                fileName = entryZip.getName();
                contentType = MimetypesFileTypeMap.getDefaultFileTypeMap().getContentType(fileName); //URLConnection.guessContentTypeFromName(fileName);

                row.setAttribute("FileName", fileName);
                row.setAttribute("FileType", contentType);
                row.setAttribute("ClobFile",
                                 createBlobDomain(inputExtractFile));
            }
        } catch (IOException ex) {
            log.error("Error en uploadAttachFileChanged: " + ex.getMessage(), ex);
            System.out.println("Error en uploadAttachFileChanged - " + ex.getMessage());
            //uploadAttachFileChanged
            ex.printStackTrace();
        }
    }

    public void setAttachInputFile(RichInputFile attachInputFile) {
        this.attachInputFile = attachInputFile;
    }

    public RichInputFile getAttachInputFile() {
        return attachInputFile;
    }

    public long getMaximum() {
        return 100;
    }

    public long getValue() {
        return this.procVal;
    }

    public AttachmentTypeInfoBean getAttachTypeInfo(String attachmentType,
                                        OracleConnection trans) throws SQLException {
        String st =
            "begin XXSS_PE_LOC_GLOBAL_PKG.pr_get_class_name(?, ?, ?, ?, ?, ?, ?, ?,?,?); end;";//KMORI
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setString(1, attachmentType);
        acs.registerOutParameter(2, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 5);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(5, Types.VARCHAR, 0, 5);
        acs.registerOutParameter(6, Types.VARCHAR, 0, 5);
        acs.registerOutParameter(7, Types.VARCHAR, 0, 5);
        acs.registerOutParameter(8, Types.VARCHAR, 0, 150);
        acs.registerOutParameter(9, Types.VARCHAR, 0, 150);//KMORI
        acs.registerOutParameter(10, Types.VARCHAR, 0, 150);//KMORI

        acs.executeUpdate();
        
        AttachmentTypeInfoBean attachTypeInfo = new AttachmentTypeInfoBean();
        
        attachTypeInfo.setClassName(acs.getString(2));
        attachTypeInfo.setFiscalClassFlag(acs.getString(3));
        attachTypeInfo.setFiscalClassCode(acs.getString(4));
        attachTypeInfo.setJobFlag(acs.getString(5));
        attachTypeInfo.setHeaderLineFlag(acs.getString(6));
        attachTypeInfo.setFileSeparator(acs.getString(7));
        attachTypeInfo.setFiscalClassTypeCode(acs.getString(8));
        attachTypeInfo.setRutReport(acs.getString(9));//KMORI
        attachTypeInfo.setRutReport2(acs.getString(10));//KMORI
        String report = acs.getString(9);//KMORI
        String report2 = acs.getString(10);//KMORI
      System.out.println("Ruta reporte : " +report);//KMORI
      System.out.println("Ruta reporte2 : " +report2);
        return (attachTypeInfo);
    }
    
    public void updateStatusAttach(Number attachId, String status, String message,
                                        OracleConnection trans) throws SQLException {
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_update_attach_status(?, ?, ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setNUMBER(1, attachId);
        acs.setString(2, status);
        acs.setString(3, message);        

        acs.executeUpdate();
    }
    
    public String getJobSeq(OracleConnection trans) throws SQLException {
        String pSequenceName = "loc_pe_job_id_s";
        int sequenceValue = 0;
        
        if (pSequenceName != null && !pSequenceName.equals("")) {
                
            String st = "select loc_pe_job_id_s.nextval from dual";            
            OraclePreparedStatement ps = (OraclePreparedStatement)trans.prepareStatement(st);            
            OracleResultSet rs = (OracleResultSet)ps.executeQuery();
            
            if (rs.next()) {
                sequenceValue = rs.getInt(1);
            }
        }
        
        return(String.valueOf(sequenceValue));
    }
    
    private void executeFiscalClassRep(String[] concatClassCode, String report) throws MalformedURLException,
                                                InvalidParametersException,
                                                AccessDeniedException,
                                                OperationFailedException {
        ScheduleService svcBI = null;
        svcBI =
                BIScheduleWSUtil.createScheduleService(this.jwtFusion, this.instanceName,
                                                       this.dataCenter);

        if (svcBI != null) {
            System.out.println("ScheduleService created");
        } else {
            System.out.println("ScheduleService not created");
            log.error("ScheduleService not created");
            return;
        }
        
        
        

        String personEmail = userEmail;

        ScheduleRequest scheduleRep = new ScheduleRequest();

        EMailDeliveryOption email = new EMailDeliveryOption();
        email.setEmailAttachmentName("PadronesSunat");
        email.setEmailFrom("no.reply@oraclecloud.com");
        email.setEmailServerName("smtp.oraclecloud.com");
        email.setEmailSubject("Reporte para carga de padrones SUNAT");
        //email.setEmailTo("evelyn.juarez@neora.com.pe,juan.gonzales@neora.com.pe");
        email.setEmailTo(personEmail);

        DeliveryChannels delivChann = new DeliveryChannels();
        ArrayOfEMailDeliveryOption delivery = new ArrayOfEMailDeliveryOption();
        delivery.getItem().add(email);
        delivChann.setEmailOptions(delivery);
        
        ArrayOfParamNameValue arrayParam = new ArrayOfParamNameValue();

        ParamNameValue param = null;
        ArrayOfString arrayValues = null;

        param = new ParamNameValue();
        arrayValues = new ArrayOfString();
        param.setName("P_CONCAT_CLASS_CODE");
        
        for (int i=0; i<concatClassCode.length; i++) {
            arrayValues.getItem().add(concatClassCode[i]);
        }
        
        //arrayValues.getItem().add(concatClassCode);
        
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        ParamNameValues params = new ParamNameValues();
        params.setListOfParamNameValues(arrayParam);
        
        ReportRequest rep = new ReportRequest();
        rep.setParameterNameValues(params);
        rep.setAttributeCalendar("Gregorian");
        rep.setAttributeFormat("XLSX");
        rep.setAttributeLocale("English (United States)");
        rep.setAttributeTemplate("Reporte para carga de padrones SUNAT");
        rep.setAttributeTimezone("(UTC-05:00) Lima - Peru Time (PET)");
      //  rep.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Reporte para carga de padrones SUNAT.xdo");
        rep.setReportAbsolutePath(report);//KMORI
        rep.setSizeOfDataChunkDownload(-1);

        scheduleRep.setDeliveryChannels(delivChann);
        scheduleRep.setReportRequest(rep);

        String user = this.userName;
        String pass = this.biUserPass;
        String returnCode = svcBI.scheduleReport(scheduleRep, user, pass);
        
        System.out.println("ScheduleService: returnCode = " + returnCode);
        log.info("ScheduleService: returnCode = " + returnCode);
    }
    
    public void pr_clear_log(OracleConnection trans) throws SQLException {
       
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_clear_log(?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setString(1, "");

            acs.executeUpdate();
    }

    public void run() {
        System.out.println("jwt=" + this.jwtFusion);
        log.info("jwt=" + this.jwtFusion);
        
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;

        String className;
        IProcessAttachment process;
        AttachmentBean attach=null;
        AttachmentTypeInfoBean attachTypeInfo=null;
        String runReportFlag = "";
        //String concatPartyClassCode = "";
        List<String> concatPartyClassCode = new ArrayList<String>();
        String report = "";
        String report2 = "";
        String jobFlag = "";
        String jobId = "";
        
        statusProcess = "";
        messageProcess = "";

        procVal = 2;

        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            this.pr_clear_log(conn);
            
            for (Object obj: this.attachmentList) {
                attach = (AttachmentBean)obj;
                
                attachTypeInfo = this.getAttachTypeInfo(attach.getAttachmentType(), conn);
                
                className = attachTypeInfo.getClassName();
                
                if ("Y".equals(attachTypeInfo.getFiscalClassFlag())) {
                    runReportFlag = attachTypeInfo.getFiscalClassFlag();
                }
                
                jobFlag = attachTypeInfo.getJobFlag();
                
                this.processFileName = attach.getAttachmentName();
                
                Thread.sleep(2000);
                
                if ("Y".equals(jobFlag)) {
                
                
                    jobId = this.getJobSeq(conn);
                    
                    JobKey jobKey = new JobKey(jobId);
                    Class<Job> classVar = (Class<Job>)Job.class.getClassLoader().loadClass(className);
                    pr_log("1","",conn);
                    //JobDetail job = JobBuilder.newJob(LocPeNoHabidoProcessAttach.class)
                    JobDetail job = JobBuilder.newJob(classVar)
                        .withIdentity(jobKey)
                        .usingJobData("idAttachment", attach.getAttachmentId().stringValue())
                        .usingJobData("jwt", this.jwtFusion)
                        .usingJobData("biUserName", this.userName)
                        .usingJobData("biUserPass", this.biUserPass)
                        .usingJobData("userEmail", this.userEmail)
                        .usingJobData("instanceName", this.instanceName)
                        .usingJobData("dataCenter", this.dataCenter)
                        .usingJobData("classCode", attachTypeInfo.getFiscalClassCode())
                        .usingJobData("attachmentType", attach.getAttachmentType())
                       
                        .usingJobData("headerLineFlag", attachTypeInfo.getHeaderLineFlag())
                        .usingJobData("fileSeparator", attachTypeInfo.getFileSeparator())
                        .usingJobData("fiscalClassTypeCode", attachTypeInfo.getFiscalClassTypeCode())
                        .usingJobData("report",attachTypeInfo.getRutReport())//KMORI
                        .usingJobData("report2",attachTypeInfo.getRutReport2())//KMORI
                        
                        .build();
                    //job.getJobDataMap().put("trans", this.transPage);
                    pr_log("2","",conn);                   
                    Trigger triggerJob = TriggerBuilder.newTrigger()
                        .withIdentity(jobId)
                        .forJob(jobKey)
                        .startNow()
                        .build();
                    pr_log("3","",conn);
                    System.out.println("antes schedule process....");
                    
                    this.updateStatusAttach(attach.getAttachmentId(), "PROCESSING", "Solicitud " + jobId, conn);
                    pr_log("4","",conn);
                    Scheduler schedulerJob = new StdSchedulerFactory().getScheduler("JCSScheduler");
                    //schedulerJob.start();
                    //schedulerJob.triggerJob(jobKey);
                    schedulerJob.scheduleJob(job, triggerJob);
                    
                    //this.processFileName = "Job " + jobId;
                    
                    System.out.println("schedule process....");
                    
                    if (procVal + (int)(100/attachmentList.size()) > 99) {
                        pr_log("5","",conn);
                        procVal = 99;
                    } else {
                        pr_log("6","",conn);
                        procVal = procVal + (int)(100/attachmentList.size());
                    } 
                    System.out.println("actualiza contador....");
                } else {
                    pr_log("1","",conn);
                   //log.info("1");
                    this.updateStatusAttach(attach.getAttachmentId(), "PROCESSING", "", conn);
                    pr_log("2",className,conn);
                   //log.info("2");
                    concatPartyClassCode.add(attachTypeInfo.getFiscalClassCode());
                    pr_log("3",attachTypeInfo.getFiscalClassCode(),conn);
                    //concatPartyClassCode = concatPartyClassCode + attachTypeInfo.getFiscalClassCode() + "|";
                    report = attachTypeInfo.getRutReport();//KMORI
                    pr_log("4",report,conn);
                    report2 = attachTypeInfo.getRutReport2();//KMORI
                    //log.info("Ruta reporte2 : " +report2);
                    pr_log("5",report2,conn);
                    pr_log("6",attach.getAttachmentId().stringValue(),conn);
                   // log.info("3");
                    process = (IProcessAttachment)AttachmentPageHelper.class.getClassLoader().loadClass(className).newInstance(); 
                    pr_log("6.1","",conn);
                    process.processFile(attach.getAttachmentId().stringValue(), this.jwtFusion, this.userName, this.biUserPass, this.userEmail, this.instanceName, this.dataCenter,report,report2,attachTypeInfo.getFiscalClassCode());
                  //  log.info("4");
                    pr_log("7","",conn);
                    if (procVal + (int)(100/attachmentList.size()) > 99) {
                        procVal = 99;
                    } else {
                        procVal = procVal + (int)(100/attachmentList.size());
                    }                
                    
                    if (IProcessAttachment.ERROR.equals(process.getStatus())) {
                    //    log.info("5");
                        pr_log("8",process.getStatus(),conn);
                        statusProcess = process.getStatus();
                        messageProcess = process.getMessage();
                        this.updateStatusAttach(attach.getAttachmentId(), "ERROR", messageProcess, conn);
                    }
                }                
            }
            
            log.info("statusProcess: " + statusProcess);
            log.info("runReportFlag: " + runReportFlag);
            log.info("concatPartyClassCode: " + concatPartyClassCode);
            
            if ("".equals(statusProcess)) {
                statusProcess = IProcessAttachment.SUCCESS;
                
                //if ("Y".equals(runReportFlag) && !"".equals(concatPartyClassCode)) {
              /*  if ("Y".equals(runReportFlag) && concatPartyClassCode.size() > 0) {
                    pr_log("9",runReportFlag,conn);
                    this.executeFiscalClassRep(concatPartyClassCode.toArray(new String[concatPartyClassCode.size()]),report);//KMORI
                    pr_log("10",runReportFlag,conn);
                }*/
            }

            procVal = 100;
        } catch (Exception ex) {
            log.error("Error en run(): " + ex.getMessage(), ex);
            log.error(Arrays.toString(ex.getStackTrace()));
            try {
                pr_log("Error",ex.getMessage(),conn);
            } catch (SQLException e) {
            }
            System.out.println("Error en run() - " + ex.getMessage());
            ex.printStackTrace();
            procVal = 100;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on run(): " + sqlex.getMessage(), sqlex);
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
                
            }
        }
        }

    public void pr_log(String pNombre,String pTipo, OracleConnection trans) throws SQLException {
       
            String st =
                "begin XXSS_PE_LOC_DETRACC_PKG.pr_log(?,?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setString(1, pNombre);
            acs.setString(2, pTipo);

            acs.executeUpdate();
    }

    public void setProcessPopup(RichPopup processPopup) {
        this.processPopup = processPopup;
    }

    public RichPopup getProcessPopup() {
        return processPopup;
    }

    public void pollListenerEvent(PollEvent pollEvent) {
        // Add event code here...
        //System.out.println("poll refresh");
        if (procVal == getMaximum()) {
            //System.out.println("Llego al maximo (" + pollInterval);
            //Setting poll interval to negative value to stop polling
            this.pollInterval = -1;
            this.procVal = 0;
            //Refresh poll component
            RichPopup popup = this.getProcessPopup();
            popup.hide();
            //AdfFacesContext.getCurrentInstance().getViewScope().put("refreshNeeded", Boolean.TRUE); 

            if (this.statusProcess.equals(IProcessAttachment.ERROR)) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                     this.messageProcess, "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                System.out.println(this.messageProcess);
            }
            
            DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
            DCIteratorBinding attachIter =
                bindings.findIteratorBinding("XxssPeLocAttachmentsVO1Iterator");

            ViewObject voTableData = attachIter.getViewObject();
            voTableData.executeQuery();

            //AdfFacesContext.getCurrentInstance().addPartialTarget(this.getPanelCollection());
            AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAttachTable());
            AdfFacesContext.getCurrentInstance().addPartialTarget(pollEvent.getComponent());
            
        }
    }

    public void processButtonAction(ActionEvent actionEvent) {
        // Add event code here...
        DCBindingContainer bindings = (DCBindingContainer)this.getBindings();
        DCIteratorBinding attachIter =
            bindings.findIteratorBinding("XxssPeLocAttachmentsVO1Iterator");
        RowSetIterator attachRSIter = attachIter.getRowSetIterator();
        
        List attachmentList = new ArrayList();
        RowKeySet selectedAttach = this.getAttachTable().getSelectedRowKeys();
        Iterator selectedAttachIter = selectedAttach.iterator();
        
        String attachId, attachType, attachName, attachStatus;
        
        while (selectedAttachIter.hasNext()) {
            Key key = (Key)((List)selectedAttachIter.next()).get(0);
            Row currentRow = attachRSIter.getRow(key);
            
            attachId = ((oracle.jbo.domain.Number)currentRow.getAttribute("AttachmentId")).stringValue();
            attachType = (String)currentRow.getAttribute("AttachmentType");
            attachName = (String)currentRow.getAttribute("FileName");
            attachStatus = (String)currentRow.getAttribute("Status");
            
            if ("PROCESSED".equals(attachStatus) || "PROCESSING".equals(attachStatus)) {
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "El archivo " + attachName + " ya fue procesado.",
                                     "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return;
            }
            
            attachmentList.add(new AttachmentBean((oracle.jbo.domain.Number)currentRow.getAttribute("AttachmentId"), attachName, attachType));
        }
        
        if (attachmentList.size() > 0) {
            
            //this.transPage = ((ApplicationModuleImpl)bindings.findDataControl("XxssPeLocAttachmentsAMDataControl").getApplicationModule()).getDBTransaction();
            this.jwtFusion =  JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");
            this.attachmentList = attachmentList;
            
            RichPopup popup = this.getProcessPopup();
            RichPopup.PopupHints hints = new RichPopup.PopupHints();
            popup.show(hints);

            this.pollInterval = 2000;
            //Set initial progrss to zero
            this.procVal = 0;
            this.showExitProgressPopup = false;
            this.userName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.biUserName}");
            this.userEmail = JSFUtils.resolveExpressionAsString("#{pageFlowScope.personEmail}");
            this.biUserPass= JSFUtils.resolveExpressionAsString("#{pageFlowScope.biUserPass}");
            
            this.instanceName = JSFUtils.resolveExpressionAsString("#{pageFlowScope.instanceName}");
            this.dataCenter = JSFUtils.resolveExpressionAsString("#{pageFlowScope.dataCenter}");
            
            //Start a thread
            Thread t = new Thread(this);
            t.start();
            
        
            
        } else {
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Seleccionar por lo menos un registro.",
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void setPollInterval(int pollInterval) {
        this.pollInterval = pollInterval;
    }

    public int getPollInterval() {
        return pollInterval;
    }

    public void setShowExitProgressPopup(boolean showExitProgressPopup) {
        this.showExitProgressPopup = showExitProgressPopup;
    }

    public boolean isShowExitProgressPopup() {
        return showExitProgressPopup;
    }

    public void exitProgressPopupAction(ActionEvent actionEvent) {
        // Add event code here...
        RichPopup popup = this.getProcessPopup();
        popup.hide();
    }

    public void downloadFileAction(ActionEvent actionEvent) {
        // Add event code here...
    }

    public void setDownloadFileLink(RichCommandLink downloadFileLink) {
        this.downloadFileLink = downloadFileLink;
    }

    public RichCommandLink getDownloadFileLink() {
        return downloadFileLink;
    }

    public void setHideDownloadFileButton(RichCommandButton hideDownloadFileButton) {
        this.hideDownloadFileButton = hideDownloadFileButton;
    }

    public RichCommandButton getHideDownloadFileButton() {
        return hideDownloadFileButton;
    }

    public void downloadActionListenerAction(FacesContext facesContext,
                                             OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("ClobFile");
        if (attr == null) {
            return;
        }

        // the value is a BlobDomain data type
        BlobDomain blob = (BlobDomain)attr.getInputValue();

        try { // copy hte data from the BlobDomain to the output stream
            IOUtils.copy(blob.getInputStream(), outputStream);
            // flush the outout stream
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            blob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    public void downloadFileLinkAction(ActionEvent actionEvent) {
        // Add event code here...
        FacesContext fctx = FacesContext.getCurrentInstance();
        String buttonId = hideDownloadFileButton.getClientId(fctx);
        StringBuilder script = new StringBuilder();
        script.append("var dlButton = AdfPage.PAGE.findComponentByAbsoluteId('" +
                      buttonId + "');");
        script.append("dlEvent = new AdfActionEvent(dlButton);");
        //script.append("dlButton.queueEvent(dlEvent, true);");
        script.append("dlEvent.forceFullSubmit();");
        script.append("dlEvent.noResponseExpected();");
        script.append("AdfActionEvent.queue(dlButton, dlButton.getPartialSubmit());");
        ExtendedRenderKitService erks =
            Service.getService(fctx.getRenderKit(), ExtendedRenderKitService.class);
        erks.addScript(fctx, script.toString());
    }

    public void deleteAttachmentAction(ActionEvent actionEvent) {
        // Add event code here...
        Object result;
        BindingContainer bindings = getBindings();
        OperationBinding operationBindingDelete =
            bindings.getOperationBinding("Delete");
        result = operationBindingDelete.execute();
        if (!operationBindingDelete.getErrors().isEmpty()) {
            return;
        }

        OperationBinding operationBindingCommit =
            bindings.getOperationBinding("Commit");
        result = operationBindingCommit.execute();
        if (!operationBindingCommit.getErrors().isEmpty()) {
            return;
        }

        AdfFacesContext.getCurrentInstance().addPartialTarget(this.getAttachTable());
    }

    public void setDownloadOutputLink(RichCommandLink downloadOutputLink) {
        this.downloadOutputLink = downloadOutputLink;
    }

    public RichCommandLink getDownloadOutputLink() {
        return downloadOutputLink;
    }

    public void downloadOutputListenerAction(FacesContext facesContext,
                                             OutputStream outputStream) {
        // Add event code here...
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();

        // get an ADF attributevalue from the ADF page definitions
        AttributeBinding attr =
            (AttributeBinding)bindings.getControlBinding("OutputFile");
        if (attr == null) {
            log.error("Can't find OutputFile binding...");
            return;
        }

        // the value is a BlobDomain data type
        BlobDomain blob = (BlobDomain)attr.getInputValue();

        try { // copy hte data from the BlobDomain to the output stream
            //Date nowDate = new Date();
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            //this.fileNameOut = "TaxImplementationWorkbook" + sdf.format(nowDate) + ".zip";
            ByteArrayOutputStream byteOS = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(byteOS);
                        
            //System.out.println("blob.length " + blob.getLength());
            log.info("blob.length " + blob.getLength());
            
            byte[] fileBytes = blob.toByteArray();
            
            //System.out.println("fileBytes.length " + fileBytes.length);  
            log.info("fileBytes.length " + fileBytes.length);
            
            zos.putNextEntry(new ZipEntry("TaxImplementationData.csv"));
            zos.write(fileBytes);
            zos.closeEntry();
            zos.finish();
            
            byteOS.writeTo(outputStream);

            //IOUtils.copy(blob.getInputStream(), outputStream);
            // flush the outout stream
            outputStream.flush();
            outputStream.close();
            // cloase the blob to release the recources
            blob.closeOutputStream();
        } catch (IOException e) {
            // handle errors
            log.error("Error en downloadOutputListenerAction(): " + e.getMessage(), e);
            log.error(Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
            FacesMessage msg =
                new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                 "");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void downloadOutputReporteListenerAction(FacesContext facesContext,
                                             OutputStream outputStream){
           /* InitialContext ctx = null;
            OracleConnection conn = null;
            DataSource ds = null;
    */
            // Add event code here...
               
           /* ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();            
            conn.setAutoCommit(false);
                */
            DCBindingContainer bindings = (DCBindingContainer)this.getBindings();  
                        
            XxssPeLocAttachmentsAMImpl guiaAM = (XxssPeLocAttachmentsAMImpl)bindings.findDataControl("XxssPeLocAttachmentsAMDataControl").getApplicationModule();
            ViewObject vo = guiaAM.getXxssPeLocAttachmentsVO1();
            Row row = vo.getCurrentRow();

            ClobDomain clob = (ClobDomain)row.getAttribute("OutputFileReport");
               // this.pr_log("0", "antes if", conn);
            try { 
                if (clob == null) {
                    //this.pr_log("0", "entro null", conn);
                    log.error("Can't find OutputFile binding...");
                    return;
                }
               // this.pr_log("0", "despues if", conn);
            // the value is a BlobDomain data type
            
           
                ByteArrayOutputStream byteOS = new ByteArrayOutputStream();
                ZipOutputStream zos = new ZipOutputStream(byteOS);
                            
                //System.out.println("blob.length " + blob.getLength());
                log.info("blob.length " + clob.getLength());
                
                byte[] fileBytes = clob.toByteArray();
                
                //System.out.println("fileBytes.length " + fileBytes.length);  
                log.info("fileBytes.length " + fileBytes.length);
                
                zos.putNextEntry(new ZipEntry("ReportePadronSunat.csv"));
                zos.write(fileBytes);
                zos.closeEntry();
                zos.finish();
                
                byteOS.writeTo(outputStream);

                //IOUtils.copy(blob.getInputStream(), outputStream);
                // flush the outout stream
                outputStream.flush();
                outputStream.close();
                // cloase the blob to release the recources
                clob.closeOutputStream();
            } catch (IOException e) {
                // handle errors
                log.error("Error en downloadOutputListenerAction(): " + e.getMessage(), e);
                log.error(Arrays.toString(e.getStackTrace()));
                e.printStackTrace();
                FacesMessage msg =
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(),
                                     "");
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }

    }

    public void setFileNameOut(String fileNameOut) {
        this.fileNameOut = fileNameOut;
    }

    public String getFileNameOut() {
        return fileNameOut;
    }

    public void setProcessFileName(String processFileName) {
        this.processFileName = processFileName;
    }

    public String getProcessFileName() {
        return processFileName;
    }

    public void setDownloadOutputLinkReporte(RichCommandLink downloadOutputLinkReporte) {
        this.downloadOutputLinkReporte = downloadOutputLinkReporte;
    }

    public RichCommandLink getDownloadOutputLinkReporte() {
        return downloadOutputLinkReporte;
    }
}
