package xxss.oracle.localizations.pe.app.guiaremision.etd;

public class GlobalParameters {
    
    String instanceName;
    String dataCenter;
    String fusionUserName;
    String fusionUserPass;
    
    public GlobalParameters() {
        super();
    }


    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setDataCenter(String dataCenter) {
        this.dataCenter = dataCenter;
    }

    public String getDataCenter() {
        return dataCenter;
    }

    public void setFusionUserName(String fusionUserName) {
        this.fusionUserName = fusionUserName;
    }

    public String getFusionUserName() {
        return fusionUserName;
    }

    public void setFusionUserPass(String fusionUserPass) {
        this.fusionUserPass = fusionUserPass;
    }

    public String getFusionUserPass() {
        return fusionUserPass;
    }
}
