
package xxss.oracle.localizations.servicesprod.gosocketprod.types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xxss.oracle.localizations.servicesprod.gosocketprod.types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xxss.oracle.localizations.servicesprod.gosocketprod.types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConvertSignDocument }
     * 
     */
    public ConvertSignDocument createConvertSignDocument() {
        return new ConvertSignDocument();
    }

    /**
     * Create an instance of {@link ReceiveMessageResponse }
     * 
     */
    public ReceiveMessageResponse createReceiveMessageResponse() {
        return new ReceiveMessageResponse();
    }

    /**
     * Create an instance of {@link DispatchDocument }
     * 
     */
    public DispatchDocument createDispatchDocument() {
        return new DispatchDocument();
    }

    /**
     * Create an instance of {@link ConvertSignDocumentResponse }
     * 
     */
    public ConvertSignDocumentResponse createConvertSignDocumentResponse() {
        return new ConvertSignDocumentResponse();
    }

    /**
     * Create an instance of {@link ConvertDocumentResponse }
     * 
     */
    public ConvertDocumentResponse createConvertDocumentResponse() {
        return new ConvertDocumentResponse();
    }

    /**
     * Create an instance of {@link DispatchDocumentResponse }
     * 
     */
    public DispatchDocumentResponse createDispatchDocumentResponse() {
        return new DispatchDocumentResponse();
    }

    /**
     * Create an instance of {@link ReceiveMessage }
     * 
     */
    public ReceiveMessage createReceiveMessage() {
        return new ReceiveMessage();
    }

    /**
     * Create an instance of {@link ConvertDocument }
     * 
     */
    public ConvertDocument createConvertDocument() {
        return new ConvertDocument();
    }

}
