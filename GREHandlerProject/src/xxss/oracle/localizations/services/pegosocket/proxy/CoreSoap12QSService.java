package xxss.oracle.localizations.services.pegosocket.proxy;

import java.io.File;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (11.1.1.0.0, build 130224.1947.04102)

@WebServiceClient(wsdlLocation="https://testsoagbss.opc.oracleoutsourcing.com/osb/GoSocketPeru/ProxyService/GoSocketPeru_PS?wsdl",
  targetNamespace="http://tempuri.org/", name="CoreSoap12QSService")
public class CoreSoap12QSService
  extends Service
{
  private static URL wsdlLocationURL;

  private static Logger logger;
  static
  {
    try
    {
      logger = Logger.getLogger("xxss.oracle.localizations.services.pegosocket.proxy.CoreSoap12QSService");
      URL baseUrl = CoreSoap12QSService.class.getResource(".");
      if (baseUrl == null)
      {
        wsdlLocationURL =
            CoreSoap12QSService.class.getResource("https://testsoagbss.opc.oracleoutsourcing.com/osb/GoSocketPeru/ProxyService/GoSocketPeru_PS?wsdl");
        if (wsdlLocationURL == null)
        {
          baseUrl = new File(".").toURL();
          wsdlLocationURL =
              new URL(baseUrl, "https://testsoagbss.opc.oracleoutsourcing.com/osb/GoSocketPeru/ProxyService/GoSocketPeru_PS?wsdl");
        }
      }
      else
      {
                if (!baseUrl.getPath().endsWith("/")) {
         baseUrl = new URL(baseUrl, baseUrl.getPath() + "/");
}
                wsdlLocationURL =
            new URL(baseUrl, "https://testsoagbss.opc.oracleoutsourcing.com/osb/GoSocketPeru/ProxyService/GoSocketPeru_PS?wsdl");
      }
    }
    catch (MalformedURLException e)
    {
      logger.log(Level.ALL,
          "Failed to create wsdlLocationURL using https://testsoagbss.opc.oracleoutsourcing.com/osb/GoSocketPeru/ProxyService/GoSocketPeru_PS?wsdl",
          e);
    }
  }

  public CoreSoap12QSService()
  {
    super(wsdlLocationURL,
          new QName("http://tempuri.org/", "CoreSoap12QSService"));
  }

  public CoreSoap12QSService(URL wsdlLocation, QName serviceName)
  {
    super(wsdlLocation, serviceName);
  }

  @WebEndpoint(name="CoreSoap12QSPort")
  public CoreSoap getCoreSoap12QSPort()
  {
    return (CoreSoap) super.getPort(new QName("http://tempuri.org/",
                                              "CoreSoap12QSPort"),
                                    CoreSoap.class);
  }

  @WebEndpoint(name="CoreSoap12QSPort")
  public CoreSoap getCoreSoap12QSPort(WebServiceFeature... features)
  {
    return (CoreSoap) super.getPort(new QName("http://tempuri.org/",
                                              "CoreSoap12QSPort"),
                                    CoreSoap.class, features);
  }
}
