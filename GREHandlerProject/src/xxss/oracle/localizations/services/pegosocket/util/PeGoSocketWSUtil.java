package xxss.oracle.localizations.services.pegosocket.util;

import java.net.MalformedURLException;

import java.net.URL;

import javax.xml.namespace.QName;

import org.apache.log4j.Logger;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.pegosocket.proxy.CoreSoap;
import xxss.oracle.localizations.services.pegosocket.proxy.CoreSoap12QSService;

public class PeGoSocketWSUtil {
    private static Logger log = Logger.getLogger(PeGoSocketWSUtil.class);
    
    public PeGoSocketWSUtil() {
        super();
    }
    
    public static CoreSoap createCoreSoap(String url) throws MalformedURLException {
        CoreSoap12QSService service_Service = null;
        CoreSoap service = null;
        
        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
            new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        
        service_Service = new CoreSoap12QSService(new URL(url), new QName("http://tempuri.org/", "CoreSoap12QSService"));
        
        service = service_Service.getCoreSoap12QSPort();
        
        if (service == null) {
            log.info("SignatureService is null");
        } else {
            log.info("SignatureService created.");
        }
        
        return(service);
    }
}
