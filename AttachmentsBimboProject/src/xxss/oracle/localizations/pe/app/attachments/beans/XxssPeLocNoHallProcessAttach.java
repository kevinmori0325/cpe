package xxss.oracle.localizations.pe.app.attachments.beans;

import com.sun.tools.xjc.reader.dtd.bindinfo.BIUserConversion;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.io.OutputStream;

import java.io.StringReader;

import java.sql.SQLException;

import java.sql.Types;

import java.util.Arrays;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.Number;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;

import org.apache.commons.io.IOUtils;

import org.apache.log4j.Logger;

import xxss.oracle.localizations.pe.app.attachments.interfaces.IProcessAttachment;
import xxss.oracle.localizations.pe.app.attachments.services.BIScheduleWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil2;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValues;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportResponse;

public class XxssPeLocNoHallProcessAttach implements IProcessAttachment {
    private static Logger log = Logger.getLogger(XxssPeLocNoHallProcessAttach.class);
    private String status;
    private String message;

    public XxssPeLocNoHallProcessAttach() {
        super();
    }
    public void pr_clear_log(OracleConnection trans) throws SQLException {
       
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_clear_log(?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setString(1, "");

            acs.executeUpdate();
    }

    private BlobDomain createBlobDomain(InputStream file) {
        // init the internal variables
        InputStream in = null;
        BlobDomain blobDomain = null;
        OutputStream out = null;

        try {
            // Get the input stream representing the data from the client
            in = file; //file.getInputStream();
            // create the BlobDomain datatype to store the data in the db
            blobDomain = new BlobDomain();
            // get the outputStream for hte BlobDomain
            out = blobDomain.getBinaryOutputStream();
            // copy the input stream into the output stream
            /*
                 * IOUtils is a class from the Apache Commons IO Package (http://www.apache.org/)
                 * Here version 2.0.1 is used
                 * please download it directly from http://projects.apache.org/projects/commons_io.html
                 */
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.fillInStackTrace();
        }

        // return the filled BlobDomain
        return blobDomain;
    }

    public void readReportOutput(InputStream reportIs, OracleConnection trans,
                                 Number idAttachment) {
        try {
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_read_report_output(pt_report_blob => ?, pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setBlob(1, reportIs);
            acs.setNUMBER(2, idAttachment);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
            acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            String procStatus = acs.getString(3);
            String procMessage = acs.getString(4);

            if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }

        } catch (SQLException exsql) {
            status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();
            log.error("Error SQL en readReportOutput(): " + exsql.getMessage(), exsql);
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage());
        } catch (Exception e) {
            status = ERROR;
            message =
                    "Error en XxssPeLocNoHallProcessAttach: " + e.getMessage();
            log.error("Error en readReportOutput(): " + e.getMessage(), e);
            System.out.println("Error en readReportOutput() - " + e.getMessage());
        }
        ;
    }

    public void callProcessAttachDB(String pAttachId, OracleConnection trans) {
        try {
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_process_file_NH(pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, new Number(pAttachId));
            acs.registerOutParameter(2, Types.VARCHAR, 0, 1);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            String procStatus = acs.getString(2);
            String procMessage = acs.getString(3);

            if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }
        } catch (SQLException exsql) {
            status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();
            log.error("Error SQL en callProcessAttachDB(): " + exsql.getMessage(), exsql);
            System.out.println("Error SQL en callProcessAttachDB() - " + exsql.getMessage());
        } catch (Exception e) {
            status = ERROR;
            message =
                    "Error en v: " + e.getMessage();
            log.error("Error en callProcessAttachDB(): " + e.getMessage(), e);
            System.out.println("Error en callProcessAttachDB() - " + e.getMessage());
        }
        ;
    }

    public void pr_reporte(String idAttach, String concatClassCode,
                           OracleConnection conn) throws SQLException {


        OraclePreparedStatement ps;
        OracleResultSet rs;
        StringBuffer strBuffer = new StringBuffer();
        String ln = System.getProperty("line.separator");


        //this.pr_log("1", "", conn);
        this.fillPartyClassif(concatClassCode, conn);
        //this.pr_log("2", "", conn);
        String sql = "select PARTY_NUMBER,\n" +
            "       PARTY_NAME,\n" +
            "       PARTY_CLASSIF_CODE,\n" +
            "       PARTY_CLASSIF_TYPE_CODE,\n" +
            "       EFFECTIVE_FROM,\n" +
            "       EFFECTIVE_TO,\n" +
            "       CUSTOMER_FLAG,\n" +
            "       SUPPLIER_FLAG,\n" +
            "       TEMPLATE_ID,\n" +
            "       PARTY_TYPE,\n" +
            "       TAX_REGISTRATION_NUM,\n" +
            "       ACTION\n" +
            "  from xxss_pe_loc_ws_party_CLASS_gt\n" +
            "  order by ACTION, TAX_REGISTRATION_NUM";

        ps =
    (OraclePreparedStatement)conn.prepareStatement(sql, OracleResultSet.TYPE_FORWARD_ONLY,
                                                OracleResultSet.CONCUR_READ_ONLY);

        rs = (OracleResultSet)ps.executeQuery();

        strBuffer.append("ACCION A REALIZAR" + "," + "NRO. DE TERCERO" + "," +
                         "RUC" + "," + "NOMBRE DE TERCERO" + "," +
                         "ES PROVEEDOR" + "," + "ES CLIENTE" + "," +
                         "TIPO DE CLASIFICACION" + "," + "CLASIFICACION" +
                         "," + "FECHA EFECTIVA DESDE" + "," +
                         "FECHA EFECTIVA HASTA" + ln);

        while (rs.next()) {
            //this.pr_log("3", rs.getString("PARTY_NAME"), conn);

            strBuffer.append(rs.getString("ACTIOn") + "," +
                             rs.getString("PARTY_NUMBER") + "," +
                             rs.getString("TAX_REGISTRATION_NUM") + "," +
                             rs.getString("PARTY_NAME") + "," +
                             rs.getString("SUPPLIER_FLAG") + "," +
                             rs.getString("CUSTOMER_FLAG") + "," +
                             rs.getString("PARTY_CLASSIF_TYPE_CODE") + "," +
                             rs.getString("PARTY_CLASSIF_CODE") + "," +
                             rs.getString("EFFECTIVE_FROM") + "," +
                             rs.getString("EFFECTIVE_TO") + ln);
        }


        //PROCESO PARA ACTUALIZAR CAMPO OUTFILE_REPORT
        this.updateByteReport(idAttach, strBuffer.toString(), conn);
        //this.pr_log("4", "updateByteReport", conn);
        ps.close();

    }

    private void fillPartyClassif(String concatClassCode,
                                  OracleConnection conn) throws SQLException {
        OracleCallableStatement cs;
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_ws_fill_party_classif_gt(?); end;";

        cs = (OracleCallableStatement)conn.prepareCall(st);
        cs.setString(1, concatClassCode);

        cs.executeUpdate();
        cs.close();
    }

    public void updateByteReport(String pIdFactCert, String pTrama,
                                 OracleConnection trans) {
        try {
            Number nAttachId = new Number(pIdFactCert);
            StringReader requestSr = new StringReader(pTrama);
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_update_bytes_report(?, ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, nAttachId);
            acs.setClob(2, requestSr);

            acs.executeUpdate();
        } catch (SQLException sqlex) {
            //   log.error("Error en updateStatusFactCert(): " + sqlex.getMessage(), sqlex);
            //   log.error(Arrays.toString(sqlex.getStackTrace()));
            System.out.println("Error en updateByteReport(): " +
                               sqlex.getMessage());
        }
    }

    public void processFile(String idAttachment, String jwt, String biUserName,
                            String biUserPass, String userEmail,
                            String instanceName, String dataCenter,
                            String report, String report2, String classCode) {
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;

        // Llamada a WS para ejecutar reporte
        ExternalReportWSSService svc = null;
        //ScheduleService svcBI = null;
        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();

            conn.setAutoCommit(false);
            
            //       svc = ExternalReportWSUtil.createExternalReportWSSService(jwt, instanceName, dataCenter);
            svc =
    ExternalReportWSUtil2.createExternalReportWSSServiceUserToken2(instanceName,
                                                               dataCenter,
                                                               biUserName,
                                                               biUserPass);
            //svcBI = BIScheduleWSUtil.createScheduleService(jwt, instanceName, dataCenter);

            if (svc != null) {
                System.out.println("ExternalReportWS created");
            } else {
                this.status = ERROR;
                this.message = "ExternalReportWS is not created";
                log.error(message);
                System.out.println(message);
                return;
            }

            /*if (svcBI != null) {
                System.out.println("ScheduleService created");
            } else {
                this.status = ERROR;
                this.message = "ScheduleService is not created";
                System.out.println(message);
                return;
            }*/

            ReportRequest req = new ReportRequest();
            ArrayOfParamNameValue arrayParam = new ArrayOfParamNameValue();

            ParamNameValue param = null;
            ArrayOfString arrayValues = null;

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_DM_DEBUG_FLAG");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);


            req.setParameterNameValues(arrayParam);
            //req.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Party Tax Classifications.xdo");
            req.setReportAbsolutePath(report2); //KMORI
            req.setSizeOfDataChunkDownload(-1);

            ReportResponse resp = svc.runReport(req, "");

            byte[] reportOutputBytes = resp.getReportBytes();

            if (reportOutputBytes == null || reportOutputBytes.length == 0) {
                this.status = ERROR;
                this.message =
                        "Party Tax Classification report cannot be executed.";
                log.error(message);
                System.out.println(message);
                return;
            }

            InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);
            this.readReportOutput(reportIs, conn, new Number(idAttachment));

            if (this.status != null && this.status.equals(ERROR)) {
                return;
            }

            this.callProcessAttachDB(idAttachment, conn);
            if (this.status != null && this.status.equals(ERROR)) {
                return;
            }

            this.pr_reporte(idAttachment, classCode, conn);

            // llamando a ws para ejecucion y envio de reporte
            /*String personEmail = userEmail;

            ScheduleRequest scheduleRep = new ScheduleRequest();

            EMailDeliveryOption email = new EMailDeliveryOption();
            email.setEmailAttachmentName("PadronesSunat");
            email.setEmailFrom("noreply@neora.com.pe");
            email.setEmailServerName("smtp.neora.com.pe");
            email.setEmailSubject("Reporte para carga de padrones SUNAT");
            email.setEmailTo(personEmail);

            DeliveryChannels delivChann = new DeliveryChannels();
            ArrayOfEMailDeliveryOption delivery =
                new ArrayOfEMailDeliveryOption();
            delivery.getItem().add(email);
            delivChann.setEmailOptions(delivery);

            xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest rep =
                new xxss.oracle.localizations.services.fusion.bischeduleservice.types.ReportRequest();
            rep.setAttributeCalendar("Gregorian");
            rep.setAttributeFormat("XLSX");
            rep.setAttributeLocale("English (United States)");
            rep.setAttributeTemplate("Reporte para carga de padrones SUNAT");
            rep.setAttributeTimezone("(UTC-05:00) Lima - Peru Time (PET)");
            rep.setReportAbsolutePath("/Custom/PE Localizaciones/Padrones SUNAT/Reporte para carga de padrones SUNAT.xdo");
            rep.setSizeOfDataChunkDownload(-1);

            scheduleRep.setDeliveryChannels(delivChann);
            scheduleRep.setReportRequest(rep);

            String user = biUserName;
            String pass = biUserPass;
            String returnCode = svcBI.scheduleReport(scheduleRep, user, pass);

            if(returnCode == null || "".equals(returnCode)) {
                this.status = ERROR;
                this.message = "Error al ejecutar reporte BI.";
                return;
            }*/

            this.status = SUCCESS;

            conn.commit();

        } catch (Exception ex) {
            this.status = ERROR;
            this.message = ex.getMessage();
            log.error("Error en processFile(): " + ex.getMessage(), ex);
            System.out.println("Error en processFile() - " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("SQLException on processFile(): " +
                              sqlex.getMessage(), sqlex);
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }

            }
        }
    }
    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
