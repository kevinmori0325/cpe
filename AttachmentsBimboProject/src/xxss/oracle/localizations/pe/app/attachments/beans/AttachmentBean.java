package xxss.oracle.localizations.pe.app.attachments.beans;

import oracle.jbo.domain.Number;

public class AttachmentBean {
    private String attachmentName;
    private String attachmentType;
    private Number attachmentId;

    public AttachmentBean(Number attachId, String attachName,
                          String attachType) {
        super();
        this.attachmentId = attachId;
        this.attachmentName = attachName;
        this.attachmentType = attachType;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentId(Number attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Number getAttachmentId() {
        return attachmentId;
    }
}
