package xxss.oracle.localizations.pe.app.attachments.beans;

public class AttachmentTypeInfoBean {
    private String className;
    private String fiscalClassFlag;
    private String fiscalClassCode;
    private String jobFlag;
    private String headerLineFlag;
    private String fileSeparator;
    private String fiscalClassTypeCode;
    private String rutReport;
    private String rutReport2;
    
    public AttachmentTypeInfoBean() {
        super();
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public void setFiscalClassFlag(String fiscalClassFlag) {
        this.fiscalClassFlag = fiscalClassFlag;
    }

    public String getFiscalClassFlag() {
        return fiscalClassFlag;
    }

    public void setFiscalClassCode(String fiscalClassCode) {
        this.fiscalClassCode = fiscalClassCode;
    }

    public String getFiscalClassCode() {
        return fiscalClassCode;
    }

    public void setJobFlag(String jobFlag) {
        this.jobFlag = jobFlag;
    }

    public String getJobFlag() {
        return jobFlag;
    }

    public void setHeaderLineFlag(String headerLineFlag) {
        this.headerLineFlag = headerLineFlag;
    }

    public String getHeaderLineFlag() {
        return headerLineFlag;
    }

    public void setFileSeparator(String fileSeparator) {
        this.fileSeparator = fileSeparator;
    }

    public String getFileSeparator() {
        return fileSeparator;
    }

    public void setFiscalClassTypeCode(String fiscalClassTypeCode) {
        this.fiscalClassTypeCode = fiscalClassTypeCode;
    }

    public String getFiscalClassTypeCode() {
        return fiscalClassTypeCode;
    }

  public void setRutReport(String rutReport) {
    this.rutReport = rutReport;
  }

  public String getRutReport() {
    return rutReport;
  }

  public void setRutReport2(String rutReport2) {
    this.rutReport2 = rutReport2;
  }

  public String getRutReport2() {
    return rutReport2;
  }
}
