package xxss.oracle.localizations.pe.app.attachments.services;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService_Service;
//import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
//import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService_Service;

public class ExternalReportWSUtil2 {
    public ExternalReportWSUtil2() {
        super();
    }
    
    public static ExternalReportWSSService createExternalReportWSSService(String jwt, String instanceName, String dataCenter) throws MalformedURLException {
        ExternalReportWSSService_Service service_Service;
                ExternalReportWSSService service = null;

                //String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

                if (jwt == null || "".equals(jwt)) {
                    return service;
                }

                // JWT requires no client policy (with this unpatched version of JDev)
                // TODO:  research patch that adds JWT client policy to OWSM on client side

                SecurityPolicyFeature[] secFeatures =
                    new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
                //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

                service_Service = new ExternalReportWSSService_Service(new URL("https://" + instanceName + ".fa." + dataCenter + ".oraclecloud.com/xmlpserver/services/ExternalReportWSSService"), 
                                                                       new QName("http://xmlns.oracle.com/oxp/service/PublicReportService", "ExternalReportWSSService"));
                service =
                        service_Service.getExternalReportWSSService(secFeatures);
                //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
                if (service == null) {
                    System.out.println("ExternalReportWSSService is null");
                } else {
                    System.out.println("ExternalReportWSSService created.");
                }

                System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

                // add JWT auth map to HTTP header
                BindingProvider bp = (BindingProvider)service;
                Map<String, List<String>> authMap =
                    new HashMap<String, List<String>>();
                List<String> authZlist = new ArrayList<String>();
                authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
                authMap.put("Authorization", authZlist);

                bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS,
                                           authMap);

                return service;
    }
    
    public static ExternalReportWSSService createExternalReportWSSServiceUserToken2(String instanceName,
                                                                                       String dataCenter, String userName,
                                                                                       String userPass) throws MalformedURLException {
            ExternalReportWSSService_Service service_Service;
            ExternalReportWSSService service = null;
            
            service_Service =
                    new ExternalReportWSSService_Service();

            if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                return service;
            }

            SecurityPolicyFeature[] secFeatures =
            { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

            
            service = service_Service.getExternalReportWSSService(secFeatures);

            if (service == null) {
                System.out.println("ExternalReportWSSService is null");
            } else {
                System.out.println("ExternalReportWSSService created.");
            }

            // add JWT auth map to HTTP header
            WSBindingProvider wsbp = (WSBindingProvider)service;
            wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
            wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
            wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com:443/xmlpserver/services/ExternalReportWSSService");
            wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

            return service;
        }
    
  /*  public static ExternalReportWSSService createExternalReportWSSServiceUserToken2(String instanceName,
                                                                                   String dataCenter, String userName,
                                                                                   String userPass) throws MalformedURLException {
        ExternalReportWSSService_Service service_Service;
        ExternalReportWSSService service = null;

        

        if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
            return service;
        }



        SecurityPolicyFeature[] secFeatures =
        { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

        service_Service =
                new ExternalReportWSSService_Service(new URL("https://" + instanceName + ".fa." + dataCenter +
                                                             ".oraclecloud.com/xmlpserver/services/ExternalReportWSSService"),
                                                     new QName("http://xmlns.oracle.com/oxp/service/PublicReportService",
                                                               "ExternalReportWSSService"));
        service = service_Service.getExternalReportWSSService(secFeatures);

        if (service == null) {
            System.out.println("ExternalReportWSSService is null");
        } else {
            System.out.println("ExternalReportWSSService created.");
        }

        
        WSBindingProvider wsbp = (WSBindingProvider)service;
        wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
        wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
        wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

        return service;
    }
*/
}
