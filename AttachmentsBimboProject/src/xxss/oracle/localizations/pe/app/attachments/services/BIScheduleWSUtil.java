package xxss.oracle.localizations.pe.app.attachments.services;

import com.sun.xml.ws.client.BindingProviderProperties;
import com.sun.xml.ws.developer.WSBindingProvider;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import javax.xml.ws.handler.MessageContext;

import weblogic.wsee.jws.jaxws.owsm.SecurityPolicyFeature;

import xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy.ScheduleService;
import xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy.ScheduleService_Service;
import xxss.oracle.localizations.services.fusion.userdetailsservice.proxy.UserDetailsService;
import xxss.oracle.localizations.services.fusion.userdetailsservice.proxy.UserDetailsService_Service;

public class BIScheduleWSUtil {
    public BIScheduleWSUtil() {
        super();
    }

    public static ScheduleService createScheduleService(String jwt, String instanceName, String dataCenter) throws MalformedURLException {
        ScheduleService_Service service_Service;
        ScheduleService service = null;

        //String jwt = JSFUtils.resolveExpressionAsString("#{pageFlowScope.jwt}");

        if (jwt == null || "".equals(jwt)) {
            return service;
        }

        // JWT requires no client policy (with this unpatched version of JDev)
        // TODO:  research patch that adds JWT client policy to OWSM on client side

        SecurityPolicyFeature[] secFeatures =
            new SecurityPolicyFeature[] { new SecurityPolicyFeature("") };
        //{ new SecurityPolicyFeature("oracle/wss_jwt_token_over_ssl_client_policy") };

        service_Service = new ScheduleService_Service(new URL("https://" + instanceName + ".fa." + dataCenter + ".oraclecloud.com/xmlpserver/services/v2/ScheduleService"), 
                                                      new QName("http://xmlns.oracle.com/oxp/service/v2", "ScheduleService"));
        service =
                service_Service.getScheduleService(secFeatures);
        //service_Service.getPartnerTaxProfileServiceSoapHttpPort();
        if (service == null) {
            System.out.println("ScheduleService is null");
        } else {
            System.out.println("ScheduleService created.");
        }

        System.out.println("XxssPeLocalizaciones: jwt=" + jwt);

        // add JWT auth map to HTTP header
        BindingProvider bp = (BindingProvider)service;
        Map<String, List<String>> authMap =
            new HashMap<String, List<String>>();
        List<String> authZlist = new ArrayList<String>();
        authZlist.add(new StringBuilder().append("Bearer ").append(jwt).toString());
        authMap.put("Authorization", authZlist);

        bp.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS,
                                   authMap);

        return service;
    }
    
    public static ScheduleService createScheduleService2(String instanceName,
                                                                                           String dataCenter, String userName,
                                                                                           String userPass) throws MalformedURLException {
                ScheduleService_Service service_Service;
                ScheduleService service = null;
                
                service_Service =
                        new ScheduleService_Service();

                if (!(userName != null && !"".equals(userName) && userPass != null && !"".equals(userPass))) {
                    return service;
                }

                SecurityPolicyFeature[] secFeatures =
                { new SecurityPolicyFeature("oracle/wss_username_token_over_ssl_client_policy") };

                
                service = service_Service.getScheduleService(secFeatures);

                if (service == null) {
                    System.out.println("ScheduleService is null");
                } else {
                    System.out.println("ScheduleService created.");
                }

                // add JWT auth map to HTTP header
                WSBindingProvider wsbp = (WSBindingProvider)service;
                wsbp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, userName);
                wsbp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, userPass);
                wsbp.getRequestContext().put(WSBindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://"+instanceName+".fa."+dataCenter+".oraclecloud.com/xmlpserver/services/v2/ScheduleService");
                wsbp.getRequestContext().put(BindingProviderProperties.REQUEST_TIMEOUT, 1800000);

                return service;
            }
}
