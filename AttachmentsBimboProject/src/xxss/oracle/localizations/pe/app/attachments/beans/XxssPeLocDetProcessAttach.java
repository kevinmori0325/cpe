package xxss.oracle.localizations.pe.app.attachments.beans;

import com.sun.tools.xjc.reader.dtd.bindinfo.BIUserConversion;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.InputStreamReader;

import java.io.OutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jbo.domain.BlobDomain;
import oracle.jbo.domain.Number;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import oracle.jdbc.OracleConnection;

import oracle.jdbc.OracleResultSet;

import org.apache.commons.io.IOUtils;

import org.apache.log4j.Logger;

import xxss.oracle.localizations.pe.app.attachments.interfaces.IProcessAttachment;
import xxss.oracle.localizations.pe.app.attachments.services.BIScheduleWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ErpObjAttachWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil2;
import xxss.oracle.localizations.pe.app.utils.JSFUtils;
import xxss.oracle.localizations.services.fusion.bischeduleservice.proxy.ScheduleService;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ArrayOfEMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.DeliveryChannels;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.EMailDeliveryOption;
import xxss.oracle.localizations.services.fusion.bischeduleservice.types.ScheduleRequest;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.proxy.ErpObjectAttachmentService;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.proxy.ServiceException;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.types.AttachmentDetails;
import xxss.oracle.localizations.services.fusion.erpobjattachmentservice2.types.ObjectFactory;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue;
import xxss.oracle.localizations.services.fusion.externalreportservice.types.ParamNameValues;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest;
import xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportResponse;
import xxss.oracle.localizations.services.util.ErpObjAttachWSUtil2;

public class XxssPeLocDetProcessAttach implements IProcessAttachment {
    private static Logger log = Logger.getLogger(XxssPeLocDetProcessAttach.class);
    private String status;
    private String message;

    public XxssPeLocDetProcessAttach() {
        super();
    }
    
    public void pr_clear_log(OracleConnection trans) throws SQLException {
       
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_clear_log(?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setString(1, "");

            acs.executeUpdate();
    }

 

    public void readReportOutput(InputStream reportIs, OracleConnection trans,
                                 Number idAttachment) {
        try {
            String st =
                "begin XXSS_PE_LOC_DETRACC_PKG.pr_read_report_output(pt_report_blob => ?, pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setBlob(1, reportIs);
            acs.setNUMBER(2, idAttachment);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
            acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            String procStatus = acs.getString(3);
            String procMessage = acs.getString(4);

            if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }
            acs.close();
        } catch (SQLException exsql) {
            status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();
            log.error("Error SQL en readReportOutput(): " + exsql.getMessage(), exsql);
            System.out.println("Error SQL en readReportOutput() - " + exsql.getMessage());
            exsql.printStackTrace();
        } catch (Exception e) {
            status = ERROR;
            message =
                    "Error en XxssPeLocNoHallProcessAttach: " + e.getMessage();
            log.error("Error en readReportOutput(): " + e.getMessage(), e);
            System.out.println("Error en readReportOutput() - " + e.getMessage());
            e.printStackTrace();
        }
        ;
    }

    public void callProcessAttachDB(String pAttachId, OracleConnection trans) {
        try {
            String st =
                "begin XXSS_PE_LOC_DETRACC_PKG.pr_read_file_det(pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, new Number(pAttachId));
            acs.registerOutParameter(2, Types.VARCHAR, 0, 1);
            acs.registerOutParameter(3, Types.VARCHAR, 0, 4000);

            acs.executeUpdate();

            String procStatus = acs.getString(2);
            String procMessage = acs.getString(3);

            if (procStatus.equals(ERROR)) {
                this.status = procStatus;
                this.message = procMessage;
            }
            
            acs.close();
        } catch (SQLException exsql) {
            status = ERROR;
            message =
                    "Error al ejecutar procedimiento pl/sql: " + exsql.getMessage();
            log.error("Error SQL en callProcessAttachDB(): " + exsql.getMessage(), exsql);
            System.out.println("Error SQL en callProcessAttachDB() - " + exsql.getMessage());
            exsql.printStackTrace();
        } catch (Exception e) {
            status = ERROR;
            message ="Error en XxssPeLocDetProcessAttach: " + e.getMessage();
            log.error("Error en callProcessAttachDB(): " + e.getMessage(), e);
            System.out.println("Error en callProcessAttachDB() - " + e.getMessage());
            e.printStackTrace();
        }
        ;
    }
    
    public void updateConsStatus(int pIdConstancia, OracleConnection trans) throws SQLException {
       
            String st =
                "begin XXSS_PE_LOC_DETRACC_PKG.pr_update_cons_status(?,?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setInt(1, pIdConstancia);
            acs.setString(2, "PROCESSED");

            acs.executeUpdate();
        acs.close();
    }
    
    public void updateStatusAttach(String attachId, String status, String message,
                                        OracleConnection trans) {
        try {
            Number nAttachId = new Number(attachId);
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_update_attach_status(?, ?, ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, nAttachId);
            acs.setString(2, status);
            acs.setString(3, message);        

            acs.executeUpdate();
            acs.close();
        } catch (SQLException sqlex) {
            log.error("Error en updateStatusAttach(): " + sqlex.getMessage(), sqlex);
            log.error(Arrays.toString(sqlex.getStackTrace()));
        }
    }
    
    public void sendToAttach(OracleConnection trans, ErpObjectAttachmentService svc) throws SQLException,
                                                                    ServiceException {
        String lquery = "select ID_CONSTANCIA,\n" + 
        "       INVOICE_ID,\n" + 
        "       NRO_CONSTANCIA,\n" + 
        "       PERIODO_TRIB,\n" + 
        "       to_char(FECHA_PAGO, 'dd/mm/yyyy') FECHA_PAGO,\n" + 
        "       BUSINESS_UNIT_NAME,\n" + 
        "       VENDOR_NUM,\n" + 
        "       INVOICE_NUM\n" + 
        "  from xxss_pe_loc_detracc_const\n" + 
        " where status = 'IDENTIFIED'\n" + 
        " order by BUSINESS_UNIT_NAME, VENDOR_NUM, INVOICE_NUM";
        
        ResultSet resultado;
        
        List<AttachmentDetails> attachments = new ArrayList<AttachmentDetails>();
        AttachmentDetails attach;
        
        PreparedStatement lStmt = trans.prepareStatement(lquery, OracleResultSet.TYPE_FORWARD_ONLY, 
                                                         OracleResultSet.CONCUR_READ_ONLY);
        resultado = lStmt.executeQuery();
        ObjectFactory objfactory = new ObjectFactory();
        String result;
        
        while (resultado.next()) {
            attach = new AttachmentDetails();
            
            String bu = resultado.getString("BUSINESS_UNIT_NAME");
            String invoice = resultado.getString("INVOICE_NUM");
            String vendornum = resultado.getString("VENDOR_NUM");
            /*System.out.println("bu: " + bu);
            System.out.println("invoice: " + invoice);
            System.out.println("vendornum: " + vendornum);*/
            
            attach.setTitle(objfactory.createAttachmentDetailsTitle("Constancia de Detraccion"));
            attach.setAttachmentType(objfactory.createAttachmentDetailsAttachmentType("TEXT"));
            attach.setUserKeyA(objfactory.createAttachmentDetailsUserKeyA(bu));
            attach.setUserKeyB(objfactory.createAttachmentDetailsUserKeyB(invoice));
            attach.setUserKeyC(objfactory.createAttachmentDetailsUserKeyC(vendornum));
            attach.setUserKeyD(objfactory.createAttachmentDetailsUserKeyD("#NULL"));
            attach.setUserKeyE(objfactory.createAttachmentDetailsUserKeyE("#NULL"));
            attach.setContent(objfactory.createAttachmentDetailsContent(resultado.getString("NRO_CONSTANCIA") + "|" + resultado.getString("FECHA_PAGO")));
            
            attachments.clear();
            attachments.add(attach);
            
            result = svc.uploadAttachment("Payables Invoice", "AP_SUPPORTING_DOC", "yes", attachments);
            
            System.out.println("Attachment Result: " + result);
            
            if (result.indexOf("SUCCEEDED") > -1) {
                this.updateConsStatus(resultado.getInt("ID_CONSTANCIA") ,trans);
            } 
            
        }
    }

    public void processFile(String idAttachment, String jwt, String biUserName, String biUserPass, String userEmail, 
                            String instanceName, String dataCenter, String report, String report2,String classCode) {        
        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;
        
        // Llamada a WS para ejecutar reporte
        ExternalReportWSSService svc = null;
        ErpObjectAttachmentService svcAttach = null;
        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();
            
            conn.setAutoCommit(false);
            
                       
            svc = ExternalReportWSUtil2.createExternalReportWSSServiceUserToken2(instanceName,
                                                                                        dataCenter,
                                                                                        biUserName, 
                                                                                        biUserPass);
            if (svc != null) {
                System.out.println("ExternalReportWS created");
            } else {
                this.status = ERROR;
                this.message = "ExternalReportWS is not created";
                log.error(message);
                System.out.println(message);
                return;
            }
            
            svcAttach = ErpObjAttachWSUtil2.createErpObjectAttachmentService2(instanceName,
                                                                                        dataCenter,
                                                                                        biUserName, 
                                                                                        biUserPass);
            if (svcAttach != null) {
                System.out.println("ErpObjectAttachmentService created");
            } else {
                this.status = ERROR;
                this.message = "ErpObjectAttachmentService is not created";
                log.error(message);
                System.out.println(message);
                return;
            }

            ReportRequest req = new ReportRequest();
            ArrayOfParamNameValue arrayParam = new ArrayOfParamNameValue();

            ParamNameValue param = null;
            ArrayOfString arrayValues = null;

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);

            param = new ParamNameValue();
            arrayValues = new ArrayOfString();
            param.setName("XDO_DM_DEBUG_FLAG");
            arrayValues.getItem().add("off");
            param.setValues(arrayValues);
            arrayParam.getItem().add(param);


            req.setParameterNameValues(arrayParam);
        //  req.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Invoice without Det.xdo");
            req.setReportAbsolutePath(report2);//KMORI
            req.setSizeOfDataChunkDownload(-1);

            ReportResponse resp = svc.runReport(req, "");

            byte[] reportOutputBytes = resp.getReportBytes();

            if (reportOutputBytes == null || reportOutputBytes.length == 0) {
                //this.status = ERROR;
                //this.message = "Invoice without Det report cannot be executed.";
                //log.error(message);
                //System.out.println(message);
                this.status = SUCCESS;
                this.message = "Invoice without Det report: No rows to be processed.";
                log.info(message);
                this.updateStatusAttach(idAttachment, "PROCESSED", this.message, conn);
                return;
            }

            InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);
            this.readReportOutput(reportIs, conn, new Number(idAttachment));

            if (this.status != null && this.status.equals(ERROR)) {
                return;
            }

            this.callProcessAttachDB(idAttachment, conn);
            if (this.status != null && this.status.equals(ERROR)) {
                return;
            }

            // llamando a ws para ejecucion y envio de reporte
            this.sendToAttach(conn, svcAttach);           

            this.status = SUCCESS;
            
            conn.commit();

        } catch (Exception ex) {
            this.status = ERROR;
            this.message = ex.getMessage();
            log.error("Error en processFile(): " + ex.getMessage(), ex);
            System.out.println("Error en processFile() - " + ex.getMessage());
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("SQLException on processFile(): " + sqlex.getMessage(), sqlex);
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }
                
            }
        }
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
