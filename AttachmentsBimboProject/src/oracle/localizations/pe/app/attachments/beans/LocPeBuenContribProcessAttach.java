package oracle.localizations.pe.app.attachments.beans;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import java.io.StringReader;

import java.net.MalformedURLException;

import java.sql.SQLException;
import java.sql.Types;

import java.util.Arrays;

import java.util.Date;

import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.jbo.domain.Number;
import oracle.jbo.server.DBTransaction;

import oracle.jdbc.OracleCallableStatement;

import oracle.jdbc.OracleConnection;

import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;


import xxss.oracle.localizations.pe.app.attachments.interfaces.IProcessAttachment;
import xxss.oracle.localizations.pe.app.attachments.services.BIScheduleWSUtil;
import xxss.oracle.localizations.pe.app.attachments.services.ExternalReportWSUtil2;
import xxss.oracle.localizations.services.fusion.externalreportservice2.proxy.ExternalReportWSSService;
import xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy.ScheduleService;

public class LocPeBuenContribProcessAttach implements Job {
    private static Logger log = Logger.getLogger(LocPeBuenContribProcessAttach.class);
    public LocPeBuenContribProcessAttach() {
        super();
    }

    public void readReportOutput(InputStream reportIs, OracleConnection trans,
                                 Number idAttachment) throws SQLException,
                                                             Exception {
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_read_report_output(pt_report_blob => ?, pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setBlob(1, reportIs);
        acs.setNUMBER(2, idAttachment);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 1);
        acs.registerOutParameter(4, Types.VARCHAR, 0, 4000);

        acs.executeUpdate();

        String procStatus = acs.getString(3);
        String procMessage = acs.getString(4);

        if (procStatus.equals(IProcessAttachment.ERROR)) {
            throw new Exception(procMessage);
        }

    }
    
    public void pr_log(String pNombre,String pTipo, OracleConnection trans) throws SQLException {
       
            String st =
                "begin XXSS_PE_LOC_DETRACC_PKG.pr_log(?,?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setString(1, pNombre);
            acs.setString(2, pTipo);

            acs.executeUpdate();
    }
    
    public void callProcessAttachDB(String pAttachId,
                                    OracleConnection trans) throws SQLException,
                                                                Exception {

        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_process_file(pn_attachment_id => ?, xv_status => ?, xv_message => ?); end;";
        OracleCallableStatement acs =
            (OracleCallableStatement)trans.prepareCall(st);

        acs.setNUMBER(1, new Number(pAttachId));
        acs.registerOutParameter(2, Types.VARCHAR, 0, 1);
        acs.registerOutParameter(3, Types.VARCHAR, 0, 4000);

        acs.executeUpdate();

        String procStatus = acs.getString(2);
        String procMessage = acs.getString(3);

        if (procStatus.equals(IProcessAttachment.ERROR)) {
            throw new Exception(procMessage);
        }

        ;
    }
    
    private String executeFiscalClassRep(String concatClassCode, String jwtFusion, String instanceName, String dataCenter, String userEmail, String biUserName, String biUserPass,String report) throws MalformedURLException,
                                                xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy.InvalidParametersException,
                                                xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy.AccessDeniedException,
                                                xxss.oracle.localizations.services.fusion.bischeduleservice2.proxy.OperationFailedException, Exception {
        ScheduleService svcBI = null;
        svcBI = BIScheduleWSUtil.createScheduleService2(instanceName, dataCenter,biUserName,biUserPass);

        if (svcBI != null) {
            System.out.println("ScheduleService created");
        } else {
            throw new Exception("ScheduleService not created");
        }


        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ScheduleRequest scheduleRep = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ScheduleRequest();

        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.EMailDeliveryOption email = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.EMailDeliveryOption();
        email.setEmailAttachmentName("PadronesSunat");
        email.setEmailFrom("no.reply@oraclecloud.com");
        email.setEmailServerName("smtp.oraclecloud.com");
        email.setEmailSubject("Reporte para carga de padrones SUNAT");
        email.setEmailTo(userEmail);

        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.DeliveryChannels delivChann = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.DeliveryChannels();
        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ArrayOfEMailDeliveryOption delivery = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ArrayOfEMailDeliveryOption();
        delivery.getItem().add(email);
        delivChann.setEmailOptions(delivery);
        
        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ArrayOfParamNameValue arrayParam = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ArrayOfString();
        param.setName("P_CONCAT_CLASS_CODE");
        arrayValues.getItem().add(concatClassCode);
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);
        
        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ParamNameValues params = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ParamNameValues();
        params.setListOfParamNameValues(arrayParam);

        xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ReportRequest rep = new xxss.oracle.localizations.services.fusion.bischeduleservice2.types.ReportRequest();
        rep.setParameterNameValues(params);
        rep.setAttributeCalendar("Gregorian");
        rep.setAttributeFormat("XLSX");
        rep.setAttributeLocale("English (United States)");
        rep.setAttributeTemplate("Reporte para carga de padrones SUNAT");
        rep.setAttributeTimezone("(UTC-05:00) Lima - Peru Time (PET)");
  //    rep.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Reporte para carga de padrones SUNAT.xdo");
      rep.setReportAbsolutePath(report);//KMORI
        rep.setSizeOfDataChunkDownload(-1);

        scheduleRep.setDeliveryChannels(delivChann);
        scheduleRep.setReportRequest(rep);

        String user = biUserName;
        String pass = biUserPass;
        String returnCode = svcBI.scheduleReport(scheduleRep, user, pass);
        
        return(returnCode);
    }
    
    public void updateStatusAttach(String attachId, String status, String message,
                                        OracleConnection trans) {
        try {
            Number nAttachId = new Number(attachId);
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_update_attach_status(?, ?, ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, nAttachId);
            acs.setString(2, status);
            acs.setString(3, message);        

            acs.executeUpdate();
        } catch (SQLException sqlex) {
            log.error("Error en updateStatusAttach(): " + sqlex.getMessage(), sqlex);
            log.error(Arrays.toString(sqlex.getStackTrace()));
        }
    }
    
    private byte[] runTaxReport(String jwt,String biUserName,String biUserPass, String instanceName, String dataCenter,String report2) throws MalformedURLException,
                                                xxss.oracle.localizations.services.fusion.externalreportservice.proxy.InvalidParametersException,
                                                xxss.oracle.localizations.services.fusion.externalreportservice.proxy.AccessDeniedException,
                                                xxss.oracle.localizations.services.fusion.externalreportservice.proxy.OperationFailedException, Exception {
        ExternalReportWSSService svc = null;        
        //svc = ExternalReportWSUtil.createExternalReportWSSService(jwt, instanceName, dataCenter);
        svc = ExternalReportWSUtil2.createExternalReportWSSServiceUserToken2(instanceName,
                                                                                   dataCenter,
                                                                                   biUserName, 
                                                                                   biUserPass);
        
        if (svc != null) {
            System.out.println("ExternalReportWS created");
        } else {
            throw new Exception("ExternalReportWS is not created.");
        }
        
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest req = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportRequest();
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue arrayParam = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfParamNameValue();

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue param = null;
        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString arrayValues = null;

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_ESTIMATE_XML_DATA_SIZE");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_GEN_SQL_EXPLAIN_PLAN");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);

        param = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ParamNameValue();
        arrayValues = new xxss.oracle.localizations.services.fusion.externalreportservice2.types.ArrayOfString();
        param.setName("XDO_DM_DEBUG_FLAG");
        arrayValues.getItem().add("off");
        param.setValues(arrayValues);
        arrayParam.getItem().add(param);


        req.setParameterNameValues(arrayParam);
 //     req.setReportAbsolutePath("/Custom/Localizaciones Peruanas Corp/Reportes/PAAS/Party Tax Classifications.xdo");
        req.setReportAbsolutePath(report2);//KMORI
        req.setSizeOfDataChunkDownload(-1);

        xxss.oracle.localizations.services.fusion.externalreportservice2.types.ReportResponse resp = svc.runReport(req, "");
        
        return(resp.getReportBytes());
        
    }
    
    private void fillPartyClassif(String concatClassCode,
                                  OracleConnection conn) throws SQLException {
        OracleCallableStatement cs;
        String st =
            "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_ws_fill_party_classif_gt(?); end;";

        cs = (OracleCallableStatement)conn.prepareCall(st);
        cs.setString(1, concatClassCode);

        cs.executeUpdate();
        cs.close();
    }

    public void pr_reporte(String idAttach, String concatClassCode,
                           OracleConnection conn) throws SQLException {


        OraclePreparedStatement ps;
        OracleResultSet rs;
        StringBuffer strBuffer = new StringBuffer();
        String ln = System.getProperty("line.separator");


        //this.pr_log("1", "", conn);
        this.fillPartyClassif(concatClassCode, conn);
        //this.pr_log("2", "", conn);
        String sql = "select PARTY_NUMBER,\n" +
            "       PARTY_NAME,\n" +
            "       PARTY_CLASSIF_CODE,\n" +
            "       PARTY_CLASSIF_TYPE_CODE,\n" +
            "       EFFECTIVE_FROM,\n" +
            "       EFFECTIVE_TO,\n" +
            "       CUSTOMER_FLAG,\n" +
            "       SUPPLIER_FLAG,\n" +
            "       TEMPLATE_ID,\n" +
            "       PARTY_TYPE,\n" +
            "       TAX_REGISTRATION_NUM,\n" +
            "       ACTION\n" +
            "  from xxss_pe_loc_ws_party_CLASS_gt\n" +
            "  order by ACTION, TAX_REGISTRATION_NUM";

        ps =
    (OraclePreparedStatement)conn.prepareStatement(sql, OracleResultSet.TYPE_FORWARD_ONLY,
                                                OracleResultSet.CONCUR_READ_ONLY);

        rs = (OracleResultSet)ps.executeQuery();

        strBuffer.append("ACCION A REALIZAR" + "," + "NRO. DE TERCERO" + "," +
                         "RUC" + "," + "NOMBRE DE TERCERO" + "," +
                         "ES PROVEEDOR" + "," + "ES CLIENTE" + "," +
                         "TIPO DE CLASIFICACION" + "," + "CLASIFICACION" +
                         "," + "FECHA EFECTIVA DESDE" + "," +
                         "FECHA EFECTIVA HASTA" + ln);

        while (rs.next()) {
            //this.pr_log("3", rs.getString("PARTY_NAME"), conn);

            strBuffer.append(rs.getString("ACTIOn") + "," +
                             rs.getString("PARTY_NUMBER") + "," +
                             rs.getString("TAX_REGISTRATION_NUM") + "," +
                             rs.getString("PARTY_NAME") + "," +
                             rs.getString("SUPPLIER_FLAG") + "," +
                             rs.getString("CUSTOMER_FLAG") + "," +
                             rs.getString("PARTY_CLASSIF_TYPE_CODE") + "," +
                             rs.getString("PARTY_CLASSIF_CODE") + "," +
                             rs.getString("EFFECTIVE_FROM") + "," +
                             rs.getString("EFFECTIVE_TO") + ln);
        }


        //PROCESO PARA ACTUALIZAR CAMPO OUTFILE_REPORT
        this.updateByteReport(idAttach, strBuffer.toString(), conn);
        //this.pr_log("4", "updateByteReport", conn);
        ps.close();

    }

    public void updateByteReport(String pIdFactCert, String pTrama,
                                 OracleConnection trans) {
        try {
            Number nAttachId = new Number(pIdFactCert);
            StringReader requestSr = new StringReader(pTrama);
            String st =
                "begin XXSS_PE_LOCSUNATPARTYCLASS_PKG.pr_update_bytes_report(?, ?); end;";
            OracleCallableStatement acs =
                (OracleCallableStatement)trans.prepareCall(st);

            acs.setNUMBER(1, nAttachId);
            acs.setClob(2, requestSr);

            acs.executeUpdate();
        } catch (SQLException sqlex) {
            //   log.error("Error en updateStatusFactCert(): " + sqlex.getMessage(), sqlex);
            //   log.error(Arrays.toString(sqlex.getStackTrace()));
            System.out.println("Error en updateByteReport(): " +
                               sqlex.getMessage());
        }
    }

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("entro job");

        InitialContext ctx = null;
        OracleConnection conn = null;
        DataSource ds = null;

        JobKey key = jobExecutionContext.getJobDetail().getKey();

        JobDataMap dataMap =
            jobExecutionContext.getJobDetail().getJobDataMap();

        String idAttachment = dataMap.getString("idAttachment");
        /*DBTransaction trans =
            (DBTransaction)dataMap.getWrappedMap().get("trans");*/
        String jwt = dataMap.getString("jwt");
        String biUserName = dataMap.getString("biUserName");
        String biUserPass = dataMap.getString("biUserPass");
        String userEmail = dataMap.getString("userEmail");
        String instanceName = dataMap.getString("instanceName");
        String dataCenter = dataMap.getString("dataCenter");
        String classCode = dataMap.getString("classCode");
        String report = dataMap.getString("report");
        String report2 = dataMap.getString("report2");

        // Llamada a WS para ejecutar reporte
        try {
            ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/XXSSPELOCDBCSDS");
            conn = (OracleConnection)ds.getConnection();

            conn.setAutoCommit(false);

            byte[] reportOutputBytes =
                this.runTaxReport(jwt, biUserName, biUserPass, instanceName,
                                  dataCenter, report2);

            if (reportOutputBytes == null || reportOutputBytes.length == 0) {
                throw new Exception("Party Tax Classification report cannot be executed.");
            }

            log.info("Party Tax Classification report was executed. " +
                     reportOutputBytes.length);

            InputStream reportIs = new ByteArrayInputStream(reportOutputBytes);

            this.readReportOutput(reportIs, conn, new Number(idAttachment));
            this.callProcessAttachDB(idAttachment, conn);

            //this.pr_log("UserMail", userEmail, conn);

            this.pr_reporte(idAttachment, classCode, conn);
            //this.pr_log("pr_reporte", classCode, conn);
            conn.commit();

            // llamando a ws para ejecucion y envio de reporte
            /* String returnCode = this.executeFiscalClassRep(classCode, jwt, instanceName, dataCenter, userEmail, biUserName, biUserPass, report);

            if(returnCode == null || "".equals(returnCode)) {
                throw new Exception("Error al ejecutar reporte BI.");
            }
            */

        } catch (Exception ex) {
            log.error("Error en execute(): " + ex.getMessage(), ex);
            log.error(Arrays.toString(ex.getStackTrace()));
            System.out.println("Error en execute() - " + ex.getMessage());
            this.updateStatusAttach(idAttachment, "ERROR", ex.getMessage(),
                                    conn);
            JobExecutionException e2 = new JobExecutionException(ex);
            // Quartz will automatically unschedule
            // all triggers associated with this job
            // so that it does not run again
            e2.setUnscheduleAllTriggers(true);
            throw e2;
            //ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException sqlex) {
                    log.error("Error on run(): " + sqlex.getMessage(), sqlex);
                    log.error(Arrays.toString(sqlex.getStackTrace()));
                }

            }
        }
    }
}
